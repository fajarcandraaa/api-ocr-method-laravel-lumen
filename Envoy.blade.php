@servers(['web' => 'malang@149.129.239.15'])

@story('deploy')
    git
    composer
@endstory

@task('git')
    cd insurance-backend
    git pull
@endtask

@task('composer')
    cd insurance-backend
    /usr/bin/php7.3 /usr/local/bin/composer install
    /usr/bin/php7.3 /usr/local/bin/composer dump-autoload
    php7.3 artisan migrate
    pm2 restart ins-be-worker.yml
@endtask
