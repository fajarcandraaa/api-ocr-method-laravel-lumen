<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// use Carbon\Carbon;
// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Log;
// use Illuminate\Support\Facades\Storage;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/test-get', 'HistoryController@testGetBalance');
$router->post('/test-insert', 'HistoryController@testInsert');
$router->post('/test-update', 'HistoryController@testUpdateStatus');




$router->get('/key', function () {
    return str_random(32);
});

