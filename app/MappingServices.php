<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class MappingServices extends Model
{

    public function id_prov_service()
    {
        return $this->belongsTo('App\ProviderServices', 'id_prov_service')->with(['provider_type','mid','service_child','parent_service_id']);
        // return $this->belongsTo('App\ProviderServices', 'id_prov_service')->with('parent_service_id');
    }

    public function id_iziservice_code()
    {
        return $this->belongsTo('App\IziserviceCode','id_iziservice_code');
        // ->select('id','iziservice_name','iziservice_code');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances', 'insurance_id');
    }
    
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function last_modified_by()
    {
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    use SoftDeletes;
    protected $fillable = [
        'id_prov_service',
        'id_iziservice_code',
        'created_by',
        'last_modified_by',
        'insurance_id'
    ];

    protected $hidden = [];
}
