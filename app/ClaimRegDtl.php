<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class ClaimRegDtl extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, SoftDeletes;
    protected $table        = 'claimregdtl';

    public function claimregHdr()
    {
        return $this->belongsTo('App\ClaimRegHdr','claimregno','claimregno');
    }
    protected $fillable     = [
        'claimregno',
        'detail_parentno',
        'servicecode',
        'benefitcode',
        'length_ofstay',
        'qty',
        'old_qty',
        'benefitentitlement',
        'incuredexpense',
        'old_incuredexpense',
        'amountreimbursed',
        'excesscharge',
        'amountdeclained',
        'reasonCode',
        'insurance_id',
        'seq_number',
        'isExclude'
    ];
    protected $hidden       = [];
}
