<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class RequestTemplate extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, SoftDeletes;
    protected $table    = 'requesttemplate';

    public function plan()
    {
        return $this->belongsTo('App\CodeMasters', 'plan');
    }

    public function docType()
    {
        return $this->belongsTo('App\CodeMasters', 'documentType');
    }

    public function provider_mid()
    {
        return $this->belongsTo('App\Biz_providers', 'provider_mid','merchant_id');
    }
    

    protected $fillable =   [
        'id',
        'provider_mid',
        'branch',
        'plan',
        'documentType',
        'isPriority',
        'path',
        'replaced',
        'documentStatus',
        'status',
        'createBy',
        'lastUpdateBy',
        'ocrtotext',
        'documentName',
        'documentNumber',
        'status',
        'isReplace',
        'ReffNo',
        'insurance_id'
    ];

    protected $hidden   =   [];
}