<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reasons extends Model
{
    use SoftDeletes;

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function last_modified_by()
    {
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances','insurance_id');
    }

    public function eob_type(){
        return $this->belongsTo('App\CodeMasters','eob_type');
    }

    protected $fillable = [
        'reason_code',
        'reason_name',
        'eob_type',
        'description',
        'created_at',
        'updated_at',
        'created_by',
        'last_modified_by',
        'deleted_at',
        'insurance_id'
    ];

    protected $hidden = [];
}
