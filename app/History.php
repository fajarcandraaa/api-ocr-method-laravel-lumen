<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class History extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'ref_trans',
        'ref_id',
        'qty',
        'balance',
        'insurance_id',
        'status',
    ];

    public function refTransDesc(){
        return $this->belongsTo('App\CodeMasters','ref_trans', 'code')->where('codeType','TRN');
    }

    public function insurance(){
        return $this->belongsTo('App\Insurances','insurance_id', 'id');
    }

    protected $table = 'ocr_log_page_transactions';
    protected $hidden = [];
}
