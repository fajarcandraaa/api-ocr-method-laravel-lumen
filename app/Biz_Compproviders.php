<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Biz_Compproviders extends Model
{
    use SoftDeletes;

    protected $table = 'biz_compproviders';

    public function providers()
    {
        return $this->hasOne('App\Biz_providers','compprovid');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances','insurance_id');
    }
    public function bank_infos(){
        return $this->hasMany('App\BankInfos','ref_id')->with('bank_code');
    }

    public function contact_person(){
        return $this->hasMany('App\Finances','ref_id');
    }

    public function provider_type(){
        return $this->belongsTo('App\CodeMasters','provider_type');
    }

    public function country(){
        return $this->belongsTo('App\Countries','country','country_code');
    }

    public function prov(){
        return $this->belongsTo('App\Provinces','prov');
    }

    public function city(){
        return $this->belongsTo('App\Cities','city');
    }

    public function district(){
        return $this->belongsTo('App\Districts','district');
    }

    public function postal_code(){
        return $this->belongsTo('App\Subdistricts','postal_code');
    }

    public function status(){
        return $this->belongsTo('App\CodeMasters','status');
    }

    public function created_by(){
        return $this->belongsTo('App\User','created_by');
    }

    public function last_modified_by(){
        return $this->belongsTo('App\User','last_modified_by');
    }

    protected $fillable = [
        'provider_name',
        'provider_type',
        'provider_email',
        'IsPartner',
        'Branch',
        'Address1',
        'Address2',
        'Address3',
        'Address4',
        'country',
        'prov',
        'city',
        'district',
        'postal_code',
        'PhoneNo',
        'FaxNo',
        'Profile',
        'SIUPNo',
        'Domicile',
        'TaxNo',
        'NotarialDeed',
        'LicenseNo',
        'AggrEffectiveDate',
        'AggrExpiryDate',
        'RenewalDate',
        'DiscProvider',
        'Remark',
        'IsDifferentHospital',
        'Flag',
        'created_at',
        'updated_at',
        'created_by',
        'last_modified_by',
        'biz_prov_no',
        'status',
        'biz_prov_code',
        'insurance_id'
    ];

    protected $hidden = [];
}