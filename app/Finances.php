<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Finances extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'ref_type',
        'ref_id',
        'pic_name',
        'pic_phone',
        'pic_email',
        'pic_type'

    ];

    protected $table = 'contact_persons';

    protected $hidden = [
    ];
}
