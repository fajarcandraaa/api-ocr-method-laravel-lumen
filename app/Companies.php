<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Companies extends Model
{
    use SoftDeletes;

    public function company_type(){
        return $this->belongsTo('App\CodeMasters','company_type');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances','insurance_id');
    }

    public function parent_comp_id(){
        return $this->belongsTo('App\Companies','parent_comp_id');
    }

    public function bank_infos()
    {
        return $this->hasMany('App\BankInfos', 'ref_id')->with('bank_code');
    }

    public function contact_person()
    {
        return $this->hasMany('App\Finances', 'ref_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Countries', 'country', 'country_code');
    }

    public function prov()
    {
        return $this->belongsTo('App\Provinces', 'prov');
    }

    public function city()
    {
        return $this->belongsTo('App\Cities', 'city');
    }

    public function district()
    {
        return $this->belongsTo('App\Districts', 'district');
    }

    public function postal_code()
    {
        return $this->belongsTo('App\Subdistricts', 'postal_code');
    }

    public function status()
    {
        return $this->belongsTo('App\CodeMasters', 'status');
    }

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function last_modified_by()
    {
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    protected $fillable = [
        'company_name',
        'have_parent_comp',
        'company_type',
        'branch',
        'tax_no',
        'phoneno',
        'email',
        'faxno',
        'address1',
        'address2',
        'address3',
        'address4',
        'status',
        'country',
        'prov',
        'city',
        'district',
        'postal_code',
        'remark',
        'policy_number',
        'policy_holder',
        'payor',
        'effectivedate',
        'expirydate',
        'renewaldate',
        'flag',
        'created_at',
        'updated_at',
        'created_by',
        'last_modified_by',
        'deleted_at',
        'parent_comp_id',
        'company_code',
        'insurance_id'
    ];

    protected $hidden = [];
}
