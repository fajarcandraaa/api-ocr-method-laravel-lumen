<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class Diagnosis extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;
    protected $table = 'diagnosis';
    public function poly()
    {
        return $this->belongsTo('App\CodeMasters','poly', 'codeDesc');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances','insurance_id');
    }
    
    protected $fillable = [
        'diagnosisCode', 
        'diagnosisName', 
        'scientificDesc', 
        'description', 
        'standardFee', 
        'standardTreatment',
        'poly',
        'isExclusionPolicy',
        'isPreExisting',
        'flag',
        'createBy',
        'lastUpdateBy',
        'ICDType',
        'insurance_id'
    ];
    protected $hidden = [];
}