<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class MappingBenefits extends Model
{
    public function id_iziservice_code()
    {
        return $this->belongsTo('App\IziserviceCode', 'id_iziservice_code');
        // ->select('id','iziservice_name','iziservice_code');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances', 'insurance_id');
    }

    public function benefit_type()
    {
        return $this->belongsTo('App\CodeMasters', 'benefit_type');
    }

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function last_modified_by()
    {
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    use SoftDeletes;
    protected $fillable = [
        'insurance_id',
        'benefit_type',
        'benefit_code',
        'benefit_name',
        'seq_number',
        'id_iziservice_code',
        'created_by',
        'last_modified_by'
    ];

    protected $hidden = [];
}
