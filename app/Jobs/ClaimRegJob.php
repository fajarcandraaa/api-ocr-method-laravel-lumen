<?php

namespace App\Jobs;

use Illuminate\Http\File as File;
use Illuminate\Support\Facades\Storage as Storage;
use App\Http\Controllers\ClaimRegistrationController as ClaimRegistrationController;
use App\Http\Controllers\ClaimSubmissionController as ClaimSubmissionController;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;

use App\ClaimRegDocConverts as ClaimRegDocConverts;

class ClaimRegJob extends Job
{
    private $claimsubmissioncontroller;
    private $claimregcontroller;
    private $pathOriSupportDoc;
    private $category;
    private $claimregno;
    private $claimregdocconv;
    private $idclaimregdocsupport;
    private $type;
    private $destinationOrig;
    private $sourceOrig;
    private $destinationConv;
    private $sourceConv;
    private $baseName;
    // private $pdffilepath;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type = null, $supportDoctypes = null, $category = null, $claimregno = null, $idclaimregdocsupport = null, $destinationOrig = null, $sourceOrig = null, $destinationConv = null, $sourceConv = null, $baseName = null)
    {
        // $reqdata = new Request;
        $this->claimsubmissioncontroller = new ClaimSubmissionController;
        $this->claimregcontroller = new ClaimRegistrationController;
        $this->pathOriSupportDoc = $supportDoctypes;
        $this->category = $category;
        $this->claimregno = $claimregno;
        // $this->claimregdocconv = new ClaimRegDocConverts;
        $this->idclaimregdocsupport = $idclaimregdocsupport;
        $this->type = $type;
        $this->destinationOrig = $destinationOrig;
        $this->sourceOrig = $sourceOrig;
        $this->destinationConv = $destinationConv;
        $this->sourceConv = $sourceConv;
        $this->baseName = $baseName;
        // $this->pdffilepath = $pdffilepath;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        switch ($this->type) {
            case 'Convert':

                // $this->pathOriSupportDoc = str_replace(base_path('public/'), '', $this->pathOriSupportDoc);
                $convertImage   = $this->claimregcontroller->pdftoimageTemplates($this->pathOriSupportDoc);

                //loop create claimregdoc_converts
                foreach ($convertImage['image'] as $c) {
                    $y = 0;
                    foreach ($c as $cc) {
                        // move support doc convert & save ke claimregdoc convert
                        $folderConvert = 'assets/claim/' . $this->claimregno . "/converts/";
                        if (!is_dir($folderConvert)) {
                            mkdir($folderConvert, 0777, true);
                        }
                        $pathConSupportDoc[$y] = Helper::moveImg($folderConvert, $cc['convert_path'], $folderConvert . basename($cc['convert_path']));
                        $docConMain = ClaimRegDocConverts::create([
                            'id_claimregdoc' => $this->idclaimregdocsupport,
                            'path' => $pathConSupportDoc[$y]
                        ]);

                        //upload converts support docs
                        Storage::disk('minio')->putFileAs($this->destinationConv, new File($pathConSupportDoc[$y]), basename($pathConSupportDoc[$y]));
                        $y++;
                    }
                }

                //upload original support files
                Storage::disk('minio')->putFileAs($this->destinationOrig, new File($this->pathOriSupportDoc), basename($this->pathOriSupportDoc));
                $this->claimsubmissioncontroller->createOCRqueue($this->destinationOrig, $this->baseName);
                break;

            case 'Upload':
                switch ($this->category) {
                    case 'main':
                        Log::info("masuk queue main");

                        //upload main doc ori to claimregno/originals
                        Storage::disk('minio')->putFileAs($this->destinationOrig, new File($this->sourceOrig), basename($this->sourceOrig));
                        $this->claimsubmissioncontroller->createOCRqueue($this->destinationOrig, $this->baseName);
                        //upload main doc convert to claimregno/originals
                        Log::info("Upload File Main ke MinIO");
                        foreach ($this->sourceConv as $sc) {
                            Storage::disk('minio')->putFileAs($this->destinationConv, new File($sc), basename($sc));
                        }
                        Log::info("Upload Main FIle selesai");
                        break;
                    case 'support':
                        Log::info("masuk queue support");

                        Log::info("destorig ".$this->destinationOrig);

                        $claimregnoo = explode("/", $this->destinationOrig);
                        $claimregnooo = $claimregnoo[2];

                        $in = 0;
                        foreach ($this->sourceOrig as $sourceOrig) {
                            //upload support doc ori to claimregno/originals
                            Storage::disk('minio')->putFileAs($this->destinationOrig, new File($sourceOrig), basename($sourceOrig));

                            if ($this->pathOriSupportDoc[$in]) {
                                $cekocrread = $this->claimregcontroller->checkOcrRead($this->pathOriSupportDoc[$in]);
                                if ($cekocrread) {
                                    $this->claimsubmissioncontroller->createOCRqueue($this->destinationOrig, basename($sourceOrig));
                                }
                            }
                            $in++;
                        }

                        //upload main doc convert to claimregno/originals
                        foreach ($this->sourceConv as $sc) {
                            Storage::disk('minio')->putFileAs($this->destinationConv, new File($sc), basename($sc));
                        }
                        $assignClaim   = $this->claimregcontroller->assignClaim($claimregnooo, $this->baseName);
                        Log::info("masuk assignTo");


                        break;

                    default:
                        # code...
                        break;
                }

                break;
            default:
                # code...
                break;
        }
    }
}
