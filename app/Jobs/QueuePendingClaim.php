<?php

namespace App\Jobs;
use App\Http\Controllers\PendingClaimController;

class QueuePendingClaim extends Job
{
    private $insurance_id;
    private $pendingclaim;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($insurance_id)
    {
        $this->insurance_id = $insurance_id;
        $this->pendingclaim = new PendingClaimController;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return $this->pendingclaim->runReQueue($this->insurance_id);
    }
}
