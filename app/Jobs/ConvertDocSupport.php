<?php

namespace App\Jobs;
use App\Http\Controllers\ClaimRegistrationController as ClaimRegistrationController;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper;
use App\Jobs\ClaimRegJob;
use App\ClaimRegDoc;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ConvertDocSupport extends Job
{
    // use DispatchesJobs;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $claimregcontroller;
    private $folderOri;
    private $insurance_id;
    private $user_id;
    private $doc_support;
    // private $data;
    private $claimregno;
    private $pathOrig;
    private $pathConv;
    private $documentRoot;

    public function __construct($insurance_id, $user_id, $doc_support, $claimregno, $pathOrig, $pathConv, $folderOri, $documentRoot)
    {
        $this->insurance_id = $insurance_id;
        $this->user_id = $user_id;
        $this->doc_support = $doc_support;
        $this->documentRoot = $documentRoot;
        $this->claimregno = $claimregno;
        $this->pathOrig = $pathOrig;
        $this->pathConv = $pathConv;
        $this->folderOri = $folderOri;
        $this->claimregcontroller = new ClaimRegistrationController;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        $this->claimregcontroller->convertDocSupport($this->insurance_id, $this->user_id, $this->doc_support, $this->claimregno, $this->pathOrig, $this->pathConv, $this->folderOri, $this->documentRoot);
    }
}
