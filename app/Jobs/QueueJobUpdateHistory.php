<?php

namespace App\Jobs;
use App\Http\Controllers\HistoryController;

class QueueJobUpdateHistory extends Job
{
    private $data;
    private $history;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->data = $data;
        $this->history = new HistoryController;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return $this->history->updateHistory($this->data);
    }
}
