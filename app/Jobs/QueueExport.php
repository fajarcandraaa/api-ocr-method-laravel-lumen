<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Exports\ClaimDocsExport;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\NotificationController;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class QueueExport extends Job
{
    private $data;
    private $result;
    private $filename;
    private $typename;
    private $tokenuser;
    private $exportType;
    private $exportName;
    private $notificationcontroller;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($exportType ,$data, $exportName, $tokenuser)
    {
        $this->data       = $data;
        $this->tokenuser  = $tokenuser;
        $this->exportType = $exportType;
        $this->exportName = $exportName;
        $this->notificationcontroller = new NotificationController;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $date = Carbon::now()->setTimezone('Asia/Jakarta')->format("YmdHis");
        $dateDirPDF = Carbon::now()->setTimezone('Asia/Jakarta')->format("Y-m-d");
        $strRandom = Str::random(16);

        switch ($this->exportName) {
            case 'ClaimDocs': {
                $this->typename = 'Claim Documents';
                $filename = "ClaimDocuments_{$date}_{$strRandom}";
                if($this->exportType == 'Excel') {
                    $this->filename = "{$filename}.xlsx";
                    $this->result   = new ClaimDocsExport($this->data);
                } else {
                    $this->filename = "{$filename}.pdf";
                    $this->result = PDF::loadview('claimdocs', ['claimdocs' => $this->data]);
                }
                break;
            }
        }

        if($this->exportType == 'Excel') {
            $path = "/public/export/excel/";
            if (!is_dir(storage_path("app{$path}/{$dateDirPDF}"))) {
                mkdir(storage_path("app{$path}/{$dateDirPDF}"), 0777, true);
            }

            $filepath = $path."{$dateDirPDF}/".$this->filename;
            
            $store = Excel::store($this->result, $this->filename, "public_export_excel");

            if($store) {
                rename(storage_path("app{$path}").$this->filename, storage_path("app{$filepath}"));
                $this->tokenuser ?
                    $this->pushNotifSuccess('excel') :
                        $this->logChecker($filepath, 'tokenNotFound');
            } else {
                $this->logChecker($filepath);
                $this->tokenuser ?
                    $this->pushNotifError($filepath) :
                        $this->logChecker($filepath, 'tokenNotFound');
            }
        } else {
            $filepath = "public/export/pdf/{$dateDirPDF}/{$this->filename}";
            $store = Storage::put($filepath, $this->result->output());

            if($store) {
                $this->tokenuser ?
                    $this->pushNotifSuccess('pdf') :
                        $this->logChecker($filepath, 'tokenNotFound');
            } else {
                $this->logChecker($filepath);
                $this->tokenuser ?
                    $this->pushNotifError($filepath) :
                        $this->logChecker($filepath, 'tokenNotFound');
            }
        }
    }

    public function pushNotifSuccess($typefile)
    {
        $titleTypeFile = $typefile == 'pdf' ? 'Pdf' : 'Excel';
        $title = "Export {$this->typename} {$titleTypeFile}.";
        $message = (string) json_encode([
            "message" => "Successfully",
            "percentage" => "100%",
            "file" => [
                "type" => "{$typefile}",
                "download" => URL::to("/") . "/export-data/{$typefile}?filename={$this->filename}"
            ]
        ]);
        $this->notificationcontroller->PushNotif($title, $message, "", $this->tokenuser);
    }

    public function pushNotifError($filepath)
    {
        $this->notificationcontroller->PushNotif("Error Export {$this->filename}.", (string) json_encode([
            "message" => "Error Export {$this->filename}.",
            "error"   => [
                [
                    "message" => "Error Export {$this->filename}",
                    "reason" => "errorExportData",
                    "filepath" => "{$filepath}"
                ]
            ]
        ]), "", $this->tokenuser);
    }

    public function logChecker($filepath, $errorReason = "")
    {
        switch ($errorReason) {
            case 'tokenNotFound':
                Log::info("Token firebase not found, in filepath : {$filepath}");
                break;

            default:
                Log::error("Error Export : {$filepath}");
                break;
        }
    }
}
