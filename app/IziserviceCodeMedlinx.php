<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IziserviceCodeMedlinx extends Model
{
    protected $connection = 'mysql2';
    protected $table = null;

    function __construct() {
        $this->table = getenv('DB_DATABASE2') .'.iziservice_codes';
    }
    
    protected $fillable = [
        'iziservice_code', 'iziservice_name', 'description', 'created_by', 'last_modified_by'
    ];

    // public function name()
    // {
    //     return $this->belongsTo('App\ProvidersInsurance','merchant_id','merchant_id')->select('id','merchant_id','name');
    // }
}
