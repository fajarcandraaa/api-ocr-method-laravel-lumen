<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ProviderServices extends Model
{

    public function provider_type()
    {
        return $this->belongsTo('App\CodeMasters', 'provider_type');
    }

    public function mid()
    {
        return $this->belongsTo('App\Biz_providers', 'mid', 'merchant_id')->with('compprov');
    }

    public function compprov()
    {
        return $this->belongsTo('App\Biz_compproviders', 'mid', 'merchant_id');
    }

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function last_modified_by()
    {
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances','insurance_id');
    }

    public function service_child()
    {
        return $this->hasMany('App\ProviderServices','parent_service_id');
    }

    public function parent_service_id()
    {
        return $this->belongsTo('App\ProviderServices', 'id','parent_service_id');
    }

    use SoftDeletes;
    protected $fillable = [
        'service_code',
        'parent_service_id',
        'service_name',
        'amount',
        'description',
        'provider_type',
        'mid',
        'created_by',
        'last_modified_by',
        'insurance_id',
        'isSubservice',
        'position'
    ];

    protected $hidden = [];
}
