<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class RolePermissionMedlinx extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'role_permissions';
    protected $fillable = [
        'role_id','permission','created_by','last_modified_by'
    ];
}