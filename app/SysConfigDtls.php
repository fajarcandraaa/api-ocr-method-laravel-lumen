<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysConfigDtls extends Model
{
    use SoftDeletes;

    protected $table = 'sysconfigdtl';
    protected $fillable = [
        'confId',
        'dtlName',
        'seqNo',
        'dtlDesc',
        'created_at',
        'updated_at',
        'dtlJoin'
    ];

    protected $hidden = [];
}
