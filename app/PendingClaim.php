<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class PendingClaim extends Model
{
    use SoftDeletes;

    protected $table    = 'pendingclaims';
    protected $fillable = [
        'claimregno',
        'type',
        'category',
        'destinationOrig',
        'sourceOrig',
        'destinationConv',
        'supportDoctypes',
        'insurance_id',
        'basename',
        'totalpages'
    ];
    protected $hidden = [];
}
