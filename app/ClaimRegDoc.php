<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class ClaimRegDoc extends Model implements AuthenticatableContract, AuthorizableContract
{
    

    public function claimregdoc_converts()
    {
        return $this->hasMany('App\ClaimRegDocConverts', 'id_claimregdoc');
    }

    use Authenticatable, Authorizable, SoftDeletes;
    protected $table    = 'claimregdoc';
    protected $fillable = [
        'claimregno',
        'insurance_id',
        'path',
        'doc_name',
        'doc_type',
        'doc_category',
        'created_by',
        'last_update_by',
        'basename'
    ];
    protected $hidden = [];

    public static function imgValidationRule()
    {
        $rules = [
            'img' => 'file|mimes:pdf,jpeg,png,jpg|max:10240'
			// 'support' => 'file|image|mimes:jpeg,png,jpg,pdf|max:2048',
        ];
        return $rules;
    }
}