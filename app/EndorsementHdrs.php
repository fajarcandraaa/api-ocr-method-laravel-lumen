<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class EndorsementHdrs extends Model
{
    use SoftDeletes;
    protected $table        = 'endorsementhdr';

    public function memberid()
    {
        return $this->belongsTo('App\Members', 'memberid');
    }

    public function companyid()
    {
        return $this->belongsTo('App\Companies', 'companyid');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances', 'insurance_id');
    }

    public function policy_type()
    {
        return $this->belongsTo('App\CodeMasters', 'policy_type');
    }

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function last_modified_by()
    {
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    public function endorsement_dtl()
    {
        // return $this->hasMany('App\EndorsementDtls', 'endorsementno', 'endorsementno')->with('endorsement_benefit');
        return $this->hasMany('App\EndorsementDtls', 'endorsementno', 'endorsementno');
    }

    public function endorsement_benefit()
    {
        // return $this->hasMany('App\EndorsementDtls', 'endorsementno', 'endorsementno')->with('endorsement_benefit');
        return $this->hasMany('App\EndorsementBenefits', 'endorsementno', 'endorsementno');
    }

    protected $fillable     = [
        'endorsementno',
        'policy_type',
        'policyno',
        'memberid',
        'companyid',
        'path',
        'filename',
        'insurance_id',
        'remark',
        'created_by',
        'last_modified_by',
        'start_effective_date',
        'end_effective_date'
    ];
    protected $hidden       = [];
}
