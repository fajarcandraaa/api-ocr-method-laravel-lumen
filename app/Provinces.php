<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Provinces extends Model 
{

    public function country(){
        return $this->belongsTo('App\Countries','country_code','country_code');
    }

    use SoftDeletes;
    protected $fillable = [
        'country_code', 'prov_code','prov_desc', 'created_by', 'last_modified_by'
    ];

    protected $hidden = [
    ];

    
}
