<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notifications extends Model
{
    use SoftDeletes;

    public function requester()
    {
        return $this->belongsTo('App\User', 'requester');
    }

    protected $table = 'notifications';
    protected $fillable = [
        'userId',
        'title',
        'message',
        'type',
        'isRead',
        'requester',
        'payload'
    ];
    protected $hidden = [];
}