<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class ClaimRegHdr extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, SoftDeletes;
    protected $table        = 'claimreghdr';

    public function memberid()
    {
        return $this->belongsTo('App\Members', 'memberid');
    }
    public function companyid()
    {
        return $this->belongsTo('App\Companies', 'companyid');
    }
    
    public function reasonid()
    {
        return $this->belongsTo('App\Reasons', 'reasonid')->with('eob_type');
    }

    public function diagnosis()
    {
        return $this->belongsTo('App\Diagnosis', 'diagnosis');
    }
    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances', 'insurance_id');
    }
    public function plan()
    {
        return $this->belongsTo('App\CodeMasters', 'plan', 'code');
    }
    
    public function benefit_plan()
    {
        return $this->belongsTo('App\CodeMasters', 'benefit_plan', 'code');
    }
    
    public function eob_status() {
        return $this->belongsTo('App\CodeMasters', 'eob_status');
    }

    public function status()
    {
        return $this->belongsTo('App\CodeMasters', 'status', 'code')->where('codetype','CLS');
    }
    public function bizprovid()
    {
        return $this->belongsTo('App\Biz_providers', 'bizprovid')->with('compprov');
    }
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function assignTo()
    {
        return $this->belongsTo('App\User', 'assignTo');
    }

    public function last_update_by()
    {
        return $this->belongsTo('App\User', 'last_update_by');
    }

    public function claimregdoc()
    {
        return $this->hasMany('App\ClaimRegDoc', 'claimregno', 'claimregno')->with('claimregdoc_converts');
    }

    public function claimactivity()
    {
        return $this->hasMany('App\ClaimActivity', 'claimregno', 'claimregno')->with(['created_by','acttype' => function ($q) {
            $q->where('codeType', "CLS");
        }]);
    }

    public function claimregdtl()
    {
        return $this->hasMany('App\ClaimRegDtl', 'claimregno', 'claimregno');
    }

    public function claimdoc()
    {
        return $this->hasMany('App\ClaimDocs', 'plan', 'plan');
    }

    protected $fillable     = [
        'claimregno',
        'memberid',
        'reasonid',
        'plan',
        'bizprovid',
        'companyid',
        'ref_no',
        'remark',
        'eob_status',
        'payment_status',
        'subtotal',
        'old_subtotal',
        'status',
        'created_by',
        'last_update_by',
        'insurance_id',
        'is_sent_mail',
        'indexno',
        'cardno',
        'policyno',
        'assignTo',
        'vip',
        'benefit_plan',
        'admissiondate',
        'isOcrRead',
        'dischargedate',
        'discount',
        'old_discount',
        'rounding',
        'old_rounding',
        'adminfee',
        'old_adminfee',
        'grantotalocr',
        'old_grantotalocr',
        'grantotalanalyst',
        'old_grandtotalanalyst'
    ];
    protected $hidden       = [];
}
