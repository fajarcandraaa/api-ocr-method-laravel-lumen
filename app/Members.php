<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Members extends Model
{

    use SoftDeletes;
    protected $table = 'members';

    protected $fillable = [
        'insurance_id',
        'member_id',
        'member_title',
        'member_name',
        'member_type',
        'company_id',
        'birth_date',
        'birth_place',
        'gender',
        'marital_status',
        'citizen',
        'phone_no',
        'email',
        'address',
        'country',
        'prov',
        'city',
        'district',
        'postal_code',
        'identity_no',
        'family_no',
        'tax_no',
        'occupation',
        'position',
        'source_income',
        'source_income_remark',
        'salary',
        'effective_date',
        'expire_date',
        'policy_no',
        'policy_holder',
        'policy_member_type',
        'family_code',
        'payor',
        'policy_effective_date',
        'policy_expiry_date',
        'policy_renewal_date',
        'card_no',
        'member_plan',
        'plan_amt',
        'premium_type',
        'premium_member',
        'term_payment',
        'member_setting',
        'member_setting_remark',
        'remark',
        'status',
        'created_by',
        'last_modified_by'
    ];

    public function member_title(){
        return $this->belongsTo('App\CodeMasters','member_title');
    }

    public function member_type(){
        return $this->belongsTo('App\CodeMasters','member_type');
    }

    public function company_id(){
        return $this->belongsTo('App\Companies','company_id');
    }
    
    public function marital_status(){
        return $this->belongsTo('App\CodeMasters','marital_status');
    }

    public function citizen(){
        return $this->belongsTo('App\CodeMasters','citizen');
    }

    public function country(){
        return $this->belongsTo('App\Countries','country','country_code');
    }

    public function prov(){
        return $this->belongsTo('App\Provinces','prov');
    }

    public function city(){
        return $this->belongsTo('App\Cities','city');
    }

    public function district(){
        return $this->belongsTo('App\Districts','district');
    }

    public function postal_code(){
        return $this->belongsTo('App\Subdistricts','postal_code');
    }

    public function occupation(){
        return $this->belongsTo('App\CodeMasters','occupation');
    }

    public function source_income(){
        return $this->belongsTo('App\CodeMasters','source_income');
    }

    public function member_plan(){
        return $this->belongsTo('App\CodeMasters','member_plan');
    }

    public function premium_type(){
        return $this->belongsTo('App\CodeMasters','premium_type');
    }

    public function term_payment(){
        return $this->belongsTo('App\CodeMasters','term_payment');
    }

    public function bank_infos(){
        return $this->hasMany('App\BankInfos','ref_id')->with('bank_code');
    }
    
    public function insurance_id(){
        return $this->belongsTo('App\Insurances','insurance_id');
    }

    public function policy_member_type(){
        return $this->belongsTo('App\CodeMasters','policy_member_type');
    }

    public function status(){
        return $this->belongsTo('App\CodeMasters','status');
    }

    public function created_by(){
        return $this->belongsTo('App\User','created_by');
    }

    public function last_modified_by(){
        return $this->belongsTo('App\User','last_modified_by');
    }

    protected $hidden = [
    ];

    public static function getValidationRule()
    {
        $rules = [
        'member_id' => 'required',
        // 'member_title' => 'required',
        'member_name' => 'required',
        'member_type' => 'required',
        'birth_date' => 'required',
        'birth_place' => 'required',
        'gender' => 'required',
        'marital_status' => 'required',
        'citizen' => 'required',
        'phone_no' => 'required',
        'email' => 'required',
        'address' => 'required',
        'country' => 'required',
        'prov' => 'required',
        'city' => 'required',
        'district' => 'required',
        'postal_code' => 'required',
        'identity_no' => 'required',
        'family_no' => 'required',
        'occupation' => 'required',
        // 'position' => 'required',
        // 'source_income' => 'required',
        // 'source_income_remark' => 'required',
        'effective_date' => 'required',
        'expire_date' => 'required',
        'policy_no' => 'required',
        // 'policy_member_type' => 'required',
        // 'family_code' => 'required',
        // 'payor' => 'required',
        'card_no' => 'required',
        'member_plan' => 'required',
        'plan_amt' => 'required',
        // 'premium_member' => 'required',
        'term_payment' => 'required',
        ];
        return $rules;
    }
}
