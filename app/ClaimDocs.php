<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ClaimDocs extends Model
{

    use SoftDeletes;

    public function insurances(){
        return $this->belongsTo('App\Insurances','insurance_id');
    }

    public function modifiedBy(){
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    public function createdBy(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function plan(){
        return $this->belongsTo('App\CodeMasters','plan', 'code');
    }

    public function claimregdoc(){
        return $this->belongsTo('App\ClaimRegDoc','id', 'doc_type');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public $incrementing = false;
    protected $fillable = [
        'claim_doc_no','insurance_id','claim_doc_name','plan','remark','is_mandatory','isOcrRead','created_by','last_modified_by'
    ];

    public static function getValidationRule()
    {
        $rules = [
			'claim_doc_name' => 'required',
            'plan' => 'required',
        ];
        return $rules;
    }
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}