<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Insurances extends Model
{

    use SoftDeletes;

    public function addresses(){
        return $this->hasMany('App\InsuranceAddresses','insurance_id');
    }

    public function claimdocs(){
        return $this->hasMany('App\ClaimDocs');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'insuranceName','binNumber','icdType','testCardNumber','testCardBirthDate','logo','testCard','created_by','last_modified_by'
    ];

    public static function getValidationRule()
    {
        $rules = [
            'insuranceName' => 'required',
			'binNumber' => 'required',
            'icdType' => 'required',
			'testCardNumber' => 'required',
			'testCardBirthDate' => 'required',
        ];
        return $rules;
    }
    
    public static function imgValidationRule()
    {
        $rules = [
            'logo' => 'file|image|mimes:jpeg,png,jpg|max:2048',
			'testCard' => 'file|image|mimes:jpeg,png,jpg|max:2048',
        ];
        return $rules;
    }
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}