<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class RolePermissions extends Model
{

    // use SoftDeletes;

    
    public function roles(){
        return $this->belongsTo('App\Roles','role_id');
    }

    public function modifiedBy(){
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    public function createdBy(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances','insurance_id');
    }


    protected $fillable = [
        'role_id','permission','created_by','last_modified_by','insurance_id'
    ];


    // protected $hidden = ['role_id'];
}