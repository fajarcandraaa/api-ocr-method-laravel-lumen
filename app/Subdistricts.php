<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Subdistricts extends Model 
{

    public function district(){
        return $this->belongsTo('App\Districts','district_id');
    }

    use SoftDeletes;
    protected $fillable = [
        'district_id', 'postal_code','subdistrict_desc', 'created_by', 'last_modified_by'
    ];

    protected $hidden = [
    ];

    
}
