<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class ApvProcess extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, SoftDeletes;
    protected $table    = 'apvprocess';

    public function apvprocessDtl()
    {
        return $this->belongsTo('App\ApvProcessDtl','apvid');
    }
    
    protected $fillalbe = [
        'apvid',
        'processname',
        'created_by',
        'last_update_by'
    ];

    protected $hidden = [];
}