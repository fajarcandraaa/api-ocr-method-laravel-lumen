<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class RequestUW extends Model
{
    protected $table    = 'request_uw';


    public function provider_type()
    {
        return $this->belongsTo('App\CodeMasters', 'provider_type');
    }

    public function mid()
    {
        return $this->belongsTo('App\Biz_providers', 'mid', 'merchant_id')->with('compprov');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances', 'insurance_id');
    }

    public function service_child()
    {
        return $this->hasMany('App\ProviderServices', 'parent_service_id');
    }

    public function id_parent_service()
    {
        return $this->belongsTo('App\ProviderServices', 'id_parent_service');
    }

    public function id_iziservice_code()
    {
        return $this->belongsTo('App\IziserviceCode','id_iziservice_code');
        // ->select('id','iziservice_name','iziservice_code');
    }

    public function id_benefit()
    {
        return $this->belongsTo('App\MappingBenefits', 'id_benefit');
    }

    use SoftDeletes;
    protected $fillable = [
        'id',
        'mid',
        'parent_service_id',
        'id_prov_service',
        'id_iziservice_code',
        'service_name',
        'provider_type',
        'insurance_id',
        'id_benefit',
        'claimregno',
        'is_mapped',
    ];

    protected $hidden = [];
}
