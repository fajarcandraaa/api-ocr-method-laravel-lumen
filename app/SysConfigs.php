<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysConfigs extends Model
{
    use SoftDeletes;

    protected $table = 'sysconfig';

    public function sysconfdtl(){
        return $this->hasMany('App\SysConfigDtls','confId','confId');
    }

    protected $fillable = [
        'confName',
        'seqNo',
        'isTier',
        'isEditable',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [];
}
