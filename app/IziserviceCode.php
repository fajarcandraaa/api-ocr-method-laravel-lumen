<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class IziserviceCode extends Model
{

    use SoftDeletes;

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function last_modified_by()
    {
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    public function plan()
    {
        return $this->belongsTo('App\CodeMasters', 'plan', 'code')->where('codeType','BEN');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'iziservice_code', 'iziservice_name', 'description', 'created_by', 'last_modified_by','plan'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
