<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Biz_providers extends Model
{
    use SoftDeletes;

    public function compprov()
    {
        return $this->belongsTo('App\Biz_Compproviders','compprovid');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances','insurance_id');
    }

    public function bank_infos(){
        return $this->hasMany('App\BankInfos','ref_id')->with('bank_code');
    }

    public function created_by(){
        return $this->belongsTo('App\User','created_by');
    }

    public function last_modified_by(){
        return $this->belongsTo('App\User','last_modified_by');
    }

    public function type(){
        return $this->belongsTo('App\CodeMasters','type');
    }

    public function province(){
        return $this->belongsTo('App\Provinces','province');
    }

    public function country(){
        return $this->belongsTo('App\Countries','country','country_code');
    }

    public function city(){
        return $this->belongsTo('App\Cities','city');
    }

    public function district(){
        return $this->belongsTo('App\Districts','district');
    }

    public function postal_code(){
        return $this->belongsTo('App\Subdistricts','postal_code');
    }

    public function contact_person(){
        return $this->hasMany('App\Finances','ref_id');
    }
    
    protected $fillable = [
        'merchant_id',
        'type',
        'name',
        'phone',
        'address',
        'country',
        'city',
        'province',
        'district',
        'postal_code',
        'fax',
        'compprovid',
        'provider_email',
        'website',
        'pic_marketing_name',
        'pic_marketing_email',
        'pic_marketing_phone',
        'created_by',
        'last_modified_by',
        'insurance_id'
    ];

    protected $hidden = [];
}