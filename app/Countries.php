<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Countries extends Model 
{
    use SoftDeletes;
    protected $fillable = [
        'country_code', 'country_desc', 'created_by', 'last_modified_by'
    ];

    protected $hidden = [
    ];

    
}
