<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use App\BankInfos;
use App\Finances;
use App\Companies;
use App\CodeMasters;
use App\User;
use App\Notifications;
use App\RolePermissions;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use \LaravelFCM\Facades\FCM;

use Illuminate\Support\Facades\Log;

use Illuminate\Http\Request;
use File;
use DB;

class Helper
{
    public static function checkDuplicate($table, $type, $vals,$oldID=null)
    {
        $model = '\\App\\' . $table;
        $status = false;
        $msg = 'no duplicate, data safe to be added';
        switch ($table) {
            case 'Biz_Compproviders':
                $col = array('provider_name', 'Branch', 'provider_email');
                break;

            case 'Companies':
                $col = array('company_name', 'branch');
                break;

            case 'EndorsementHdrs':
                $col = array('endorsementno');
                break;

            case 'Roles':
                $col = array('rolename', 'insurance_id');
                break;
        }
        switch ($type) {
            case 'name-branch':
                $val = explode("|", $vals);
                $data = $model::where([[$col[0], $val[0]], [$col[1], $val[1]]])->first();
                if ($data) {
                    $status = true;
                    $msg = $val[0] . "' in Branch '" . $val[1] . "' already exists";
                }
                break;

            case 'email':
                $data = $model::where($col[2], $vals)->first();
                if ($data) {
                    $status = true;
                    $msg = "Email '" . $vals . "' already exists";
                }
                break;

            case 'endorsementno':
                $data = $model::where($col[0], $vals)->first();
                if ($data) {
                    $status = true;
                    $msg = "EndorsementNo '" . $vals . "' already exists";
                }
                break;

            case 'rolename-insurance':
                $val = explode("|", $vals);
                $data = $model::where([[$col[0], $val[0]], [$col[1], $val[1]]])->first();
                if($oldID){
                    if($data && $data->id != $oldID){
                        $data=true;
                    }else{
                        $data=false;
                    }
                }
                if ($data) {
                    $status = true;
                    $msg = $val[0] . "' in insurance_id '" . $val[1] . "' already exists";
                }
                break;
        }

        $result['status'] = (bool) $status;
        $result['message'] = $msg;
        return $result;
    }


    public static function shout(string $string)
    {
        return strtoupper($string);
    }

    public static function uploadPdf($pdf, $path)
    {
        if ($pdf) {
            $folder = $path . "/";
            $time = time();
            $savedName = $time . "." . $pdf->getClientOriginalExtension();
            $pdf->move($folder, $savedName);
            $savedName = $folder . $savedName;
            chmod(base_path() . '/public/' . $savedName, 0666);
        } else {
            $savedName = "";
        }
        return $savedName;
    }

    public static function uploadTempImg($img, $path)
    {
        if ($img) {
            $folder = $path . "/";
            $time = time();
            $savedName = $folder . $time . "." . $img->getClientOriginalExtension();
            $img->move($folder, $savedName);
            chmod(base_path() . '/public/' . $savedName, 0666);
        } else {
            $savedName = "";
        }
        return $savedName;
    }

    public static function moveImg($newPath, $tempPath, $lastname = null)
    {
        Log::debug('tempPath : ' . $tempPath);
        if ($tempPath) {

            $random = Str::random(40);
            $folder = $newPath . "/";

            Log::debug('lastname : ' . $lastname);
            if ($lastname) {
                $savedName = $lastname;
            } else {
                $ext = substr($tempPath, strpos($tempPath, ".") + 1);
                $savedName = $folder . $random . "." . $ext;
            }

            Log::debug('newPath : ' . $newPath);
            if (!is_dir($newPath)) {
                mkdir($newPath, 0777, true);
                chmod($newPath, 0777);
                Log::debug('mkdir');
            }

            Log::debug('saveName : ' . $savedName);
            rename($tempPath, $savedName);
            Log::debug('done!!!');
        } else {
            $savedName = $lastname;
        }

        return $savedName;
    }

    public static function dynamicSearch($all_data, $wheres, $keyword)
    {
        // echo $keyword;die;
        $result = [];

        $all_data = $all_data->where(function ($q) use ($wheres, $keyword) {

            foreach ($wheres as $k => $where) {
                $q = $q->orWhere($where, 'like', '%' . $keyword . '%');
            }
        });
        $result = $all_data;
        return $result;
    }


    public static function partialSearch($all_data, $wheres, $keyword)
    {
        // echo $keyword;die;
        $result = [];

        $all_data = $all_data->where(function ($q) use ($wheres, $keyword) {

            foreach ($keyword as $carien) {
                $q = $q->orWhere($wheres, 'like', '%' . $carien . '%');
            }
        });
        $result = $all_data;
        return $result;
    }


    public static function filterSearch($all_data, $table, $filter)
    {
        $result = [];

        foreach ($filter as $f) {
            $filterdata     = json_decode($f);
            $filterby       = $filterdata->by;
            $filtervalue    = $filterdata->value;
            if (Schema::hasColumn($table, $filterby)) {
                if (is_array($filtervalue)) {
                    $all_data   = $all_data->whereIn($filterby, $filtervalue);
                } else {
                    $all_data       = $all_data->where($filterby, 'like', '%' . $filtervalue . '%');
                }
            }
        }


        $result = $all_data;
        return $result;
    }

    public static function sorting($all_data, $sortby, $sortvalue)
    {
        // echo $keyword;die;

        if (!$sortby) {
            $sortby = 'created_at';
            $sortvalue = 'desc';
        }

        $all_data = $all_data->orderBy($sortby, $sortvalue);

        $result = $all_data;
        return $result;
    }

    public static function addBankInfo($source, $ref_type, $ref_id)
    {
        $data = BankInfos::create([
            'ref_type' => $ref_type,
            'ref_id' => $ref_id,
            'bank_code' => $source['bank_code'],
            'bank_branch' => ($source['bank_branch'] ? $source['bank_branch'] : ""),
            'bank_account' => ($source['bank_account'] ? $source['bank_account'] : ""),
            'bank_account_name' => ($source['bank_account_name'] ? $source['bank_account_name'] : ""),
            'created_by' => $source['id_user']
        ]);

        return $data;
    }

    public static function addCP($source, $ref_type, $ref_id)
    {
        $cp = [];
        $i = 0;
        foreach ($source as $data) {
            $cp[$i] = Finances::create([
                'ref_type' => ($ref_type ? $ref_type : $data['ref_type']),
                'ref_id' => ($ref_id ? $ref_id : $data['ref_id']),
                'pic_name' => $data['pic_name'],
                'pic_phone' => $data['pic_phone'],
                'pic_email' => $data['pic_email'],
                'pic_type' => ($data['pic_type'] ? $data['pic_type'] : "")
            ]);
            $i++;
        }

        return $cp;
    }

    public static function getCodeMasterId($codeType, $codeDesc)
    {
        $id = CodeMasters::where([['codeType', '=', $codeType], ['codeDesc', '=', $codeDesc]])->first();
        $id = $id->id;
        return $id;
    }

    public static function getKey($key, $table, $column, $value)
    {
        $model = '\\App\\' . $table;
        $id = $model::where($column, '=', $value)->first();
        $return = $id->$key;
        return $return;
    }

    public static function replaceRedis($data, $balance_active, $balance_pending = null)
    {
        app('redis')->set("balance_{$data['insurance_id']}_ACTIVE", $balance_active);
        app('redis')->set("balance_{$data['insurance_id']}_PENDING", 0);

        app('redis')->expire("balance_{$data['insurance_id']}_ACTIVE", 300);
        app('redis')->expire("balance_{$data['insurance_id']}_PENDING", 300);
    }

    public function getUserByRolePermission($permission)
    {
        $roleid = RolePermissions::where('permission', $permission)->get('role_id');
        $idroles = [];
        foreach ($roleid as $key => $value) {
            array_push($idroles, $value->role_id);
        }
        $cek_permission = User::whereIn('role_user', $idroles)->get();
        return $cek_permission;
    }

    public static function pushUpdateBalance($insurance_id)
    {
        $admins = User::where('insurance_id', $insurance_id)->get();
        foreach ($admins as $mimin) {
            $tokenuser = $mimin->tokenfirebase;
            if ($tokenuser) {
                $title = "BALANCE_UPDATED";
                $message = (string) json_encode([
                    "message" => "amount of balance updated"
                ]);
                Log::debug('kirim ke user : ' . $mimin->id . ', nama : ' . $mimin->name . ', token : ' . $tokenuser);
                self::PushNotif($tokenuser, $title);
            }
        }
    }

    public static function sendNotifRequest($notif)
    {
        foreach ($notif['to'] as $user) {
            $tokenUser = ($user->isLogin == 1 ? $user->tokenfirebase : null);

            $data = [
                'userId' => $user->id,
                'title' => $notif['title'],
                'message' => $notif['message'],
                'type' => $notif['type'],
                'requester' => $notif['requester'],
                'isRead' => '0',
                'payload' => json_encode($notif['payload'])
            ];

            self::createNotifications($data);

            if ($tokenUser) {
                self::PushNotif($tokenUser, $notif['type'], $notif['alert'], $notif['notification']);
            }
        }
    }

    public static function PushNotif($tokenuser, $title, $alert = false, $notification = false)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setTitle($title);
        $notificationBuilder->setBody('push notif');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'title' => $title,
            'alert' => $alert,
            'notification' => $notification,
        ]);
        $option = $optionBuilder->build();
        // $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $tokenuser; //"a_registration_from_your_database";
        // print_r('aaaaassss');die();
        $downstreamResponse = FCM::sendTo($token, $option, null, $data);
    }

    public static function createNotifications($data)
    {
        $notification   = new Notifications;
        $notification->fill($data);
        $notification->save();

        return true;
    }

    //OCR AI
    public static function ocrEnchancement($req){
        // Parameter Request ada 2 yaitu text dan mid
    
        ini_set("max_execution_time", "0");
        if(!isset($req['text'])) return ["status"=>false,"message"=>"Text is required"];
        if(!isset($req['mid'])) return ["status"=>false,"message"=>"Mid is required"];
    
        $final = self::predict($req);
        return $final;
        // return response()->json($final);
    }
    
    public static function predict($req){
        $text = strtolower($req['text']);
        $mid = strtolower($req['mid']);

        $arr_text = explode(" ", $text);
        $th = 70; // Nilai Threshold (Nilai yang menentukan hasil dari prediksi dapat diubah menyesuaikan dari keakuratan hasil prediksi)
        $phase1 = [];
        $phase2 = [];
        $final = [];
        
        if($mid){ // pengecekan nilai mid
            $medicine = DB::select("SELECT service_name AS name FROM provider_services WHERE mid = $mid"); 
        }

        foreach ($medicine as $real_name) {//processing phase1 
            $test = $text;
            $lower_name = strtolower($real_name->name);
            for ($i=0;$i<strlen($test);$i++) { //processing character on string
            $old_char = substr($test,$i,1);
            $old_percentage = 0;
            if($old_char !== " "){
                foreach (range('a','z') as $value2) {//try to change character and check similarity after that
                $transform = substr_replace($test, $value2, $i,1);
                similar_text($lower_name, $transform,$percentage);
                if($percentage>$old_percentage){
                    if($percentage > $th){ //try to get higher or equal th2 if not return to old character
                    $test = substr_replace($transform, $value2, $i,1);
                    $phase1[] = ["real_name"=>$real_name->name,"prediction"=>mb_convert_encoding($test, 'UTF-8', 'UTF-8'),"percentage"=>$percentage];
                    }
                }
                $old_percentage = $percentage;
                // echo $real_name." - ".$transform." - ".$percentage."<BR>";
                }
            }
            }
        }

        foreach ($phase1 as $key => $value) {
            if($value["percentage"] > ($th+10)) $arr_text = [];
        }

        if(count($arr_text) >1){
            foreach ($phase1 as $key => $value) {
            foreach ($arr_text as $key2 => $value2) {
                if(strrpos($value["real_name"], $value2)){
                if(isset($phase2[$value["real_name"]])){
                    $phase2[$value["real_name"]] += 1;
                }
                else{
                    $phase2[$value["real_name"]] = 0;
                }
                }
            }
            }
        }
        else{
            foreach ($phase1 as $key => $value) {
            $phase2[$value["real_name"]] = 0;
            }
        }

        foreach ($phase2 as $key => $value) {
            similar_text(strtolower($key), $text, $percentage);
            if($percentage >= $th){
            $final["process"][] = ["real_name"=>$key,"score"=>$percentage];
            }
        }

        if(isset($final["process"])){
            $score = 0;
            foreach ($final["process"] as $key => $value) {
            if($value["score"] > $score){
                $final["high_prediction"] = $value["real_name"];
                $final["score"] = $value["score"];
                $score = $value["score"];
            }
            }
        }
        return $final;
    }

    public function findBox_insideBox($source)
    {
        $bill = array_search('Billing_Amount',array_column($source,'Name'));
        return $source[$bill]['Value'];
    }
}
