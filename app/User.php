<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;
    protected $dates =['deleted_at'];
    protected $table = 'users';
    
    // public function permissions(){
    //     return $this->hasMany('App\RolePermissionMedlinx', 'role_id','role_user')->select('role_id','permission');
    // }

    public function permissions(){
        return $this->hasMany('App\RolePermissions','role_id','role_user');
    }

    public function roles(){
        return $this->belongsTo('App\Roles', 'role_user');
    }
    public function insurance()
    {
        return $this->belongsTo('App\Insurances', 'insurance_id');
    }

    public $incrementing = false;
    
    protected $fillable = [
        'name', 'email', 'username', 'phone', 'insurance_id', 'role_user', 'status', 'isLogin', 'lastLoginDate', 'lastLoginTime', 'isLogout','tokenfirebase', 'isInsurance'
    ];

    protected $hidden = [
        'password',
    ];
}
