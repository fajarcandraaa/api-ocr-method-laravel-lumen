<?php

namespace App\Imports;

use App\ClaimDocs;
use Maatwebsite\Excel\Concerns\ToModel;
// use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;


class ClaimDocsImport implements ToModel,WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ClaimDocs([
            'claim_doc_no' => $row['claim_doc_no'],
			'claim_doc_name' => $row['claim_doc_name'],
			'plan' => $row['plan'],
			'remark' => $row['remark'],
            'is_mandatory' => $row['is_mandatory'],
        ]);
    }

    // public function HeadingRow(): int
    // {
    //     return 2;
    // }

    public function startRow(): int
    {
        return 2;
    }
}
