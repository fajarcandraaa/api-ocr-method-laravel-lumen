<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class PendingClaimConvert extends Model
{
    use SoftDeletes;

    protected $table    = 'pendingclaims_converts';
    protected $fillable = [
        'claimregno',
        'category',
        'path'
    ];
    protected $hidden = [];
}
