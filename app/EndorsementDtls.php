<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class EndorsementDtls extends Model
{
    use SoftDeletes;
    protected $table        = 'endorsementdtl';

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function last_modified_by()
    {
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    public function endorsement_benefit()
    {
        // return $this->hasMany('App\EndorsementBenefits', 'endorsementno', 'endorsementno')->with('reviewstep');
        return $this->hasMany('App\EndorsementBenefits', 'endorsementno', 'endorsementno');
    }

    protected $fillable     = [
        'endorsementno',
        'seqNo',
        'path',
        'created_by',
        'last_modified_by'
    ];
    protected $hidden       = [];
}
