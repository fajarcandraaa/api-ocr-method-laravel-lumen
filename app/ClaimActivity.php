<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class ClaimActivity extends Model implements AuthenticatableContract, AuthorizableContract
{
    public function acttype()
    {
        return $this->belongsTo('App\CodeMasters', 'acttype','code');
    }

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    use Authenticatable, Authorizable, SoftDeletes;
    protected $table    = 'claimactivity';

    protected $fillable = [
        'claimregno',
        'acttype',
        'remark',
        'created_by',
        'last_update_by',
        'insurance_id'
    ];

    protected $hidden = [];
}
