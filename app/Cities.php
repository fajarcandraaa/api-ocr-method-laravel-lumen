<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Cities extends Model 
{

    public function province(){
        return $this->belongsTo('App\Provinces','prov_id');
    }

    use SoftDeletes;
    protected $fillable = [
        'prov_id', 'city_code','city_desc','city_type', 'created_by', 'last_modified_by'
    ];

    protected $hidden = [
    ];

    
}
