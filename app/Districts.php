<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Districts extends Model 
{

    public function city(){
        return $this->belongsTo('App\Cities','city_id');
    }

    use SoftDeletes;
    protected $fillable = [
        'city_id', 'district_code','district_desc', 'created_by', 'last_modified_by'
    ];

    protected $hidden = [
    ];

    
}
