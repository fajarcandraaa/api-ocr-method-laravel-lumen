<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class CodeMasters extends Model 
{
    use SoftDeletes;
    protected $fillable = [
        'codeType', 'code', 'codeSeq', 'codeDesc', 'codeRemark', 'codeVal1', 
        'codeVal2', 'codeVal3', 'created_by', 'last_modified_by', 'active'
    ];

    protected $hidden = [
    ];

    
}
