<?php

namespace App\Exports;

use App\Reasons;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;


class ReasonsExport implements WithMapping, WithHeadings, FromArray
{
    /**
     * @return \Illuminate\Support\Collection
     */
    function __construct(array $reason)
    {
        $this->reason = $reason;
    }

    public function array(): array
    {
        return $this->reason;
    }

    public function map($reason): array
    {
        return [
            $reason['reason_code'],
            $reason['reason_name'],
            $reason['description'],
            $reason['eob_type'],
        ];
    }

    public function headings(): array
    {
        return [
            '* Reason Code',
            '* Reason Name',
            'Description',
            '* Explanation of Benefit Type'
        ];
    }
}
