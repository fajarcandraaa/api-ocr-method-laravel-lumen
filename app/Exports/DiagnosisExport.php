<?php

namespace App\Exports;

use App\Diagnosis;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;


class DiagnosisExport implements WithMapping, WithHeadings, FromArray
{
    /**
     * @return \Illuminate\Support\Collection
     */
    function __construct(array $diag)
    {
        $this->diag = $diag;
    }

    public function array(): array
    {
        return $this->diag;
    }

    public function map($diag): array
    {
        return [
            $diag['diagnosisCode'],
            $diag['diagnosisName'],
            $diag['scientificDesc'],
            $diag['description'],
            $diag['standardFee'],
            $diag['standardTreatment'],
            $diag['poly'],
            $diag['isExclusionPolicy'],
            $diag['isPreExisting'],
        ];
    }

    public function headings(): array
    {
        return [
            'Diagnosis Code',
            '* Diagnosis Name',
            'Scientific Description',
            '* Description',
            '* Standard Fee',
            'Standard Treatment',
            'Poly',
            'Exclusion',
            'Pre-Existing'
        ];
    }
}
