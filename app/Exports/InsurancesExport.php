<?php

namespace App\Exports;

use App\Insurances;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class InsurancesExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Insurances::get([
            'insuranceName',
            'binNumber',
            'icdType',
            'testCardNumber'
        ]);
    }

    public function headings(): array
    {
        return [
            'insurance_name',
            'binNumber',
            'icdType',
            'testCardNumber'
        ];
    }
}
