<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserManagementExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $insurance_id;

    function __construct($insurance_id) {
            $this->insurance_id = $insurance_id;
    }

    public function collection()
    {
        $claimDocs=User::whereIn('insurance_id',$this->insurance_id)
        ->get(['name',
        'address',
        'role_user',
        'insurance_id']);
        return $claimDocs;
    }

    public function headings(): array
    {
        return [
            'name',
            'address',
            'role_user',
            'insurance_id'
        ];
    }
}
