<?php

namespace App\Exports;

use App\Reasons;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;


class ProviderServicesExport implements WithMapping, WithHeadings, FromArray
{
    /**
     * @return \Illuminate\Support\Collection
     */
    function __construct(array $ps)
    {
        $this->ps = $ps;
    }

    public function array(): array
    {
        return $this->ps;
    }

    public function map($ps): array
    {
        return [
            $ps['service_code'],
            $ps['service_name'],
            $ps['amount'],
            $ps['description'],
            $ps['provider_type']['codeDesc'],
            $ps['mid']
        ];
    }

    public function headings(): array
    {
        return [
            'Provider Service Code',
            '* Provider Service Name',
            'Amount',
            'Description',
            '* Provider Type',
            '* Provider'
        ];
    }
}
