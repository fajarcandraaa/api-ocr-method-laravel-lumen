<?php

namespace App\Exports;


use App\CodeMasters;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;

class CompprovExport implements WithMapping, WithHeadings, FromArray
{
    function __construct(array $compprov)
    {
        $this->compprov = $compprov;
    }

    public function array(): array
    {
        return $this->compprov;
    }

    public function map($compprov): array
    {
        return [
            ($compprov['IsPartner'] == "0" ? "No" : "Yes"),
            $compprov['biz_prov_no'],
            $compprov['provider_type']['codeDesc'],
            $compprov['provider_name'],
            $compprov['biz_prov_code'],
            $compprov['Branch'],
            $compprov['country']['country_desc'],
            $compprov['prov']['prov_desc'],
            $compprov['city']['city_type']." ".$compprov['city']['city_desc'],
            $compprov['district']['district_desc'],
            $compprov['postal_code']['postal_code'],
            $compprov['Address1'],
            $compprov['Profile'],
            $compprov['SIUPNo'],
            $compprov['TaxNo'],
            $compprov['Domicile'],
            $compprov['NotarialDeed'],
            $compprov['LicenseNo'],
            ($compprov['AggrEffectiveDate'] ? date('d/m/Y', strtotime($compprov['AggrEffectiveDate'])) : ""),
            ($compprov['AggrExpiryDate'] ? date('d/m/Y', strtotime($compprov['AggrExpiryDate'])) : "" ),
            ($compprov['RenewalDate'] ? date('d/m/Y', strtotime($compprov['RenewalDate'])) : ""),
            doubleval($compprov['DiscProvider']),
            $compprov['provider_email'],
            $compprov['PhoneNo'],
            $compprov['FaxNo'],
            $compprov['Remark'],
            ($compprov['IsDifferentHospital'] == "0" ? "No" : "Yes"),
            $compprov['providers']['merchant_id'],
            $compprov['providers']['type']['codeDesc'],
            $compprov['providers']['name'],
            $compprov['providers']['phone'],
            $compprov['providers']['fax'],
            $compprov['providers']['provider_email'],
            $compprov['providers']['country']['country_desc'],
            $compprov['providers']['province']['prov_desc'],
            $compprov['providers']['city']['city_type']." ".$compprov['providers']['city']['city_desc'],
            $compprov['providers']['district']['district_desc'],
            $compprov['providers']['postal_code']['postal_code'],
            $compprov['providers']['address'],
            ($compprov['marketing'] ? $compprov['marketing']['pic_name'] : ""),
            ($compprov['marketing'] ? $compprov['marketing']['pic_phone'] : ""),
            ($compprov['marketing'] ? $compprov['marketing']['pic_email'] : ""),
            ($compprov['marketing'] ? ($compprov['marketing']['ref_type'] == "biz_compproviders" ? "Provider Company" : "Provider") : ""),
            ($compprov['finance'] ? $compprov['finance']['pic_name'] : ""),
            ($compprov['finance'] ? $compprov['finance']['pic_phone'] : ""),
            ($compprov['finance'] ? $compprov['finance']['pic_email'] : ""),
            ($compprov['finance'] ? ($compprov['finance']['ref_type'] == "biz_compproviders" ? "Provider Company" : "Provider" ) : ""),
            // $compprov['bank_infos'],
            ($compprov['bank_infos'] ? $compprov['bank_infos'][0]['bank_code']['codeDesc'] : ""),
            (isset($compprov['bank_infos'][0]['bank_branch']) ? $compprov['bank_infos'][0]['bank_branch'] : ""),
            (isset($compprov['bank_infos'][0]['bank_account']) ? $compprov['bank_infos'][0]['bank_account'] : ""),
            (isset($compprov['bank_infos'][0]['bank_account_name']) ? $compprov['bank_infos'][0]['bank_account_name'] : "")
        ];
    }

    public function headings(): array
    {
        return [
            'Is a Partner',
            'Provider Company ID',
            '* Provider Company Type',
            '* Provider Company Name',
            'Provider Company Code',
            'Branch',
            '* Provider Company Country',
            '* Provider Company Province',
            '* Provider Company City',
            '* Provider Company Distric',
            '* Provider Company Postal Code',
            '* Provider Company Address',
            'Provider Company Profile',
            'SIUP No',
            'NPWP No',
            'Surat Keterangan Domisili',
            'Akta Notaris',
            'Izin Klinik / Rumah Sakit',
            'Agreements Effective Date',
            'Agreement Expiry Date',
            'Renewal Date',
            'Discount From Provider',
            '* Provider Company Email',
            '* Provider Company Phone No',
            'Provider Company Fax No',
            'Remark',
            'Hospital Data is Different from Provider Data?',
            'Provider MID',
            '* Provider Type',
            '* Provider Name',
            '* Provider Phone No',
            'Provider Fax No',
            '* Provider Email',
            '* Provider Country',
            '* Provider Province',
            '* Provider City',
            '* Provider District',
            '* Provider Postal Code',
            '* Provider Address',
            'Marketing Name',
            'Marketing Phone No',
            'Marketing Email',
            'Marketing Category',
            'Finance Name',
            'Finance Phone No',
            'Finance Email',
            'Finance Category',
            '* Bank Name',
            '* Bank Branch',
            '* Account No',
            '* Account Name'
        ];
    }
}
