<?php

namespace App\Exports;


use App\CodeMasters;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;

class CompaniesExport implements WithMapping, WithHeadings, FromArray
{
    function __construct(array $comp)
    {
        $this->comp = $comp;
    }

    public function array(): array
    {
        return $this->comp;
    }

    public function map($comp): array
    {
        return [
            ($comp['have_parent_comp'] == 1 ? "Yes" : "No"),
            ($comp['have_parent_comp'] == 1 ? $comp['parent_comp_id']['company_name'] : ""),
            $comp['company_code'],
            $comp['company_type']['codeDesc'],
            $comp['company_name'],
            $comp['branch'],
            $comp['status']['codeDesc'],
            $comp['tax_no'],
            $comp['phoneno'],
            $comp['faxno'],
            $comp['email'],
            $comp['address1'],
            $comp['country']['country_desc'],
            $comp['prov']['prov_desc'],
            $comp['city']['city_type']." ".$comp['city']['city_desc'],
            $comp['district']['district_desc'],
            $comp['postal_code']['postal_code'],
            $comp['remark'],
            $comp['policy_number'],
            substr($comp['policy_holder'],23),
            $comp['payor'],
            date('d/m/Y',strtotime($comp['effectivedate'])),
            date('d/m/Y',strtotime($comp['expirydate'])),
            ($comp['renewaldate'] ? date('d/m/Y',strtotime($comp['renewaldate'])) : null),
            ($comp['bank_infos'] ? $comp['bank_infos'][0]['bank_code']['codeDesc'] : ""),
            ($comp['bank_infos'][0]['bank_branch'] ? $comp['bank_infos'][0]['bank_branch'] : ""),
            ($comp['bank_infos'][0]['bank_account'] ? $comp['bank_infos'][0]['bank_account'] : ""),
            ($comp['bank_infos'][0]['bank_account_name'] ? $comp['bank_infos'][0]['bank_account_name'] : ""),
            ($comp['contact_person'][0]['pic_name'] ? $comp['contact_person'][0]['pic_name'] : ""),
            ($comp['contact_person'][0]['pic_phone'] ? $comp['contact_person'][0]['pic_phone'] : ""),
            ($comp['contact_person'][0]['pic_email'] ? $comp['contact_person'][0]['pic_email'] : "")
        ];
    }

    public function headings(): array
    {
        return [
            'Have Parent Company',
            'Company Parent Name',
            'Company Code',
            '* Company Type',
            '* Company Name',
            'Branch',
            '* Company Status',
            'NPWP No',
            '* Phone No',
            'Fax No',
            '* Email',
            '* Address',
            '* Country',
            '* Province',
            '* City',
            '* District',
            '* Postal Code',
            'Remark',
            '* Policy No',
            '* Policy Holder',
            'Payor',
            '* Policy Effective Date',
            '* Policy Expiry Date',
            'Renewal Date',
            '* Bank Name',
            '* Branch',
            '* Account No',
            '* Account Name',
            '* PIC Name',
            '* PIC Phone No',
            '* PIC Email'
        ];
    }
}
