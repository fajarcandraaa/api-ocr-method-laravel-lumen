<?php

namespace App\Exports;

use App\ClaimDocs;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;

class ClaimDocsExport implements WithMapping, WithHeadings, FromArray
{
    /**
    * @return \Illuminate\Support\Collection
    */

    function __construct(array $claim)
    {
        $this->claim = $claim;
    }

    public function array(): array
    {
        return $this->claim;
    }

    public function map($compprov): array
    {
        return [
            $compprov['claim_doc_no'],
            $compprov['claim_doc_name'],
            $compprov['plan']['code'],
            $compprov['remark'],
            ($compprov['is_mandatory'] == "1" ? "Yes" : "No"),
            ($compprov['isOcrRead'] == "1" ? "Yes" : "No")
        ];
    }

    public function headings(): array
    {
        return [
            'Claim Document No',
            '* Claim Document Name',
            '* Plan',
            '* Remark',
            'Is Mandatory',
            'Is OCR Read',
        ];
    }
}
