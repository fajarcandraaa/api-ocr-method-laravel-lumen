<?php

namespace App\Exports;

use App\Members;
use App\CodeMasters;
use App\BankInfos;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;

class MembersExport implements WithMapping, WithHeadings, FromArray
{
    protected $insurance_id;

    // function __construct($insurance_id) {
    //         $this->insurance_id = $insurance_id;
    // }

    // constructor array
    function __construct(array $members)
    {
        $this->members = $members;
    }

    public function array(): array
    {
        return $this->members;
    }

    public function map($members): array
    {
        // $bank_infos=$members['bank_infos'][0];
        // print_r ($bank_infos);
        // die;
        return [
            $members['member_id'],
            ($members['gender']),
            $members['member_name'],
            $members['status']['codeDesc'],
            $members['member_type']['codeDesc'],
            ($members['company_id'] ? $members['company_id']['company_name'] : ""),
            $members['birth_place'],
            date('d/m/Y', strtotime($members['birth_date'])),
            $members['marital_status']['codeDesc'],
            $members['citizen']['codeDesc'],
            $members['phone_no'],
            $members['email'],
            $members['address'],
            $members['country']['country_desc'],
            $members['prov']['prov_desc'],
            $members['city']['city_type']." ".$members['city']['city_desc'],
            $members['district']['district_desc'],
            $members['postal_code']['postal_code'],
            $members['identity_no'],
            $members['family_no'],
            ($members['tax_no'] ? $members['tax_no'] : ""),
            $members['occupation']['codeDesc'],
            $members['position'],
            $members['source_income']['codeDesc'],
            ($members['salary'] ? $members['salary'] : ""),
            ($members['policy_no'] ? $members['policy_no'] : ""),
            $members['policy_member_type']['codeDesc'],
            $members['policy_holder'],
            date('d/m/Y', strtotime($members['effective_date'])),
            date('d/m/Y', strtotime($members['expire_date'])),
            $members['family_code'],
            $members['payor'],
            (strpos($members['member_setting'], '1') === false ? "No" :  "Yes"),
            (strpos($members['member_setting'], '2') === false ? "No" :  "Yes"),
            ($members['other_policy_no'] ? $members['other_policy_no'] : "") ,
            ($members['other_policy_name'] ?  $members['other_policy_name'] : ""),
            (strpos($members['member_setting'], '3') === false ? "No" :  "Yes"),
            (strpos($members['member_setting'], '4') === false ? "No" :  "Yes"),
            (strpos($members['member_setting'], '5') === false ? "No" :  "Yes"),
            (strpos($members['member_setting'], '6') === false ? "No" :  "Yes"),
            (strpos($members['member_setting'], '7') === false ? "No" :  "Yes"),
            (strpos($members['member_setting'], '8') === false ? "No" :  "Yes"),
            date('d/m/Y', strtotime($members['policy_effective_date'])),
            date('d/m/Y', strtotime($members['policy_expiry_date'])),
            ($members['policy_renewal_date'] ? date('d/m/Y', strtotime($members['policy_renewal_date'])) : ""),
            $members['card_no'],
            $members['member_plan']['codeDesc'],
            $members['plan_amt'],
            $members['premium_type']['codeDesc'],
            ($members['premium_member'] ? $members['premium_member'] : ""),
            $members['term_payment']['codeDesc'],
            $members['remark'],
            ($members['bank_infos'] ? $members['bank_infos'][0]['bank_code']['codeDesc'] : ""),
            ($members['bank_infos'][0]['bank_branch'] ? $members['bank_infos'][0]['bank_branch'] : ""),
            ($members['bank_infos'][0]['bank_account'] ? $members['bank_infos'][0]['bank_account'] : ""),
            ($members['bank_infos'][0]['bank_account_name'] ? $members['bank_infos'][0]['bank_account_name'] : "")
        ];
    }

    public function headings(): array
    {
        return [
            'Member No',
            'Gender',
            '* Member Name',
            '* Member Status',
            '* Member Type',
            'Company',
            '* Birth Place',
            '* Date of Birth',
            '* Marital Status',
            '* Citizen',
            '* Phone No',
            '* Email',
            '* Address',
            '* Country',
            '* Province',
            '* City',
            '* District',
            '* Postal Code',
            '* KTP No',
            '* KK No',
            'NPWP No',
            '* Occupation',
            'Position',
            '* Source Income',
            'Salary',
            'Policy No',
            '* Membership',
            '* Policy Holder',
            '* Members Effective Date',
            '* Members Expire Date',
            'Family Code',
            '* Payor',
            'Occur in Other Membership',
            'Have policy from another insurance',
            'Another Policy No',
            'Another Insurance Name',
            'VIP',
            'Hold Card Swipe and Claim',
            'Pre-Existing Waived',
            'Exclude this Member',
            'Dummy Member',
            'Black list Person',
            '* Policy Effective Date',
            '* Policy Expire Date',
            'Renewal Policy Date',
            '* Card No',
            '* Member Plan',
            '* Plan Amount',
            '* Premium Type',
            'Premium Member',
            '*Term of Payment',
            'Remark',
            '* Bank Name',
            '* Bank Branch',
            '* Account No',
            '* Account Name'
        ];
    }
}
