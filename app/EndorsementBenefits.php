<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class EndorsementBenefits extends Model
{
    use SoftDeletes;
    protected $table        = 'endorsementbenefit';

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function last_modified_by()
    {
        return $this->belongsTo('App\User', 'last_modified_by');
    }

    // public function reviewstep()
    // {
    //     return $this->hasMany('App\SysConfigs', 'reviewstep', 'seqNo');
    // }

    protected $fillable     = [
        'endorsementno',
        'reviewstep',
        'endorsement_page',
        'created_by',
        'last_modified_by'
    ];
    protected $hidden       = [];
}
