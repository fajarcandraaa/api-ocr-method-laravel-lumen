<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class ClaimRegDocConverts extends Model 
{
    protected $table    = 'claimregdoc_converts';
    protected $fillable = [
        'id_claimregdoc',
        'path',
        'is_cover'
    ];
    protected $hidden = [];
}