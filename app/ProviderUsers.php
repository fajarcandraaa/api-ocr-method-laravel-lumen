<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderUsers extends Model
{
    use SoftDeletes;
    protected $dates =['deleted_at'];
    protected $table = 'provider_users';

    public function biz_providers()
    {
        return $this->belongsTo('App\Biz_providers','mid','merchant_id');
    }

    public function biz_compproviders()
    {
        return $this->belongsTo('App\Biz_compproviders','mid');
    }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances','insurance_id');
    }
    
    protected $fillable = [
        'provuserid', 'username', 'email', 'mid', 'providertype', 'provname', 'created_by', 'last_modifies_by', 'division', 'status', 'phonenumber', 'insurance_id'
    ];

    protected $hidden = [];
}