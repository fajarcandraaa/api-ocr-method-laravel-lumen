<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Roles extends Model
{
    public function users(){
        return $this->hasMany('App\User','role_user');
    }

    // public function created_by(){
    //     return $this->belongsTo('App\User','created_by');
    // }

    // public function last_modified_by(){
    //     return $this->belongsTo('App\User','last_modified_by');
    // }

    public function insurance_id()
    {
        return $this->belongsTo('App\Insurances','insurance_id');
    }

    public function permissions(){
        return $this->hasMany('App\RolePermissions','role_id');
    }

    protected $fillable = [
        'rolecode', 'rolename','set_default', 'insurance_id', 'isInsurance'
    ];
    
    public static function getValidationRule()
    {
        $rules = [
            'rolename' => 'required',
        ];
        return $rules;
    }

    protected $hidden = [];
}