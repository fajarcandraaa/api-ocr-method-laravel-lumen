<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'po_number',
        'qty',
        'insurance_id',
        'status',
        'trans_date',
        'created_by',
        'approved_by',
        'approved_at',
        'imagepath',
        'notes',
        'reason'
    ];

    protected $table = 'purchase_orders';
    protected $hidden = [];

    public function statusDesc(){
        return $this->belongsTo('App\CodeMasters','status', 'code')->where('codeType','PCO');
    }

    public function approvedByDesc(){
        return $this->belongsTo('App\User','approved_by', 'id');
    }

    public function createdByDesc(){
        return $this->belongsTo('App\User','created_by', 'id');
    }

    public function insurance(){
        return $this->belongsTo('App\Insurances','insurance_id', 'id');
    }

    public function reason(){
        return $this->belongsTo('App\CodeMasters','reason', 'code')->where('codeType','RSN');
    }
}
