<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class BankInfos extends Model
{


    use SoftDeletes;

    public function bank_code(){
        return $this->belongsTo('App\CodeMasters','bank_code');
    }

    protected $fillable = [
        'bank_id',
        'ref_type',
        'ref_id',
        'bank_code',
        'bank_branch',
        'bank_account',
        'bank_account_name',
        'created_by',
        'last_modified_by'
    ];

    protected $hidden = [
    ];
}
