<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceAddresses extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'insurance_id','address'
    ];

    public function insurance(){
        return $this->belongsTo('App\Insurances');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}