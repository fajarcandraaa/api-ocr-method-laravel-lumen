<?php

namespace App\Http\Middleware;

use Closure;

class AuthInsuranceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request){
            $headerKey = $request->header('auth-key');
            //$headerTotal = $request->input('total');

            $hashKey = hash('sha256', 'authentication_to_insurance_api_123456789');
            
            switch (true) {
                case empty($headerKey):
                    return response([
                        'error' => [
                            'code' => 404,
                            'message' => 'authentication key cannot be empty!'
                        ]
                    ], 400);
                    break;
                case $headerKey !== $hashKey:
                    return response([
                        'code' => 400,
                        'message' => 'entered authentication key is not match'
                    ]);
                    break;
                
                default:
                    return $next($request);
                    break;
            }
        }
    }
}
