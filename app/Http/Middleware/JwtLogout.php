<?php
namespace App\Http\Middleware;
use Closure;
use Exception;
use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
class JwtLogout
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('token');
        
        if ($token == null) {
            return response()->json(['message' => 'Please cek your token verification'], 400);
        } else {
            try {
                list($header, $payload, $signature) = explode(".", $token);
                $payload = json_decode(base64_decode($payload));

                $user = User::find($payload->usr);
                $request->auth = $user;
                return $next($request);
            } catch (\Exception $e) {
                return response()->json(['message' => 'Something wrong with your token, please contact your adminstrator'], 401);
            }
            
        }
    }
}