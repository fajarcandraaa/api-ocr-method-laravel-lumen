<?php
namespace App\Http\Middleware;
use Closure;
use Exception;
use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
class JwtRefresh
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token      = $request->header('tokenrefresh');
        
        if ($token == null) {
            return response()->json(['message' => 'Please cek your refresh-token verification'], 400);
        } else {
            try {
                $cektoken   = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
                if ($cektoken->sub != "refresh-token") {
                    return response()->json([
                        'error' => 'Use your refresh token please.'
                    ], 401);
                } else {
                    if(!$token) {
                        return response()->json([
                            'error' => 'Token not provided.'
                        ], 401);
                    }
                    try {
                        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
                    } catch(ExpiredException $e) {
                        return response()->json([
                            'error' => 'Provided token is expired.'
                        ], 400);
                    } catch(Exception $e) {
                        return response()->json([
                            'error' => 'An error while decoding token.'
                        ], 400);
                    }
                }
            } catch(ExpiredException $e) {
                return response()->json([
                    'error' => 'Provided token is expired.'
                ], 400);
            } catch(Exception $e) {
                return response()->json([
                    'error' => 'An error while decoding token.'
                ], 400);
            }

            $user = User::find($credentials->usr);
            $request->auth = $user;
            return $next($request);
        }
    }
}