<?php

namespace App\Http\Middleware;

use Closure;

class ExampleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request){
            $headerKey = $request->header('X-Api-Key');
            $headerTotal = $request->input('total');

            $hashKey = hash('sha256', $headerTotal.'key');
            
            switch (true) {
                case empty($headerKey):
                    return response([
                        'status' => false,
                        'message' => 'key cant empty'
                    ]);
                    break;
                case $headerKey !== $hashKey:
                    return response([
                        'status' => false,
                        'message' => 'key not match'
                    ]);
                    break;
                
                default:
                    return $next($request);
                    break;
            }
        }
    }
}
