<?php
namespace App\Http\Middleware;
use Closure;
use Exception;
use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('token');

        if ($token == null) {
            return response()->json(['message' => 'Please cek your token verification'], 400);
        } else {   
            try {
                $decodetoken    = JWT::decode($token, env('JWT_SECRET'), array('HS256'));
                // print_r($decodetoken);die();
                $medlinxkey     = $decodetoken->app;
                if ($decodetoken->sub == "refresh-token") {
                    return response()->json([
                        'error' => 'Use your access token please.'
                    ], 401);
                } else {
                    if(!$token) {
                        return response()->json([
                            'error' => 'Token not provided.'
                        ], 401);
                    }
                    if ($medlinxkey == env('APP_KEY')) {
                            $client = new Client(['headers' => ['token' => $token]]);
                            try {
                                $response = $client->request('GET', env('APP_CORE').'/valtoken/');
                            } catch (RequestException $e) {
                                return response()->json(['message' => 'An error while decoding token.'], 400);
                            }
                            $user = json_decode($response->getBody()->getContents());
                            $user->medlinx_internal = true;
                    } else {
                        try {
                            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
                        } catch(ExpiredException $e) {
                            return response()->json([
                                'error' => 'Provided token is expired.'
                            ], 400);
                        } catch(Exception $e) {
                            return response()->json([
                                'error' => 'An error while decoding token.'
                            ], 400);
                        }
                        $user = User::find($credentials->usr);
                        $user->medlinx_internal = false;
                        
                        if($user->tokenfirebase != $credentials->tokenfirebase){
                            return response()->json([
                                'error' => 'Provided token mismatch '.$user->tokenfirebase.' != '.$credentials->tokenfirebase
                            ], 402);
                        }
                    }
                }
                
            } catch(ExpiredException $e) {
                return response()->json([
                    'error' => 'Provided token is expired.'
                ], 400);
            } catch(Exception $e) {
                return response()->json([
                    'error' => 'An error while decoding token.'
                ], 400);
            }
            $request->auth  = $user;
            // print_r($request->auth);die();    
            return $next($request);
        }
    }
}