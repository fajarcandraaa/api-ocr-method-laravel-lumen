<?php
namespace App\Http\Controllers;

use App\Provinces;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ProvincesController extends Controller
{
    
    public function getProvinces($country_code=null)
    {
        $status = true;
        $error = "";
        if($country_code){
            $provinces = Provinces::with('country')->where('country_code',$country_code)->get();
            }else{
                $provinces = Provinces::with('country')->get();
            }
        
        if(!$provinces->count()){
            $status = false;
            $error = "data not available";
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($provinces) ? $provinces : null),
            "error"     => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

}