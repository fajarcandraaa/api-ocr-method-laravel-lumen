<?php
namespace App\Http\Controllers;

use App\Cities;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CitiesController extends Controller
{
    
    public function getCities($prov_id=null)
    {
        $status = true;
        $error = "";
        if($prov_id){
            $cities = Cities::with('province')->where('prov_id',$prov_id)->get();
            }else{
                $cities = Cities::with('province')->get();
            }
        
        if(!$cities->count()){
            $status = false;
            $error = "data not available";
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($cities) ? $cities : null),
            "error"     => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

}