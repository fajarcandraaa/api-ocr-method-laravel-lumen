<?php

namespace App\Http\Controllers;

use App\MappingBenefits;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ProviderServicesController as ProviderServicesController;
use Illuminate\Http\Request;


class MappingBenefitsController extends Controller
{

    private $provservice;
    // private $id_admin;
    // private $insurance_id;
    public function __construct(Request $request)
    {
        $this->provservice = new ProviderServicesController($request);
        // $this->id_admin = $request->auth->id;
        // $this->insurance_id = $request->auth->insurance_id;
    }

    public function addMappingBenefit($auth, $data)
    {
        try {
            $msg = "add Mapping Benefit Failed. ";
            $ms = MappingBenefits::create([
                'insurance_id' => $data['insurance_id'],
                'benefit_type' => $data['benefit_type'],
                'seq_number' => $data['seq_number'],
                'benefit_code' => $data['benefit_code'],
                'benefit_name' => $data['benefit_name'],
                'id_iziservice_code' => $data['id_iziservice_code'],
                'created_by' => $auth->id
            ]);

            return ' add mapping benefit id ' . $ms->id . ' succeed.';
        } catch (\Exception $msg) {
            return ' add mapping benefit failed.';
        }
    }

    public function processMappingBenefit(Request $request)
    {
        // print_r($request->auth);die();
        $data = $request->json()->all();
        $valuee = $data['data'];
        $msg = "";

        if (count($data['deleted_id']) > 0) {
            $value2 = $data['deleted_id'];
            $getmap   = MappingBenefits::whereIn('id', $value2);
            if ($getmap) {
                $getmap->delete();
            }
        }

        try {
            foreach ($valuee as $value1) {
                if ($value1['id']) {
                    $ms = $this->updateMappingBenefit($request->auth, $value1, $value1['id']);
                    $msg .= $ms;
                } else {
                    $ms = $this->addMappingBenefit($request->auth, $value1);
                    $msg .= $ms;
                }
            }
            return response()->json("Data Updated Successfully", 200);
        } catch (\Exception $e) {
            return response()->json("Data Update Failed", 409);
        }
    }


    public function showMappingBenefit(Request $request, $id = null)
    {
        $sortby         = $request->input('sortby');
        $sortvalue      = $request->input('sortvalue');
        $status         = false;
        $error          = "data not found";
        $getmapben      = MappingBenefits::with(['insurance_id', 'id_iziservice_code', 'benefit_type', 'created_by', 'last_modified_by']);
        // ->get();
        // return $getmapservice;
        if ($request->auth->isInsurance) {
            $getmapben   = $getmapben->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id) {
            $getmapben   = $getmapben->where('id', $id)->first();

            if ($getmapben) {
                $status     = true;
                $error      = null;
            }
        } else {
            if ($request->has('filter')) {
                $table          = 'mapping_benefits';
                $filter         = $request->input('filter');
                $getmapben      = Helper::filterSearch($getmapben, $table, $filter);
                foreach ($filter as $f) {
                    $filterdata         = json_decode($f);
                    $filterby           = $filterdata->by;
                    $filtervalue        = $filterdata->value;
                    if (is_array($filtervalue)) {
                        if ($filterby == 'codeDesc') {
                            $getmapben   = $getmapben->whereHas('benefit_type', function ($query) use ($filtervalue) {
                                $query->whereIn('codeDesc', $filtervalue);
                            });
                        } elseif ($filterby == 'iziservice') {
                            $getmapben   = $getmapben->whereHas('id_iziservice_code', function ($query) use ($filtervalue) {
                                $query->whereIn('iziservice_name', $filtervalue);
                            });
                        } elseif ($filterby == 'insurancename') {
                            $getmapben   = $getmapben->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                $query->whereIn('insuranceName', $filtervalue);
                            });
                        }
                    } else {
                        if ($filterby == 'codeDesc') {
                            $getmapben   = $getmapben->whereHas('benefit_type', function ($query) use ($filtervalue) {
                                $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'iziservice') {
                            $getmapben   = $getmapben->whereHas('id_iziservice_code', function ($query) use ($filtervalue) {
                                $query->where('iziservice_name', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'insurancename') {
                            $getmapben   = $getmapben->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                $query->where('insuranceName', 'like', '%' . $filtervalue . '%');
                            });
                        }
                    }
                }
            }

            if ($getmapben->count()) {
                $status         = true;
                $error          = null;
            }

            $getmapben   = Helper::sorting($getmapben, $sortby, $sortvalue);
            $limit          = $request->has('limit') ? $request->input('limit') : 20;
            $page           = $request->has('page') ? $request->input('page') : 1;
            $getmapben   = $getmapben->paginate($limit, ['*'], 'page', $page);
            $meta           = [
                'page'          => (int) $getmapben->currentPage(),
                'perPage'       => (int) $getmapben->perPage(),
                'total'         => (int) $getmapben->total(),
                'totalPage'     => (int) $getmapben->lastPage()
            ];
            $getmapben   = $getmapben->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getmapben) ? $getmapben : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function updateMappingBenefit($auth, $req, $id)
    {
        $data   = MappingBenefits::find($id);

        try {
            $msg = "update Mapping Benefit Failed. ";
            if (!$auth->medlinx_internal) {
                $ms = $data->update([
                    'insurance_id' => $auth->insurance_id,
                    'benefit_type' => $req['benefit_type'],
                    'benefit_code' => $req['benefit_code'],
                    'seq_number' => $data['seq_number'],
                    'benefit_name' => $req['benefit_name'],
                    'id_iziservice_code' => $req['id_iziservice_code'],
                    'last_modified_by' => $auth->id
                ]);
            } else {
                $ms = $data->update([
                    'insurance_id' => $req['insurance_id'],
                    'benefit_type' => $req['benefit_type'],
                    'benefit_code' => $req['benefit_code'],
                    'seq_number' => $data['seq_number'],
                    'benefit_name' => $req['benefit_name'],
                    'id_iziservice_code' => $req['id_iziservice_code'],
                    'last_modified_by' => $auth->id
                ]);
            }

            return ' update mapping benefit id ' . $ms->id . ' succeed.';
        } catch (\Exception $msg) {
            return ' update mapping benefit failed.';
        }
    }

    public function deleteMappingBenefit(Request $request, $id)
    {
        $getmapben   = MappingBenefits::find($id);
        if ($getmapben) {
            $getmapben->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function massDeleteMappingBenefit(Request $request)
    {
        $getmapservice   = MappingBenefits::whereIn('id', $request->id);
        if ($getmapservice) {
            $getmapservice->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }
}
