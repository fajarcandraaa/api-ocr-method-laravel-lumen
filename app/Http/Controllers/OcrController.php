<?php

namespace App\Http\Controllers;

use Imagick;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Spatie\PdfToText\Pdf;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Helper;


use Smalot\PdfParser\Parser;

class OcrController extends Controller
{
    private $notificationcontroller;

    public function __construct(NotificationController $notificationcontroller)
    {
        $this->notificationcontroller = $notificationcontroller;
    }
    public function uploadPdf(Request $request)
    {
        $status     = false;
        $error      = "upload pdf failed";
        $tokenuser  = $request->input('tokenFirebase');
        $title      = "OCR Process";
        
        $rules      = [
            "file" => "required|mimes:pdf"
        ];
        $pdf = $request->file('pdf');
        // $filename = Helper::uploadPdf($pdf, storage_path('app/pdf/'));
        $filename = Helper::uploadPdf($pdf, "assets/pdf");
        if ($filename) {
            $status = true;
            $error = "";
        }
        $converImage = [];
        if($status){
            $message    = "Convert to image";
            $message2   = "25%";
            $this->notificationcontroller->PushNotif($title, $message, $message2, $tokenuser);
            $converImage = $this->pdftoimage($filename, $tokenuser);
        } 

        $message    = "Mapping OCR";
        $message2   = "75%";
        $this->notificationcontroller->PushNotif($title, $message, $message2, $tokenuser);

        $response = [
            "status" => (bool) $status,
            "data" => $converImage,
            "error" => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function pdftoimage($pdfFile, $tokenuser)
    {
        $pdf        = new \Spatie\PdfToImage\Pdf($pdfFile);
        $filename   = basename($pdfFile);
        $total      =  $pdf->getNumberOfPages();
        $image      = [];
        $title      = "OCR Process";
        
        foreach (range(1, $total) as $pageNumber) {
            $img['full_path']   = storage_path('app/convert/').$filename.'-page'.$pageNumber.'.png';
            $img['rel_path']    = '/storage/convert/'.$filename.'-page'.$pageNumber.'.png';
            $img['pageNumber']  = $pageNumber;
            $pdf->setPage($pageNumber)
            ->setOutputFormat('png')
            ->saveImage($img['full_path']);

            
            $message    = "Tesseract Process page : ".$pageNumber." of ".$total;
            $message2   = "50%";
            $this->notificationcontroller->PushNotif($title, $message, $message2, $tokenuser);
            $img['ocr']     = $this->testocr( $img['full_path'], $tokenuser);
            $image[]        = $img;
        }
        
        
        $data = [
            'total' =>  $pdf->getNumberOfPages(),
            'image' => $image
        ];
        
        return $data;
    }

    public function testocr($pathFile, $tokenuser)
    {
        date_default_timezone_set("Asia/Bangkok");
        $array      = array();
        $ocrtext    = (new TesseractOCR($pathFile))->lang('ind','eng','msa')->run();
        $filename   = explode(".",basename($pathFile));
        $a          = explode("\n",$ocrtext);
        $i          = 0;
        foreach ($a as $b) {
            if ($b != "" && $b != " " && $b != "  " && $b != "   " && $b != "    " && $b != "     "  && $b != "      ") {
                $array["line" . $i] = $b;
                $i++;
            }
        }
        Storage::put($filename[0].'-'.date("Ymd").date("His").'.txt', json_encode($array, JSON_PRETTY_PRINT));
        
        return $array;
    }

    public function upload()
    {
        return view('upload');
    }

    public function txtProcess(Request $request)
    {
        $data   = $request->json()->all();
        $hasil  = $data['data']['image'];

        $ocr    = [];
        $page   = 1;
        foreach ($hasil as $key => $value) {
            array_push($ocr, array("pageNumber" => $page));
            foreach ($value['ocr'] as $data) {
                array_push($ocr, $data);
            }
            $page++;
        }

        $keywords = array (
            "Subject",
            "Invoice No",
            "Invoice Date",
            "Invoice Amount",
            "Invoice Amount In Words",
            "Invoice",
            "SI No",
            "Admit Date",
            "IP No / UHID",
            "Patien Name",
            "Bill Amount",
            "Remarks",
            "Admission",
            "Total Amount",
            "Administration Fee",
            "Account",
            "Total Amount Cover by Payer",
            "Service(s)",
            "Page",
            "Print Date",
            "Printed By",
            "Bill No",
            "Patient Name",
            "Bill Date",
            "MR No",
            "Payer",
            "IP No",
            "Address",
            "Ward",
            "Primary Doctor",
            "Admission Date",
            "Age",
            "Discharge Date",
            "Gender",
            "Grand Total",
            "From Date",
            "To Date",
            "Bed Type",
            "Bed No",
            "Doctor Name",
            "Services(s)",
            "Qty",
            "Price",
            "No",
            "Trans Date",
            "Drugs & Consumables",
            "Consumables Type",
            "UOM",
            "Payer Amount",
            "Patient Amount",
            "Total"
        );

        $result = [];
        // foreach ($ocr as $data){
        //     foreach ($keywords as $keyword) {
        //     if (stripos($data, $keyword)) {
        //         if(isset($result[$keyword])){
        //             $i=count($result[$keyword]);
        //             $result[$keyword][$i] = $data;
        //         }else{
        //             $result[$keyword][0]=$data;
        //         }
        //     }
        // }
        // }
        // return $result;
        return $ocr;
        // return response()->json($ocr);
    }

    public function testocr_v(Request $request)
    {

        $imageAnnotator = new ImageAnnotatorClient();
        $fileName       = $request->file('image');

        # prepare the image to be annotated
        $image          = file_get_contents($fileName);
        $response       = $imageAnnotator->textDetection($image);
        $texts          = $response->getTextAnnotations();
        $i              = 0;
        if ($texts) {
            foreach ($texts as $text) {
                echo "block" . $i . " : ";
                echo $text->getDescription() . PHP_EOL;
                $i++;
            }
        } else {
            echo ('No label found' . PHP_EOL);
        }
    }
}
