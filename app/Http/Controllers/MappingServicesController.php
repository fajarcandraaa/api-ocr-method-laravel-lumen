<?php

namespace App\Http\Controllers;

use App\MappingServices;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ProviderServicesController as ProviderServicesController;
use App\ProviderServices;
use Illuminate\Http\Request;


class MappingServicesController extends Controller
{

    private $provservice;
    private $id_admin;
    public function __construct(Request $request)
    {
        $this->provservice = new ProviderServicesController($request);
        $this->id_admin = $request->auth->id;
    }

    // public function addMapping(Request $request)
    // {
    //     try {
    //         $msg = "add Mapping Service Failed. ";
    //         if ($request->isNewService) {
    //             $provservicedata = new Request;
    //             $provservicedata->replace([
    //                 'service_code' => $request->prov_service_code,
    //                 'service_name' => $request->prov_service_name,
    //                 'provider_type' => $request->provider_type,
    //                 'mid' => $request->mid,
    //                 'created_by' => $request->auth->id
    //             ]);
    //             try {
    //                 $ps = $this->provservice->addService($provservicedata);
    //                 $aa = $ps->getData()->data;
    //                 $idservice = $aa->id;
    //             } catch (\Exception $e) {
    //                 $msg .= "add service failed. ";
    //                 throw new \Exception();
    //             }
    //         } else {
    //             $idservice = $request->prov_service_name;
    //         }

    //         $ms = MappingServices::create([
    //             'id_prov_service' => $idservice,
    //             'id_iziservice_code' => $request->id_iziservice_code,
    //             'created_by' => $request->auth->id
    //         ]);

    //         return ' add mapping service id '. $ms->id.' succeed.';

    //     } catch (\Exception $msg) {
    //         return ' add mapping service id '. $ms->id.' failed.';
    //     }
    // }

    public function addMapping($data)
    {

        try {
            $msg = "add Mapping Service Failed. ";
            if ($data['isNewService']) {
                $provservicedata = new Request;
                $provservicedata->replace([
                    'parent_service_id' => $data['parent_service_id'],
                    'service_code' => $data['prov_service_code'],
                    'service_name' => $data['prov_service_name'],
                    'provider_type' => $data['provider_type'],
                    'mid' => $data['mid'],
                    'insurance_id' => $data['insurance_id'],
                    'created_by' => $this->id_admin
                ]);
                try {
                    $ps = $this->provservice->addService($provservicedata);
                    $aa = $ps->getData()->data;
                    $idservice = $aa->id;
                } catch (\Exception $e) {
                    $msg .= "add service failed. ";
                    throw new \Exception();
                }
            } else {
                $idservice = $data['prov_service_name'];
            }

            $ms = MappingServices::create([
                'id_prov_service' => $idservice,
                'id_iziservice_code' => $data['id_iziservice_code'],
                'created_by' => $this->id_admin,
                'insurance_id' => $data['insurance_id']
            ]);

            return ' add mapping service id ' . $ms->id . ' succeed.';
        } catch (\Exception $msg) {
            return ' add mapping service failed.';
        }
    }

    public function processMapping(Request $request)
    {
        $data = $request->json()->all();
        $valuee = $data['data'];
        $msg = "";

        if (count($data['deleted_id']) > 0) {
            $value2 = $data['deleted_id'];
            $getmap   = MappingServices::whereIn('id', $value2);
            if ($getmap) {
                $getmap->delete();
            }
        }

        try {
            foreach ($valuee as $value1) {
                if ($value1['id']) {
                    $ms = $this->updateMapping($value1, $value1['id']);
                    $msg .= $ms;
                } else {
                    $ms = $this->addMapping($value1);
                    $msg .= $ms;
                }
            }
            return response()->json("Data Updated Successfully", 200);
        } catch (\Exception $e) {
            return response()->json("Data Update Failed", 409);
        }
    }


    public function showMapping(Request $request, $id = null)
    {
        $sortby         = $request->input('sortby');
        $sortvalue      = $request->input('sortvalue');
        $insurance_id = $request->auth->insurance_id;
        $status         = false;
        $error          = "data not found";
        $getmapservice   = MappingServices::with(['id_prov_service', 'id_iziservice_code', 'created_by', 'last_modified_by','insurance_id'])
                            ->whereHas('id_prov_service',function($query) {return $query->where('parent_service_id','!=', null);});
        // ->get();
        // return $getmapservice;
        if ($request->auth->isInsurance) {
            $getmapservice   = $getmapservice->where('insurance_id', $request->auth->insurance_id)
            ->whereHas('id_prov_service', function ($query) use ($insurance_id) {
                $query->where('insurance_id', $insurance_id);
            });
        }

        // return $getmapservice->get();

        if ($id) {
            $getmapservice   = $getmapservice->where('id', $id)->first();

            if ($getmapservice) {
                $status     = true;
                $error      = null;
            }
        } else {
            if ($request->has('filter')) {
                $table          = 'mapping_services';
                $filter         = $request->input('filter');
                $getmapservice   = Helper::filterSearch($getmapservice, $table, $filter);
                foreach ($filter as $f) {
                    $filterdata         = json_decode($f);
                    $filterby           = $filterdata->by;
                    $filtervalue        = $filterdata->value;
                    if (is_array($filtervalue)) {
                        if ($filterby == 'provider_type') {
                            $getmapservice   = $getmapservice->whereHas('id_prov_service', function ($query) use ($filtervalue) {
                                $query->whereHas('provider_type', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            });
                        } elseif ($filterby == 'provider') {
                            $getmapservice   = $getmapservice->whereHas('id_prov_service', function ($query) use ($filtervalue) {
                                $query->whereHas('mid', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            });
                        } elseif ($filterby == 'iziservice') {
                            $getmapservice   = $getmapservice->whereHas('id_iziservice_code', function ($query) use ($filtervalue) {
                                $query->whereIn('iziservice_name', $filtervalue);
                            });
                        } elseif ($filterby == 'insurancename') {
                            $getmapservice   = $getmapservice->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                $query->whereIn('insuranceName', $filtervalue);
                            });
                        } 
                    } else {
                        if ($filterby == 'provider_type') {
                            $getmapservice   = $getmapservice->whereHas('id_prov_service', function ($query) use ($filtervalue) {
                                $query->whereHas('provider_type', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            });
                        } elseif ($filterby == 'provider') {
                            $getmapservice   = $getmapservice->whereHas('id_prov_service', function ($query) use ($filtervalue) {
                                $query->whereHas('mid', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            });
                        } elseif ($filterby == 'iziservice') {
                            $getmapservice   = $getmapservice->whereHas('id_iziservice_code', function ($query) use ($filtervalue) {
                                $query->where('iziservice_name', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'insurancename') {
                            $getmapservice   = $getmapservice->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                $query->where('insuranceName', 'like', '%' . $filtervalue . '%');
                            });
                        }
                    }
                }
            }

            if ($getmapservice->count()) {
                $status         = true;
                $error          = null;
            }

            $getmapservice   = Helper::sorting($getmapservice, $sortby, $sortvalue);
            $limit          = $request->has('limit') ? $request->input('limit') : 20;
            $page           = $request->has('page') ? $request->input('page') : 1;
            $getmapservice   = $getmapservice->paginate($limit, ['*'], 'page', $page);
            $meta           = [
                'page'          => (int) $getmapservice->currentPage(),
                'perPage'       => (int) $getmapservice->perPage(),
                'total'         => (int) $getmapservice->total(),
                'totalPage'     => (int) $getmapservice->lastPage()
            ];
            $getmapservice   = $getmapservice->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getmapservice) ? $getmapservice : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function updateMapping($req, $id)
    {
        $data   = MappingServices::find($id);

        try {
            $msg = "update Mapping Service Failed. ";
            if ($req['isNewService']) {
                $provservicedata = new Request;
                $provservicedata->replace([
                    'parent_service_id' => $req['parent_service_id'],
                    'service_code' => $req['prov_service_code'],
                    'service_name' => $req['prov_service_name'],
                    'provider_type' => $req['provider_type'],
                    'insurance_id' => $data['insurance_id'],
                    'mid' => $req['mid'],
                    'created_by' => $this->id_admin
                ]);
                try {
                    $ps = $this->provservice->addService($provservicedata);
                    $aa = $ps->getData()->data;
                    $idservice = $aa->id;
                } catch (\Exception $e) {
                    $msg .= "add service failed. ";
                    throw new \Exception();
                }
            } else {
                $idservice = $req['prov_service_name'];
            }

            $ms = $data->update([
                'id_prov_service' => $idservice,
                'id_iziservice_code' => $req['id_iziservice_code'],
                'last_modified_by' => $this->id_admin,
                'insurance_id' => $req['insurance_id']
            ]);

            return ' update mapping service id ' . $ms->id . ' succeed.';
        } catch (\Exception $msg) {
            return ' update mapping service failed.';
        }
    }

    public function deleteMapping(Request $request, $id)
    {
        $getmapservice   = MappingServices::find($id);
        if ($getmapservice) {
            $getmapservice->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function massDeleteMapping(Request $request)
    {
        $getmapservice   = MappingServices::whereIn('id', $request->id);
        if ($getmapservice) {
            $getmapservice->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }
}
