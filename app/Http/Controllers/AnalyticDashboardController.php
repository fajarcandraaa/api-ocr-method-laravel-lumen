<?php

namespace App\Http\Controllers;

use App\CodeMasters;
use App\ClaimRegHdr;
use App\Companies;
use App\Diagnosis;
use Illuminate\Http\File as File;
use Illuminate\Support\Facades\Storage as Storage;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Google\Rpc\Help;
use Illuminate\Http\Request;


class AnalyticDashboardController extends Controller
{
    public function getClaimRegDash(Request $request, $id = null)
    {
        $sortby                 = $request->input('sortby');
        $sortvalue              = $request->input('sortvalue');
        // return $sortvalue;
        $status                 = true;
        $error                  = null;
        $getClaimRegistered     = ClaimRegHdr::with([
            // 'memberid',
            'companyid',
            'insurance_id',
            'bizprovid',
            'plan',
            'claimactivity',
            'status'
        ]);
        // print_r($request->auth);die();
        if ($request->auth->isInsurance) {
            $getClaimRegistered   = $getClaimRegistered->where('insurance_id', $request->auth->insurance_id);
        }
        // $getClaimRegistered     = $getClaimRegistered->orderBy('id'); //grouping data by detail parent column
        if ($request->has('keyword')) {
            $keyword        = $request->keyword;
            $where          = array(
                "claimregno",
                "memberid",
                "plan",
                "bizprovid",
                "companyid",
                "ref_no"
            );
            $getClaimRegistered = Helper::dynamicSearch($getClaimRegistered, $where, $keyword);
            if ($getClaimRegistered->count() < 1) {
                $getClaimRegistered    = ClaimRegHdr::with([
                    'memberid',
                    'companyid',
                    'insurance_id',
                    'bizprovid',
                    'plan',
                    'claimactivity',
                    'status'
                ]);

                if ($request->auth->isInsurance) {
                    $getClaimRegistered   = $getClaimRegistered->where('insurance_id', $request->auth->insurance_id);
                }

                $getClaimRegistered = $getClaimRegistered->whereHas('memberid', function ($query) use ($keyword) {
                    $query->where('member_name', 'like', '%' . $keyword . '%');
                })->OrWhereHas('status', function ($query) use ($keyword) {
                    $query->where('codeDesc', 'like', '%' . $keyword . '%');
                })->OrWhereHas('bizprovid', function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%');
                })->OrWhereHas('plan', function ($query) use ($keyword) {
                    $query->where('codeDesc', 'like', '%' . $keyword . '%');
                })->OrWhereHas('memberid', function ($query) use ($keyword) {
                    $query->where('policy_no', 'like', '%' . $keyword . '%');
                })->OrWhereHas('companyid', function ($query) use ($keyword) {
                    $query->where('company_name', 'like', '%' . $keyword . '%');
                });
            }
        } else {
            if ($request->has('filter')) {
                $table          = 'claimreghdr';
                $filter         = $request->input('filter');
                $filtername         = json_decode($filter[0]);
                $filternameby       = $filtername->by;
                $filternamevalue    = $filtername->value;
                if ($filternameby == 'startendDate') {
                    $filterdate             = explode("/", $filternamevalue);
                    $where                  = 'created_at';
                    $getClaimRegistered       = $getClaimRegistered->whereBetween($where, [$filterdate[0], $filterdate[1]]);
                } else {
                    $getClaimRegistered    = Helper::filterSearch($getClaimRegistered, $table, $filter);
                }
                foreach ($filter as $f) {
                    $filterdata     = json_decode($f);
                    $filterby       = $filterdata->by;
                    $filtervalue    = $filterdata->value;

                    if (is_array($filtervalue)) {
                        if ($filterby == 'status') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('status', function ($query) use ($filtervalue) {
                                $query->whereIn('codeDesc', $filtervalue);
                            });
                        } elseif ($filterby == 'member') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('memberid', function ($query) use ($filtervalue) {
                                $query->whereIn('member_name', $filtervalue);
                            });
                        } elseif ($filterby == 'provider') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('bizprovid', function ($query) use ($filtervalue) {
                                $query->whereIn('name', $filtervalue);
                            });
                        } elseif ($filterby == 'notes') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('claimregactivity', function ($query) use ($filtervalue) {
                                $query->whereIn('remark', $filtervalue);
                            });
                        } elseif ($filterby == 'codeDesc') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('plan', function ($query) use ($filtervalue) {
                                $query->whereIn('codeDesc', $filtervalue);
                            });
                        } elseif ($filterby == 'policy') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('memberid', function ($query) use ($filtervalue) {
                                $query->whereIn('policy_no', $filtervalue);
                            });
                        } elseif ($filterby == 'company') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('companyid', function ($query) use ($filtervalue) {
                                $query->whereIn('company_name', $filtervalue);
                            });
                        }
                    } else {
                        if ($filterby == 'status') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('status', function ($query) use ($filtervalue) {
                                $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'member') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('memberid', function ($query) use ($filtervalue) {
                                $query->where('member_name', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'provider') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('bizprovid', function ($query) use ($filtervalue) {
                                $query->where('name',  'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'notes') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('claimregactivity', function ($query) use ($filtervalue) {
                                $query->where('remark', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'codeDesc') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('plan', function ($query) use ($filtervalue) {
                                $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'policy') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('memberid', function ($query) use ($filtervalue) {
                                $query->where('policy_no', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'company') {
                            $getClaimRegistered = $getClaimRegistered->whereHas('companyid', function ($query) use ($filtervalue) {
                                $query->where('company_name', 'like', '%' . $filtervalue . '%');
                            });
                        }
                    }
                }
            }
        }

        if ($getClaimRegistered->count() < 1) {
            $status     = false;
            $error      = "data not found";
        }
        $getClaimRegistered     = Helper::sorting($getClaimRegistered, $sortby, $sortvalue);
        $limit                  = $request->has('limit') ? $request->input('limit') : 20;
        $page                   = $request->has('page') ? $request->input('page') : 1;
        $getClaimRegistered     = $getClaimRegistered->paginate($limit, ['*'], 'page', $page);

        $meta                   = [
            'page'          => (int) $getClaimRegistered->currentPage(),
            'perPage'       => (int) $getClaimRegistered->perPage(),
            'total'         => (int) $getClaimRegistered->total(),
            'totalPage'     => (int) $getClaimRegistered->lastPage()
        ];

        // $getClaimRegistered     = $out; 
        $getClaimRegistered     = $getClaimRegistered->toArray()['data'];
        //


        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($getClaimRegistered) ? $getClaimRegistered : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function getChartTotalClaim(Request $request)
    {

        $status = false;
        $error = "data not found";
        $total = ClaimRegHdr::distinct();

        if ($request->has('filter')) {
            $table          = 'claimreghdr';
            $filter         = $request->input('filter');
            $filtername         = json_decode($filter[0]);
            $filternameby       = $filtername->by;
            $filternamevalue    = $filtername->value;
            if ($filternameby == 'startendDate') {
                $filterdate             = explode("/", $filternamevalue);
                $where                  = 'created_at';
                $getClaimRegistered       = $total->whereRaw(
                    "(created_at >= ? AND created_at <= ?)",
                    [$filterdate[0] . " 00:00:00", $filterdate[1] . " 23:59:59"]
                );
            }
        }
        $getClaimRegistered = $getClaimRegistered->where('insurance_id', $request->auth->insurance_id);
        $total = $getClaimRegistered->count('claimregno');
        if ($total > 0) {
            $status = true;
            $error = null;
        }
        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($total) ? $total : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function getChartTotalbyPlan(Request $request)
    {

        $status = false;
        $error = "data not found";

        // $total = ClaimRegHdr::select(\DB::raw('plan, count(claimregno) as count'))->where('insurance_id', $request->auth->insurance_id)->groupBy('plan')->get();
        if ($request->has('filter')) {
            $table          = 'claimreghdr';
            $filter         = $request->input('filter');
            $filtername         = json_decode($filter[0]);
            $filternameby       = $filtername->by;
            $filternamevalue    = $filtername->value;
            if ($filternameby == 'startendDate') {
                $filterdate             = explode("/", $filternamevalue);
                $where                  = 'created_at';
                $getClaimRegistered       = ClaimRegHdr::whereRaw(
                    "(created_at >= ? AND created_at <= ?)",
                    [$filterdate[0] . " 00:00:00", $filterdate[1] . " 23:59:59"]
                );
            }
        }
        $total = $getClaimRegistered->select(\DB::raw('plan, count( distinct claimregno) as count'))->where('insurance_id', $request->auth->insurance_id)->groupBy('plan')->get();
        if ($total->count() > 0) {
            $status = true;
            $error = null;
        }

        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($total) ? $total : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function getChartTotalbyCompany(Request $request)
    {

        $status = false;
        $error = "data not found";

        if ($request->has('filter')) {
            $table          = 'claimreghdr';
            $filter         = $request->input('filter');
            $filtername         = json_decode($filter[0]);
            $filternameby       = $filtername->by;
            $filternamevalue    = $filtername->value;
            if ($filternameby == 'startendDate') {
                $filterdate             = explode("/", $filternamevalue);
                $where                  = 'created_at';
                $getClaimRegistered       = ClaimRegHdr::whereRaw(
                    "(created_at >= ? AND created_at <= ?)",
                    [$filterdate[0] . " 00:00:00", $filterdate[1] . " 23:59:59"]
                );
            }
        }

        $total = $getClaimRegistered->select(\DB::raw('companyid, count( distinct claimregno) as count'))->where('insurance_id', $request->auth->insurance_id)->groupBy('companyid')->whereNotNull('companyid')->limit(5)->get();
        foreach ($total as $claim) {
            $c = Companies::select('company_name')->where('id', $claim->companyid)->first();
            $claim->company_name = $c->company_name;
        }
        if ($total->count() > 0) {
            $status = true;
            $error = null;
        }

        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($total) ? $total : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function getChartTotalbyStatus(Request $request)
    {

        $status = false;
        $error = "data not found";

        if ($request->has('filter')) {
            $table          = 'claimreghdr';
            $filter         = $request->input('filter');
            $filtername         = json_decode($filter[0]);
            $filternameby       = $filtername->by;
            $filternamevalue    = $filtername->value;
            if ($filternameby == 'startendDate') {
                $filterdate             = explode("/", $filternamevalue);
                $where                  = 'created_at';
                $getClaimRegistered       = ClaimRegHdr::whereRaw(
                    "(created_at >= ? AND created_at <= ?)",
                    [$filterdate[0] . " 00:00:00", $filterdate[1] . " 23:59:59"]
                );
            }
        }
        $total = $getClaimRegistered->select(\DB::raw('eob_status, count(distinct claimregno) as count'))->where('insurance_id', $request->auth->insurance_id)->groupBy('eob_status')->get();
        
        foreach ($total as $claim) {
            if($claim->eob_status){
                $c = CodeMasters::select('codeDesc')->where('id', $claim->eob_status)->first();
            $claim->eob_status_desc = $c->codeDesc;
            }else {
            $claim->eob_status_desc = null;
            }
            
        }
        if ($total->count() > 0) {
            $status = true;
            $error = null;
        }

        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($total) ? $total : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function getChartTotalbyDiagnosis(Request $request)
    {

        $status = false;
        $error = "data not found";

        if ($request->has('filter')) {
            $table          = 'claimreghdr';
            $filter         = $request->input('filter');
            $filtername         = json_decode($filter[0]);
            $filternameby       = $filtername->by;
            $filternamevalue    = $filtername->value;
            if ($filternameby == 'startendDate') {
                $filterdate             = explode("/", $filternamevalue);
                $where                  = 'created_at';
                $getClaimRegistered       = ClaimRegHdr::whereRaw(
                    "(created_at >= ? AND created_at <= ?)",
                    [$filterdate[0] . " 00:00:00", $filterdate[1] . " 23:59:59"]
                );
            }
        }
        $total = $getClaimRegistered->select(\DB::raw('diagnosis, count(distinct claimregno) as count'))->where('insurance_id', $request->auth->insurance_id)->groupBy('diagnosis')->get();
        
        foreach ($total as $claim) {
            if($claim->diagnosis){
                $c = Diagnosis::select('diagnosisName')->where('id', $claim->diagnosis)->first();
            $claim->diagnosis_name = $c->diagnosisName;
            }else {
            $claim->diagnosis_name = null;
            }
            
        }
        if ($total->count() > 0) {
            $status = true;
            $error = null;
        }

        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($total) ? $total : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }
}
