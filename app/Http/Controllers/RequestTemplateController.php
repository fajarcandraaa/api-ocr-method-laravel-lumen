<?php
namespace App\Http\Controllers;

use Auth;
use App\RequestTemplate;
use App\Exports\UserManagementExport;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AuthController;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\PdfToText\Pdf;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\File;
use Response;

class RequestTemplateController extends Controller
{
    public function addNewTemplates(Request $request)
    {
        $request->request->add(['path' => $request->input('path'), 'createBy' => $request->auth->name]);
        $this->validate($request, [
            'provider_mid'  => 'required|string',
            'branch'        => 'string',
            'plan'          => 'integer',
            'documentType'  => 'integer',
            'isPriority'    => 'integer',
            'replaced'      => 'integer',
            'status'        => 'integer',
            'createBy'      => 'string',
            'insurance_id'  => 'integer'
        ]);
        

        try {
            $reqTemplates   = new RequestTemplate;
            $params         = $request->all();
            $reqTemplates->fill($params);
            $cekTemplates   = RequestTemplate::where('path', $request->input('path'))
                                ->first();
            if ($cekTemplates) {
                return response()->json(['message' => 'Maybe your path or branch is alredy exist'], 409);
            } else {
                $reqTemplates->save();
                return response()->json(['Request Template Data' => $reqTemplates, 'message' => 'CREATED'], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Template Registration Failed'], 409);
        }
    }

    public function showAllDataTemplates(Request $request, $id = null)
    {
        $sortby             = $request->input('sortby');
        $sortvalue          = $request->input('sortvalue');
        $status             = true;
        $error              = null;
        $isprior            = "0";
        $prior              = "1";
        $default            = RequestTemplate::where('isPriority',$isprior)
                                ->orderBy('createBy', 'desc');
        // print_r($default->toSql());die();
        $getReqTemplates    = RequestTemplate::with(['plan','docType','provider_mid'])
                                ->where('isPriority',$prior)
                                ->orderBy('createBy', 'desc')
                                ->union($default);
        /*
        Query optional :
        SELECT ROW_NUMBER() OVER (ORDER BY requesttemplate.isPriority DESC, requesttemplate.id ASC) as `nomor`, 
        `requesttemplate`.* FROM `requesttemplate` WHERE `requesttemplate`.`deleted_at` IS NULL
        */

        if ($request->auth->isInsurance) {
            $getReqTemplates   = $getReqTemplates->where('insurance_id', $request->auth->insurance_id);
        }
        
        if ($id) {
            $getReqTemplates    = $getReqTemplates->where('id',$id)->first();
            // $getReqTemplates    = RequestTemplate::Where('id',$id)->first();

            if (!$getReqTemplates) {
                $status     = false;
                $error      = "data not found";
            }
        } else {
            if ($request->has('keyword')) {
                $keyword            = $request->keyword;
                $where              = array("documentNumber","branch","isPriority","status","replaced");
                $getReqTemplates    = Helper::dynamicSearch($getReqTemplates, $where, $keyword);
                if (!$getReqTemplates->count()) {
                    $getReqTemplates    = RequestTemplate::with(['plan','docType','provider_mid'])->where('isPriority',$prior)
                                        ->orderBy('createBy', 'desc')
                                        ->union($default);

                    $getReqTemplates    = $getReqTemplates->whereHas('plan', function ($query) use ($keyword) {
                        $query->where('codeDesc','like','%'.$keyword.'%');
                    })->orWHereHas('docType', function ($query) use ($keyword) {
                        $query->where('codeDesc','like','%'.$keyword.'%');
                    });
                }
                $status             = true;
                $error              = null;
                $getReqTemplates    = Helper::sorting($getReqTemplates, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table              = "requesttemplate";
                    $filter             = $request->input('filter');
                    $getReqTemplates    = Helper::filterSearch($getReqTemplates,$table,$filter);
                    foreach ($filter as $f) {
                        $filterdata         = json_decode($f);
                        $filterby           = $filterdata->by;
                        $filtervalue        = $filterdata->value;

                        if ($filterby == 'plan') {
                            $getReqTemplates   = $getReqTemplates->WhereHas('roles', function ($query) use ($filtervalue) {
                                $query->where('rolename', 'like','%'.$filtervalue.'%');
                                });
                        } elseif ($filterby == 'docType') {
                            $getReqTemplates   = $getReqTemplates->WhereHas('docType', function ($query) use ($filtervalue) {
                                $query->where('rolename', 'like','%'.$filtervalue.'%');
                                });
                        }
                    }
                }
            }
            // $getReqTemplates    = Helper::sorting($getReqTemplates, $sortby, $sortvalue);
            $limit              = $request->has('limit') ? $request->input('limit') : 20;
            $page               = $request->has('page') ? $request->input('page') : 1;
            $getReqTemplates    = $getReqTemplates->paginate($limit, ['*'], 'page', $page);
            $number             = ($getReqTemplates->currentPage() - 1) * $getReqTemplates->perPage() + 1;
            foreach ($getReqTemplates as $g) {
                $g->nomorurut = $number++;
            }
            $meta               = [
                'page'      => (int) $getReqTemplates->currentPage(),
                'perPage'   => (int) $getReqTemplates->perPage(),
                'total'     => (int) $getReqTemplates->total(),
                'totalPage' => (int) $getReqTemplates->lastPage()
            ];
            $getReqTemplates    = $getReqTemplates->toArray()['data'];
            
        }
        

        if (!$getReqTemplates) {
            $status         = false;
            $error          = "data not found";
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getReqTemplates) ? $getReqTemplates : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function updateTemplates(Request $request, $id)
    {
        $request->request->add(['lastUpdateBy' => $request->auth->name]);
        $this->validate($request, [
            'branch'        => 'string',
            'plan'          => 'integer',
            'documentType'  => 'integer',
            'isPriority'    => 'integer',
            'path'          => 'string',
            'replaced'      => 'integer',
            'status'        => 'integer',
            'createBy'      => 'string',
            'insurance_id'  => 'integer'
        ]);

        $data   = RequestTemplate::find($id);
        if ($data != null) {
            $cekTemplates   = RequestTemplate::where(function ($query) use ($request) {
                            $query->where('path', $request->input('path'))
                            ->orWhere('branch', $request->input('branch'));
                            })->where('id','!=',$id)->first();
            if ($cekTemplates) {
                return response()->json(['message' => 'Maybe your path or branch is alredy exist'], 409);
            } else {
                $params     = $request->all();
                $data->fill($params);
                $data->save();
                return response()->json(['status' => (bool) true, 'message' => 'Your data has been update'], 200);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
        }
    }

    public function massDeleteTemplates(Request $request)
    {
        $reqTemplates   = RequestTemplate::whereIn('id',$request->id);
        if ($reqTemplates) {
            $reqTemplates->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function countingData()
    {
        $table          = new RequestTemplate;
        $unregisterd    = $table->select('documentStatus')->where('documentStatus','0')->get();
        $registerd      = $table->select('documentStatus')->where('documentStatus','2')->get();
        $replaced       = $table->select('documentStatus')->where('documentStatus','3')->get();
        $err            = $table->select('documentStatus')->where('documentStatus','4')->get();

        $data               = [
            'Document_unregistered'     => $unregisterd->count(),
            'Document_registered'       => $registerd->count(),
            'Document_replaced'         => $replaced->count(),
            'Document_error'            => $replaced->count()
        ];
        $response = [
            'status'    => true,
            'data'      => $data
            
        ];
        return response()->json($response);
    }

    public function uploadpdftemplate(Request $request)
    {
        $status     = false;
        $error      = "upload pdf failed";
        $rules      = [
            "file" => "required|mimes:pdf"
        ];
        $pdf        = $request->file('pdf');
        $pdfname    = time(). "." . $pdf->getClientOriginalExtension();
        Storage::disk('minio')->putFileAs('Medlinx/registerTemplate',new File($pdf),$pdfname);
        // $filename = Helper::uploadPdf($pdf, storage_path('app/pdf'));
        $filename   = Helper::uploadPdf($pdf, "assets/pdf");
        // $filename = Storage::disk('minio')->url('Medlinx/registerTemplate/'.$pdfname);
        // $filename = response(Storage::disk('minio')->get('Medlinx/registerTemplate/'.$pdfname),200,$headers);
        // Storage::disk('minio')->files('/Medlinx/registerTemplate')->store($pdf);
        if ($filename) {
            $status     = true;
            $error      = "";
        }
        $convertImage   = [];
        if ($status) {
            $convertImage   = $this->pdftoimageTemplates($filename);
        }
        
        $response   = [
            "status"    => (bool) $status,
            "data"      => $convertImage,
            "error"     => (isset($error) ? $error : null)
        ];
        
        return response()->json($response);
    }

    public function pdftoimageTemplates($pdfFile)
    {
        // print_r($pdfFile);die();
        $pdf        = new \Spatie\PdfToImage\Pdf($pdfFile);
        $filename   = basename($pdfFile);
        $total      =  $pdf->getNumberOfPages();
        $image      = [];

        foreach (range(1, $total) as $pageNumber) {
            $originalimagePath   = storage_path('app/convert/').$filename.'-page'.$pageNumber.'.png';
            $img['rel_path']    = '/storage/convert/'.$filename.'-page'.$pageNumber.'.png';
            $img['pageNumber']  = $pageNumber;
            $pdf->setPage($pageNumber)
            ->setOutputFormat('png')
            ->saveImage($originalimagePath);
            
            //-------------------------------upload original image to minIO---------------------
            Storage::disk('minio')->putFileAs('Medlinx/registerTemplate/originalImage',new File($originalimagePath),$filename.'-page'.$pageNumber.'.png');
            
            // ------------------greyscale image converter------------------
            $imagename  = $filename.'-page'.$pageNumber.'.png';
            $dest_path  = storage_path('app/grey');
            $grayimage  = $dest_path.'/gray_'.$imagename;
            $gambar     = Image::make($originalimagePath);
            $gambar->greyscale()->save($grayimage);

            //-------------------------------upload grayscale image to minIO---------------------
            Storage::disk('minio')->putFileAs('Medlinx/registerTemplate/grayscaleImage',new File($grayimage),'gray_'.$imagename);
            // $img['gry_path']    = storage_path('app/grey/').'gray_'.$filename.'-page'.$pageNumber.'.png';
            // -------------------------------------------------------------

            $img['ocr']     = $this->tesseractProcess($grayimage);
            $image[]        = $img;
        }
        
        // ------------------convert oct to .txt file---------------------
        $fixname     = explode(".",basename($filename));
        $fixname2    = $fixname[0].'-'.date("Ymd").date("His");
        Storage::disk('minio')->put('Medlinx/registerTemplate/tesseractText/'.$fixname2.'.txt', json_encode($image[0]['ocr'], JSON_PRETTY_PRINT));
        // $fileurl  = Storage::url('Medlinx/registerTemplate/tesseractText/'.$fixname2.'.txt');
        // ---------------------------------------------------------------
        
        //--------------delete image file from local directory----------------- 
        unlink($originalimagePath);
        unlink($grayimage);
        unlink($pdfFile);
        //---------------------------------------------------------------------

        $data = [
            'total'     =>  $pdf->getNumberOfPages(),
            'image'     => $image,
            // 'text_path' => substr($fileurl,9)
        ];

        return $data;
    }

    public function tesseractProcess($pathFile)
    {
        
        date_default_timezone_set("Asia/Bangkok");
        $array      = array();
        $ocrtext    = (new TesseractOCR($pathFile))->lang('ind','eng','msa')->run();
        $filename   = explode(".",basename($pathFile));
        $a          = explode("\n",$ocrtext);
        $i          = 0;
        foreach ($a as $b) {
            if ($b != "" && $b != " " && $b != "  " && $b != "   " && $b != "    " && $b != "     "  && $b != "      ") {
                $array["line" . $i] = $b;
                $i++;
            }
        }
        
        return $array;
    }

    public function statusUpdate(Request $request, $docNo)
    {
        $data       = RequestTemplate::find($docNo);
        $docStat    = $request->input('docStatus');

        try {
            if ($docStat == 'Unregistered') {
                $data->documentStatus = 0;
            } elseif ($docStat == 'On Progress') {
                $data->documentStatus = 1;
            } elseif ($docStat == 'Registered') {
                $data->documentStatus = 2;
            } elseif ($docStat == 'Replaced') {
                $data->documentStatus = 3;
            }

            $data->save();
            return response()->json(['status' => (bool) true, 'message' => 'Your template has been update'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Update Template Failed!'], 409);
        }
    }
}