<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\BankInfos;
use App\Finances;
use App\Companies;
use Illuminate\Support\Facades\DB as DB;
use App\Cities;
use App\Provinces;
use App\Districts;
use App\CodeMasters;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CompaniesExport;
use App\Members;
use Barryvdh\DomPDF\Facade as PDF;


class CompaniesController extends Controller
{

    public static function checkDuplicate(Request $request)
    {
        $result = Helper::checkDuplicate('Companies', $request->type, $request->vals);
        return response()->json($result);
    }

    public function addNewCompanies(Request $request)
    {
        // try {
        // $cekcomp = Companies::where([['company_name', $request->company_name], ['branch', $request->branch]])
        //     ->first();
        // if ($cekcomp) {
        //     return response()->json(['message' => "Company already exist!"], 409);
        // } else {
        do {
            //code1 : codeval1 dari code master
            $rand = str_pad(mt_rand(1, 999999999), 9, '0', STR_PAD_LEFT);
            $city_code = str_pad($request->city_code, 4, '0', STR_PAD_LEFT);
            $company_code = $request->comp_code1 . $city_code . $request->postal_code . $rand;
            $cekcomp    = Companies::where('company_code', '=', $company_code)->first();
        } while ($cekcomp);
        if ($request->policy_holder == "self") {
            $policy_holder = $company_code . " - " . $request->company_name;
        } else {
            $policy_holder = Companies::where('id', '=', $request->parent_comp_id)->first();
            $policy_holder = $policy_holder->policy_holder;
        }
        $comp = Companies::create([
            'company_name' => $request->company_name,
            'have_parent_comp' => $request->have_parent_comp,
            'company_type' => $request->company_type,
            'branch' => $request->branch,
            'tax_no' => $request->tax_no,
            'phoneno' => $request->phoneno,
            'email' => $request->email,
            'faxno' => $request->faxno,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'address3' => $request->address3,
            'address4' => $request->address4,
            'status' => $request->status,
            'country' => $request->country,
            'prov' => $request->prov,
            'city' => $request->city,
            'district' => $request->district,
            'postal_code' => $request->postal_code,
            'remark' => $request->remark,
            'policy_number' => $request->policy_number,
            'policy_holder' => $policy_holder,
            'payor' => $request->payor,
            'effectivedate' => $request->effectivedate,
            'expirydate' => $request->expirydate,
            'renewaldate' => $request->renewaldate,
            'created_by' => $request->id_user,
            'parent_comp_id' => ($request->parent_comp_id == null ? null : $request->parent_comp_id),
            'company_code' => $company_code,
            'insurance_id' => $request->auth->isInsurance ? $request->auth->insurance_id : $request->input('insurance_id')
        ]);


        if ($comp) {
            try {
                if (isset($request->bank_code)) {
                    Helper::addBankInfo($request, "companies", $comp->id);
                }

                if (isset($request->contact_person)) {
                    Helper::addCP($request->contact_person, "companies", $comp->id);
                }
            } catch (\Exception $e) {
                $cek = Companies::find($comp->id);
                if ($cek) {
                    $cek->delete();
                }
                $cek = BankInfos::where([['ref_type', 'companies'], ['ref_id', $comp->id]]);
                if ($cek) {
                    $cek->delete();
                }
                $cek = Finances::where([['ref_type', 'companies'], ['ref_id', $comp->id]]);
                if ($cek) {
                    $cek->delete();
                }
                return response()->json(['message' => 'Company Registration Failed!'], 409);
            }
        }

        return response()->json(['message' => 'DATA CREATED SUCCESSFULLY'], 200);
        // }
        // } catch (\Exception $e) {
        //     return response()->json(['message' => 'Company Registration Failed!'], 409);
        // }
    }

    public function updateCompanies(Request $request, $id)
    {
        $data   = Companies::find($id);

        // try {
        if ($request->policy_holder == "self") {
            $policy_holder = $data->company_code . " - " . $request->company_name;
        } else {
            $policy_holder = Companies::where('id', '=', $request->parent_comp_id)->first();
            $policy_holder = $policy_holder->policy_holder;
        }

        $data->update([
            'company_name' => $request->company_name,
            'have_parent_comp' => $request->have_parent_comp,
            'company_type' => $request->company_type,
            'branch' => $request->branch,
            'tax_no' => $request->tax_no,
            'phoneno' => $request->phoneno,
            'email' => $request->email,
            'faxno' => $request->faxno,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'address3' => $request->address3,
            'address4' => $request->address4,
            'status' => $request->status,
            'country' => $request->country,
            'prov' => $request->prov,
            'city' => $request->city,
            'district' => $request->district,
            'postal_code' => $request->postal_code,
            'remark' => $request->remark,
            'policy_number' => $request->policy_number,
            'policy_holder' => $policy_holder,
            'payor' => $request->payor,
            'effectivedate' => $request->effectivedate,
            'expirydate' => $request->expirydate,
            'renewaldate' => $request->renewaldate,
            'last_modified_by' => $request->id_user,
            'parent_comp_id' => ($request->parent_comp_id == null ? null : $request->parent_comp_id),
            'insurance_id' => $request->auth->isInsurance ? $request->auth->insurance_id : $request->input('insurance_id')
        ]);


        if ($data) {

            if (isset($request->bank_code)) {
                BankInfos::where([['ref_id', $id], ['ref_type', "companies"]])->delete();
                Helper::addBankInfo($request, "companies", $data->id);
            }

            if (isset($request->contact_person)) {
                Finances::where([['ref_id', $id], ['ref_type', "companies"]])->delete();
                Helper::addCP($request->contact_person, "companies", $data->id);
            }

            $cek_member = Members::where('company_id', (string) $data->id);
            if ($cek_member) {
                $cek_member->update(['policy_holder' => (string) $policy_holder]);
            }
        }
        return response()->json(['status' => (bool) true, 'message' => 'Data successfully updated'], 200);
        // } catch (\Exception $e) {
        //     return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
        // }
    }

    public function deleteCompanies($id)
    {
        $comp   = Companies::find($id);
        $compbank = BankInfos::where([['ref_id', $id], ['ref_type', "companies"]]);
        $compcp = Finances::where([['ref_id', $id], ['ref_type', "companies"]]);

        if ($comp) {
            try {
                $comp->delete();
                $compbank->delete();
                $compcp->delete();
                return response()->json(['status' => (bool) true], 200);
            } catch (\Exception $e) {
                return response()->json(['status' => (bool) false, 'message' => 'delete data failed'], 409);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'data not found'], 409);
        }
    }

    public function mass_delete(Request $request)
    {
        $id = $request->id;
        $comp = Companies::whereIn('id', $id);
        $compbank = BankInfos::where('ref_type', "companies");
        $compcp = Finances::where('ref_type', "companies");
        $compbank = $compbank->whereIn('ref_id', $id);
        $compcp = $compcp->whereIn('ref_id', $id);

        if ($comp) {
            try {
                $comp->delete();
                $compbank->delete();
                $compcp->delete();
                return response()->json(['status' => (bool) true], 200);
            } catch (\Exception $e) {
                return response()->json(['status' => (bool) false, 'message' => 'delete data failed'], 409);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'data not found'], 409);
        }
    }

    public function showCompanies(Request $request, $id = null)
    {
        $sortby     = $request->input('sortby');
        $sortvalue  = $request->input('sortvalue');
        $status     = true;
        $error      = null;
        $getcomp    = Companies::with([
            'company_type',
            'country',
            'prov',
            'city',
            'district',
            'postal_code',
            'bank_infos' => function ($query) {
                $query->where('ref_type', '=', 'companies');
            },
            'contact_person' => function ($query) {
                $query->where('ref_type', '=', 'companies');
            },
            'status',
            'parent_comp_id',
            'created_by',
            'last_modified_by',
            'insurance_id'
        ]);

        if ($request->auth->isInsurance) {
            $getcomp   = $getcomp->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id) {
            $getcomp = $getcomp->where('id', $id)->first();
            if (!$getcomp) {
                $status     = false;
                $error      = "data not found";
            }
        } else {
            if ($request->has('keyword')) {
                $keyword            = $request->keyword;
                $wheres             = array(
                    "company_code",
                    "company_name",
                    "branch",
                    // "status",
                    "policy_number",
                    "policy_holder",
                    "payor",
                    "phoneno",
                    "tax_no",
                    // "created_by"
                );
                $getcomp   = Helper::dynamicSearch($getcomp, $wheres, $keyword);
                // $getcomp    = Companies::with([
                //     'company_type',
                //     'country',
                //     'prov',
                //     'city',
                //     'district',
                //     'postal_code',
                //     'bank_infos' => function ($query) {
                //         $query->where('ref_type', '=', 'companies');
                //     },
                //     'contact_person' => function ($query) {
                //         $query->where('ref_type', '=', 'companies');
                //     },
                //     'status',
                //     'parent_comp_id',
                //     'created_by',
                //     'last_modified_by',
                //     'insurance_id'
                // ]);
                $getcomp = $getcomp->orWhereHas('status', function ($query) use ($keyword) {
                    $query->where('codeDesc', 'like', '%' . $keyword . '%');
                })->OrWhereHas('created_by', function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%');
                });

                // if (!$getcomp->count()) {
                //     $getcomp    = Companies::with([
                //         'company_type',
                //         'country',
                //         'prov',
                //         'city',
                //         'district',
                //         'postal_code',
                //         'bank_infos' => function ($query) {
                //             $query->where('ref_type', '=', 'companies');
                //         },
                //         'contact_person' => function ($query) {
                //             $query->where('ref_type', '=', 'companies');
                //         },
                //         'status',
                //         'parent_comp_id',
                //         'created_by',
                //         'last_modified_by',
                //         'insurance_id'
                //     ]);
                //     $getcomp = $getcomp->whereHas('status', function ($query) use ($keyword) {
                //         $query->where('codeDesc', 'like', '%' . $keyword . '%');
                //     })->OrWhereHas('created_by', function ($query) use ($keyword) {
                //         $query->where('name', 'like', '%' . $keyword . '%');
                //     })->OrWhereHas('insurance_id', function ($query) use ($keyword) {
                //         $query->where('insuranceName', 'like', '%' . $keyword . '%');
                //     });
                // }
                $status             = true;
                $error              = null;
                $getcomp = Helper::sorting($getcomp, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table              = 'companies';
                    $filter             = $request->input('filter');
                    $filtername         = json_decode($filter[0]);
                    $filternameby       = $filtername->by;
                    $filternamevalue    = $filtername->value;
                    if ($filternameby == 'startendDate') {
                        $filterdate     = explode("/", $filternamevalue);
                        $where          = array('effectivedate', 'expirydate');
                        $getcomp        = $getcomp->whereBetween($where[0], [date($filterdate[0]), date($filterdate[1])])
                            ->orWhereBetween($where[1], [date($filterdate[0]), date($filterdate[1])]);
                    } else {
                        $getcomp    = Helper::filterSearch($getcomp, $table, $filter);
                    }

                    foreach ($filter as $f) {
                        $filterdata         = json_decode($f);
                        $filterby           = $filterdata->by;
                        $filtervalue        = $filterdata->value;

                        if (is_array($filtervalue)) {
                            if ($filterby == 'name') {
                                $getcomp   = $getcomp->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'statusDesc') {
                                $getcomp   = $getcomp->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                                // print_r($getcomp->toSql());die();
                            }
                        } else {
                            if ($filterby == 'name') {
                                $getcomp   = $getcomp->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'statusDesc') {
                                $getcomp   = $getcomp->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            }
                        }
                    }
                }
            }

            $getcomp = Helper::sorting($getcomp, $sortby, $sortvalue);
            $limit     = $request->has('limit') ? $request->input('limit') : 20;
            $page      = $request->has('page') ? $request->input('page') : 1;
            $getcomp   = $getcomp->paginate($limit, ['*'], 'page', $page);
            $meta      = [
                'page'      => (int) $getcomp->currentPage(),
                'perPage'   => (int) $getcomp->perPage(),
                'total'     => (int) $getcomp->total(),
                'totalPage' => (int) $getcomp->lastPage()
            ];
            $getcomp  = $getcomp->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getcomp) ? $getcomp : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function excelCompanies(Request $request)
    {
        $id = $request->id;
        $comp = Companies::with(
            [
                'company_type',
                'country',
                'prov',
                'city',
                'district',
                'postal_code',
                'bank_infos' => function ($query) {
                    $query->where('ref_type', '=', 'companies');
                },
                'contact_person' => function ($query) {
                    $query->where('ref_type', '=', 'companies');
                },
                'status',
                'parent_comp_id',
                'created_by',
                'last_modified_by'
            ]
        );

        if ($id or $id !== null) {
            $comp->whereIn('id', $id);
        }

        $comp = $comp->get();

        $comp = new CompaniesExport($comp->toArray());
        $date = date("dmy");
        return Excel::download($comp, 'Companies-' . $date . '.xlsx');
    }

    public function pdfCompanies(Request $request)
    {

        $comp = Companies::with(
            [
                'company_type',
                'country',
                'prov',
                'city',
                'district',
                'postal_code',
                'bank_infos' => function ($query) {
                    $query->where('ref_type', '=', 'companies');
                },
                'contact_person' => function ($query) {
                    $query->where('ref_type', '=', 'companies');
                },
                'status',
                'parent_comp_id',
                'created_by',
                'last_modified_by'
            ]
        );

        if ($request->id) {
            $id = $request->id;
            $comp->whereIn('id', $id);
        }

        $comp = $comp->get();

        $pdf = PDF::loadview('companies', ['comp' => $comp->toArray()])->setPaper('a4', 'landscape');
        $date = date("dmy");

        return $pdf->download('companies-' . $date . '.pdf');
    }

    public function import(Request $request)
    {
        $data = $request->json()->all();
        $result = [];
        $i = 0;
        $k = 0;
        $comp_id_awal = "baru";
        foreach ($data as $value1) {
            $msg = "";

            //company type
            try {
                $comp_code1_id = Helper::getCodeMasterId("CPT", $value1['company_type']);
            } catch (\Exception $e) {
                $msg .= " company type not found.";
            }

            //parent comp
            if ($value1['parent_name'] !== "") {
                try {
                    $parent_comp_id = Helper::getKey("id", "Companies", "company_name", $value1['parent_name']);
                } catch (\Exception $e) {
                    $msg .= " parent company not found.";
                }
            }

            //status
            if (strtolower($value1['status']) !== "active" && strtolower($value1['status']) !== "in-active") {
                $msg .= " company status should be 'Active' or 'In-Active'.";
            }

            //country
            try {
                $country_code = Helper::getKey("country_code", "Countries", "country_desc", $value1['country']);
            } catch (\Exception $e) {
                $msg .= " Country not found.";
            }

            // echo $country_code . " ";
            //province
            if (isset($country_code)) {
                if ($value1['prov'] !== "") {
                    try {
                        $prov_id = Provinces::where([["prov_desc", $value1['prov']], ['country_code', $country_code]])->first();
                        $prov_id = $prov_id->id;
                    } catch (\Exception $e) {
                        $msg .= " Province not found.";
                    }
                }
            }

            // echo $prov_id . " ";

            //city
            if (isset($prov_id)) {
                try {
                    $cek = explode(" ", strtolower($value1['city']));
                    if ($cek[0] == "kabupaten") {
                        $city_id = Cities::where('city_desc',substr($value1['city'],10))->first();
                        $city_id=$city_id->id;
                    }elseif ($cek[0] == "kota") {
                        $city_id = Cities::where('city_desc',substr($value1['city'],5))->first();
                        $city_id=$city_id->id;
                    } else {
                        $city = DB::table('cities')
                            ->where('city_desc', $value1['city'])
                            ->get();
                        if ($city->count() == 0) {
                            throw new \Exception();
                        } elseif ($city->count() == 1) {
                            $city_id = $city[0]->id;
                        } else {
                            $city_id = [];
                            foreach ($city as $c) {
                                array_push($city_id, $c->id);
                            }
                            $district = Districts::where('district_desc', $value1['district'])
                                ->whereIn('city_id', $city_id)
                                ->get();
                            if ($district->count()) {
                                $city_id = $district[0]->city_id;
                                $district_id = $district[0]->id;
                            } else {
                                throw new \Exception();
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $msg .= " City not found.";
                }
            }

            // echo $city_id . " ";

            //district
            if (isset($city_id) && !isset($district_id)) {
                try {
                    $district_id = Helper::getKey("id", "Districts", "district_desc", $value1['district']);
                } catch (\Exception $e) {
                    $msg .= " District not found.";
                }
            }

            // echo $district_id . " ";
            // die;
            //postal

            if (isset($district_id)) {
                try {
                    Helper::getKey("id", "Subdistricts", "postal_code", $value1['postal_code']);
                } catch (\Exception $e) {
                    $msg .= " Postal code not found.";
                }
            }


            //bank name
            try {
                Helper::getCodeMasterId("BNK", $value1['bank_name']);;
            } catch (\Exception $e) {
                $msg .= " bank name not found.";
            }

            try {

                if ($msg !== "") {
                    throw new \Exception($msg);
                }

                if (!$value1['company_code'] or $value1['company_code'] == "") {
                    do {
                        $rand = str_pad(mt_rand(1, 999999999), 9, '0', STR_PAD_LEFT);
                        // $city_id = Helper::getKey("id", "Cities", "city_desc", $value1['city']);
                        $city_code = Cities::find($city_id)->city_code;
                        $city_code = str_pad($city_code, 4, '0', STR_PAD_LEFT);
                        $comp_code1_id = Helper::getCodeMasterId("CPT", $value1['company_type']);
                        $comp_code1 = CodeMasters::find($comp_code1_id);
                        $value1['company_code'] = $comp_code1->codeVal1 . $city_code . $value1['postal_code'] . $rand;
                        $cekcomp    = Companies::where('company_code', '=', $value1['company_code'])->first();
                    } while ($cekcomp);
                } else {
                    try {
                        $comp_id_awal = Helper::getKey("id", "Companies", "company_code", $value1['company_code']);
                        if ($comp_id_awal) {
                            $cek_member = Members::where('company_id', (string) $comp_id_awal);
                        }
                    } catch (\Exception $e) {
                        $comp_id_awal = "baru";
                    }
                }

                if ($value1['parent_name'] !== "") {
                    $parent_comp_id = Helper::getKey("id", "Companies", "company_name", $value1['parent_name']);
                    $policy_holder = Companies::where('id', '=', $parent_comp_id)->first();
                    $policy_holder = $policy_holder->policy_holder;
                } else {
                    $policy_holder = $value1['company_code'] . " - " . $value1['company_name'];
                }

                $comp = Companies::updateOrCreate(
                    [
                        'company_code' => $value1['company_code']
                    ],
                    [
                        'company_name' => $value1['company_name'],
                        'have_parent_comp' => $value1['have_parent_comp'],
                        'branch' => $value1['branch'],
                        'tax_no' => $value1['tax_no'],
                        'phoneno' => $value1['phoneno'],
                        'email' => $value1['email'],
                        'faxno' => $value1['faxno'],
                        'address1' => $value1['address1'],
                        'status' => (Helper::getCodeMasterId("BES", $value1['status']) ? Helper::getCodeMasterId("BES", $value1['status']) : null),
                        'country' => $country_code,
                        'prov' => $prov_id,
                        'city' => $city_id,
                        'district' => $district_id,
                        'postal_code' => Helper::getKey("id", "Subdistricts", "postal_code", $value1['postal_code']),
                        'remark' => $value1['remark'],
                        'policy_number' => $value1['policy_number'],
                        'policy_holder' => $policy_holder,
                        'payor' => $value1['payor'],
                        'effectivedate' => date('Y-m-d H:i:s', strtotime($value1['effectivedate'])),
                        'expirydate' => date('Y-m-d H:i:s', strtotime($value1['expirydate'])),
                        'renewaldate' => ($value1['renewaldate'] ? date('Y-m-d H:i:s', strtotime($value1['renewaldate'])) : null),
                        'created_by' => $request->auth->id,
                        'last_modified_by' => $request->auth->id,
                        'parent_comp_id' => ($value1['parent_name'] == !"" ? $parent_comp_id : null),
                        'company_code' => $value1['company_code'],
                        'company_type' => Helper::getCodeMasterId("CPT", $value1['company_type'])
                    ]
                );

                if ($comp) {
                    $comp_id = $comp->id;
                    $cek = BankInfos::where([['ref_type', 'companies'], ['ref_id', $comp_id]])->first();
                    if ($cek) {
                        $deleted_bank = $cek['id'];
                        // return $deleted_bank;
                        $cek->delete();
                    }
                    $cek = Finances::where([['ref_type', 'companies'], ['ref_id', $comp_id]]);
                    if ($cek) {
                        $deleted_cp = $cek->get('id');
                        $deleted_cps = [];
                        foreach ($deleted_cp as $del) {
                            array_push($deleted_cps, $del->id);
                        }
                        // return $deleted_cps;
                        $cek->delete();
                    }

                    try {
                        $value1['bank_code'] = Helper::getCodeMasterId("BNK", $value1['bank_name']);
                        $bank = Helper::addBankInfo($value1, "companies", $comp_id);
                        $cp = Helper::addCP($value1['contact_person'], "companies", $comp_id);
                        if ($comp_id_awal !== "baru") {
                            $cek_member = Members::where('company_id', (string) $comp_id_awal);
                            if ($cek_member) {
                                $cek_member->update(['policy_holder' => (string) $policy_holder]);
                            }
                        }
                    } catch (\Exception $e) {
                        if ($comp_id_awal == "baru") {
                            $cek = Companies::find($comp_id);
                            if ($cek) {
                                $cek->delete();
                            }
                            $cek = BankInfos::where([['ref_type', 'companies'], ['ref_id', $comp_id]]);
                            if ($cek) {
                                $cek->delete();
                            }
                            $cek = Finances::where([['ref_type', 'companies'], ['ref_id', $comp_id]]);
                            if ($cek) {
                                $cek->delete();
                            }
                        } else {
                            $cek = BankInfos::where([['ref_type', 'companies'], ['ref_id', $comp_id]]);
                            if ($cek) {
                                $cek->delete();
                            }
                            $cek = Finances::where([['ref_type', 'companies'], ['ref_id', $comp_id]]);
                            if ($cek) {
                                $cek->delete();
                            }
                            BankInfos::withTrashed()->find($deleted_bank)->restore();
                            foreach ($deleted_cps as $del) {
                                Finances::withTrashed()->find($del)->restore();
                            }
                        }
                        throw new \Exception("bank or contact person data is invalid");
                    }

                    $result['valid'][$i] = $value1;
                    $result['valid'][$i]['is_valid'] = $value1['is_valid'];
                    $result['valid'][$i]['msg'] = $value1['msg'];

                    $i++;
                }
            } catch (\Exception $msg) {

                $result['invalid'][$k] = $value1;
                $result['invalid'][$k]['is_valid'] = false;
                $result['invalid'][$k]['msg'] = $msg->getMessage();
                $k++;
            }
        }
        return response()->json($result, 200);
    }
}
