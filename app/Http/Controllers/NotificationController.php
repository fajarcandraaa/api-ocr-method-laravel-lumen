<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use \LaravelFCM\Facades\FCM;
use App\Notifications;
use App\Helpers\Helper;

class NotificationController extends Controller
{
    public function PushNotif($title, $message, $message2, $tokenuser)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($message);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'title' => $title,
            'body' => $message,
            'body2' => $message2
        ]);
        // $notificationBuilder = new PayloadNotificationBuilder('my title');
        // $notificationBuilder->setBody('Hello world');

        // $dataBuilder = new PayloadDataBuilder();
        // $dataBuilder->addData([
        //     'title' => 'OCR Process',
        //     'body' => 'auuuwooooo'
        // ]);

        $option = $optionBuilder->build();
        // $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $tokenuser; //"a_registration_from_your_database";
        // print_r('aaaaassss');die();
        $downstreamResponse = FCM::sendTo($token, $option, null, $data);

        // $downstreamResponse->numberSuccess();
        // $downstreamResponse->numberFailure();
        // $downstreamResponse->numberModification();

        // // return Array - you must remove all this tokens in your database
        // $downstreamResponse->tokensToDelete();

        // // return Array (key : oldToken, value : new token - you must change the token in your database)
        // $downstreamResponse->tokensToModify();

        // // return Array - you should try to resend the message to the tokens in the array
        // $downstreamResponse->tokensToRetry();

        // // return Array (key:token, value:error) - in production you should remove from your database the tokens
        // $downstreamResponse->tokensWithError();
    }

    public function newNotifications(Request $request)
    {
        $this->validate($request, [
            'userId'    => 'required|integer',
            'title'     => 'string',
            'message'   => 'string',
            'type'      => 'string',
            'isRead'    => 'string'
        ]);

        try {
            $notification   = new Notifications;
            $params         = $request->all();
            $notification->fill($params);
            $notification->save();

            return true;
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to create notification'], 409);
        }
    }

    public function getNotifications(Request $request, $id = null)
    {
        if (!$request->has('sortby')) {
            $sortby         = 'id';
            $sortvalue      = 'desc';
        } else {
            $sortby         = $request->input('sortby');
            $sortvalue      = $request->input('sortvalue');
        }

        $status         = false;
        $error          = "you don't have notifications";
        $getnotificactions   = Notifications::with('requester')->where('userId', $id);

        if ($getnotificactions->count() > 0) {
            // return $getnotificactions->get();
            // echo "here";
            // die;
            $status     = true;
            $error      = "You have notifications";
        }

        // if ($request->has('keyword')) {
        //     $keyword            = $request->keyword;
        //     $where              = array("title", "message", "type");
        //     $getnotificactions  = Helper::dynamicSearch($getnotificactions, $where, $keyword);

        //     $status             = true;
        //     $error              = null;
        // } else {
        //     if ($request->has('filter')) {
        //         $table                  = 'notifications';
        //         $filter                 = $request->input('filter');
        //         $getnotificactions      = Helper::filterSearch($getnotificactions, $table, $filter);
        //     }
        // }

        if ($request->has('limit')) {
            $getnotificactions  = Helper::sorting($getnotificactions, $sortby, $sortvalue);
            $limit              = $request->has('limit') ? $request->input('limit') : null;
            $page               = $request->has('page') ? $request->input('page') : 1;
            $getnotificactions  = $getnotificactions->paginate($limit, ['*'], 'page', $page);
            $meta               = [
                'page'      => (int) $getnotificactions->currentPage(),
                'perPage'   => (int) $getnotificactions->perPage(),
                'total'     => (int) $getnotificactions->total(),
                'totalPage' => (int) $getnotificactions->lastPage()
            ];
            $getnotificactions  = $getnotificactions->toArray()['data'];
        } else {
            $getnotificactions  = Helper::sorting($getnotificactions, $sortby, $sortvalue);

            $getnotificactions = $getnotificactions->get();
        }


        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getnotificactions) ? $getnotificactions : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function limitNotifications($id = null)
    {
        $notification       = new Notifications;
        $getnotificactions  = $notification->where('userId', $id)
            ->where('isRead', '0')
            ->orderByDesc('created_at')
            ->limit(5);

        return response()->json($getnotificactions);
    }

    public function isReadNotification(Request $request)
    {
        $notif     = Notifications::whereIn('id', $request->id)->update(array('isRead' => $request->input('isRead')));
        return "update read status succeed";
    }
}
