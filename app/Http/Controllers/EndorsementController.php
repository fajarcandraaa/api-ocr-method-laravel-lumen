<?php

namespace App\Http\Controllers;

use App\CodeMasters;
use App\EndorsementBenefits;
use App\EndorsementHdrs;
use App\EndorsementDtls;
use Illuminate\Http\File as File;
use Illuminate\Support\Facades\Storage as Storage;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobs\ClaimRegJob;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class EndorsementController extends Controller
{
    public function uploadpdftemplate(Request $request)
    {
        $status     = false;
        $error      = "upload pdf failed";
        $rules      = [
            "img" => "required|mimes:pdf"
        ];
        $pdf        = $request->file('img');
        $basename   = $pdf->getClientOriginalName();
        $pdfname    = time() . "." . $pdf->getClientOriginalExtension();
        $filename   = Helper::uploadPdf($pdf, "temp/endorsement");
        if ($filename) {
            $status     = true;
            $error      = "";
        }
        $convertImage   = [];

        $convertImage   = $this->pdftoimageTemplates(base_path() . '/public/' . $filename);
        $convertImage['original_file'] = $filename;
        $response   = [
            "status"    => (bool) $status,
            "data"      => $convertImage,
            "name"      => $basename,
            "error"     => (isset($error) ? $error : null)
        ];


        return response()->json($response);
    }

    public function pdftoimageTemplates($pdfFile)
    {
        $pdf        = new \Spatie\PdfToImage\Pdf($pdfFile);
        $filename   = pathinfo($pdfFile, PATHINFO_FILENAME);
        $total      =  $pdf->getNumberOfPages();
        $image      = [];

        $path = 'temp/endorsement/';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $i = 0;
        foreach (range(1, $total) as &$pageNumber) {
            $img[$i]['convert_path']   = $path . $filename . '-page' . $pageNumber . '.png';
            $img[$i]['pageNumber']  = $pageNumber;
            $pdf->setPage($pageNumber)
                ->setOutputFormat('png')
                ->saveImage($img[$i]['convert_path']);
            $i++;
        }

        $image[]        = $img;

        $data = [
            'total'     =>  $pdf->getNumberOfPages(),
            'image'     => $image
        ];

        return $data;
    }

    public static function checkDuplicate(Request $request)
    {
        $result = Helper::checkDuplicate('EndorsementHdrs', $request->type, $request->vals);
        return response()->json($result);
    }

    //new with queue
    public function addEndorsement(Request $request)
    {
        // return $request->endorsement_benefit;
        // try {

        // $endhdr   = new EndorsementHdrs;
        $endno = $request->endorsementno;
        $pathendno = str_replace("/", "_", $endno);
        // move endorsement doc asli
        $folderOri = $_SERVER['DOCUMENT_ROOT'] . '/assets/endorsement/' . $pathendno . "/originals/";
        if (!is_dir($folderOri)) {
            mkdir($folderOri, 0777, true);
            chmod($folderOri, 0777);
        }

        $pathOriMainDoc = Helper::moveImg($folderOri, $request->path, $folderOri . $request->filename);
        $posisi = strpos($pathOriMainDoc, "assets");
        $savepath = substr($pathOriMainDoc, $posisi);
        $endhdr = EndorsementHdrs::create([
            'endorsementno' => $endno,
            'policy_type' => $request->policy_type,
            'policyno' => $request->policyno,
            'memberid' => $request->memberid,
            'companyid' => $request->companyid,
            'path' => $savepath,
            'filename' => $request->filename,
            'remark' => $request->remark,
            'start_effective_date' => $request->start_effective_date,
            'end_effective_date' => $request->end_effective_date,
            'created_by' => $request->auth->id,
            'insurance_id' => $request->auth->isInsurance ? $request->auth->insurance_id : $request->input('insurance_id'),
        ]);

        // move endors doc convert & save ke endors convert
        $folderConvert = $_SERVER['DOCUMENT_ROOT'] . '/assets/endorsement/' . $pathendno . "/converts/";
        if (!is_dir($folderConvert)) {
            mkdir($folderConvert, 0777, true);
            chmod($folderOri, 0777);
        }
        $kk = 0;
        $pathConMainDoc = [];
        foreach ($request->convert_path as $eachMainConv) {
            $pathConMainDoc[$kk] = Helper::moveImg($folderConvert, $eachMainConv, $folderConvert . basename($eachMainConv));
            $posisi = strpos($pathConMainDoc[$kk], "assets");
            $savepath = substr($pathConMainDoc[$kk], $posisi);
            $docConMain = EndorsementDtls::create([
                'endorsementno' => $endno,
                'seqNo' => $kk + 1,
                'path' => $savepath
            ]);
            $kk++;
        }

        // ------------- upload minio main document ------------- //

        //create folder endors/originalsend
        $pathOrig = 'Transaction/endorsement/' . $pathendno . '/originals';
        Storage::disk('minio')->makeDirectory($pathOrig, 0775, true);
        // $this->claimsubmissioncontroller->createOCR();

        //create folder endorsement/converts
        $pathConv = 'Transaction/endorsement/' . $pathendno . '/converts';
        Storage::disk('minio')->makeDirectory($pathConv, 0775, true);

        if (isset($pathOriMainDoc)) {

            //upload main doc ori to endorsement/originals
            Storage::disk('minio')->putFileAs($pathOrig, new File($pathOriMainDoc), basename($pathOriMainDoc));

            //upload main doc convert to claimregno/originals
            foreach ($pathConMainDoc as $sc) {
                Storage::disk('minio')->putFileAs($pathConv, new File($sc), basename($sc));
            }
        }

        foreach ($request->endorsement_benefit as $ben) {
            $endbenefit = EndorsementBenefits::create([
                'endorsementno' => $endno,
                'reviewstep' => $ben['reviewstep'],
                'endorsement_page' => $ben['endorsement_page']
            ]);
        }
        return response()->json(['message' => 'sukses'], 200);
        // } catch (\Exception $e) {
        //     return response()->json(['message' => 'Endorsement registration failed'], 409);
        // }
    }

    public function getEndorsement(Request $request, $id = null)
    {
        $sortby                 = $request->input('sortby');
        $sortvalue              = $request->input('sortvalue');
        $status                 = true;
        $error                  = null;
        $getEndorsement     = EndorsementHdrs::with([
            'insurance_id',
            'created_by',
            'last_modified_by',
            'policy_type',
            'endorsement_dtl',
            'endorsement_benefit'
        ]);

        if ($request->auth->isInsurance) {
            $getEndorsement   = $getEndorsement->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id) {
            $getEndorsement = $getEndorsement->where('id', $id)->first();

            if (!$getEndorsement) {
                $status     = false;
                $error      = "data not found";
            }
        } else {
            // $getEndorsement     = $getEndorsement->orderBy('id'); //grouping data by detail parent column
            if ($request->has('keyword')) {
                $keyword        = $request->keyword;
                $where          = array(
                    "endorsementno",
                    "policyno",
                    "memberid",
                    "start_effective_date",
                    "end_effective_date",
                    "created_at"
                );
                $getEndorsement = Helper::dynamicSearch($getEndorsement, $where, $keyword);
                if (!$getEndorsement->count()) {
                    $status     = false;
                    $error      = "data not found";
                }
            } else {
                if ($request->has('filter')) {
                    $table          = 'endorsementhdr';
                    $filter         = $request->input('filter');
                    $filtername         = json_decode($filter[0]);
                    $filternameby       = $filtername->by;
                    $filternamevalue    = $filtername->value;
                    if ($filternameby == 'startendDate') {
                        $filterdate             = explode("/", $filternamevalue);
                        $where                  = array('start_effective_date', 'end_effective_date');
                        $getEndorsement       = $getEndorsement->whereBetween($where[0], [$filterdate[0], $filterdate[1]])
                            ->orWhereBetween($where[1], [$filterdate[0], $filterdate[1]]);
                    } elseif ($filternameby == 'created_at') {
                        // echo "aaaa";die;
                        $filterdate             = explode("/", $filternamevalue);
                        $where                  = 'created_at';
                        $getEndorsement       = $getEndorsement->where('created_at', '>=', $filterdate[0].' 00:00:00')
                        ->where('created_at', '<=', $filterdate[1].' 23:59:00');
                        // whereBetween($where, [$filterdate[0].' 00:00:00', $filterdate[1].' 00:00:00']);
                    } else {
                        $getEndorsement    = Helper::filterSearch($getEndorsement, $table, $filter);
                    }

                    foreach ($filter as $f) {
                        $filterdata     = json_decode($f);
                        $filterby       = $filterdata->by;
                        $filtervalue    = $filterdata->value;

                        if (is_array($filtervalue)) {
                            if ($filterby == 'codeDesc') {
                                $getEndorsement = $getEndorsement->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            }
                        } else {
                            if ($filterby == 'codeDesc') {
                                $getEndorsement = $getEndorsement->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            }
                        }
                    }
                }
            }

            if ($getEndorsement->count() < 1) {
                $status     = false;
                $error      = "data not found";
            }

            // print_r($sortvalue);die();
            $getEndorsement     = Helper::sorting($getEndorsement, $sortby, $sortvalue);
            $limit              = $request->has('limit') ? $request->input('limit') : 20;
            $page               = $request->has('page') ? $request->input('page') : 1;
            $getEndorsement     = $getEndorsement->paginate($limit, ['*'], 'page', $page);

            $meta                   = [
                'page'          => (int) $getEndorsement->currentPage(),
                'perPage'       => (int) $getEndorsement->perPage(),
                'total'         => (int) $getEndorsement->total(),
                'totalPage'     => (int) $getEndorsement->lastPage()
            ];


            // $getEndorsement     = $out;
            $getEndorsement     = $getEndorsement->toArray()['data'];
            //
        }

        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($getEndorsement) ? $getEndorsement : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function updateEndorsement(Request $request, $id)
    {
        $endno = $request->endorsementno;
        $file_path = $request->path;
        $convert_path = $request->convert_path;
        $endhdr = EndorsementHdrs::find($id);
        $pathendno = str_replace("/", "_", $endno);


        if ($file_path !== null or $file_path !== "") {
            // move endorsement doc asli
            $folderOri = $_SERVER['DOCUMENT_ROOT'] . '/assets/endorsement/' . $pathendno . "/originals/";
            if (!is_dir($folderOri)) {
                mkdir($folderOri, 0777, true);
                chmod($folderOri, 0777);
            }
            $pathOriMainDoc = Helper::moveImg($folderOri, $request->path, $folderOri . $request->filename);
            $posisi = strpos($pathOriMainDoc, "assets");
            $savepath = substr($pathOriMainDoc, $posisi);
            $filename = $request->filename;
        } else {
            $pathOriMainDoc = $endhdr->path;
            $filename = $endhdr->filename;
        }

        $endhdr->update([
            'policy_type' => $request->policy_type,
            'memberid' => $request->memberid,
            'companyid' => $request->companyid,
            'path' => $savepath,
            'filename' => $filename,
            'remark' => $request->remark,
            'last_modified_by' => $request->auth->id,
            'insurance_id' => $request->auth->isInsurance ? $request->auth->insurance_id : $request->input('insurance_id'),
        ]);

        if ($convert_path) {
            // move endors doc convert & save ke endors convert
            $folderConvert = $_SERVER['DOCUMENT_ROOT'] . '/assets/endorsement/' . $pathendno . "/converts/";
            if (!is_dir($folderConvert)) {
                mkdir($folderConvert, 0777, true);
                chmod($folderOri, 0777);
            }
            $kk = 0;
            $pathConMainDoc = [];
            $docConMainOld = EndorsementDtls::where('endorsementno', $endno);
            $docConMainOld->forceDelete();
            foreach ($request->convert_path as $eachMainConv) {
                $pathConMainDoc[$kk] = Helper::moveImg($folderConvert, $eachMainConv, $folderConvert . basename($eachMainConv));
                $posisi = strpos($pathConMainDoc[$kk], "assets");
                $savepath = substr($pathConMainDoc[$kk], $posisi);
                $docConMain = EndorsementDtls::create([
                    'endorsementno' => $endno,
                    'seqNo' => $kk + 1,
                    'path' => $savepath
                ]);
                $kk++;
            }
        }

        // ------------- upload minio main document ------------- //



        if (isset($pathOriMainDoc)) {

            if ($file_path) {
                $pathOrig = 'Transaction/endorsement/' . $pathendno . '/originals';

                //hapus old data
                Storage::disk('minio')->deleteDirectory($pathOrig);

                //create folder endors/originals
                Storage::disk('minio')->makeDirectory($pathOrig, 0775, true);

                //upload main doc ori to endorsement/originals
                Storage::disk('minio')->putFileAs($pathOrig, new File($pathOriMainDoc), basename($pathOriMainDoc));
            }

            if ($convert_path) {
                $pathConv = 'Transaction/endorsement/' . $pathendno . '/converts';

                //hapus old data
                Storage::disk('minio')->deleteDirectory($pathConv);

                //create folder endorsement/converts
                Storage::disk('minio')->makeDirectory($pathConv, 0775, true);

                //upload main doc convert to claimregno/originals
                foreach ($pathConMainDoc as $sc) {
                    Storage::disk('minio')->putFileAs($pathConv, new File($sc), basename($sc));
                }
            }
        }

        $endbenefitold = EndorsementBenefits::where('endorsementno', $endno)->forceDelete();
        foreach ($request->endorsement_benefit as $ben) {
            $endbenefit = EndorsementBenefits::create([
                'endorsementno' => $endno,
                'reviewstep' => $ben['reviewstep'],
                'endorsement_page' => $ben['endorsement_page']
            ]);
        }
        // } catch (\Exception $e) {
        //     return response()->json(['message' => 'Endorsement registration failed'], 409);
        // }
        return response()->json(['status' => (bool) true, 'message' => 'Your data has been updated'], 200);
    }
}
