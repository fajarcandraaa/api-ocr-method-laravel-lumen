<?php

namespace App\Http\Controllers;

use Auth;
use App\CodeMasters;
use App\Insurances;
use App\ClaimRegHdr;
use App\History;
use App\PurchaseOrder;
use App\Jobs\QueueJobCreateHistory;
use App\Jobs\QueueJobCreatePo;
use App\Jobs\QueueJobUpdateHistory;
use App\Jobs\QueuePendingClaim;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper;
use DB;

class HistoryController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Helper;
    }
    public function createHistory($data = [])
    {
        if (!is_array($data)) return false;
        if (count($data) <= 0) return false;

        $balance = $this->getBalance($data, false);
        Log::info("balance awal di create history " . $balance);

        $balance_new = (((int) $balance) + ((int) $data['qty']));
        Log::info("balance setelah dihitung di create history " . $balance_new);

        $fillable = [
            'ref_trans' => $data['ref_trans'],
            'ref_id' => $data['ref_id'],
            'insurance_id' => $data['insurance_id'],
            'qty' => $data['qty'],
            'balance' => $balance_new,
            'status' => $data['status']
        ];

        $history = new History();
        $history->fill($fillable);
        $history->save();

        //cek if ref trans 1 maka run re queue
        if ($history) {
            if ($data['ref_trans'] == 1) {
                $this->dispatch(new QueuePendingClaim($data['insurance_id']));
            } else {

                $key_active = "balance_{$data['insurance_id']}_ACTIVE";

                if (app('redis')->exists($key_active)) {
                    $balance_active = $balance_new;
                    Log::info("balance yang di set ke redis di create history " . $balance_active);

                    $this->helper->replaceRedis($data,$balance_active,null);
                }

            }
            $this->helper->pushUpdateBalance($data['insurance_id']);

        }

        return true;
    }

    public function createPurchaseOrder($data = null)
    {
        if ($data == null) return false;
        $po = new PurchaseOrder();
        $po->fill($data);
        $po->save();
        return $po;
    }

    public function updateHistory($data = [])
    {
        $code = CodeMasters::where("codeType", "TRN")
            ->where('code', $data['ref_trans'])
            ->first();

        if ($data['ref_trans'] == '1') {
            $initPurchaseOrder = new PurchaseOrder();
            $purchaseOrder = $initPurchaseOrder->where('insurance_id', $data['insurance_id']);
            $purchaseOrder = $initPurchaseOrder->where('po_number', $data['po_number']);
            $poGet = $purchaseOrder->first();
        } else {
            $initHistory = new History();
            $history = $initHistory->where('insurance_id', $data['insurance_id']);
            $history = $initHistory->where('ref_trans', $code->id);
            $history = $initHistory->where('ref_id', $data['ref_id']);
        }
        if ($poGet->status != 1) {
            return false;
        } else {
            Log::info("POget : " . $poGet->qty);
            if ($data['status'] == 'SUCCESS') {
                $updated = [
                    'status' => 2
                ];
                if ($data['ref_trans'] == '1') {
                    $updated['approved_by'] = $data['approved_by'];
                    $updated['approved_at'] = date('Y-m-d H:i:s');
                    $purchaseOrder->update($updated);

                    $this->runCreateHistoryQueue([
                        'status' => 1,
                        'qty' => $poGet->qty,
                        'ref_id' => $data['ref_id'],
                        'ref_trans' => $data['ref_trans'],
                        'insurance_id' => $data['insurance_id'],
                    ]);
                } else $history->update($updated);
            } else {
                $updated = [
                    'status' => 3
                ];
                if ($data['ref_trans'] == '1') {
                    $updated['reason'] = $data['reason'];
                    $updated['approved_by'] = $data['approved_by'];
                    $updated['approved_at'] = date('Y-m-d H:i:s');
                    $purchaseOrder->update($updated);
                } else {
                    $currentHistory = $history->first();
                    $history->update($updated);
                    $currentBalance = $this->getBalance($data, false);
                    $fillable = [
                        'ref_trans' => $currentHistory->ref_trans,
                        'ref_id' => $currentHistory->ref_id,
                        'insurance_id' => $currentHistory->insurance_id,
                        'qty' => abs($currentHistory->qty),
                        'balance' => ($currentBalance + abs($currentHistory->qty)),
                        'status' => 1
                    ];
                    $saveUpdate = $initHistory->fill($fillable);
                    $saveUpdate->save();
                }
            }
            return true;
        }
    }

    public function runCreateHistoryQueue($data = [])
    {
        Log::info("action : create history => data : " . json_encode($data));

        $dateNow = date("YmdHis");

        if ($data['ref_trans'] != '1') {
            $data['qty'] = 0 - $data['qty'];
        }

        $this->dispatch(new QueueJobCreateHistory($data));
        return true;
    }

    public function runCreatePoQueue($data = null)
    {
        $this->dispatch(new QueueJobCreatePo([
            'status' => 2,
            'qty' => $data['qty'],
            'po_number' => $data['ref_id'],
            'trans_date' => date('Y-m-d H:i:s'),
            'created_by' => $data['created_by'],
            'insurance_id' => $data['insurance_id'],
        ]));
        return true;
    }

    public function runUpdateHistoryQueue($data = [])
    {
        Log::info("action : update history => data : " . json_encode($data));

        if ($data['ref_trans'] != '1') {
            $history = new History;
            $history = $history->where('insurance_id', $data['insurance_id']);
            $history = $history->where('ref_id', $data['ref_id']);
            $historyGet = $history->first();

            $historyCount = $history->whereIn('status', [1, 3]);
            $historyCount = $historyCount->count();

            if ($historyCount > 0) return false;

            $qty = $historyGet->qty;
        } else {
            $data['ref_id'] = $data['po_number'];
            $po = new PurchaseOrder();
            $po = $po->where('insurance_id', $data['insurance_id']);
            $po = $po->where('po_number', $data['po_number']);
            $poGet = $po->first();

            $po = $po->whereIn('status', [2, 3]);
            $po = $po->count();

            if ($po > 0) return false;
        }

        $key_active = "balance_{$data['insurance_id']}_ACTIVE";
        $key_pending = "balance_{$data['insurance_id']}_PENDING";

        if ($data['status'] == 'SUCCESS') {
            if ($data['ref_trans'] != '1') {
                if (app('redis')->exists($key_pending)) {
                    $balance_pending = (abs(app('redis')->get($key_pending)) - abs($qty));

                    app('redis')->set($key_pending, $balance_pending);
                    app('redis')->expire($key_pending, 900);
                }
            } else {
                if (app('redis')->exists($key_active)) {
                    // $balance_active = (abs(app('redis')->get($key_active)) + abs($poGet->qty));
                    $balance_active = abs(app('redis')->get($key_active));
                    $balance_new = $balance_active + abs($poGet->qty);

                    $this->helper->replaceRedis($data,$balance_new,null);


                    // app('redis')->set($key_active, $balance_active);
                    // app('redis')->expire($key_active, 900);
                }
            }
        } else {
            if ($data['ref_trans'] != '1') {
                if (app('redis')->exists($key_active)) {
                    $balance_active = abs(app('redis')->get($key_active)) + abs($qty);

                    $this->helper->replaceRedis($data,$balance_active,null);

                    // app('redis')->set($key_active, $balance_active);
                    // app('redis')->expire($key_active, 900);
                }

                if (app('redis')->exists($key_pending)) {
                    Log::info("Redis Pending : " . abs(app('redis')->get($key_pending)));

                    $balance_pending = (abs(app('redis')->get($key_pending)) - abs($qty));

                    app('redis')->set($key_pending, $balance_pending);
                    app('redis')->expire($key_pending, 900);
                }
            }
        }

        $this->dispatch(new QueueJobUpdateHistory($data));
        Log::info("Total Pending : " . abs(app('redis')->get($key_pending)));
        return true;
    }

    public function getBalance($data = null, $cache = true, $checkSufficientBalance = false)
    {
        $data['status_type'] = $data['status_type'] ?? 'ACTIVE';
        $history = History::where('insurance_id', $data['insurance_id']);
        $key = "balance_{$data['insurance_id']}_{$data['status_type']}";
        // $balance_active = app('redis')->get("balance_{$data['insurance_id']}_ACTIVE");

        // app('redis')->del($key);
        if (app('redis')->exists($key) && $cache) {
            return app('redis')->get($key);
        }

        if ($data['status_type'] == 'ACTIVE') {
            // echo "aktif";
            $history = $history->orderBy('id', 'DESC');
            $history = $history->first();
            $balance = $history->balance ?? 0;
        }

        if ($data['status_type'] == 'PENDING') {
            // echo "pending";

            $history = $history->where('status', 2);
            $balance = abs($history->sum('qty')) ?? 0;
        }

        if ($cache) {

            $this->helper->replaceRedis($data,$balance,null);
            
            // app('redis')->set($key, $balance);
            // app('redis')->expire($key, 900);
        }

        return $checkSufficientBalance ?
            (((int) $balance) >= ((int) $data['qty']))
            : ((int) $balance);
    }

    public function getBalanceThreshold(Request $request)
    {
        $dataa['insurance_id'] = $request->insurance_id;
        $dataa['status_type'] = 'ACTIVE';
        $balance = $this->getBalance($dataa);
        $po = PurchaseOrder::where('insurance_id', $request->insurance_id)->where('status', 2); //get po where approved
        $threshold = 0;

        $po = $po->orderBy('id', 'DESC');
        $po = $po->first();
        if ($po) {
            $threshold = $po->qty / 10;
        }


        $response = [
            "balance"          => (isset($balance) ? $balance : null),
            "threshold"          => (isset($threshold) ? $threshold : null)
        ];

        return response()->json($response);
    }

    public function getAnnualBalance(Request $request)
    {
        // $po = PurchaseOrder::where('insurance_id', $request->insurance_id)->where('status', 2)->sum('qty'); //get po where approved
        $po = PurchaseOrder::where('insurance_id', $request->auth->insurance_id)->where('status', 2)->whereYear('trans_date', '=', date("Y"))->sum('qty');

        $threshold = 0;
        if ($po) {
            $threshold = $po;
        }


        $response = [
            "Annual Balance"          => (isset($threshold) ? $threshold : null)
        ];

        return response()->json($response);
    }

    //Test Example
    public function testInsert(Request $request)
    {
        $params = $request->all();
        $code = CodeMasters::where("codeType", "TRN")
            ->where('code', $params['ref_trans'])
            ->first();

        $dateNow = date("YmdHis");
        $params['type'] = preg_replace('/\s+/', '', $code->codeDesc ?? '') ?? '';

        $this->validate($request, [
            'ref_trans' => 'required|string',
            'qty' => 'required|numeric|min:1',
        ]);
        $params['insurance_id'] = 1; //$request->auth->insurance_id;
        $params['created_by'] = 1; //$request->auth->id;
        $params['ref_id'] = "{$params['type']}/{$params['insurance_id']}/{$dateNow}";

        if ($params['ref_trans'] == '1') {
            $this->runCreatePoQueue($params);
        } else {
            $this->runCreateHistoryQueue($params);
        }
        return response()->json([
            "message" => "waiting approve."
        ]);
    }

    public function testUpdateStatus(Request $request)
    {
        $params = $request->all();
        $this->validate($request, [
            'ref_trans' => 'required|string',
            'ref_id' => 'string',
            'po_number' => 'string|required_if:ref_trans,==,1',
            'status' => 'required|string|in:SUCCESS,REJECT',
        ]);
        if ($params['ref_trans'] != '1' && $request->has('ref_id')) {
            return response()->json([
                "message" => "ref_id can be empty when ref_trans isn't 1."
            ]);
        }
        $params['insurance_id'] = 1; //$request->auth->insurance_id;
        $params['approved_by'] = 1; //$request->auth->id;
        $this->runUpdateHistoryQueue($params);
    }

    public function testGetBalance()
    {
        $params['insurance_id'] = 1;
        $active = floatval($this->getBalance($params));

        $params['status_type'] = "PENDING";
        $pending = floatval($this->getBalance($params));

        return response()->json([
            'status' => true,
            'data' => [
                'balance' => [
                    'active' => $active,
                    'pending' => $pending,
                    'total' => floatval($active + $pending),
                ],
            ]
        ], 200);
    }

    public function GetListPendingApproval()
    {
        $approval = PurchaseOrder::where('status', 1)->with(['reason'])->get();

        $response = [
            "status" => $approval ? (bool) true : (bool) false,
            "data"      => $approval,
            "totaldata" => count($approval)
        ];

        return response()->json($response);
    }

    public function GetListApprovedPO(Request $request)
    {
        $limit     = $request->has('limit') ? $request->input('limit') : 20;
        $page      = $request->has('page') ? $request->input('page') : 1;

        $approval = PurchaseOrder::with('insurance:id,insuranceName', 'statusDesc:id,code,codeDesc', 'createdByDesc:id,name,username', 'approvedByDesc:id,name,username', 'reason');

        if ($request->has('filter')) {
            $filter = $request->input('filter');
            foreach ($filter as $f) {
                $filterdata     = json_decode($f);
                $filterby       = $filterdata->by;
                $filtervalue    = $filterdata->value;

                switch ($filterby) {
                    case "startendDate":
                        $filterdate     = explode("/", $filtervalue);
                        $approval = $approval->whereBetween('created_at', [$filterdate[0], $filterdate[1]]);
                        break;
                    case "startendDateApproved":
                        $filterdate     = explode("/", $filtervalue);
                        $approval = $approval->whereBetween('approved_at', [$filterdate[0], $filterdate[1]]);
                        break;
                    case "status":
                        $approval = $approval->whereIn('status', $filtervalue);
                        break;
                    case "po_number":
                        $approval = $approval->where('po_number', 'LIKE', '%' . $filtervalue . '%');
                        break;
                    case "qty":
                        $approval = $approval->where('qty', $filtervalue);
                        break;
                    case "approved_by":
                        $approval = $approval->whereHas('approvedByDesc', function ($query) use ($request, $filtervalue) {
                            $query->select('id')->where('name', 'LIKE', '%' . $filtervalue . '%');
                        });
                        break;
                    case "created_by":
                        $approval = $approval->whereHas('createdByDesc', function ($query) use ($request, $filtervalue) {
                            $query->select('id')->where('name', 'LIKE', '%' . $filtervalue . '%');
                        });
                        break;
                    case "insurance_id":
                        $approval = $approval->where('insurance_id', $filtervalue);
                        break;
                    default:
                        break;
                }
            }
        }

        $approval = Helper::sorting($approval, $request->input('sortby'), $request->input('sortvalue'));

        $approval = $approval->orderBy('created_at', 'desc')->paginate($limit, ['*'], 'page', $page);

        $meta      = [
            'page'      => (int) $approval->currentPage(),
            'perPage'   => (int) $approval->perPage(),
            'total'     => (int) $approval->total(),
            'totalPage' => (int) $approval->lastPage()
        ];
        $approval  = $approval->toArray()['data'];
        return response()->json(['message' => 'success', 'data' => $approval, 'meta' => $meta], 200);
    }

    public function CreateTopUp(Request $request)
    {
        $insuranceID = $request->input('insurance_id');
        $insurance = Insurances::where('id', $insuranceID)->first();
        $insuranceName = $insurance->insuranceName;
        $currentpo = PurchaseOrder::where('insurance_id', $insuranceID);
        $countpo = $currentpo->count() + 1;
        $imagepath = Helper::moveImg("assets/imagetopup", $request->imagepath);
        $request->request->add([
            'status'        => 1,
            'trans_date'    => date("Y-m-d h:i:s"),
            'created_by'    => $request->auth->id,
            'po_number'     => 'PO/' . date("dmY") . '/' . $insuranceName . '/' . str_pad($countpo, 5, '0', STR_PAD_LEFT),
            'imagepath'     => $imagepath
        ]);
        // return 'aaaaaaa';

        $this->validate($request, [
            'qty'           => 'required|numeric',
            'notes'         => 'string',
            'insurance_id'  => 'required'
        ]);

        try {
            $topup      = new PurchaseOrder;
            $params     = $request->all();
            $topup->fill($params);

            $cektopup   = PurchaseOrder::where('po_number', $params['po_number'])->first();

            if ($cektopup) {
                return response()->json(['message' => 'Please check your PO Number, because your PO Number '.$params['po_number'].' is already exist'], 409);
            } else {
                $topup->save();
                //send motif
                $notif = [
                    'to' => $this->helper->getUserByRolePermission('Pages.MasterData.HistoryBalance.Approval'),
                    'title' => "Topup Request",
                    'type' => "REQUEST_TOPUP",
                    'requester'  => $request->auth->id,
                    'requestername'  => $request->auth->name,
                    'message'   => "Insurance ".$insuranceName." has requested for approval Purchase Order ".$request->input('qty')." page(s)",
                    'alert' => true,
                    'notification' => true,
                    'payload' => [
                        'insurance_id' => $insuranceID,
                        'insurance_name' => $insuranceName,
                        'po_number'    => $request->input('po_number'),
                        'qty' => $request->input('qty')
                    ]
                ];

                $this->helper->sendNotifRequest($notif);
                //eof send notif
                return response()->json(['data' => $topup, 'message' => 'Your have been successfully top up your balance'], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Your Top up Failed'], 409);
        }
    }

    public function ApprovalTopUp(Request $request, $id)
    {
        $purchaseorder  = PurchaseOrder::where('id', $id)->first();
        if ($request->input('action') == 'approved') {
            $params = [
                'ref_trans'     => 1,
                'po_number'     => $purchaseorder->po_number,
                'status'  => 'SUCCESS',
                'approved_by'   => $request->auth->id,
                'insurance_id'  => $purchaseorder->insurance_id,
                'reason' => $request->reason
            ];

            $this->runUpdateHistoryQueue($params);

            return response()->json(['message' => 'Aprroved', 'data' => $purchaseorder], 200);
        } elseif ($request->input('action') == 'rejected') {
            $params = [
                'ref_trans'     => 1,
                'po_number'     => $purchaseorder->po_number,
                'status'  => 'FAILED',
                'approved_by'   => $request->auth->id,
                'insurance_id'  => $purchaseorder->insurance_id,
                'reason' => $request->reason

            ];

            $this->runUpdateHistoryQueue($params);

            return response()->json(['message' => 'Rejected', 'data' => $purchaseorder], 200);
        } else {
            return response()->json(['message' => "something wrong with your action"], 409);
        }
    }

    public function getalllog(Request $request)
    {
        $limit     = $request->has('limit') ? $request->input('limit') : 20;
        $page      = $request->has('page') ? $request->input('page') : 1;
        $a_date     = date("Y-m-d");
        $time       = $request->input('time');

        $getalllog = History::with(['insurance:id,insuranceName', 'refTransDesc:id,code,codeDesc']);

        if ($request->auth->isInsurance == "1") {
            $getalllog = $getalllog->where('insurance_id', $request->auth->insurance_id);
        }

        if ($request->input('type') == "daily") {
            $timeparams = explode("/", $time);
            $getalllog = $getalllog->whereMonth('created_at', $timeparams[0])->whereYear('created_at', $timeparams[1]);
        } elseif ($request->input('type') == "monthly") {
            $getalllog = $getalllog->whereYear('created_at', $time);
        }

        if ($request->has('filter')) {
            $filter = $request->input('filter');
            foreach ($filter as $f) {
                $filterdata     = json_decode($f);
                $filterby       = $filterdata->by;
                $filtervalue    = $filterdata->value;

                switch ($filterby) {
                    case "startendDate":
                        $filterdate     = explode("/", $filtervalue);
                        $getalllog = $getalllog->whereBetween('created_at', [$filterdate[0], $filterdate[1]]);
                        break;
                    case "ref_trans":
                        $getalllog = $getalllog->where('ref_trans', $filtervalue);
                        break;
                    case "ref_id":
                        $getalllog = $getalllog->where('ref_id', 'LIKE', '%' . $filtervalue . '%');
                        break;
                    case "qty":
                        $getalllog = $getalllog->where('qty', $filtervalue);
                        break;
                    case "balance":
                        $getalllog = $getalllog->where('balance', $filtervalue);
                        break;
                    case "insurance_id":
                        $getalllog = $getalllog->where('insurance_id', $filtervalue);
                        break;
                    default:
                        $getalllog = $getalllog;
                        break;
                }
            }
        }

        $getalllog = Helper::sorting($getalllog, $request->input('sortby'), $request->input('sortvalue'));

        $getalllog = $getalllog->orderBy('created_at', 'desc')->paginate($limit, ['*'], 'page', $page);
        $meta      = [
            'page'      => (int) $getalllog->currentPage(),
            'perPage'   => (int) $getalllog->perPage(),
            'total'     => (int) $getalllog->total(),
            'totalPage' => (int) $getalllog->lastPage()
        ];
        $getalllog  = $getalllog->toArray()['data'];
        return response()->json(['message' => 'success', 'data' => $getalllog, 'meta' => $meta], 200);
    }

    public function getAverage(Request $request)
    {
        $a_date     = date("Y-m-d");
        $time       = $request->input('time');
        $average    = null;
        $data       = $request->auth->isInsurance == "1" ? History::where('ref_trans', 2)->where('insurance_id', $request->auth->insurance_id) : ($request->input('insurance_id') == 'all' ? History::where('ref_trans', 2) : History::where('ref_trans', 2)->where('insurance_id', $request->input('insurance_id')));
        if ($request->input('type') == "daily") {
            $timeparams = explode("/", $time);
            $dailydata          = $data->selectRaw('LEFT(created_at,10) as date, SUM(qty) as qty')->whereMonth('created_at', $timeparams[0])->whereYear('created_at', $timeparams[1])->groupBy('created_at')->get();
            $dailydataaverage   = $dailydata->sum('qty'); //$data->whereMonth('created_at', $timeparams[0])->sum('qty');
            $totalday           = cal_days_in_month(CAL_GREGORIAN, $timeparams[0], $timeparams[1]);
            $average            = abs(ceil(($dailydataaverage / $totalday)));
            $list               = array();
            $month              = $timeparams[0];
            $year               = $timeparams[1];

            for ($d = 1; $d <= date("t", strtotime($a_date)); $d++) {
                $time = mktime(12, 0, 0, $month, $d, $year);
                if (date('m', $time) == $month) {
                    $list[] = date('Y-m-d', $time);
                }
            }
            foreach ($dailydata as $dd) {
                $barchart[$dd->date] = $dd->qty * -1;
            }
            foreach ($list as $d) {
                $res[$d] = $barchart[$d] ?? 0;
            }
            $res = collect($res)->map(function ($item, $key) {
                return [
                    "dataList"  => $key,
                    "dataValue" => $item,
                ];
            })->values()->reject(function ($item) use ($time) {
                $isCurrentMonth = function () use ($time) {
                    return date("m/Y", strtotime($time)) == date("m/Y") ? true : false;
                };
                if (!$isCurrentMonth()) {
                    return $item['dataList'] > date("Y-m-d");
                }
                return $item;
            });

            $response           = [
                "Average"       => $average,
                "barChart"      => $res,
            ];
        } elseif ($request->input('type') == "monthly") {
            $monthlydata        = $data->selectRaw('SUM(qty)*-1 as QTY,MONTH(created_at) as MNTH')->groupBy(DB::Raw('MONTH(created_at)'))
                ->whereYear('created_at', $time)->get();
            $totalperbulan      = $monthlydata->sum('QTY'); //$data->whereYear('created_at', $time)->sum('qty');
            $existingmonth      = $data->selectRaw('MONTH(created_at) as MNTH')->distinct()->get();
            $monthlydataaverage = abs(ceil(($totalperbulan / ($existingmonth->count() == 0 ? 1 : $existingmonth->count()))));
            return $monthlydataaverage;
            $bulan = [
                "1"     => "Jan " . $time,
                "2"     => "Feb " . $time,
                "3"     => "Mar " . $time,
                "4"     => "Apr " . $time,
                "5"     => "May " . $time,
                "6"     => "Jun " . $time,
                "7"     => "Jul " . $time,
                "8"     => "Aug " . $time,
                "9"     => "Sep " . $time,
                "10"    => "Okt " . $time,
                "11"    => "Nov " . $time,
                "12"    => "Dec " . $time
            ];
            // return $bulan;
            // return count($monthlydata);
            if (count($monthlydata) == 0) {
                $response = [
                    "messege" => "Your data was not found"
                ];
            } else {
                foreach ($monthlydata as $mm) {
                    $barchartmon[] = [
                        "dataList"  => $bulan[$mm->MNTH],
                        "dataValue" => $mm->QTY,
                    ];
                }
                for ($i = count($barchartmon) + 1; $i <= 12; $i++) {
                    $barchartmon[] = [
                        "dataList"  => $bulan[$i],
                        "dataValue" => 0,
                    ];
                }
                $testdatabulanan = array_merge($bulan, $barchartmon);
                $response           = [
                    "Average"       => $monthlydataaverage,
                    "barChart"      => $barchartmon,
                ];
            }
        } else {
            $yearlydata         = $data->selectRaw('SUM(qty)*-1 as QTY,YEAR(created_at) as THN')->groupBy(DB::Raw('YEAR(created_at)'))->get();
            $totalpertahun      = $data->sum('qty');
            $existingyear       = $data->selectRaw('YEAR(created_at) as THN')->distinct()->get();
            $yearlydataaverage  = abs(ceil(($totalpertahun / $existingyear->count())));
            foreach ($yearlydata as $yy) {
                $barchartyear[] = [
                    "dataList"  => $yy->THN,
                    "dataValue" => $yy->QTY
                ];
            }
            $response           = [
                "Average"       => $yearlydataaverage,
                "barChart"      => $barchartyear,
            ];
        }

        return response()->json($response);
    }

    public function getpiechart(Request $request)
    {
        $time           = $request->input('time');
        $insuranceid    = $request->auth->isInsurance == "1" ? $request->auth->insurance_id : $request->input('insurance_id');
        $stringquery    = $request->input('insurance_id') == "all" ? " " : "AND ocr_log_page_transactions.insurance_id= $insuranceid";
        if ($request->input('action') == "plan") {
            if ($request->input('type') == "daily") {
                $timeparams = explode("/", $time);
                $piechart = DB::select('SELECT ben.codeDesc as dataList, COUNT(ocr_log_page_transactions.ref_id) AS dataValue
                                        from code_masters ben
                                        LEFT JOIN code_masters plan ON plan.codeVal1 = ben.`code` AND plan.codeType = "PLN"
                                        LEFT JOIN claimreghdr ON plan.`code`= claimreghdr.plan
                                        LEFT JOIN ocr_log_page_transactions ON ocr_log_page_transactions.ref_id = claimreghdr.claimregno 
                                        AND month(ocr_log_page_transactions.created_at)=' . $timeparams[0] .
                    ' AND year(ocr_log_page_transactions.created_at)=' . $timeparams[1] .
                    ' AND ocr_log_page_transactions.ref_trans=2 ' . $stringquery .
                    ' WHERE ben.codeType = "BEN"
                                        GROUP BY ben.codeDesc');
            } elseif ($request->input('type') == "monthly") {
                $piechart = DB::select('SELECT ben.codeDesc as dataList, COUNT(ocr_log_page_transactions.ref_id) AS dataValue
                                        from code_masters ben
                                        LEFT JOIN code_masters plan ON plan.codeVal1 = ben.`code` AND plan.codeType = "PLN"
                                        LEFT JOIN claimreghdr ON plan.`code`= claimreghdr.plan
                                        LEFT JOIN ocr_log_page_transactions ON ocr_log_page_transactions.ref_id = claimreghdr.claimregno 
                                        AND year(ocr_log_page_transactions.created_at)=' . $time .
                    ' AND ocr_log_page_transactions.ref_trans=2 ' . $stringquery .
                    ' WHERE ben.codeType = "BEN"
                                        GROUP BY ben.codeDesc');
            } else {
                $piechart = DB::select('SELECT ben.codeDesc as dataList, COUNT(ocr_log_page_transactions.ref_id) AS dataValue
                                        from code_masters ben
                                        LEFT JOIN code_masters plan ON plan.codeVal1 = ben.`code` AND plan.codeType = "PLN"
                                        LEFT JOIN claimreghdr ON plan.`code`= claimreghdr.plan
                                        LEFT JOIN ocr_log_page_transactions ON ocr_log_page_transactions.ref_id = claimreghdr.claimregno
                                        AND ocr_log_page_transactions.ref_trans=2 ' . $stringquery .
                    ' WHERE ben.codeType = "BEN"
                                        GROUP BY ben.codeDesc');
            }
        } else {
            if ($request->input('type') == "daily") {
                $timeparams = explode("/", $time);
                $piechart = DB::select('SELECT code_masters.codeDesc AS dataList, COUNT(ocr_log_page_transactions.ref_id) AS dataValue 
                                        from code_masters 
                                        LEFT JOIN claimreghdr ON code_masters.`code` = claimreghdr.plan 
                                        LEFT JOIN ocr_log_page_transactions ON ocr_log_page_transactions.ref_id=claimreghdr.claimregno 
                                        AND MONTH(ocr_log_page_transactions.created_at)=' . $timeparams[0] .
                    ' AND YEAR(ocr_log_page_transactions.created_at)=' . $timeparams[1] .
                    ' AND ocr_log_page_transactions.ref_trans=2 ' . $stringquery .
                    ' WHERE code_masters.codeType = "PLN" AND code_masters.deleted_at IS NULL 
                                        GROUP BY code_masters.codeDesc');
            } elseif ($request->input('type') == "monthly") {
                $piechart = DB::select('SELECT code_masters.codeDesc AS dataList, COUNT(ocr_log_page_transactions.ref_id) AS dataValue 
                                        from code_masters 
                                        LEFT JOIN claimreghdr ON code_masters.`code` = claimreghdr.plan 
                                        LEFT JOIN ocr_log_page_transactions ON ocr_log_page_transactions.ref_id=claimreghdr.claimregno 
                                        AND YEAR(ocr_log_page_transactions.created_at)=' . $time .
                    ' AND ocr_log_page_transactions.ref_trans=2 
                                        WHERE code_masters.codeType = "PLN" AND code_masters.deleted_at IS NULL 
                                        GROUP BY code_masters.codeDesc');
            } else {
                $piechart = DB::select('SELECT code_masters.codeDesc AS dataList, COUNT(ocr_log_page_transactions.ref_id) AS dataValue 
                                        from code_masters 
                                        LEFT JOIN claimreghdr ON code_masters.`code` = claimreghdr.plan 
                                        LEFT JOIN ocr_log_page_transactions ON ocr_log_page_transactions.ref_id=claimreghdr.claimregno 
                                        AND ocr_log_page_transactions.ref_trans=2 ' . $stringquery .
                    ' WHERE code_masters.codeType = "PLN" AND code_masters.deleted_at IS NULL 
                                        GROUP BY code_masters.codeDesc');
            }
        }
        return response()->json(['message' => (bool) true, 'data' => $piechart], 200);
    }

    public function getTimeType(Request $request)
    {
        try {
            if ($request->input('type') == "daily") {
                $data = History::where('ref_trans', 2)->where('insurance_id', $request->auth->insurance_id)
                    ->selectRaw('MONTH(created_at) as Waktu, YEAR(created_at) as Tahun')->distinct()->orderBy(DB::Raw('YEAR(created_at)'), 'desc')->orderBy(DB::Raw('MONTH(created_at)'), 'desc')->get();
    
                foreach ($data as $dailydata) {
                    $returndata[] = [
                        "idmonth"   => $dailydata->Waktu . "/" . $dailydata->Tahun,
                        "Name"      => date("F", mktime(0, 0, 0, $dailydata->Waktu, 10)) . " " . $dailydata->Tahun
                    ];
                }
    
                $response = [
                    "data" => $returndata
                ];
            } else {
                $data = History::where('ref_trans', 2)->where('insurance_id', $request->auth->insurance_id)
                    ->selectRaw('YEAR(created_at) as Waktu')->orderBy(DB::Raw('YEAR(created_at)'), 'desc')->distinct()->get();
    
                foreach ($data as $yearlydata) {
                    $returndata[] = [
                        "Name"  => $yearlydata->Waktu
                    ];
                }
    
                $response = [
                    "data" => $returndata
                ];
            }
        } catch (\Exception $msg) {
            $response = [
                "data" => []
            ];
        }
        return response()->json($response);
    }
}
