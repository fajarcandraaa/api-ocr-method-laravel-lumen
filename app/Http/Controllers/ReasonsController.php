<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Reasons;
use App\Exports\ReasonsExport;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;

class ReasonsController extends Controller
{
    public function addNewReasons(Request $request)
    {
        try {
            do {
                $rand = str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
                $cek = Reasons::where('reason_code', '=', $rand)->first();
            } while ($cek);
            $reason = Reasons::create([
                'reason_code' => $rand,
                'reason_name' => $request->reason_name,
                'eob_type' => $request->eob_type,
                'description' => $request->description,
                'created_by' => $request->id_user,
                'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id)
            ]);
            return response()->json(['message' => 'DATA CREATED SUCCESSFULLY'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Reasons Registration Failed!'], 409);
        }
    }

    public function updateReasons(Request $request, $id)
    {
        $data   = Reasons::find($id);

        try {
            $data->update([
                'reason_name' => $request->reason_name,
                'eob_type' => $request->eob_type,
                'description' => $request->description,
                'last_modified_by' => $request->id_user,
                'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id)
            ]);

            return response()->json(['status' => (bool) true, 'message' => 'Data successfully updated'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
        }
    }

    public function deleteReasons($id)
    {
        $res   = Reasons::find($id);

        if ($res) {
            try {
                $res->delete();
                return response()->json(['status' => (bool) true], 200);
            } catch (\Exception $e) {
                return response()->json(['status' => (bool) false, 'message' => 'delete data failed'], 409);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'data not found'], 409);
        }
    }

    public function mass_delete(Request $request)
    {
        $id = $request->id;
        $res = Reasons::whereIn('id', $id);

        if ($res) {
            try {
                $res->delete();
                return response()->json(['status' => (bool) true], 200);
            } catch (\Exception $e) {
                return response()->json(['status' => (bool) false, 'message' => 'delete data failed'], 409);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'data not found'], 409);
        }
    }

    public function showReasons(Request $request, $id = null)
    {
        $sortby     = $request->input('sortby');
        $sortvalue  = $request->input('sortvalue');
        $status     = false;
        $error      = "data not found";
        $getres    = Reasons::with([
            'created_by',
            'last_modified_by',
            'insurance_id',
            'eob_type'
        ]);

        if ($request->auth->isInsurance) {
            $getres   = $getres->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id) {
            $getres = $getres->where('id', $id)->first();
            if (!$getres) {
                $status     = false;
                $error      = "data not found";
            }
        } else {
            if ($request->has('keyword')) {
                $keyword            = $request->keyword;
                $wheres             = array(
                    "reason_code",
                    "reason_name",
                    "eob_type",
                    "description"
                );
                $getres   = Helper::dynamicSearch($getres, $wheres, $keyword);
                $getres     = $getres->whereHas('created_by', function ($query) use ($keyword) {
                    $query->where('name','like','%'.$keyword.'%');
                })->orWhereHas('last_modified_by', function ($query) use ($keyword) {
                    $query->where('name','like','%'.$keyword.'%');
                })->orWhereHas('insurance_id', function ($query) use ($keyword) {
                    $query->where('insuranceName','like','%'.$keyword.'%');
                });
                // if (!$getres->count()) {
                //     $getres     = $getres->whereHas('created_by', function ($query) use ($keyword) {
                //         $query->where('name','like','%'.$keyword.'%');
                //     })->orWhereHas('last_modified_by', function ($query) use ($keyword) {
                //         $query->where('name','like','%'.$keyword.'%');
                //     });
                // }
                $status             = true;
                $error              = null;
                $getres = Helper::sorting($getres, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table     = 'reasons';
                    $filter    = $request->input('filter');
                    $getres   = Helper::filterSearch($getres, $table, $filter);

                    foreach ($filter as $f) {
                        $filterdata         = json_decode($f);
                        $filterby           = $filterdata->by;
                        $filtervalue        = $filterdata->value;

                        if (is_array($filtervalue)) {
                            if ($filterby == 'createdname') {
                                $getres     = $getres->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'modifiedname') {
                                $getres     = $getres->whereHas('last_modified_by', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'insuranceId') {
                                $getres     = $getres->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                    $query->whereIn('insuranceName', $filtervalue);
                                });
                            }
                        } else {
                            if ($filterby == 'createdname') {
                                $getres     = $getres->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->where('name','like','%'.$filtervalue.'%');
                                });
                            } elseif ($filterby == 'modifiedname') {
                                $getres     = $getres->whereHas('last_modified_by', function ($query) use ($filtervalue) {
                                    $query->where('name','like','%'.$filtervalue.'%');
                                });
                            } elseif ($filterby == 'insuranceId') {
                                $getres     = $getres->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                    $query->where('insuranceName', 'like','%'.$filtervalue.'%');
                                });
                            }
                        }
                    }
                }
            }

            $getres     = Helper::sorting($getres, $sortby, $sortvalue);
            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $getres     = $getres->paginate($limit, ['*'], 'page', $page);
            $meta       = [
                'page'      => (int) $getres->currentPage(),
                'perPage'   => (int) $getres->perPage(),
                'total'     => (int) $getres->total(),
                'totalPage' => (int) $getres->lastPage()
            ];
            $getres  = $getres->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getres) ? $getres : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function import(Request $request)
    {
        $data = $request->json()->all();
        $result = [];
        $i = 0;
        $k = 0;
        foreach ($data as $value1) {
            try {
                if ($value1['reason_code'] == "") {
                    do {
                        $rand = str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
                        $cek = Reasons::where('reason_code', '=', $rand)->first();
                    } while ($cek);
                    $value1['reason_code'] = $rand;
                }

                $reason = Reasons::updateOrCreate(
                    [
                        'reason_code' => $value1['reason_code']
                    ],
                    [
                        'reason_name' => $value1['reason_name'],
                        'eob_type' => $value1['eob_type'],
                        'description' => $value1['description'],
                        'created_by' => $request->auth->id,
                        'last_modified_by' => $request->auth->id
                    ]
                );
                $result['valid'][$i] = [
                    'reason_code' => $value1['reason_code'],
                    'reason_name' => $value1['reason_name'],
                    'eob_type' => $value1['eob_type'],
                    'description' => $value1['description'],
                    'created_by' => $request->auth->id,
                    'last_modified_by' => $request->auth->id,
                    'is_valid' => $value1['is_valid'],
                    'msg' =>  $value1['msg'],
                ];

                if ($reason) {
                    $i++;
                }
            } catch (\Exception $e) {
                $result['invalid'][$k] = [
                    'reason_code' => $value1['reason_code'],
                    'reason_name' => $value1['reason_name'],
                    'eob_type' => $value1['eob_type'],
                    'description' => $value1['description'],
                    'created_by' => $request->auth->id,
                    'last_modified_by' => $request->auth->id,
                    'is_valid' => false,
                    'msg' => "saving data failed",
                ];
                $k++;
            }
        }
        return response()->json($result, 200);
    }

    public function excelReasons(Request $request)
    {
        $id = $request->id;

        $reason = Reasons::with([
            'created_by',
            'last_modified_by'
        ]);

        if ($id or $id !== null) {
            $reason->whereIn('id', $id);
        }

        $reason = $reason->get();

        $reason = new ReasonsExport($reason->toArray());
        $date = date("dmy");

        return Excel::download($reason, 'Reasons-' . $date . '.xlsx');
    }

    public function pdfReasons(Request $request)
    {
        $date = date("dmy");

        $id = $request->id;

        $reason = Reasons::with([
            'created_by',
            'last_modified_by'
        ]);

        if ($id or $id !== null) {
            $reason->whereIn('id', $id);
        }

        $reason = $reason->get();

        $pdf = PDF::loadview('reasons', ['reasons' => $reason]);
        return $pdf->download('Reasons-' . $date . '.pdf');
    }
}
