<?php

namespace App\Http\Controllers;

use App\CodeMasters;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CodeMastersController extends Controller
{

    public function getCodes(Request $request)
    {
        $status = true;
        $error = "";


        if ($request->codeType) {

            $codeType = $request->codeType;
            $codes = CodeMasters::whereIn('codeType', $codeType)->where('active',"1")->get();
        } else {
            $codes = CodeMasters::where('active',"1")->get();
        }


        if (!$codes->count()) {
            $status = false;
            $error = "data not available";
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($codes) ? $codes : null),
            "error"     => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }
}
