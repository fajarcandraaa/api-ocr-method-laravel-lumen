<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Biz_providers;
use App\BankInfos;
use App\Finances;
use App\Exports\UserManagementExport;
use App\Helpers\Helper;
use App\Http\Controllers\AuthController;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;

class ProviderController extends Controller
{

    public function generateMID($ins_id, $type)
    {
        $cek = Biz_providers::max('id');
        if (!$cek) {
            $hit = "1";
        } else {
            $hit = $cek;
            $hit++;
        }

        $ins_id = str_pad($ins_id, 9, '0', STR_PAD_LEFT);
        $hit = str_pad($hit, 4, '0', STR_PAD_LEFT);
        $code = Helper::getKey("code", "CodeMasters", "id", $type);
        $code = str_pad($code, 2, '0', STR_PAD_LEFT);

        $mid = $ins_id . $hit . $code;
        return $mid;
    }

    public function addNewProvFromComp($id_comp, $dataprov)
    {
        // try {
        // $cekprovider    = biz_providers::where('merchant_id', $dataprov['merchant_id'])
        //     ->orWhere('name', $dataprov['name'])
        //     ->first();
        // if ($cekprovider) {
        //     return response()->json(['message' => "Merchant ID or Provider's Name already exist!"], 409);
        // } else {
        if (strlen($dataprov['merchant_id']) == 9) {
            $mid = $dataprov['merchant_id'];
        } else {
            $mid = $this->generateMID($dataprov['merchant_id'], $dataprov['type']);
        }
        // return $mid;
        // die;
        // print_r($dataprov);die();
        $provider = biz_providers::create([
            'merchant_id' => $mid,
            'type' => $dataprov['type'],
            'name' => $dataprov['name'],
            'phone' => $dataprov['phone'],
            'address' => $dataprov['address'],
            'city' => $dataprov['city'],
            'province' => $dataprov['province'],
            'district' => $dataprov['district'],
            'postal_code' => $dataprov['postal_code'],
            'country' => $dataprov['country'],
            'fax' => $dataprov['fax'],
            'compprovid' => $id_comp,
            'provider_email' => $dataprov['provider_email'],
            'website' => (!isset($dataprov['website']) ? "" : $dataprov['website']),
            'created_by' => $dataprov['id_user'],
            'last_modified_by' => $dataprov['id_user'],
            'insurance_id' => $dataprov['merchant_id']
        ]);

        if ($provider) {
            try {
                if (isset($dataprov['bank_code'])) {
                    Helper::addBankInfo($dataprov, "biz_providers", $provider->id);
                }

                if (isset($dataprov['contact_person'])) {
                    Helper::addCP($dataprov['contact_person'], "biz_providers", $provider->id);
                }
            } catch (\Exception $e) {
                $cek = Biz_providers::find($provider->id);
                if ($cek) {
                    $cek->delete();
                }
                $cek = BankInfos::where([['ref_type', 'biz_providers'], ['ref_id', $provider->id]]);
                if ($cek) {
                    $cek->delete();
                }
                $cek = Finances::where([['ref_type', 'biz_providers'], ['ref_id', $provider->id]]);
                if ($cek) {
                    $cek->delete();
                }
                throw new \Exception();
                // return response()->json(['message' => 'Provider Registration Failed!'], 409);
            }
            return $provider;
        }

        // // }
        // } catch (\Exception $e) {
        //     return response()->json(['message' => 'Provider Registration Failed!'], 409);
        // }
    }

    public function updateProvFromComp($id_comp, $dataprov)
    {
        $mid = $dataprov['merchant_id'];

        $provider = biz_providers::where('merchant_id', $mid)->firstOrFail();
        
             $provider->merchant_id = $mid;
             $provider->type = $dataprov['type'];
             $provider->name = $dataprov['name'];
             $provider->phone = $dataprov['phone'];
             $provider->address = $dataprov['address'];
             $provider->city = $dataprov['city'];
             $provider->province = $dataprov['province'];
             $provider->district = $dataprov['district'];
             $provider->postal_code = $dataprov['postal_code'];
             $provider->country = $dataprov['country'];
             $provider->fax = $dataprov['fax'];
             $provider->compprovid = $id_comp;
             $provider->provider_email = $dataprov['provider_email'];
             $provider->website = (!isset($dataprov['website']) ? "" : $dataprov['website']);
             $provider->created_by = $dataprov['id_user'];
             $provider->last_modified_by = $dataprov['id_user'];
             $provider->insurance_id = $dataprov['merchant_id'];
        $provider->save();

        if ($provider) {
            try {
                if (isset($dataprov['bank_code'])) {
                    Helper::addBankInfo($dataprov, "biz_providers", $provider->id);
                }
                if (isset($dataprov['contact_person'])) {
                    Helper::addCP($dataprov['contact_person'], "biz_providers", $provider->id);
                }
            } catch (\Exception $e) {
                $cek = Biz_providers::find($provider->id);
                if ($cek) {
                    $cek->delete();
                }
                $cek = BankInfos::where([['ref_type', 'biz_providers'], ['ref_id', $provider->id]]);
                if ($cek) {
                    $cek->delete();
                }
                $cek = Finances::where([['ref_type', 'biz_providers'], ['ref_id', $provider->id]]);
                if ($cek) {
                    $cek->delete();
                }
                throw new \Exception();
                // return response()->json(['message' => 'Provider Registration Failed!'], 409);
            }
            return $provider;
        }

        // // }
        // } catch (\Exception $e) {
        //     return response()->json(['message' => 'Provider Registration Failed!'], 409);
        // }
    }

    // public function addNewProvider(Request $request)
    // {

    //     $this->validate($request, [
    //         'merchant_id'           => 'required|integer',
    //         'type'                  => 'required',
    //         'name'                  => 'required|string',
    //         'phone'                 => 'required|string',
    //         'address'               => 'string',
    //         'fax'                   => 'required|string',
    //         'provider_email'        => 'required|email',
    //         'bank_account'          => 'required|integer',
    //         'bank_branch'           => 'required|string',
    //         'bank_account_name'     => 'required|string',
    //         'website'               => 'required|string',
    //         'pic_marketing_name'    => 'required|string',
    //         'pic_marketing_phone'   => 'required|string',
    //         'bank_code'             => 'required'
    //     ]);

    //     try {
    //         $cekprovider    = biz_providers::where('merchant_id', $request->input('merchant_id'))
    //             ->orWhere('name', $request->input('name'))
    //             ->first();
    //         if ($cekprovider) {
    //             return response()->json(['message' => "Merchant ID or Provider's Name already exist!"], 409);
    //         } else {
    //             $provider = biz_providers::create([
    //                 'merchant_id' => $request->merchant_id,
    //                 'type' => $request->type,
    //                 'name' => $request->name,
    //                 'phone' => $request->phone,
    //                 'address' => $request->address,
    //                 'city' => $request->city,
    //                 'province' => $request->province,
    //                 'area_code' => $request->area_code,
    //                 'fax' => $request->fax,
    //                 'compprovid' => $request->compprovid,
    //                 'provider_email' => $request->provider_email,
    //                 'website' => $request->website,
    //                 'pic_marketing_name' => $request->pic_marketing_name,
    //                 'pic_marketing_email' => $request->pic_marketing_email,
    //                 'pic_marketing_phone' => $request->pic_marketing_phone,
    //                 'created_by' => $request->id_user,
    //                 'last_modified_by' => $request->id_user
    //             ]);

    //             if (isset($request->bank_code)) {
    //                 $now = date("Y-m-d h:i:s");
    //                 $bank_infos = [
    //                     'ref_type' => 'biz_providers',
    //                     'ref_id' => $provider->id,
    //                     'bank_code' => $request->bank_code,
    //                     'bank_branch' => $request->bank_branch,
    //                     'bank_account' => $request->bank_account,
    //                     'bank_account_name' => $request->bank_account_name,
    //                     'created_at' => $now,
    //                     'created_by' => $request->id_user,
    //                     'last_modified_by' => $request->id_user,
    //                 ];

    //                 $bank_infos = BankInfos::insert($bank_infos);
    //                 $provider->bank_infos = $bank_infos;
    //             }


    //             return response()->json(['Provider' => $provider, 'message' => 'CREATED'], 200);
    //         }
    //     } catch (\Exception $e) {
    //         return response()->json(['message' => 'Provider Registration Failed!'], 409);
    //     }
    // }

    // public function updateProvider(Request $request, $id)
    // {
    //     // $this->validate($request, [
    //     //     'merchant_id'           => 'required|integer',
    //     //     'type'                  => 'required',
    //     //     'name'                  => 'required|string',
    //     //     'phone'                 => 'required|string',
    //     //     'address'               => 'string',
    //     //     'fax'                   => 'required|string',
    //     //     'provider_email'        => 'required|email',
    //     //     'bank_account'          => 'required|integer',
    //     //     'bank_branch'           => 'required|string',
    //     //     'bank_account_name'     => 'required|string',
    //     //     'website'               => 'required|string',
    //     //     'pic_marketing_name'    => 'required|string',
    //     //     'pic_marketing_phone'   => 'required|string',
    //     //     'bank_code'             => 'required'
    //     // ]);

    //     $data   = biz_providers::find($id);

    //     if ($data != null) {
    //         $cekproviders   = biz_providers::where(function ($query) use ($request) {
    //             $query->where('merchant_id', $request->input('merchant_id'))
    //                 ->orWhere('name', $request->input('name'));
    //         })->where('id', '!=', $id)->first();
    //         if ($cekproviders) {
    //             return response()->json(['message' => "Provider's Name or Merchant ID already exist!"], 409);
    //         } else {

    //             $data->update([
    //                 'merchant_id' => $request->merchant_id,
    //                 'type' => $request->type,
    //                 'name' => $request->name,
    //                 'phone' => $request->phone,
    //                 'address' => $request->address,
    //                 'city' => $request->city,
    //                 'province' => $request->province,
    //                 'area_code' => $request->area_code,
    //                 'fax' => $request->fax,
    //                 'compprovid' => $request->compprovid,
    //                 'provider_email' => $request->provider_email,
    //                 'website' => $request->website,
    //                 'pic_marketing_name' => $request->pic_marketing_name,
    //                 'pic_marketing_email' => $request->pic_marketing_email,
    //                 'pic_marketing_phone' => $request->pic_marketing_phone,
    //                 'created_by' => $request->id_user,
    //                 'last_modified_by' => $request->id_user
    //             ]);

    //             $bank_infos = BankInfos::where('ref_id',$id)->delete();

    //             if(isset($request->bank_code)){
    //                 $now=date("Y-m-d h:i:s");
    //                 $bank_infos = [
    //                     'ref_type' => 'biz_providers',
    //                     'ref_id' => $data->id,
    //                     'bank_code' => $request->bank_code,
    //                     'bank_branch' => $request->bank_branch,
    //                     'bank_account' => $request->bank_account,
    //                     'bank_account_name' => $request->bank_account_name,
    //                     'created_at' => $now,
    //                     'created_by' => $request->id_user,
    //                     'last_modified_by' => $request->id_user,
    //                 ];

    //                 $bank_infos = BankInfos::insert($bank_infos);

    //                 $provider=Biz_providers::with([
    //                     'type',
    //                     'city',
    //                     'province',
    //                     'bank_infos' => function ($query) {
    //                         $query->where('ref_type', '=', 'biz_providers');
    //                     },
    //                     'created_by',
    //                     'last_modified_by'
    //                 ])->get()->find($id);

    //                     $provider->bank_infos = $bank_infos;
    //                 }

    //             return response()->json(['Provider' => $provider,'status' => (bool) true, 'message' => 'Your data has been update'], 200);
    //         }
    //     } else {
    //         return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
    //     }
    // }

    public function deleteProvider(Request $request, $id)
    {
        $provider   = biz_providers::find($id);
        if ($provider) {
            $provider->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function mass_delete(Request $request)
    {
        $provider = Biz_providers::whereIn('id', $request->id);
        $provider->delete();
        return response('Deleted Successfully', 200);
    }

    public function showProvider(Request $request, $id = null)
    {
        $sortby     = $request->input('sortby');
        $sortvalue  = $request->input('sortvalue');
        $status     = true;
        $error      = "";
        // $id         = $request->input('idprov');
        $providers  = biz_providers::with([
            'type',
            'city',
            'province',
            'bank_infos' => function ($query) {
                $query->where('ref_type', '=', 'biz_providers');
            },
            'created_by',
            'last_modified_by'
        ]);

        if ($request->auth->isInsurance) {
            $providers   = $providers->where('insurance_id', $request->auth->insurance_id);
        }
        
        if ($id) {
            $providers  = $providers->where('id', $id)->first();

            if ($providers) {
                $status     = true;
                $error      = null;
            }
        } else {
            if ($request->has('keyword')) {
                $keyword    = $request->keyword;
                // print_r($providers);die();
                //field yang dicari
                $wheres     = array("merchant_id", "type", "name", "district");
                $providers = Helper::dynamicSearch($providers, $wheres, $keyword);

                if (!$providers->count()) {
                    $providers  = biz_providers::with([
                        'type',
                        'city',
                        'province',
                        'bank_infos' => function ($query) {
                            $query->where('ref_type', '=', 'biz_providers');
                        },
                        'created_by',
                        'last_modified_by'
                    ]);

                    $providers  = $providers->whereHas('type', function ($query) use ($keyword) {
                        $query->where('codeDesc', 'like', '%' . $keyword . '%');
                    })->orWhereHas('city', function ($query) use ($keyword) {
                        $query->where('city_desc', 'like', '%' . $keyword . '%');
                    })->orWhereHas('province', function ($query) use ($keyword) {
                        $query->where('prov_desc', 'like', '%' . $keyword . '%');
                    })->orWhereHas('bank_infos', function ($query) use ($keyword) {
                        $query->where('bank_branch', 'like', '%' . $keyword . '%')
                            ->orWhere('bank_account_name', 'like', '%' . $keyword . '%');
                    })->orWhereHas('created_by', function ($query) use ($keyword) {
                        $query->where('name', 'like', '%' . $keyword . '%');
                    })->orWhereHas('last_modified_by', function ($query) use ($keyword) {
                        $query->where('name', 'like', '%' . $keyword . '%');
                    });
                }
                $status     = true;
                $error      = null;
                $providers  = Helper::sorting($providers, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table      = 'biz_providers';
                    $filter     = $request->input('filter');
                    $providers  = Helper::filterSearch($providers, $table, $filter);

                    foreach ($filter as $f) {
                        $filterdata         = json_decode($f);
                        $filterby           = $filterdata->by;
                        $filtervalue        = $filterdata->value;

                        if ($filterby == 'codeDesc') {
                            $providers->whereHas('type', function ($query) use ($filtervalue) {
                                $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'city_desc') {
                            $providers->whereHas('city', function ($query) use ($filtervalue) {
                                $query->where('city_desc', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'prov_desc') {
                            $providers->whereHas('province', function ($query) use ($filtervalue) {
                                $query->where('prov_desc', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'bank_branch' || $filterby == 'bank_account_name') {
                            $providers->whereHas('bank_infos', function ($query) use ($filtervalue) {
                                $query->where('bank_branch', 'like', '%' . $filtervalue . '%')
                                    ->orWhere('bank_account_name', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'namecreated') {
                            $providers->whereHas('created_by', function ($query) use ($filtervalue) {
                                $query->where('name', 'like', '%' . $filtervalue . '%');
                            });
                        } elseif ($filterby == 'namemodified') {
                            $providers->whereHas('last_modified_by', function ($query) use ($filtervalue) {
                                $query->where('name', 'like', '%' . $filtervalue . '%');
                            });
                        }
                    }
                }
            }
            // print_r($providers->count());die();
            $limit      = $request->has('limit') ? $request->input('limit') : 100;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $providers  = $providers->paginate($limit, ['*'], 'page', $page);
            $meta       = [
                'page'      => (int) $providers->currentPage(),
                'perPage'   => (int) $providers->perPage(),
                'total'     => (int) $providers->total(),
                'totalPage' => (int) $providers->lastPage()
            ];
            $providers   = $providers->toArray()['data'];
        }

        if (!$providers) {
            $status = false;
            $error = "data not found";
        }

        $response = [
            "status" => (bool) $status,
            "data" => (isset($providers) ? $providers : null),
            "meta" => (isset($meta) ? $meta : null),
            "error" => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }
}
