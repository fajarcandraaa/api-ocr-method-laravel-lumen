<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function exportData(Request $request, $type = null)
    {
        $date            = Carbon::now()->setTimezone('Asia/Jakarta')->format("Y-m-d");
        $filename        = $request->get('filename') ? $request->get('filename') : "";
        $folderTypeData  = $type ? $type :  "";
        $path            = "/public/export/{$folderTypeData}/{$date}/{$filename}";

        if(file_exists(storage_path("app{$path}"))) {
            return Storage::download("{$path}");
        } else {
            Log::error("error get file {$folderTypeData}.");
            return response([
                "message" => "File {$folderTypeData} not found, please check your filename!"
            ], 422);
        }
    }
}
