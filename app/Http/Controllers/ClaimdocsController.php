<?php

namespace App\Http\Controllers;

use App\ClaimDocs;
use App\User;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Exports\ClaimDocsExport;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Jobs\QueueExport;


class ClaimdocsController extends Controller
{
    private $notificationcontroller;

    public function __construct()
    {
        $this->notificationcontroller = new NotificationController;
    }

    public function showAllClaimDocs(Request $request, $id = null)
    {
        $sortby         = $request->input('sortby');
        $sortvalue      = $request->input('sortvalue');
        $status         = false;
        $error          = "Data not found";
        $claimDocs      = ClaimDocs::with(['insurances', 'modifiedBy', 'createdBy', 'plan']);

        if ($request->auth->isInsurance) {
            $claimDocs      = $claimDocs->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id) {
            $claimDocs   = $claimDocs->where('id', $id)->first();

            if ($claimDocs) {
                $status     = true;
                $error      = null;
            }
        } else {
            if ($request->has('keyword')) {
                $keyword = $request->keyword;
                $wheres = array("claim_doc_no", "claim_doc_name", "created_at", "is_mandatory");
                $claimDocs = Helper::dynamicSearch($claimDocs, $wheres, $keyword);
                $claimDocs = $claimDocs->orWhereHas('insurances', function ($query) use ($keyword) {
                        $query->where('insuranceName', 'like', '%' . $keyword . '%');
                    })->orWhereHas('modifiedBy', function ($query) use ($keyword) {
                        $query->where('name', 'like', '%' . $keyword . '%');
                    })->orWhereHas('createdBy', function ($query) use ($keyword) {
                        $query->where('name', 'like', '%' . $keyword . '%');
                    })->orWhereHas('plan', function ($query) use ($keyword) {
                        $query->where('codeDesc', 'like', '%' . $keyword . '%');
                    });

                // if (!$claimDocs->count()) {
                //     $claimDocs = $claimDocs->where(function ($q) use ($keyword) {
                //         $q      = $q->WhereHas('insurances', function ($query) use ($keyword) {
                //             $query->where('insuranceName', 'like', '%' . $keyword . '%');
                //         })->orWhereHas('modifiedBy', function ($query) use ($keyword) {
                //             $query->where('name', 'like', '%' . $keyword . '%');
                //         })->orWhereHas('createdBy', function ($query) use ($keyword) {
                //             $query->where('name', 'like', '%' . $keyword . '%');
                //         })->orWhereHas('plan', function ($query) use ($keyword) {
                //             $query->where('codeDesc', 'like', '%' . $keyword . '%');
                //         });
                //     });
                // }
                $status     = true;
                $error      = null;
                $claimDocs  = Helper::sorting($claimDocs, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table          = 'claim_docs';
                    $filter         = $request->input('filter');
                    $claimDocs      = Helper::filterSearch($claimDocs, $table, $filter);
                    foreach ($filter as $f) {
                        $filterdata     = json_decode($f);
                        $filterby       = $filterdata->by;
                        $filtervalue    = $filterdata->value;

                        if (is_array($filtervalue)) {
                            if ($filterby == 'createdbyname') {
                                $claimDocs   = $claimDocs->WhereHas('createdBy', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'plancodedesc') {
                                $claimDocs   = $claimDocs->WhereHas('plan', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'insurances') {
                                $claimDocs   = $claimDocs->WhereHas('insurances', function ($query) use ($filtervalue) {
                                    $query->whereIn('insuranceName', $filtervalue);
                                });
                            }
                        } else {
                            if ($filterby == 'createdbyname') {
                                $claimDocs   = $claimDocs->WhereHas('createdBy', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'plancodedesc') {
                                $claimDocs   = $claimDocs->WhereHas('plan', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'insurances') {
                                $claimDocs   = $claimDocs->WhereHas('insurances', function ($query) use ($filtervalue) {
                                    $query->where('insuranceName', 'like', '%' . $filtervalue . '%');
                                });
                                // print_r($claimDocs->toSql());die();
                            }
                        }
                    }
                }
            }
            $claimDocs  = Helper::sorting($claimDocs, $sortby, $sortvalue);
            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $claimDocs  = $claimDocs->paginate($limit, ['*'], 'page', $page);
            $meta       = [
                'page'      => (int) $claimDocs->currentPage(),
                'perPage'   => (int) $claimDocs->perPage(),
                'total'     => (int) $claimDocs->total(),
                'totalPage' => (int) $claimDocs->lastPage()
            ];
            $claimDocs   = $claimDocs->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($claimDocs) ? $claimDocs : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function filterGetAllCalaimDocs(Request $request)
    {
        $table  = 'claim_docs';
        $claimDocs = ClaimDocs::with('insurances')->with('modifiedBy')->with('createdBy')->with('plan');

        if ($request->has('filter')) {
            $filter     = $request->input('filter');
            $claimDocs      = Helper::filterSearch($claimDocs, $table, $filter);
            foreach ($filter as $f) {
                $filterdata     = json_decode($f);
                $filterby       = $filterdata->by;
                $filtervalue    = $filterdata->value;

                if ($filterby == 'name') {
                    $claimDocs   = $claimDocs->WhereHas('createdBy', function ($query) use ($filtervalue) {
                        $query->where('name', 'like', '%' . $filtervalue . '%');
                    });
                }
            }
        }

        $limit      = $request->has('limit') ? $request->input('limit') : 20;
        $page       = $request->has('page') ? $request->input('page') : 1;
        $claimDocs   = $claimDocs->paginate($limit, ['*'], 'page', $page);
        $meta       = [
            'page'      => (int) $claimDocs->currentPage(),
            'perPage'   => (int) $claimDocs->perPage(),
            'total'     => (int) $claimDocs->total(),
            'totalPage' => (int) $claimDocs->lastPage()
        ];
        $claimDocs   = $claimDocs->toArray()['data'];

        if (!$claimDocs) {
            $error = "data not found";
            return response()->json(['status' => false, 'data' =>  $error], 409);
        } else {
            return response()->json(['status' => true, 'data' =>  $claimDocs, 'meta' => $meta], 200);
        }
    }

    public function create(Request $request)
    {

        $this->validate($request, ClaimDocs::getValidationRule());
        
        $jmlDoc = ClaimDocs::select('claim_doc_no')->where('insurance_id', ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id))
        ->orderBy('claim_doc_no', 'desc')->first();
        if (!$jmlDoc) {
            $jmlDoc = "00001";
        } else {
            $jmlDoc = substr($jmlDoc->claim_doc_no, 4);
            $jmlDoc += 1;
            $jmlDoc = str_pad($jmlDoc, 5, '0', STR_PAD_LEFT);
        }

        $docNo = $request->codeDoc . "-" . $jmlDoc;

        $claimDocs = ClaimDocs::create([
            'claim_doc_no' => $docNo,
            'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id),
            'claim_doc_name' => $request->claim_doc_name,
            'plan' => $request->plan,
            'remark' => $request->remark,
            'is_mandatory' => $request->is_mandatory,
            'created_by' => $request->id_user,
            'isOcrRead' => $request->isOcrRead,
        ]);

        return response()->json($claimDocs, 200);
    }


    public function update($id, Request $request)
    {

        $this->validate($request, ClaimDocs::getValidationRule());

            $claimDocs = ClaimDocs::where('insurance_id', ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id))
                                    ->where('id', $id)
                                    ->first();

            $claimDocs->update([
                'claim_doc_no' => $request->claim_doc_no,
                'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id),
                'claim_doc_name' => $request->claim_doc_name,
                'plan' => $request->plan,
                'remark' => $request->remark,
                'is_mandatory' => $request->is_mandatory,
                'last_modified_by' => $request->id_user,
                'isOcrRead' => $request->isOcrRead,
            ]);
        

        return response()->json($claimDocs, 200);
    }

    public function delete(Request $request, $id)
    {
        $claimDocs = ClaimDocs::where('insurance_id', ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id))->where('id', $id)->first();

        $claimDocs->delete();
        return response('Deleted Successfully', 200);
    }

    public function mass_delete(Request $request)
    {
        $claimDocs = ClaimDocs::where('insurance_id', ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id));

        $claimDocs->whereIn('id', $request->id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function import(Request $request)
    {
        $data = $request->json()->all();
        $result = [];
        $i = 0;
        $k = 0;
        $jmlDoc = ClaimDocs::select('claim_doc_no')->where('insurance_id', ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id))
            ->orderBy('claim_doc_no', 'desc')->first();
        if (!$jmlDoc) {
            $jmlDoc = "00001";
        } else {
            $jmlDoc = substr($jmlDoc->claim_doc_no, 4);
        }
        foreach ($data['data'] as $value1) {
            try {
                if (!$value1['claim_doc_no'] or $value1['claim_doc_no'] == "") {
                    $jmlDoc += 1;
                    $jmlDoc = str_pad($jmlDoc, 5, '0', STR_PAD_LEFT);
                    $docCode = substr($value1['claim_doc_name'], 0, 3);
                    $value1['claim_doc_no'] = $docCode . "-" . $jmlDoc;
                }
                $claimDocs = ClaimDocs::updateOrCreate(
                    [
                        'claim_doc_no' => $value1['claim_doc_no'],
                        'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id)
                    ],
                    [
                        'claim_doc_name' => $value1['claim_doc_name'],
                        'plan' => Helper::getKey("code", "CodeMasters", "code", $value1['plan']),
                        'remark' => $value1['remark'],
                        'is_mandatory' => $value1['is_mandatory'],
                        'created_by' => $request->auth->id,
                        'isOcrRead' => $value1['isOcrRead'],
                    ]
                );
                if ($claimDocs) {
                    $result['valid'][$i] = $value1;
                    $result['valid'][$i]['is_valid'] = $value1['is_valid'];
                    $result['valid'][$i]['msg'] = $value1['msg'];
                    $i++;
                }
            } catch (\Exception $e) {
                $result['invalid'][$k] = $value1;
                $result['invalid'][$k]['is_valid'] = false;
                $result['invalid'][$k]['msg'] = "saving data failed";
                $k++;
            }
        }
        return response()->json($result, 200);
    }

    public function excel(Request $request)
    {
        $id = $request->id;
        $tokenuser = $request->input('tokenFirebase');

        $claimDocs = ClaimDocs::with('insurances')->with('modifiedBy')
            ->with('createdBy')->with('plan');

        if ($request->auth->isInsurance) {
            $claimDocs      = $claimDocs->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id or $id !== null) {
            $claimDocs->whereIn('id', $id);
        }

        if ($tokenuser) {
            $title = "Export Claim Documents Excel.";
            $message = (string) json_encode([
                "message" => "Starting",
                "percentage" => "0%"
            ]);
            $this->notificationcontroller->PushNotif($title, $message, "", $tokenuser);
        }

        $this->exportQueue($request, $claimDocs->get(), 'Excel');

        return response()->json([
            "message" => "Start Export Claim Documents Excel."
        ], 200);
    }

    public function pdf(Request $request)
    {
        $id = $request->id;
        $tokenuser = $request->input('tokenFirebase');

        $claimDocs = ClaimDocs::with('insurances')->with('modifiedBy')
            ->with('createdBy')->with('plan');

        if ($request->auth->isInsurance) {
            $claimDocs      = $claimDocs->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id or $id !== null) {
            $claimDocs->whereIn('id', $id);
        }

        if ($tokenuser) {
            $title = "Export Claim Documents PDF.";
            $message = (string) json_encode([
                "message" => "Starting",
                "percentage" => "0%"
            ]);
            $this->notificationcontroller->PushNotif($title, $message, "", $tokenuser);
        }

        $this->exportQueue($request, $claimDocs->get(), 'Pdf');

        return response()->json([
            "message" => "Start Export Claim Documents PDF."
        ], 200);
    }

    public function exportQueue($request, $data, $type)
    {
        $tokenuser = $request->input('tokenFirebase');

        if ($tokenuser) {
            $title = "Export Claim Documents {$type}";
            $message = (string) json_encode([
                "message" => "Processing",
                "percentage" => "50%"
            ]);
            $this->notificationcontroller->PushNotif($title, $message, "", $tokenuser);
        }

        if ($type == 'Excel') {
            $this->dispatch((new QueueExport("Excel", $data->toArray(), "ClaimDocs", $tokenuser)));
        } else {
            $this->dispatch((new QueueExport("Pdf", $data, "ClaimDocs", $tokenuser)));
        }
    }
}