<?php

namespace App\Http\Controllers;

use App\MappingServices;
use App\ClaimRegHdr;
use App\RequestUW;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ProviderServicesController as ProviderServicesController;
use App\ProviderServices;
use App\RolePermissions;
use App\User;
use Illuminate\Http\Request;


class RequestUWController extends Controller
{

    private $provservice;
    private $id_admin;
    private $name_admin;
    private $id_ins;
    private $helper;

    public function __construct(Request $request)
    {
        $this->provservice = new ProviderServicesController($request);
        $this->id_admin = $request->auth->id;
        $this->name_admin = $request->auth->name;
        $this->id_ins = $request->auth->insurance_id;
        $this->helper = new Helper;
    }

    public function addRequestUW(Request $request)
    {
        try {
            $mid = $request['mid'];
            $provider_type = $request['provider_type'];
            $claimregno = $request['claimregno'];
            $validateSn = null;
            foreach ($request['data'] as $dataservice) {
                try {
                    $id_parent_service = Helper::getKey("id", "ProviderServices", "service_name", $dataservice['parent_service_id']);
                } catch (\Exception $e) {
                    $id_parent_service = null;
                }

                try {
                    $service_name = Helper::getKey("id", "ProviderServices", "service_name", $dataservice['service_name']);
                } catch (\Exception $e) {
                    $service_name = null;
                }

                $validateSn = RequestUW::where('service_name', $dataservice['service_name'])->first();
                if (!$validateSn) {
                    $requestUW = RequestUW::create([
                        'mid' => $mid,
                        'parent_service_id' => $id_parent_service,
                        'id_prov_service' =>  $service_name,
                        'id_iziservice_code' => null,
                        'service_name' => $dataservice['service_name'],
                        'provider_type' => $provider_type,
                        'insurance_id' => $this->id_ins,
                        'id_benefit' => null,
                        'claimregno' => $claimregno
                    ]);
                    $this->sendNotifToUW($request['claimregno']);
                } else {
                    $lastclaimregno = $validateSn['claimregno'];
                    $validateSn = $validateSn->update(['claimregno' => $lastclaimregno . "|" . $request['claimregno']]);
                }
            }

            

            return ' add request to UW succeed.';
        } catch (\Exception $msg) {
            // return ' add request to UW failed.';
            return $msg;
        }
    }

    public function getAdminUW()
    {
        $roleid = RolePermissions::where('permission', 'Pages.Transaction.MappingIziservice.MissingService')->where('insurance_id', $this->id_ins)->get('role_id');
        $idroles = [];
        foreach ($roleid as $key => $value) {
            array_push($idroles, $value->role_id);
        }
        $cek_permission = User::whereIn('role_user', $idroles)->first();
        // $idanalysts=[];
        // foreach ($cek_permission as $key => $value) {
        //     array_push($idanalysts,$value->id);
        // }
        return $cek_permission;
    }

    public function sendNotifToUW($claimregno)
    {
        $notif = [
            'to' => $this->helper->getUserByRolePermission('Pages.Transaction.MappingIziservice.MissingService'),
            'title' => "Missing Benefit Mapping",
            'type' => "MISSING_SERVICE_MAPPING",
            'requester'  => $this->id_admin,
            'requestername'  => $this->name_admin,
            'message'   => "You have request to map missing benefit(s) for Claim Number " . $claimregno . ", please help to map the data",
            'payload' => [
                'claimregno' => $claimregno
            ]
        ];

        $this->helper->sendNotifRequest($notif);
        
        if ($uwreq) {
            return "request uw succeed";
        } else {
            return "request uw failed";
        }
    }

    public function sendNotifToAnalyst($claimregno)
    {
        $claimno = explode("|", $claimregno);

        foreach ($claimno as $claimnoo) {
            $cekberes = RequestUW::where('claimregno', 'like', '%' . $claimnoo . '%')->where('is_mapped', '0')->get();
            $cekberes = $cekberes->count();

            if ($cekberes == 0) {

                $notif = [
                    'to' => $analyst_data,
                    'title' => "Missing Benefit Mapping",
                    'type' => "MISSING_BENEFIT_MAPPING",
                    'requester'  => $this->id_admin,
                    'requestername'  => $this->name_admin,
                    'message'   => "Missing Benefit(s) on Claim number " . $claimregno . ", has been mapped by Underwriting, please continue to analyze",
                    'payload' => [
                        'claimregno' => $claimregno
                    ]
                ];
        
                $this->helper->sendNotifRequest($notif);
                // }

                if ($uwfeedback) {
                    return "feedback to analyst succeed";
                } else {
                    return "feedback to analyst failed";
                }
            }
        }
    }

    public function getRequestUW(Request $request, $id = null)
    {
        $sortby         = $request->input('sortby');
        $sortvalue      = $request->input('sortvalue');
        $status         = false;
        $error          = "data not found";
        $getrequw = RequestUW::where('insurance_id', $this->id_ins)->where('is_mapped', '0');

        if ($id) {
            $getrequw   = $getrequw->where('id', $id)->first();

            if ($getrequw) {
                $status     = true;
                $error      = null;
            }
        } else {
            if ($getrequw->count()) {
                $status         = true;
                $error          = null;
            }

            $getrequw   = Helper::sorting($getrequw, $sortby, $sortvalue);
            $limit          = $request->has('limit') ? $request->input('limit') : 20;
            $page           = $request->has('page') ? $request->input('page') : 1;
            $getrequw   = $getrequw->paginate($limit, ['*'], 'page', $page);
            $meta           = [
                'page'          => (int) $getrequw->currentPage(),
                'perPage'       => (int) $getrequw->perPage(),
                'total'         => (int) $getrequw->total(),
                'totalPage'     => (int) $getrequw->lastPage()
            ];
            $getrequw   = $getrequw->toArray()['data'];
        }


        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getrequw) ? $getrequw : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function processRequestUW(Request $request)
    {
        try {
            $msg = "add Mapping Service Failed. ";
            $provservicedata = new Request;
            $provservicedata->replace([
                'parent_service_id' => $request['parent_service_id'],
                'service_code' => $request['prov_service_code'],
                'service_name' => $request['prov_service_name'],
                'provider_type' => $request['provider_type'],
                'insurance_id' => $this->id_ins,
                'mid' => $request['mid'],
                'created_by' => $this->id_admin
            ]);
            try {

                $ps = $this->provservice->addService($provservicedata);

                if ($ps) {
                    $dataMapped = $ps->getData()->data;
                    // $dataMap = $ps->json()->all();

                    // $dataMapped = $dataMap['data'];
                    $ms = MappingServices::create([
                        'id_prov_service' => $dataMapped->id,
                        'id_iziservice_code' => $request['id_iziservice_code'],
                        'created_by' => $this->id_admin,
                        'insurance_id' => $this->id_ins
                    ]);

                    $updateUW = RequestUW::where('id', $request['id_req_uw'])->first();
                    $claimregno = $updateUW->claimregno;
                    $updateUW = $updateUW->update([
                        'parent_service_id' => $dataMapped->parent_service_id,
                        'id_prov_service' => $dataMapped->id,
                        'id_iziservice_code' => $request['id_iziservice_code'],
                        'is_mapped' => '1'
                    ]);

                    $this->sendNotifToAnalyst($claimregno);
                }
            } catch (\Exception $e) {
                $msg .= "add service failed. ";
                throw new \Exception();
            }

            return ' add mapping service succeed.';
        } catch (\Exception $msg) {
            return ' add mapping service failed.';
        }
    }
}
