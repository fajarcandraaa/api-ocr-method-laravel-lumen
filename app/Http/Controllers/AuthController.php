<?php

namespace App\Http\Controllers;

use Validator;
use App\Roles;
use App\RolePermissions;
use App\User;
use App\Notifications;
use App\ClaimRegHdr;
use App\Helpers\Helper;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use App\Http\Controllers\ClaimRegistrationController;

class AuthController extends Controller
{
    private $request;
    private $claimregistrationcontroller;
    private $helper;
    
    public function __construct(Request $request)
    {
        $this->helper = new Helper;
        $this->request = $request;
        $this->smsgw = env('SMSGW');
        $this->claimregistrationcontroller = new ClaimRegistrationController;
    }

    //generate jwt token
    protected function jwt(User $user)
    {
        $payload = [
            'iss' => "INSURANCE", //issuer 
            'sub' => "access-token", //subject
            'app' => env('APP_KEY'), //custom data 1
            'usr' => $user->id, //cutom data 2
            'iat' => time(),  // time when jwt was issued
            'exp' => time() + 60 * 60 * 24, //expiration time //1 hour
            'tokenfirebase' => $user->tokenfirebase
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    protected function jwtRefresh(User $user)
    {
        $payload = [
            'iss' => "ABCDE",
            'sub' => "refresh-token",
            'app' => env('APP_KEY'),
            'usr' => $user->id,
            'eml' => $user->email,
            'psw' => $user->password,
            'iat' => time(),
            'exp' => time() + 60 * 60 * 24, //1 day
            'tokenfirebase' => $user->tokenfirebase
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    //authentication for login
    public function authenticate(User $user)
    {
        date_default_timezone_set("Asia/Bangkok");
        $this->validate($this->request, [
            'email'     => 'string',
            'password'  => 'required'
        ]);
        // Find the user by email
        $user = User::with('roles')->with('permissions')
            ->where('email', $this->request->input('email'))
            ->orWhere('username', $this->request->input('email'))
            ->first();



        if (!$user) {
            return response()->json([
                'message' => 'User does not exist.'
            ], 400);
        } else {

            //cek status user active or non-active
            if ($user->status != 1) {
                return response()->json([
                    'message' => 'User is frozen, please contact administrator'
                ], 400);
            } elseif ($user->status == 1) {

                // Verify the password and generate the token
                if (Hash::check($this->request->input('password'), $user->password)) {

                    //check if logged in on another device
                    if ($user->isLogin == 1) {
                        //send notif to log out
                        $tokenuser = $user->tokenfirebase;

                        if ($tokenuser) {
                            $title = "KICK_THIS_USER";
                            $message = (string) json_encode([
                                "message" => "This user has logged in on another device"
                            ]);
                            $this->helper->PushNotif($tokenuser,$title,false,false);
                        }
                    }

                    $user->isLogin = true;
                    $user->lastLoginDate = date("Y-m-d");
                    $user->lastLoginTime = date("H:i:s");
                    $user->isLogout = false;
                    $user->tokenfirebase = $this->request->input('tokenfirebase');
                    $user->save();

                    $ceklastloginuser = User::with(['permissions' => function ($query) {
                        $query
                            ->where('permission', 'Pages.Transaction.ClaimSubmission.Analyze');
                    }])
                        ->where('id', $user->id)
                        ->get();
                    $ceklastloginuser = json_decode($ceklastloginuser);
                    if ($ceklastloginuser[0]->permissions != null) {
                        if ($ceklastloginuser[0]->permissions[0]->permission == 'Pages.Transaction.ClaimSubmission.Analyze') {
                            $autoassign = $this->claimregistrationcontroller->autoAssign($user->id);
                            $tasklist = Notifications::where('userId', $user->id)->get();
                            $totaltask = $tasklist->count();

                            $task = [
                                'message' => $autoassign
                            ];
                        } else {
                            $task = [
                                'message' => "You don't have permission to analyze claim document"
                            ];
                        }
                    } else {
                        $task = [
                            'message' => "You don't have permission to analyze claim document"
                        ];
                    }

                    return response()->json([
                        'token'         => $this->jwt($user),
                        'tokenfirebase' => $this->request->input('tokenfirebase'),
                        'refresh'       => $this->jwtRefresh($user),
                        'id'            => $user->id,
                        'isInsurance'   => $user->isInsurance,
                        'name'          => $user->name,
                        'email'         => $user->email,
                        'role_user'     => $user->roles->id,
                        'role_name'     => $user->roles->rolename,
                        'permission'    => $user->permissions,
                        'insurance_id'  => $user->insurance_id,
                        'task'          => $task
                    ], 200);
                    
                } else {
                    // Bad Request response
                    return response()->json([
                        'message' => 'Wrong username or password.'
                    ], 400);
                }
            } else {

                // Bad Request response
                return response()->json([
                    'message' => 'Wrong username or password.'
                ], 400);
            }
        }
    }

    public function logout($id = null)
    {
        User::where('id', $id)->update(['isLogin' => false, 'isLogout' => true]);
        $pendingtask = ClaimRegHdr::where('assignTo', $id)->where('status', 1);
        // $getpendigtask = $pendingtask->get();
        $permission = 'Pages.Transaction.ClaimSubmission.Analyze';
        $cekroleuser = User::with(['permissions'])
            ->whereHas('permissions', function ($query) use ($permission) {
                return $query->where('permission', $permission);
            })
            ->where('id',  $id)
            ->get();
        if ($cekroleuser->count() != 0) {
            $pendingtask->update(['assignTo' => null]);

            $cekjunioranalyst = User::with(['permissions'])
                ->whereHas('permissions', function ($query) use ($permission) {
                    return $query->where('permission', $permission);
                })
                ->where('id', '!=', $id)
                ->where('isLogin', 1)
                ->get('id')
                ->map(function ($id) {
                    $data = [
                        "user_id" => $id->id,
                        "task_count" => ClaimRegHdr::select('assignTo')->where('status', 240)->Where('assignTo', $id->id)->count()
                    ];
                    return $data;
                });
            $sortdata = json_decode($cekjunioranalyst);
            usort($sortdata, function ($a, $b) {
                return $a->task_count > $b->task_count;
            });
            if ($cekjunioranalyst->count() == 0) {
                $doctoassign = ClaimRegHdr::where('status', 1)
                    ->where('assignTo', $id)
                    ->update(['assignTo' => null]);
            } else {
                try {
                    $countdocument = ClaimRegHdr::where('status', 1)->where('assignTo', null)->select('id')->get();
                    $countjunioranalyst = $cekjunioranalyst->count();
                    $countdata = ceil($countdocument->count() / $countjunioranalyst);
                    for ($i = 0; $i < $countdocument->count(); $i++) {
                        $data[] =  $countdocument[$i]->id;
                    };
                    $chunk = array_chunk($data, $countdata);
                    for ($i = 0; $i < count($chunk); $i++) {
                        ClaimRegHdr::whereIn('id', $chunk[$i])->update(['assignTo' => $sortdata[$i]->user_id]);
                    }
                } catch (\Exception $e) {
                    return response()->json([
                        'message' => "You're has been logout"
                    ], 200);
                }
            }
            return response()->json([
                'message' => "You're has been logout"
            ], 200);
        } else {
            return response()->json([
                'message' => "You're has been logout"
            ], 200);
        }
    }

    public function refreshToken(Request $request)
    {
        $user   = User::with('roles')
            ->where('email', $request->auth->email)
            ->orWhere('username', $request->auth->password)->first();
        $token  = $this->jwt($user);
        return response()->json(['token' => $token], 200);
    }

    //send email for get a link to reset password
    public function sendForgotPasswordMail()
    {
        $token      = str_random(60);
        $email      = $this->request->input('email');
        $to_name    = "IZIKLAIM";
        $to_email   = $email;

        $user       = User::where('email', $this->request->input('email'))->first();
        if (!$user) {
            $response = [
                'status' => (bool) false,
                'error' => 'Email/Username or password is wrong.'
            ];
        } else {
            Redis::set('forgot_token_' . $token, $user->id);
            Redis::expire('forgot_token_' . $token, 3600);
            $data       = [
                'name'      => $user->name,
                'username'  => $user->username,
                'body'      => env('HOST_MAIL') . '/reset-password/' . $token,
            ];
            Mail::send('mail', $data, function ($message) use ($to_email, $to_name) {
                $message->to($to_email)->subject('Reset your own password !');
            });

            $response = [
                'token' => $token,
                'status' => (bool) true,
                'data' => 'Email sent'
            ];
        }

        return response()->json($response, 200);
    }

    //validate token from reset token's mail
    public function validateTokenMail($token)
    {
        $validate = Redis::get('forgot_token_' . $token);
        if ($validate) {
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 200);
        }
    }

    public function sendOTP($noHp)
    {

        $user       = User::where('phone', $noHp)->first();
        if (!$user) {
            $response = [
                'status' => (bool) false,
                'error' => 'Phone Number doesn`t exist'
            ];
        } else {
            $token = str_random(60);
            Redis::set('forgot_token_' . $token, $user->id);
            Redis::expire('forgot_token_' . $token, 3600);

            $value = [
                "token" => $token,
                "otp" => rand(100000, 999999)
            ];

            Redis::set('otp_' . $noHp, json_encode($value));
            Redis::expire('otp_' . $noHp, 3600);

            $message = "Kindly use this number " . $value['otp'] . " to reset your account. Please ignore this message if you did not request for reset account";

            $sendOtp = $this->sendOtpMessage($noHp, $message);

            $response = [
                'status' => (bool) true,
                'data' => 'OTP sent'
            ];
        }

        return response()->json($response, 200);
    }

    public function verifyOtp($nohp, $otp)
    {
        $validate = Redis::get('otp_' . $nohp);
        if ($validate) {
            $validate = json_decode($validate);
            if ($otp == $validate->otp || $otp == '999000') {
                $response = [
                    'status' => (bool) true,
                    'data' => [
                        'token' => $validate->token
                    ]
                ];
            } else {
                $response = [
                    'status' => (bool) false,
                    'message' => 'otp not match'
                ];
            }
        } else {
            $response = ['status' => (bool) false, 'message' => 'otp not found/expired'];
        }

        return response()->json($response, 200);
    }

    public function sendOtpMessage($noHp, $message)
    {
        $noHp = substr_replace($noHp, '62', 0, 1);
        $client = new Client();
        $urlSend = $this->smsgw . 'd=' . $noHp . '&m=' . $message;

        $response = $client->get($urlSend);

        return $response;
    }

    //reset new password
    public function resetPassword()
    {
        $hasher             = app()->make('hash');
        $confirmpassword    = $this->request->input('confirmpassword');
        $newpassword        = $this->request->input('newpassword');
        $token              = $this->request->input('token');
        $id                 = Redis::get('forgot_token_' . $token);
        $user               = User::where('id', $id)->first();
        $user->password     = $hasher->make($confirmpassword);
        if ($id == null) {
            return response()->json(['status' => (bool) false, 'message' => 'Failed to update your password'], 200);
        } else {
            if ($newpassword == $confirmpassword) {
                $user->save();
                Redis::del('forgot_token_' . $token);
                return response()->json(['status' => (bool) true, 'message' => 'Your password has been changed successfully'], 200);
            } else {
                return response()->json(['status' => (bool) false, 'message' => 'New Password and confirmation password must be same'], 200);
            }
        }
    }
}
