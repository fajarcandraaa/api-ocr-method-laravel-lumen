<?php

namespace App\Http\Controllers;

use App\SysConfigs;
use App\SysConfigDtls;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class SysConfigController extends Controller
{

    public function getCodes(Request $request)
    {
        $status = true;
        $error = "";


        $getconfig = SysConfigs::with('sysconfdtl');

        if ($request->has('filter')) {
            $table     = 'sysconfig';
            $filter    = $request->input('filter');
            $getconfig   = Helper::filterSearch($getconfig, $table, $filter);
            foreach ($filter as $f) {
                $filterdata     = json_decode($f);
                $filterby       = $filterdata->by;
                $filtervalue    = $filterdata->value;

                if (is_array($filtervalue)) {
                    if ($filterby == 'dtlJoin') {
                        $getconfig = $getconfig->whereHas('sysconfdtl', function ($query) use ($filtervalue) {
                            $query->whereIn('dtlJoin', $filtervalue);
                        });
                    }elseif ($filterby == 'dtlName') {
                        $getconfig = $getconfig->whereHas('sysconfdtl', function ($query) use ($filtervalue) {
                            $query->whereIn('dtlName', $filtervalue);
                        });
                    }
                } else {
                    if ($filterby == 'dtlJoin') {
                        $getconfig = $getconfig->whereHas('sysconfdtl', function ($query) use ($filtervalue) {
                            $query->where('dtlJoin', 'like', '%' . $filtervalue . '%');
                        });
                    }elseif ($filterby == 'dtlName') {
                        $getconfig = $getconfig->whereHas('sysconfdtl', function ($query) use ($filtervalue) {
                            $query->where('dtlName', 'like', '%' . $filtervalue . '%');
                        });
                    }
                }
            }
        }

        $getconfig=$getconfig->get();

        if (!$getconfig->count()) {
            $status = false;
            $error = "data not available";
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getconfig) ? $getconfig : null),
            "error"     => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }
}
