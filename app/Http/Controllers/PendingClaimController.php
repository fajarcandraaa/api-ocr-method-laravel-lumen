<?php

namespace App\Http\Controllers;

use App\ClaimRegHdr;
use App\PendingClaim;
use App\PendingClaimConvert;
use App\Notifications;
use App\User;
use Illuminate\Http\File as File;
use Illuminate\Support\Facades\Storage as Storage;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HistoryController;
use Google\Rpc\Help;
use Illuminate\Http\Request;
use App\Jobs\ClaimRegJob;
use App\Jobs\ConvertDocSupport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class PendingClaimController extends Controller
{
    private $historycontroller;
    private $notificationcontroller;

    public function __construct()
    {
        $this->historycontroller = new HistoryController;
        $this->notificationcontroller = new NotificationController;

    }

    public function runReQueue($insurance_id){
        $getAllPendingClaims = PendingClaim::where('insurance_id',$insurance_id);
        $getAllPendingClaimregno = $getAllPendingClaims->distinct()->get('claimregno');
        foreach ($getAllPendingClaimregno as $claimregno) {
            $dataa['insurance_id'] = $insurance_id;
            $dataa['status_type'] = 'ACTIVE';
            $checkbalance = $this->historycontroller->getBalance($dataa);

            $getPendingClaimData_Main = PendingClaim::where('claimregno',$claimregno->claimregno)
            ->where('category','main')->get();

            $getPendingClaimData_Sup = PendingClaim::where('claimregno',$claimregno->claimregno)
            ->where('category','support');

            $getPendingClaimData_Sup_supportDoctypes1 = $getPendingClaimData_Sup->select('supportDoctypes')->get();
            $getPendingClaimData_Sup_supportDoctypes = [];
            $n=0;
            foreach ($getPendingClaimData_Sup_supportDoctypes1 as $supportDoctypes) {
                $getPendingClaimData_Sup_supportDoctypes[$n] = $supportDoctypes->supportDoctypes;
                $n++;
            }
            
            $getPendingClaimData_Sup_sourceOrig1 = $getPendingClaimData_Sup->select('sourceOrig')->get();
            $getPendingClaimData_Sup_sourceOrig = [];
            $n=0;
            foreach ($getPendingClaimData_Sup_sourceOrig1 as $sourceOrig) {
                $getPendingClaimData_Sup_sourceOrig[$n] = $sourceOrig->sourceOrig;
                $n++;
            }

            $getPendingClaimData_Sup = PendingClaim::where('claimregno',$claimregno->claimregno)
            ->where('category','support')->get();

            //get all convert path
            $getConvertsMain = PendingClaimConvert::where('claimregno',$claimregno->claimregno)
            ->where('category','main')->get();
            $collectConvertsMain = [];
            $loop = 0;
            foreach ($getConvertsMain as $dbpath) {
                $collectConvertsMain[$loop] = $dbpath->path;
                $loop++;
            }

            $getConvertsSup = PendingClaimConvert::where('claimregno',$claimregno->claimregno)
            ->where('category','support')->get();
            $collectConvertsSup = [];
            $loop = 0;
            foreach ($getConvertsSup as $dbpath) {
                $collectConvertsSup[$loop] = $dbpath->path;
                $loop++;
            }
            
            // $totalpages = PendingClaim::where('claimregno',$claimregno->claimregno)
            // ->where('category','main')->first();
            // $totalpages = $totalpages->totalpages;

            $totalpages = PendingClaimConvert::where('claimregno',$claimregno->claimregno)->get();
            $totalpages = $totalpages->count();

            if ($checkbalance >= $totalpages) {
                $balance_active = app('redis')->get("balance_{$dataa['insurance_id']}_ACTIVE");

                $abs_qty = abs($totalpages);
                $balance_active = abs($balance_active) - $abs_qty;

                app('redis')->set("balance_{$dataa['insurance_id']}_ACTIVE", $balance_active);
                app('redis')->expire("balance_{$dataa['insurance_id']}_ACTIVE", 900);

                //dispatch maindocs
                $this->dispatch((new ClaimRegJob('Upload', null, $getPendingClaimData_Main[0]->category, null, null, $getPendingClaimData_Main[0]->destinationOrig, $getPendingClaimData_Main[0]->sourceOrig, $getPendingClaimData_Main[0]->destinationConv, $collectConvertsMain, $getPendingClaimData_Main[0]->basename)));

                //dispatch supportdocs
                $this->dispatch((new ClaimRegJob('Upload', $getPendingClaimData_Sup_supportDoctypes, 'support', null, null, $getPendingClaimData_Sup[0]->destinationOrig, $getPendingClaimData_Sup_sourceOrig, $getPendingClaimData_Sup[0]->destinationConv, $collectConvertsSup, $getPendingClaimData_Sup[0]->basename)));

            }
        }

        return true;

    }
}