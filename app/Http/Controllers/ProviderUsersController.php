<?php
namespace App\Http\Controllers;

use Auth;
use App\ProviderUsers;
use App\Exports\UserManagementExport;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AuthController;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Maatwebsite\Excel\Facades\Excel;

class ProviderUsersController extends Controller
{
    public function registerProv(Request $request)
    {
        $request->request->add(['provuserid' => str_random(10), 'createBy' => $request->auth->name]);
        $this->validate($request, [
            'username'          => 'string',
            'email'             => 'required|email',
            'mid'               => 'required|string',
            'providertype'      => 'string',
            'provname'          => 'string',
            'last_modifies_by'  => 'string',
            'division'          => 'required|string',
            'status'            => 'required|integer',
            'phonenumber'       => 'required|string',
            'insurance_id'      => 'integer'
        ]);
            // print_r($request);die();
        try {
            $provuser   = new ProviderUsers;
            $params     = $request->all();
            $provuser->fill($params);

            $cekprovuser    = ProviderUsers::where('email', $request->input('email'))
                                ->first();
            
            if ($cekprovuser) {
                return response()->json(['message' => 'Username or email alredy exist!'], 409);
            } else {
                // print_r($cekprovuser);die();
                $provuser->save();
                return response()->json(['providerusers' => $provuser, 'message' => 'CREATED'], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Provider User Registration Failed'], 409);
        }
    }

    public function showUserProviders(Request $request, $id = null)
    {
        $sortby         = $request->input('sortby');
        $sortvalue      = $request->input('sortvalue');
        $status         = false;
        $error          = "data not found";
        $getprovusers   = ProviderUsers::with('biz_providers.compprov','insurance_id');

        if ($request->auth->isInsurance) {
            $getprovusers   = $getprovusers->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id) {
            $getprovusers   = $getprovusers->where('id', $id)->first();

            if ($getprovusers) {
                $status     = true;
                $error      = null;
            }
        } else {
            if ($request->has('keyword')) {
                $keyword        = $request->keyword;
                $where          = array('provuserid', 'username', 'email', 'mid', 'providertype', 'provname', 'created_by', 'last_modifies_by', 'division', 'status');
                $getprovusers   = Helper::dynamicSearch($getprovusers, $where, $keyword);
                $getprovusers   = $getprovusers->whereHas('biz_providers.compprov', function ($query) use ($keyword) {
                    $query->where('PhoneNo', 'like', '%'.$keyword.'%');
                })->orWhereHas('biz_providers', function ($query) use ($keyword) {
                    $query->where('phone', 'like', '%'.$keyword.'%');
                })->orWhereHas('insurance_id', function ($query) use ($keyword) {
                    $query->where('insuranceName', 'like', '%'.$keyword.'%');
                });
                
                // if (!$getprovusers->count()) {
                //     $getprovusers   = $getprovusers->whereHas('biz_providers.compprov', function ($query) use ($keyword) {
                //         $query->where('PhoneNo', 'like', '%'.$keyword.'%');
                //     })->orWhereHas('biz_providers', function ($query) use ($keyword) {
                //         $query->where('phone', 'like', '%'.$keyword.'%');
                //     });
                // }
                $status         = true;
                $error          = null;
                $getprovusers   = Helper::sorting($getprovusers, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table          = 'provider_users';
                    $filter         = $request->input('filter');
                    $getprovusers   = Helper::filterSearch($getprovusers,$table,$filter);
                    foreach ($filter as $f) {
                        $filterdata         = json_decode($f);
                        $filterby           = $filterdata->by;
                        $filtervalue        = $filterdata->value;
                        if (is_array($filtervalue)) {
                            if ($filterby == 'phone') {
                                $getprovusers   = $getprovusers->whereHas('biz_providers', function ($query) use ($filtervalue) {
                                    $query->whereIn('phone', $filtervalue);
                                });
                            } elseif ($filterby == 'phoneno') {
                                $getprovusers   = $getprovusers->whereHas('biz_providers.compprov', function ($query) use ($filtervalue) {
                                    $query->whereIn('PhoneNo', $filtervalue);
                                });
                            } elseif ($filterby == 'name') {
                                $getprovusers   = $getprovusers->whereHas('biz_providers', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'compName') {
                                $getprovusers   = $getprovusers->whereHas('biz_providers.compprov', function ($query) use ($filtervalue) {
                                    $query->whereIn('provider_name', $filtervalue);
                                });
                            } elseif ($filterby == 'insuranceId') {
                                $getprovusers   = $getprovusers->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                    $query->whereIn('insuranceName', $filtervalue);
                                });
                            }
                        } else {
                            if ($filterby == 'phone') {
                                $getprovusers   = $getprovusers->whereHas('biz_providers', function ($query) use ($filtervalue) {
                                    $query->where('phone', 'like', '%'.$filtervalue.'%');
                                });
                            } elseif ($filterby == 'phoneno') {
                                $getprovusers   = $getprovusers->whereHas('biz_providers.compprov', function ($query) use ($filtervalue) {
                                    $query->where('PhoneNo', 'like', '%'.$filtervalue.'%');
                                });
                            } elseif ($filterby == 'name') {
                                $getprovusers   = $getprovusers->whereHas('biz_providers', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%'.$filtervalue.'%');
                                });
                            } elseif ($filterby == 'compName') {
                                $getprovusers   = $getprovusers->whereHas('biz_providers.compprov', function ($query) use ($filtervalue) {
                                    $query->where('provider_name', 'like', '%'.$filtervalue.'%');
                                });
                            } elseif ($filterby == 'insuranceId') {
                                $getprovusers   = $getprovusers->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                    $query->where('insuranceName', 'like', '%'.$filtervalue.'%');
                                });
                            }
                        }
                        
                    }
                }
            }

            $getprovusers   = Helper::sorting($getprovusers, $sortby, $sortvalue);
            $limit          = $request->has('limit') ? $request->input('limit') : 20;
            $page           = $request->has('page') ? $request->input('page') : 1;
            $getprovusers   = $getprovusers->paginate($limit,['*'],'page',$page);
            $meta           = [
                'page'          => (int) $getprovusers->currentPage(),
                'perPage'       => (int) $getprovusers->perPage(),
                'total'         => (int) $getprovusers->total(),
                'totalPage'     => (int) $getprovusers->lastPage()
            ];
            $getprovusers   = $getprovusers->toArray()['data'];
        }

        // if ($getprovusers[0]['providertype'] == 'Provider') {
        //     $getprovusers[0]['provname'] = $getprovusers[0]['biz_providers']['name'] ;
        // } elseif ($getprovusers[0]['providertype'] == 'Company') {
        //     $getprovusers[0]['provname'] = $getprovusers[0]['biz_providers']['compprov']['provider_name'] ;
        // }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getprovusers) ? $getprovusers : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function updateProvUser(Request $request, $id)
    {
        $provtype       = $request->input('provtype');
        $request->request->add(['last_modifies_by' => $request->auth->name]);
        $this->validate($request, [
            'provuserid'        => 'string',
            'username'          => 'string',
            'email'             => 'email',
            'mid'               => 'string',
            'providertype'      => 'string',
            'provname'          => 'string',
            'created_by'        => 'string',
            'division'          => 'string',
            'status'            => 'integer',
            'insurance_id'      => 'integer'
        ]);
        
        $data   = ProviderUsers::find($id);

        if ($data != null) {
            $cekprovuser = ProviderUsers::where(function ($query) use ($request) {
                            $query->where('email', $request->input('email'));
                            })->where('id', '!=', $id)->first();
            if ($cekprovuser) {
                return response()->json(['message' => 'Username or email alredy exist!'], 409);
            } else {
                $params = $request->all();
                $data->fill($params);
                $data->save();
                return response()->json(['status' => (bool) true, 'message' => 'Your data has been update'], 200);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
        }
    }

    public function deleteProvUser(Request $request, $id)
    {
        $provuser   = ProviderUsers::find($id);
        if ($provuser) {
            $provuser->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function massDeleteProvUser(Request $request)
    {
        $provuser   = ProviderUsers::whereIn('id', $request->id);
        if ($provuser) {
            $provuser->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

}