<?php
namespace App\Http\Controllers;

use App\Subdistricts;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class SubdistrictsController extends Controller
{
    
    public function getSubdistricts($district_id=null)
    {
        $status = true;
        $error = "";
        if($district_id){
            $subdistricts = Subdistricts::with('district')->where('district_id',$district_id)->get();
            }else{
                $subdistricts = Subdistricts::with('district')->get();
            }
        
        if(!$subdistricts->count()){
            $status = false;
            $error = "data not available";
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($subdistricts) ? $subdistricts : null),
            "error"     => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

}