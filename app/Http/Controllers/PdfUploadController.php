<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Exports\UserManagementExport;
use App\Helpers\Helper;
use App\Http\Controllers\AuthController;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\PdfToText\Pdf;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\File;
use Response;
class PdfUploadController extends Controller
{
    public function uploadFile(Request $request)
    {
        $pdfdata    = $request->file('pdf');
        $pdfname    = time().".".$pdfdata->getClientOriginalExtension();
        Storage::disk('minio')->putFileAs('Medlinx/registerTemplate', new File($pdfdata),$pdfname);
        $file       = Helper::uploadPdf($pdfdata,"assets/pdf"); //server
        // $file = Helper::uploadPdf($pdfdata, storage_path('app/pdf')); //windows

        $pdf        = new \Spatie\PdfToImage\Pdf($file);
        $filename  = basename($file);
        $total      = $pdf->getNumberOfPages();
        $image      = [];

        foreach (range(1, $total) as $pageNumber) {
            $originalimagePath  = storage_path('app/convert/').$filename.'-page'.$pageNumber.'.png';
            $img['rel_path']    = '/storage/convert/'.$filename.'-page'.$pageNumber.'.png';
            $img['pageNumber']  = $pageNumber;
            $pdf->setPage($pageNumber)
            ->setOutputFormat('png')
            ->saveImage($originalimagePath);

            //-------------------------------upload original image to minIO---------------------
            Storage::disk('minio')->putFileAs('Medlinx/registerTemplate/originalImage',new File($originalimagePath),$filename.'-page'.$pageNumber.'.png');
            //----------------------------------------------------------------------------------
            $image[]    = $img;
        }

        //--------------delete image file from local directory----------------- 
        unlink($originalimagePath);
        unlink($file);
        //---------------------------------------------------------------------

        $data = [
            'total'     => $pdf->getNumberOfPages(),
            'data'      => $image
        ];

        $response   = [
            "status"    => (bool) true,
            "data"      => $data
        ];

        return response()->json($response);
    }
}