<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\DiagnosisExport;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;

class DiagnosisController extends Controller
{
    private $id_admin;
    public function __construct(Request $request)
    {
        $this->id_admin = $request->auth->id;
        $this->nama_admin = $request->auth->name;
    }
    public function newDiagnosis(Request $request)
    {
        $request->request->add([
            'diagnosisCode' => str_random(10),
            'createBy' => $request->auth->name
        ]);

        $this->validate($request, [
            'diagnosisCode'     => 'required|string',
            'diagnosisName'     => 'required|string',
            'scientificDesc'    => 'string',
            'description'       => 'string',
            'standardFee'       => 'required|between:0,9.9',
            'standardTreatment' => 'integer',
            'poly'              => 'string',
            'isExclusionPolicy' => 'string',
            'isPreExisting'     => 'string',
            'insurance_id'      => 'integer'
        ]);

        try {
            $diag       = new Diagnosis;
            $params     = $request->all();
            $diag->fill($params);


            $cekdiag    = Diagnosis::where('diagnosisCode', $params['diagnosisCode'])
                ->orWhere('diagnosisName', $params['diagnosisName'])
                ->first();
            if ($cekdiag) {
                return response()->json(['message' => 'Diagnosis code/name already exist !'], 409);
            } else {
                $diag->save();
                return response()->json(['diagnosis' => $diag, 'message' => 'CREATED'], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Diagnosis Registration Failed!'], 409);
        }
    }

    public function allDiagnosis(Request $request, $id = null)
    {
        $sortby         = $request->input('sortby');
        $sortvalue      = $request->input('sortvalue');
        $status         = false;
        $error          = "data not found";
        $getdiagnosis   = new Diagnosis;

        // if ($request->auth->isInsurance) {
        //     $getdiagnosis   = $getdiagnosis->where('insurance_id', $request->auth->insurance_id);
        // }

        if ($id) {
            $getdiagnosis   = $getdiagnosis->where('id', $id)->first();

            if ($getdiagnosis) {
                $status     = true;
                $error      = null;
            }
        } else {
            if ($request->has('keyword')) {
                $keyword        = $request->keyword;
                $wheres         = array("diagnosisCode", "diagnosisName", "scientificDesc", "description");
                $getdiagnosis   = Helper::dynamicSearch($getdiagnosis, $wheres, $keyword);
                $getdiagnosis   = $getdiagnosis->orWhereHas('insurance_id', function ($query) use ($keyword) {
                                    $query->where('insuranceName', 'like', '%' . $keyword . '%');
                                });

                // if (!$getdiagnosis->count()) {
                //     $getdiagnosis   = Diagnosis::with('poly');
                //     $getdiagnosis   = $getdiagnosis->whereHas('poly', function ($query) use ($keyword) {
                //         $query->where('codeType', 'like', '%' . $keyword . '%');
                //     });
                // }
                $status         = true;
                $error          = null;
                $getdiagnosis   = Helper::sorting($getdiagnosis, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table          = 'diagnosis';
                    $filter         = $request->input('filter');
                    $getdiagnosis   = Helper::filterSearch($getdiagnosis, $table, $filter);
                    // if ($request->has('excludeId')) {
                    //     $getdiagnosis   = $getdiagnosis->whereNotIn('id',$request->input('excludeId'));
                    // }
                    foreach ($filter as $f) {
                        $filterdata     = json_decode($f);
                        $filterby       = $filterdata->by;
                        $filtervalue    = $filterdata->value;

                        if ($filterby == "codeorname") {
                            $getdiagnosis = $getdiagnosis->where("diagnosisCode", 'like', '%' . $filtervalue . '%')
                                            ->orWhere("diagnosisName", 'like', '%' . $filtervalue . '%');
                        } else {
                            if (is_array($filtervalue)) {
                                if ($filterby == 'codeType') {
                                    $getdiagnosis = $getdiagnosis->whereHas('poly', function ($query) use ($filtervalue) {
                                        $query->whereIn('codeType', $filtervalue);
                                    });
                                } elseif ($filterby == 'insuranceId') {
                                    $getdiagnosis = $getdiagnosis->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                        $query->whereIn('insuranceName', $filtervalue);
                                    });
                                }
                            } else {
                                if ($filterby == 'codeType') {
                                    $getdiagnosis = $getdiagnosis->whereHas('poly', function ($query) use ($filtervalue) {
                                        $query->where('codeType', 'like', '%' . $filtervalue . '%');
                                    });
                                } elseif ($filterby == 'insuranceId') {
                                    $getdiagnosis = $getdiagnosis->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                        $query->where('insuranceName', 'like', '%' . $filtervalue . '%');
                                    });
                                }
                            }
                        }

                    }
                } else {
                    if ($request->has('excludeId')) {
                        $getdiagnosis   = $getdiagnosis->whereNotIn('id',$request->input('excludeId'));
                    }
                }
            }

            // $totaldata      = $getdiagnosis->count();
            $getdiagnosis   = Helper::sorting($getdiagnosis, $sortby, $sortvalue);
            $limit          = $request->has('limit') ? $request->input('limit') : 100;
            $page           = $request->has('page') ? $request->input('page') : 1;
            $getdiagnosis   = $getdiagnosis->paginate($limit, ['*'], 'page', $page);
            $meta           = [
                'page'      => (int) $getdiagnosis->currentPage(),
                'perPage'   => (int) $getdiagnosis->perPage(),
                'total'     => (int) $getdiagnosis->total(),
                'totalPage' => (int) $getdiagnosis->lastPage()
            ];
            $getdiagnosis   = $getdiagnosis->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getdiagnosis) ? $getdiagnosis : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function updateDiagnosis(Request $request, $id)
    {
        $request->request->add(['lastUpdateBy' => $request->auth->name]);
        $this->validate($request, [
            'diagnosisCode'     => 'string',
            'diagnosisName'     => 'string',
            'scientificDesc'    => 'string',
            'description'       => 'string',
            'standardFee'       => 'between:0,99.99',
            'standardTreatment' => 'integer',
            'poly'              => 'string',
            'isExclusionPolicy' => 'string',
            'isPreExisting'     => 'string',
            'insurance_id'      => 'integer'
        ]);

        // print_r($request);die();

        $data   = Diagnosis::find($id);

        if ($data != null) {
            $cekdiagnosis   = Diagnosis::where(function ($query) use ($request) {
                $query->where('diagnosisCode', $request->input('diagcode'))
                    ->orWhere('diagnosisName', $request->input('diagname'));
            })->where('id', '!=', $id)->first();
            if ($cekdiagnosis) {
                return response()->json(['message' => 'Diagnosis code/name already exist !'], 409);
            } else {
                $params     = $request->all();
                $data->fill($params);
                $data->save();
                return response()->json(['status' => (bool) true, 'message' => 'Your data has been update'], 200);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
        }
    }

    public function deleteDiagnosis(Request $request, $id)
    {
        $diagnosis  = Diagnosis::find($id);
        if ($diagnosis) {
            $diagnosis->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function massDeleteDiagnosis(Request $request)
    {
        $diagnosis   = Diagnosis::whereIn('id', $request->id);
        if ($diagnosis) {
            $diagnosis->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function import(Request $request)
    {
        $data = $request->json()->all();
        $result = [];
        $i = 0;
        $k = 0;
        foreach ($data as $value1) {
            try {
                if ($value1['diagnosisCode'] == "") {
                    do {
                        $rand = str_random(10);
                        $cek = Diagnosis::where('diagnosisCode', '=', $rand)->first();
                    } while ($cek);
                    $value1['diagnosisCode'] = $rand;
                }

                $diag = Diagnosis::updateOrCreate(
                    [
                        'diagnosisCode' => $value1['diagnosisCode'],
                        'diagnosisName' => $value1['diagnosisName'],
                    ],
                    [
                        'scientificDesc' => $value1['scientificDesc'],
                        'description' => $value1['description'],
                        'standardFee' => $value1['standardFee'],
                        'standardTreatment' => $value1['standardTreatment'],
                        'poly' => $value1['poly'],
                        'isExclusionPolicy' => $value1['isExclusionPolicy'],
                        'isPreExisting' => $value1['isPreExisting'],
                        'createBy' => $this->nama_admin,
                        'lastUpdateBy' => $this->nama_admin
                    ]
                );
                $result['valid'][$i] = $diag;
                $result['valid'][$i]->is_valid = $value1['is_valid'];
                $result['valid'][$i]->msg = $value1['msg'];

                $i++;
            } catch (\Exception $e) {
                $result['invalid'][$k] = $value1;
                $result['invalid'][$k]['is_valid'] = false;
                $result['invalid'][$k]['msg'] = "failed to insert data";
                $k++;
            }
        }
        return response()->json($result, 200);
    }

    public function excelDiagnosis(Request $request)
    {
        $id = $request->id;

        if ($id or $id !== null) {
            $diag = Diagnosis::whereIn('id', $id)->get();
        } else {
            $diag = Diagnosis::all();
        }
        // $diag = $diag->get();

        $diag = new DiagnosisExport($diag->toArray());
        $date = date("dmy");

        return Excel::download($diag, 'Diagnosis-' . $date . '.xlsx');
    }

    public function pdfDiagnosis(Request $request)
    {
        $date = date("dmy");

        $id = $request->id;

        if ($id or $id !== null) {
            $diag = Diagnosis::whereIn('id', $id)->get();
        } else {
            $diag = Diagnosis::all();
        }

        $pdf = PDF::loadview('diagnosis', ['diag' => $diag]);
        return $pdf->download('Diagnosis-' . $date . '.pdf');
    }
}
