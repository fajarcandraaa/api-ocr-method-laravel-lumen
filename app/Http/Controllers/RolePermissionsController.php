<?php
namespace App\Http\Controllers;

use App\RolePermissions;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class RolePermissionsController extends Controller
{
    
    public function getRolePermissions($role_id=null)
    {
        $status = true;
        $error = "";
        $role_permissions = RolePermissions::where('isInsurance', $request->auth->isInsurance);

        if($role_id){
            $role_permissions = RolePermissions::where('role_id',$role_id);
        }

        if ($request->auth->isInsurance) {
            $role_permissions   = $role_permissions->where('insurance_id', $request->auth->insurance_id);
        }
        
        if(!$role_permissions->count()){
            $status = false;
            $error = "data not available";
        }

        $role_permissions = $role_permissions->get();

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($role_permissions) ? $role_permissions : null),
            "error"     => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function addRolePermissions($role_id,$user_id,$insuranceid,$data)
    {
        try {
            foreach ($data->permissions as $permissions) {
                $role_permissions = RolePermissions::create([
                    'role_id' => $role_id,
                    'permission' => $permissions,
                    'created_by' => $user_id,
                    'insurance_id' => $insuranceid
                ]);
            }            
            return response()->json(['message' => 'ROLE PERMISSIONS CREATED SUCCESSFULLY'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'ROLE PERMISSIONS Registration Failed!'], 409);
        }
    }

}