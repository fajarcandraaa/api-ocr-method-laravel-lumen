<?php

namespace App\Http\Controllers;

// // use File;
use Illuminate\Support\Facades\File;
use App\Insurances;
use App\InsuranceAddresses;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Helpers\Helper;
use App\Exports\InsurancesExport;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;

class InsuranceController extends Controller
{

    public function showAllInsurances(Request $request, $id = null)
    {
        $status = true;
        $error = "";

        if ($id) {
            // echo $id;die;
            $insurances = Insurances::with('addresses')->where('id', $id)->first();
        } else {
            if ($request->has('keyword')) {
                $keyword    = $request->keyword;
                $insurances = Insurances::with('addresses');
                //field yang dicari
                $wheres     = array("insuranceName", "binNumber", "icdType", "testCard", "testCardBirthDate");
                $insurances = Helper::dynamicSearch($insurances, $wheres, $keyword);
            } else {
                $insurances = Insurances::with('addresses');
            }

            $insurances = Helper::sorting($insurances, $request->input('sortby'), $request->input('sortvalue'));
            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $insurances = $insurances->paginate($limit, ['*'], 'page', $page);
            $meta       = [
                'page'      => (int) $insurances->currentPage(),
                'perPage'   => (int) $insurances->perPage(),
                'total'     => (int) $insurances->total(),
                'totalPage' => (int) $insurances->lastPage()
            ];
            $insurances   = $insurances->toArray()['data'];
        }
        if (!$insurances) {
            $status = false;
            $error = "data not found";
        }

        $response = [
            "status" => (bool) $status,
            "data" => (isset($insurances) ? $insurances : null),
            "meta" => (isset($meta) ? $meta : null),
            "error" => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    // public function showOneInsurance($id)
    // {   
    //     $insurances=Insurances::with('addresses')->get()->find($id);
    //     return response()->json($insurances);
    // }

    public function uploadImg(Request $request)
    {
        $status = false;
        $error = "upload image failed";

        $this->validate($request, Insurances::imgValidationRule());
        $img = $request->file('img');

        //uploadImage($file,"folder");
        $filename = Helper::uploadTempImg($img, "temp");
        if ($filename) {
            $status = true;
            $error = "";
        }
        $response = [
            "status" => (bool) $status,
            "data" => (isset($filename) ? $filename : null),
            "error" => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function create(Request $request)
    {

        $this->validate($request, Insurances::getValidationRule());

        $logo = Helper::moveImg("assets/insuranceLogo", $request->logo);
        $testCard = Helper::moveImg("assets/insuranceTestCard", $request->testCard);

        $insurances = Insurances::create([
            'insuranceName' => $request->insuranceName,
            'binNumber' => $request->binNumber,
            'icdType' => $request->icdType,
            'testCardNumber' => $request->testCardNumber,
            'testCardBirthDate' => $request->testCardBirthDate,
            'logo' => $logo,
            'testCard' => $testCard,
            'created_by' => $request->id_user,
        ]);
        if ($insurances) {
            try {
                if (isset($request->address)) {
                    $listAddress = [];
                    $now = date("Y-m-d h:i:s");
                    foreach ($request->address as $address) {
                        $listAddress[] = [
                            'insurance_id' => $insurances->id,
                            'address' => $address,
                            'created_at' => $now,
                        ];
                    }

                    $insurancesAddress = InsuranceAddresses::insert($listAddress);
                    $insurances->listAddress = $insurancesAddress;
                }
            } catch (\Exception $e) {
                $cek = Insurances::find($insurances->id);
                if ($cek) {
                    $cek->delete();
                }
                $cek = InsuranceAddresses::where('insurance_id', $insurances->id);
                if ($cek) {
                    $cek->delete();
                }

                return response()->json(['message' => 'Insurance Registration Failed!'], 409);
            }
        }

        return response()->json($insurances, 200);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, Insurances::getValidationRule());

        $insurances = Insurances::find($id);

        $lastLogo = $insurances->logo;
        $lastTestCard = $insurances->testCard;

        //uploadImage($file,"folder",$lastname);

        $logoPath = Helper::moveImg("assets/insuranceLogo", $request->logo, $lastLogo);
        $testCardPath = Helper::moveImg("assets/insuranceTestCard", $request->testCard, $lastTestCard);

        $insurances->update([
            'insuranceName' => $request->insuranceName,
            'binNumber' => $request->binNumber,
            'icdType' => $request->icdType,
            'testCardNumber' => $request->testCardNumber,
            'testCardBirthDate' => $request->testCardBirthDate,
            'logo' => $logoPath,
            'testCard' => $testCardPath,
            'last_modified_by' => $request->id_user,
        ]);

        if ($insurances) {
            $insuranceAddresses = InsuranceAddresses::where('insurance_id', $id)->delete();

            if (isset($request->address)) {
                $listAddress = [];
                $now = date("Y-m-d h:i:s");
                foreach ($request->address as $address) {
                    $listAddress[] = [
                        'insurance_id' => $insurances->id,
                        'address' => $address,
                        'created_at' => $now,
                    ];
                }

                $insurancesAddress = InsuranceAddresses::insert($listAddress);
                $insurances = Insurances::with('addresses')->get()->find($id);

                $insurances->listAddress = $insurancesAddress;
            }
        }


        return response()->json($insurances, 200);
    }

    public function delete($id)
    {
        $insurances = Insurances::find($id);
        // $lastLogo=$insurances->logo;
        // $lastTestCard=$insurances->testCard;
        // if($lastLogo!=="" and File::exists($lastLogo)) {
        //     File::delete($lastLogo);
        // }
        // if($lastTestCard!=="" and File::exists($lastTestCard)){
        //     File::delete($lastTestCard);
        // }
        $insurances->delete();
        return response('Deleted Successfully', 200);
    }

    public function mass_delete(Request $request)
    {
        $insurances = Insurances::whereIn('id', $request->id);
        $insurances->delete();
        return response('Deleted Successfully', 200);
    }

    public function excel()
    {
        return Excel::download(new InsurancesExport, 'insurances.xlsx');
    }

    public function pdf()
    {
        $insurances = Insurances::with('addresses')->get();

        $pdf = PDF::loadview('insurances', ['insurances' => $insurances]);
        return $pdf->download('insurances.pdf');
    }
}
