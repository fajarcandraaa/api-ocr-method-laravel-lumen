<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Biz_Compproviders;
use App\Helpers\Helper;
use App\BankInfos;
use App\Finances;
use App\Biz_providers; 
use App\Cities;
use App\Districts;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CompprovExport;
use App\Http\Controllers\ProviderController as ProviderController;
use App\Provinces;
use Illuminate\Support\Facades\DB as DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage as Storage;
use Response;



class CompanyProviderController extends Controller
{

    private $providers;
    public function __construct(Request $request)
    {
        $this->providers = new ProviderController;
        $this->insurance_id = $request->auth->insurance_id;
    }

    public static function checkDuplicate(Request $request)
    {
        $result = Helper::checkDuplicate('Biz_Compproviders', $request->type, $request->vals);
        return response()->json($result);
    }

    public function addNewCompProvider(Request $request)
    {
        // try {

        $data = $request->json()->all();
        $datacomp = $data['compproviders'];
        $dataprov = $data['providers'];
        $no = Biz_Compproviders::select('biz_prov_no')->where('provider_type', $datacomp['provider_type'])
            ->orderBy('biz_prov_no', 'desc')->first();
        if (!$no) {
            $no = "00001";
        } else {
            $no = (int) substr($no->biz_prov_no, 3); 
            $no += 1;
            $no = str_pad($no, 5, '0', STR_PAD_LEFT);
        }
        $biz_prov_no = $datacomp['biz_prov_no'] . $no;

        do {
            $cek_biz_prov_no = Biz_Compproviders::select('biz_prov_no')->where('biz_prov_no', $biz_prov_no)->first();
            if ($cek_biz_prov_no) {
                $biz_prov_no++;
            }
        } while ($cek_biz_prov_no);
        if ($request->auth->isInsurance) {
            $dataprov['merchant_id'] = $request->auth->insurance_id;
        } else {
            $dataprov['merchant_id'] = $datacomp['insurance_id'];
        }
        $compprov = Biz_Compproviders::create([
            'biz_prov_code' => $datacomp['biz_prov_code'],
            'provider_name' => $datacomp['provider_name'],
            'provider_type' => $datacomp['provider_type'],
            'provider_email' => $datacomp['provider_email'],
            'IsPartner' => $datacomp['IsPartner'],
            'Branch' => $datacomp['Branch'],
            'Address1' => $datacomp['Address1'],
            'Address2' => $datacomp['Address2'],
            'Address3' => $datacomp['Address3'],
            'Address4' => $datacomp['Address4'],
            'country' => $datacomp['country'],
            'prov' => $datacomp['prov'],
            'city' => $datacomp['city'],
            'district' => $datacomp['district'],
            'postal_code' => $datacomp['postal_code'],
            'PhoneNo' => $datacomp['PhoneNo'],
            'FaxNo' => $datacomp['FaxNo'],
            'Profile' => $datacomp['Profile'],
            'SIUPNo' => $datacomp['SIUPNo'],
            'Domicile' => $datacomp['Domicile'],
            'TaxNo' => $datacomp['TaxNo'],
            'NotarialDeed' => $datacomp['NotarialDeed'],
            'LicenseNo' => $datacomp['LicenseNo'],
            'AggrEffectiveDate' => $datacomp['AggrEffectiveDate'],
            'AggrExpiryDate' => $datacomp['AggrExpiryDate'],
            'RenewalDate' => $datacomp['RenewalDate'],
            'DiscProvider' => $datacomp['DiscProvider'],
            'Remark' => $datacomp['Remark'],
            'IsDifferentHospital' => $datacomp['IsDifferentHospital'],
            'Flag' => $datacomp['Flag'],
            'created_by' => $datacomp['id_user'],
            'biz_prov_no' => $biz_prov_no,
            'status' => $datacomp['status'],
            'insurance_id' => $request->auth->isInsurance ? $request->auth->insurance_id : $datacomp['insurance_id']
        ]);
        if ($compprov) {
            try {
                if (isset($datacomp['bank_code'])) {
                    Helper::addBankInfo($datacomp, "biz_compproviders", $compprov->id);
                }

                if (isset($datacomp['contact_person']) and count($datacomp['contact_person'])  > 0) {
                    Helper::addCP($datacomp['contact_person'], "biz_compproviders", $compprov->id);
                }
            } catch (\Exception $e) {
                $cek = Biz_Compproviders::find($compprov->id);
                if ($cek) {
                    $cek->delete();
                }
                $cek = BankInfos::where([['ref_type', 'biz_compproviders'], ['ref_id', $compprov->id]]);
                if ($cek) {
                    $cek->delete();
                }
                $cek = Finances::where([['ref_type', 'biz_compproviders'], ['ref_id', $compprov->id]]);
                if ($cek) {
                    $cek->delete();
                }
                return response()->json(['message' => 'Company Provider Registration Failed!'], 409);
            }
        }

        $this->providers->addNewProvFromComp($compprov->id, $dataprov);

        return response()->json(['message' => 'DATA CREATED SUCCESSFULLY'], 200);
        // }
        // } catch (\Exception $e) {
        //     return response()->json(['message' => 'Provider Registration Failed!'], 409);
        // }
    }

    public function updateCompProvider(Request $request, $id)
    {
        $source = $request->json()->all();
        $datacomp = $source['compproviders'];
        $dataprov = $source['providers'];
        $data   = biz_compproviders::find($id);
        $datakeep = clone $data;
        try {
            $data->update([
                'biz_prov_code' => $datacomp['biz_prov_code'],
                'provider_name' => $datacomp['provider_name'],
                'provider_type' => $datacomp['provider_type'],
                'provider_email' => $datacomp['provider_email'],
                'IsPartner' => $datacomp['IsPartner'],
                'Branch' => $datacomp['Branch'],
                'Address1' => $datacomp['Address1'],
                'Address2' => $datacomp['Address2'],
                'Address3' => $datacomp['Address3'],
                'Address4' => $datacomp['Address4'],
                'country' => $datacomp['country'],
                'prov' => $datacomp['prov'],
                'city' => $datacomp['city'],
                'district' => $datacomp['district'],
                'postal_code' => $datacomp['postal_code'],
                'PhoneNo' => $datacomp['PhoneNo'],
                'FaxNo' => $datacomp['FaxNo'],
                'Profile' => $datacomp['Profile'],
                'SIUPNo' => $datacomp['SIUPNo'],
                'Domicile' => $datacomp['Domicile'],
                'TaxNo' => $datacomp['TaxNo'],
                'NotarialDeed' => $datacomp['NotarialDeed'],
                'LicenseNo' => $datacomp['LicenseNo'],
                'AggrEffectiveDate' => $datacomp['AggrEffectiveDate'],
                'AggrExpiryDate' => $datacomp['AggrExpiryDate'],
                'RenewalDate' => $datacomp['RenewalDate'],
                'DiscProvider' => $datacomp['DiscProvider'],
                'Remark' => $datacomp['Remark'],
                'IsDifferentHospital' => $datacomp['IsDifferentHospital'],
                'Flag' => $datacomp['Flag'],
                'last_modified_by' => $datacomp['id_user'],
                'status' => $datacomp['status'],
                'insurance_id' => $datacomp['insurance_id']
            ]);

            if ($data) {
                if (isset($datacomp['bank_code'])) {
                    $cek = BankInfos::where([['ref_id', $id], ['ref_type', "biz_compproviders"]])->first();
                    try {
                        $added_bank = Helper::addBankInfo($datacomp, "biz_compproviders", $data->id);
                        // return $added_bank->id;
                        if ($cek) {
                            $deleted_cbank = $cek['id'];
                            $cek->delete();
                        }
                    } catch (\Exception $e) {
                        throw new \Exception();
                    }
                }

                if (isset($datacomp['contact_person'])) {

                    $cek = Finances::where([['ref_type', 'biz_compproviders'], ['ref_id', $id]])->get();
                    if ($cek) {
                        $deleted_ccps = [];
                        foreach ($cek as $del) {
                            array_push($deleted_ccps, $del->id);
                        }
                    }

                    $cek2 = Finances::where([['ref_type', 'biz_compproviders'], ['ref_id', $id]])->delete();
                    try {
                        $added_cp = [];
                        $added_cps = Helper::addCP($datacomp['contact_person'], "biz_compproviders", $data->id);
                        foreach ($added_cps as $ac) {
                            array_push($added_cp, $ac->id);
                        }
                    } catch (\Exception $e) {
                        throw new \Exception();
                    }
                }

                if (isset($dataprov)) {
                    $cekprov = Biz_providers::where([['compprovid', $id], ['id', $dataprov['id']]])->first();
                    $cekpbank = BankInfos::where([['ref_id', $dataprov['id']], ['ref_type', "biz_providers"]])->first();
                    $cekpcp = Finances::where([['ref_id', $dataprov['id']], ['ref_type', "biz_providers"]]);

                    if ($cekpbank) {
                        $deleted_pbank = $cekpbank['id'];
                        $cekpbank->delete();
                    }

                    if ($cekpcp) {
                        $deleted_pcp = $cekpcp->get('id');
                        $deleted_pcps = [];
                        foreach ($deleted_pcp as $del) {
                            array_push($deleted_pcps, $del->id);
                        }
                        $cekpcp->delete();
                    }

                    $this->providers->updateProvFromComp($id, $dataprov);
                    // if ($cekprov) {
                    //     $deleted_prov = $cekprov->id;
                    //     $cekprov->delete();
                    // }
                }
            }

            return response()->json(['status' => (bool) true, 'message' => 'Data successfully updated'], 200);
        } catch (\Exception $e) {
            //     //restore edited compprov
            if ($data) {
                $data->update([
                    'biz_prov_code' => $datakeep['biz_prov_code'],
                    'provider_name' => $datakeep['provider_name'],
                    'provider_type' => $datakeep['provider_type'],
                    'provider_email' => $datakeep['provider_email'],
                    'IsPartner' => $datakeep['IsPartner'],
                    'Branch' => $datakeep['Branch'],
                    'Address1' => $datakeep['Address1'],
                    'Address2' => $datakeep['Address2'],
                    'Address3' => $datakeep['Address3'],
                    'Address4' => $datakeep['Address4'],
                    'country' => $datakeep['country'],
                    'prov' => $datakeep['prov'],
                    'city' => $datakeep['city'],
                    'district' => $datakeep['district'],
                    'postal_code' => $datakeep['postal_code'],
                    'PhoneNo' => $datakeep['PhoneNo'],
                    'FaxNo' => $datakeep['FaxNo'],
                    'Profile' => $datakeep['Profile'],
                    'SIUPNo' => $datakeep['SIUPNo'],
                    'Domicile' => $datakeep['Domicile'],
                    'TaxNo' => $datakeep['TaxNo'],
                    'NotarialDeed' => $datakeep['NotarialDeed'],
                    'LicenseNo' => $datakeep['LicenseNo'],
                    'AggrEffectiveDate' => $datakeep['AggrEffectiveDate'],
                    'AggrExpiryDate' => $datakeep['AggrExpiryDate'],
                    'RenewalDate' => $datakeep['RenewalDate'],
                    'DiscProvider' => $datakeep['DiscProvider'],
                    'Remark' => $datakeep['Remark'],
                    'IsDifferentHospital' => $datakeep['IsDifferentHospital'],
                    'Flag' => $datakeep['Flag'],
                    'last_modified_by' => $datakeep['id_user'],
                    'status' => $datakeep['status'],
                    'insurance_id' => $datakeep['insurance_id']
                ]);
            }

            //delete added bank & cp
            if (isset($added_bank)) {
                BankInfos::find($added_bank->id)->delete();
                if (isset($deleted_cbank)) {
                    BankInfos::withTrashed()->find($deleted_cbank)->restore();
                }
            }
            // return $added_cp;
            if (isset($added_cp)) {
                foreach ($added_cp as $del) {
                    Finances::find($del)->delete();
                }
                // restore deleted compprov bank info & cp
                if (isset($deleted_ccps)) {
                    // foreach ($deleted_ccps as $del) {
                    Finances::withTrashed()->whereIn('id', $deleted_ccps)->restore();
                    // }
                }
            }
            // //     // //restore deleted providers
            // //     // Biz_providers::withTrashed()->find($deleted_prov)->restore();
            // //     // foreach ($deleted_ccps as $del) {
            // //     //     Finances::withTrashed()->find($del)->restore();
            // //     // }
            return response()->json(['status' => (bool) false, 'message' => $e->getMessage()], 409);
        }
    }

    public function deleteCompProvider($id)
    {
        $compprov   = biz_compproviders::find($id);
        $compbank = BankInfos::where([['ref_id', $id], ['ref_type', "biz_compproviders"]]);
        $compcp = Finances::where([['ref_id', $id], ['ref_type', "biz_compproviders"]]);
        $idprov = Biz_providers::select('id')->where('compprovid', '=', $id)->first();
        $prov = Biz_providers::where('id', $idprov->id);
        $provbank = BankInfos::where([['ref_id', $idprov->id], ['ref_type', "biz_providers"]]);
        $provcp = Finances::where([['ref_id', $idprov->id], ['ref_type', "biz_providers"]]);

        if ($compprov) {
            try {
                $compprov->delete();
                $compbank->delete();
                $compcp->delete();
                $prov->delete();
                $provbank->delete();
                $provcp->delete();
                return response()->json(['status' => (bool) true], 200);
            } catch (\Exception $e) {
                return response()->json(['status' => (bool) false, 'message' => 'delete data failed'], 409);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'data not found'], 409);
        }
    }

    public function mass_delete(Request $request)
    {
        $id = $request->id;
        $compprov = biz_compproviders::whereIn('id', $id);
        $compbank = BankInfos::where('ref_type', "biz_compproviders");
        $compcp = Finances::where('ref_type', "biz_compproviders");
        $compbank = $compbank->whereIn('ref_id', $id);
        $compcp = $compcp->whereIn('ref_id', $id);

        $idprov = Biz_providers::select('id')->whereIn('compprovid', $id)->get();
        $i = 0;
        foreach ($idprov as $id) {
            $idprovs[$i] = $id->id;
            $i++;
        }
        // print_r($idprovs);
        // die;
        $prov = Biz_providers::whereIn('id', $idprovs);
        $provbank = BankInfos::where('ref_type', "biz_providers")->whereIn('ref_id', $idprovs);
        $provcp = Finances::where('ref_type', "biz_providers")->whereIn('ref_id', $idprovs);
        // $provbank = $provbank->whereIn('ref_id',$idprovs);
        // $provcp = $provbank->whereIn('ref_id',$idprovs);

        if ($compprov) {
            try {
                $compprov->delete();
                $compbank->delete();
                $compcp->delete();
                $prov->delete();
                $provbank->delete();
                $provcp->delete();
                return response()->json(['status' => (bool) true], 200);
            } catch (\Exception $e) {
                return response()->json(['status' => (bool) false, 'message' => 'delete data failed'], 409);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'data not found'], 409);
        }
    }

    public function showCompProvider(Request $request, $id = null)
    {
        $sortby             = $request->input('sortby');
        $sortvalue          = $request->input('sortvalue');
        $status             = true;
        $error              = null;
        $getcompproviders   = biz_compproviders::with([
            'provider_type',
            'country',
            'prov',
            'city',
            'district',
            'postal_code',
            'bank_infos' => function ($query) {
                $query->where('ref_type', '=', 'biz_compproviders');
            },
            'contact_person' => function ($query) {
                $query->where('ref_type', '=', 'biz_compproviders');
            },
            'status',
            'created_by',
            'last_modified_by',
            'providers' => function ($query) {
                $query->with([
                    'contact_person' => function ($query) {
                        $query->where('ref_type', '=', 'biz_providers');
                    },
                    'bank_infos' => function ($query) {
                        $query->where('ref_type', '=', 'biz_providers');
                    },
                    'type',
                    'city',
                    'province',
                    'country',
                    'district',
                    'postal_code',
                    'created_by',
                    'last_modified_by',
                    'insurance_id'
                ]);
            },
            'insurance_id'
        ]);

        if ($request->auth->isInsurance) {
            $getcompproviders   = $getcompproviders->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id) {
            $getcompproviders = $getcompproviders->where('id', $id)->first();

            if (!$getcompproviders) {
                $status     = false;
                $error      = "data not found";
            }
        } else {
            if ($request->has('keyword')) {
                $keyword            = $request->keyword;
                $wheres             = array("id", "biz_prov_no", "provider_name", "provider_email", "PhoneNo", "AggrEffectiveDate", "created_at", "biz_prov_code");
                $getcompproviders   = Helper::dynamicSearch($getcompproviders, $wheres, $keyword);
                $getcompproviders   = $getcompproviders->orWhereHas('provider_type', function ($query) use ($keyword) {
                    $query->where('codeDesc', 'like', '%' . $keyword . '%');
                })->orWhereHas('country', function ($query) use ($keyword) {
                    $query->where('country_desc', 'like', '%' . $keyword . '%');
                })->orWhereHas('prov', function ($query) use ($keyword) {
                    $query->where('prov_desc', 'like', '%' . $keyword . '%');
                })->orWhereHas('city', function ($query) use ($keyword) {
                    $query->where('city_desc', 'like', '%' . $keyword . '%');
                })->orWhereHas('district', function ($query) use ($keyword) {
                    $query->where('district_desc', 'like', '%' . $keyword . '%');
                })->orWhereHas('postal_code', function ($query) use ($keyword) {
                    $query->where('subdistrict_desc', 'like', '%' . $keyword . '%');
                })->orWhereHas('contact_person', function ($query) use ($keyword) {
                    $query->where('pic_name', 'like', '%' . $keyword . '%');
                })->orWhereHas('status', function ($query) use ($keyword) {
                    $query->where('codeDesc', 'like', '%' . $keyword . '%');
                })->orWhereHas('created_by', function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%');
                })->orWhereHas('last_modified_by', function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%');
                })->orWhereHas('providers', function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%');
                })->orWhereHas('insurance_id', function ($query) use ($keyword) {
                        $query->where('insuranceName', 'like', '%' . $keyword . '%');
                });
                // if (!$getcompproviders->count()) {
                //     $getcompproviders   = biz_compproviders::with([
                //         'provider_type',
                //         'country',
                //         'prov',
                //         'city',
                //         'district',
                //         'postal_code',
                //         'bank_infos' => function ($query) {
                //             $query->where('ref_type', '=', 'biz_compproviders');
                //         },
                //         'contact_person' => function ($query) {
                //             $query->where('ref_type', '=', 'biz_compproviders');
                //         },
                //         'status',
                //         'created_by',
                //         'last_modified_by',
                //         'providers' => function ($query) {
                //             $query->with([
                //                 'contact_person' => function ($query) {
                //                     $query->where('ref_type', '=', 'biz_providers');
                //                 },
                //                 'bank_infos' => function ($query) {
                //                     $query->where('ref_type', '=', 'biz_providers');
                //                 },
                //                 'type',
                //                 'city',
                //                 'province',
                //                 'country',
                //                 'district',
                //                 'postal_code',
                //                 'created_by',
                //                 'last_modified_by'
                //             ]);
                //         },
                //         'insurance_id'
                //     ]);

                //     $getcompproviders = $getcompproviders->whereHas('provider_type', function ($query) use ($keyword) {
                //         $query->where('codeDesc', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('country', function ($query) use ($keyword) {
                //         $query->where('country_desc', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('prov', function ($query) use ($keyword) {
                //         $query->where('prov_desc', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('city', function ($query) use ($keyword) {
                //         $query->where('city_desc', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('district', function ($query) use ($keyword) {
                //         $query->where('district_desc', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('postal_code', function ($query) use ($keyword) {
                //         $query->where('subdistrict_desc', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('bank_infos', function ($query) use ($keyword) {
                //         $query->where('bank_branch', 'like', '%' . $keyword . '%')
                //             ->orWhere('bank_account_name', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('contact_person', function ($query) use ($keyword) {
                //         $query->where('pic_name', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('status', function ($query) use ($keyword) {
                //         $query->where('codeDesc', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('created_by', function ($query) use ($keyword) {
                //         $query->where('name', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('last_modified_by', function ($query) use ($keyword) {
                //         $query->where('name', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('providers', function ($query) use ($keyword) {
                //         $query->where('name', 'like', '%' . $keyword . '%');
                //     })->orWhereHas('insurance_id', function ($query) use ($keyword) {
                //         $query->where('insuranceName', 'like', '%' . $keyword . '%');
                //     });
                // }
                $status             = true;
                $error              = null;
                $getcompproviders   = Helper::sorting($getcompproviders, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table              = 'biz_compproviders';
                    $filter             = $request->input('filter');
                    $filtername         = json_decode($filter[0]);
                    $filternameby       = $filtername->by;
                    $filternamevalue    = $filtername->value;
                    if ($filternameby == 'startendDate') {
                        $filterdate             = explode("/", $filternamevalue);
                        $where                  = array('AggrEffectiveDate', 'AggrExpiryDate');
                        $getcompproviders       = $getcompproviders->whereBetween($where[0], [$filterdate[0], $filterdate[1]])
                            ->orWhereBetween($where[1], [$filterdate[0], $filterdate[1]]);
                    } else {
                        $getcompproviders    = Helper::filterSearch($getcompproviders, $table, $filter);
                    }

                    foreach ($filter as $f) {
                        $filterdata         = json_decode($f);
                        $filterby           = $filterdata->by;
                        $filtervalue        = $filterdata->value;

                        if (is_array($filtervalue)) {
                            if ($filterby == 'provType') {
                                $getcompproviders   = $getcompproviders->whereHas('provider_type', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'country') {
                                $getcompproviders   = $getcompproviders->whereHas('country', function ($query) use ($filtervalue) {
                                    $query->whereIn('country_desc', $filtervalue);
                                });
                            } elseif ($filterby == 'prov') {
                                $getcompproviders   = $getcompproviders->whereHas('prov', function ($query) use ($filtervalue) {
                                    $query->whereIn('prov_desc', $filtervalue);
                                });
                            } elseif ($filterby == 'city_desc') {
                                $getcompproviders   = $getcompproviders->whereHas('city', function ($query) use ($filtervalue) {
                                    $query->whereIn('city_desc', $filtervalue);
                                });
                            } elseif ($filterby == 'district') {
                                $getcompproviders   = $getcompproviders->whereHas('district', function ($query) use ($filtervalue) {
                                    $query->whereIn('district_desc', $filtervalue);
                                });
                            } elseif ($filterby == 'subdistrict') {
                                $getcompproviders   = $getcompproviders->whereHas('postal_code', function ($query) use ($filtervalue) {
                                    $query->whereIn('subdistrict_desc', $filtervalue);
                                });
                            } elseif ($filterby == 'bank_branch' || $filterby == 'bank_account_name') {
                                $providers->whereHas('bank_infos', function ($query) use ($filtervalue) {
                                    $query->whereIn('bank_branch', $filtervalue)
                                        ->orWhereIn('bank_account_name', $filtervalue);
                                });
                            } elseif ($filterby == 'contactPerson') {
                                $getcompproviders   = $getcompproviders->whereHas('contact_person', function ($query) use ($filtervalue) {
                                    $query->whereIn('pic_name', $filtervalue);
                                });
                            } elseif ($filterby == 'status') {
                                $getcompproviders   = $getcompproviders->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'namecreated') {
                                $getcompproviders   = $getcompproviders->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'namemodified') {
                                $getcompproviders   = $getcompproviders->whereHas('last_modified_by', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'providers') {
                                $getcompproviders   = $getcompproviders->whereHas('providers', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'insuranceId') {
                                $getcompproviders   = $getcompproviders->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                    $query->whereIn('insuranceName', $filtervalue);
                                });
                            }
                        } else {
                            if ($filterby == 'provType') {
                                $getcompproviders   = $getcompproviders->whereHas('provider_type', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'country_desc') {
                                $getcompproviders   = $getcompproviders->whereHas('country', function ($query) use ($filtervalue) {
                                    $query->where('country_desc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'prov') {
                                $getcompproviders   = $getcompproviders->whereHas('prov', function ($query) use ($filtervalue) {
                                    $query->where('prov_desc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'city_desc') {
                                $getcompproviders   = $getcompproviders->whereHas('city', function ($query) use ($filtervalue) {
                                    $query->where('city_desc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'district') {
                                $getcompproviders   = $getcompproviders->whereHas('district', function ($query) use ($filtervalue) {
                                    $query->where('district_desc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'subdistrict') {
                                $getcompproviders   = $getcompproviders->whereHas('postal_code', function ($query) use ($filtervalue) {
                                    $query->where('subdistrict_desc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'bank_branch' || $filterby == 'bank_account_name') {
                                $providers->whereHas('bank_infos', function ($query) use ($filtervalue) {
                                    $query->where('bank_branch', 'like', '%' . $filtervalue . '%')
                                        ->orWhere('bank_account_name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'contactPerson') {
                                $getcompproviders   = $getcompproviders->whereHas('contact_person', function ($query) use ($filtervalue) {
                                    $query->where('pic_name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'status') {
                                $getcompproviders   = $getcompproviders->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'namecreated') {
                                $getcompproviders   = $getcompproviders->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'namemodified') {
                                $getcompproviders   = $getcompproviders->whereHas('last_modified_by', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'providers') {
                                $getcompproviders   = $getcompproviders->whereHas('providers', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'insuranceId') {
                                $getcompproviders   = $getcompproviders->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                    $query->where('insuranceName', 'like', '%' . $filtervalue . '%');
                                });
                            }
                        }
                    }
                }
            }

            $getcompproviders   = Helper::sorting($getcompproviders, $sortby, $sortvalue);
            $limit              = $request->has('limit') ? $request->input('limit') : 20;
            $page               = $request->has('page') ? $request->input('page') : 1;
            $getcompproviders   = $getcompproviders->paginate($limit, ['*'], 'page', $page);
            $meta               = [
                'page'      => (int) $getcompproviders->currentPage(),
                'perPage'   => (int) $getcompproviders->perPage(),
                'total'     => (int) $getcompproviders->total(),
                'totalPage' => (int) $getcompproviders->lastPage()
            ];
            $getcompproviders   = $getcompproviders->toArray()['data'];
        }

        if (!$getcompproviders) {
            $status = false;
            $error = "data not found";
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getcompproviders) ? $getcompproviders : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function pdfCompprov(Request $request)
    {
        // $id = $request->id;
        $compprov = Biz_Compproviders::with([
            'provider_type',
            'country',
            'prov',
            'city',
            'district',
            'postal_code',
            'bank_infos' => function ($query) {
                $query->where('ref_type', '=', 'biz_compproviders');
            },
            'contact_person' => function ($query) {
                $query->where('ref_type', '=', 'biz_compproviders');
            },
            'status',
            'created_by',
            'last_modified_by'
        ]);

        if ($request->id) {
            $id = $request->id;
            $compprov->whereIn('id', $id);
        }
        $compprov = $compprov->get();

        $pdf = PDF::loadview('compprov', ['compprov' => json_encode($compprov, true)])->setPaper('a4', 'landscape');
        $date = date("dmy");

        return $pdf->download('Providers-' . $date . '.pdf');
    }

    public function excelCompprov(Request $request)
    {
        $id = $request->id;
        $compprov = Biz_Compproviders::with([
            'provider_type',
            'country',
            'prov',
            'city',
            'district',
            'postal_code',
            'bank_infos' => function ($query) {
                $query->where('ref_type', '=', 'biz_compproviders');
            },
            'contact_person' => function ($query) {
                $query->where('ref_type', '=', 'biz_compproviders');
            },
            'status',
            'created_by',
            'last_modified_by',
            'providers' => function ($query) {
                $query->with([
                    'contact_person' => function ($query) {
                        $query->where('ref_type', '=', 'biz_providers');
                    },
                    'bank_infos' => function ($query) {
                        $query->where('ref_type', '=', 'biz_providers');
                    },
                    'type',
                    'district',
                    'city',
                    'province',
                    'postal_code',
                    'country',
                    'created_by',
                    'last_modified_by'
                ]);
            }
        ]);

        if ($id or $id !== null) {
            $compprov->whereIn('id', $id);
            // return $compprov->get();
        }

        $compprov = $compprov->get();

        foreach ($compprov as $comp) {
            $comp->finance = null;
            $comp->marketing = null;

            $fin = Finances::where(function ($query) {
                return $query->where('ref_type', '=', 'biz_compproviders')
                    ->orWhere('ref_type', '=', 'biz_providers');
            })->where([['ref_id', $comp->id], ["pic_type", "=", "finance"]])
                ->orWhere([['ref_id', $comp->providers->id], ["pic_type", "=", "finance"]])->first();


            $mar = Finances::where(function ($query) {
                return $query->where('ref_type', '=', 'biz_compproviders')
                    ->orWhere('ref_type', '=', 'biz_providers');
            })->where([['ref_id', $comp->id], ["pic_type", "=", "marketing"]])
                ->orWhere([['ref_id', $comp->providers->id], ["pic_type", "=", "marketing"]])->first();

            if ($fin) {
                $comp->finance = $fin;
            }
            if ($mar) {
                $comp->marketing = $mar;
            }
        }

        // return $compprov;
        $comp = new CompprovExport($compprov->toArray());
        $date = date("dmy");
        return Excel::download($comp, 'Providers-' . $date . '.xlsx');
    }

    public function import(Request $request)
    {
        $data = $request->json()->all();
        $result = [];
        $i = 0;
        $k = 0;
        $compprov_id_awal = "baru";
        foreach ($data["data"] as $value) {
            $msg = "";

            $value1 = $value['compproviders'];
            $value2 = $value['providers'];
            $value3 = $value['bank_infos'];
            $value4 = $value['contact_person'];

            //ispartner
            if ($value1['IsPartner'] !== "0" && $value1['IsPartner'] !== "1") {
                $msg .= " IsPartner field should be '0' or '1'.";
            }

            //compprov type
            try {
                $prov_type = Helper::getCodeMasterId('PCT', $value1['provider_type']);
            } catch (\Exception $e) {
                $msg .= " Provider Company type is invalid.";
            }

            //cek biz_prov_no
            if ($value1['biz_prov_no'] !== null && strlen($value1['biz_prov_no']) > 8) {
                $msg .= " Provider id invalid'.";
            }

            //country
            try {
                $country_code = Helper::getKey("country_code", "Countries", "country_desc", $value1['country']);
            } catch (\Exception $e) {
                $msg .= " Provider company Country not found.";
            }

            // echo $country_code . " ";
            //province
            if (isset($country_code)) {
                if ($value1['prov'] !== "") {
                    try {
                        $prov_id = Provinces::where([["prov_desc", $value1['prov']], ['country_code', $country_code]])->first();
                        $prov_id = $prov_id->id;
                    } catch (\Exception $e) {
                        $msg .= " Provider company Province not found.";
                    }
                }
            }

            // echo $prov_id . " ";

            //city
            if (isset($prov_id)) {
                try {
                    $cek = explode(" ", strtolower($value1['city']));
                    if ($cek[0] == "kabupaten") {
                        $city_id = Cities::where('city_desc',substr($value1['city'],10))->first();
                        $city_id=$city_id->id;
                    }elseif ($cek[0] == "kota") {
                        $city_id = Cities::where('city_desc',substr($value1['city'],5))->first();
                        $city_id=$city_id->id;
                    } else {
                        $city = DB::table('cities')
                            ->where('city_desc', $value1['city'])
                            ->get();
                        if ($city->count() == 0) {
                            throw new \Exception();
                        } elseif ($city->count() == 1) {
                            $city_id = $city[0]->id;
                        } else {
                            $city_id = [];
                            foreach ($city as $c) {
                                array_push($city_id, $c->id);
                            }
                            $district = Districts::where('district_desc', $value1['district'])
                                ->whereIn('city_id', $city_id)
                                ->get();
                            if ($district->count()) {
                                $city_id = $district[0]->city_id;
                                $district_id = $district[0]->id;
                            } else {
                                throw new \Exception();
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $msg .= " Provider company City not found.";
                }
            }

            // echo $city_id . " ";

            //district
            if (isset($city_id) && !isset($district_id)) {
                try {
                    $district_id = Helper::getKey("id", "Districts", "district_desc", $value1['district']);
                } catch (\Exception $e) {
                    $msg .= " Provider company District not found.";
                }
            }

            // echo $district_id . " ";
            // die;
            //postal

            if (isset($district_id)) {
                try {
                    Helper::getKey("id", "Subdistricts", "postal_code", $value1['postal_code']);
                } catch (\Exception $e) {
                    $msg .= " Provider company postal code not found.";
                }
            }

            //country
            try {
                $pcountry_code = Helper::getKey("country_code", "Countries", "country_desc", $value2['country']);
            } catch (\Exception $e) {
                $msg .= " Provider Country not found.";
            }

            // echo $country_code . " ";
            //province
            if (isset($pcountry_code)) {
                if ($value2['province'] !== "") {
                    try {
                        $pprov_id = Provinces::where([["prov_desc", $value2['province']], ['country_code', $pcountry_code]])->first();
                        $pprov_id = $pprov_id->id;
                    } catch (\Exception $e) {
                        $msg .= " Provider Province not found.";
                    }
                }
            }

            // echo $prov_id . " ";

            //city
            if (isset($pprov_id)) {
                try {
                    $cek = explode(" ", strtolower($value2['city']));
                    if ($cek[0] == "kabupaten") {
                        $pcity_id = Cities::where('city_desc',substr($value2['city'],10))->first();
                        $pcity_id=$pcity_id->id;
                    }elseif ($cek[0] == "kota") {
                        $pcity_id = Cities::where('city_desc',substr($value2['city'],5))->first();
                        $pcity_id=$pcity_id->id;
                    } else {
                        $pcity = DB::table('cities')
                            ->where('city_desc', $value2['city'])
                            ->get();

                        if ($pcity->count() == 0) {
                            throw new \Exception();
                        } elseif ($pcity->count() == 1) {
                            $pcity_id = $pcity[0]->id;
                        } else {
                            $pcity_id = [];
                            foreach ($pcity as $c) {
                                array_push($pcity_id, $c->id);
                            }
                            $pdistrict = Districts::where('district_desc', $value2['district'])
                                ->whereIn('city_id', $pcity_id)
                                ->get();
                            if ($pdistrict->count()) {
                                $pcity_id = $pdistrict[0]->city_id;
                                $pdistrict_id = $pdistrict[0]->id;
                            } else {
                                throw new \Exception();
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $msg .= " Provider City not found.";
                }
            }

            // echo $city_id . " ";

            //district
            if (isset($pcity_id) && !isset($pdistrict_id)) {
                try {
                    $pdistrict_id = Districts::where('district_desc', $value2['district'])->first();
                    $pdistrict_id = $pdistrict_id->id;
                } catch (\Exception $e) {
                    $msg .= " Provider District not found.";
                }
            }

            // echo $district_id . " ";
            // die;
            //postal

            if (isset($pdistrict_id)) {
                try {
                    Helper::getKey("id", "Subdistricts", "postal_code", $value2['postal_code']);
                } catch (\Exception $e) {
                    $msg .= " Provider company postal code not found.";
                }
            }


            //isdifferent
            if ($value1['IsDifferentHospital'] !== "0" && $value1['IsDifferentHospital'] !== "1") {
                $msg .= " IsDifferentHospital field should be '0' or '1'.";
            }

            //status
            if (strtolower($value1['status']) !== "active" && strtolower($value1['status']) !== "in-active") {
                $msg .= " Status should be 'Active' or 'In-Active'.";
            }

            //providers validation
            //prov type            
            try {
                $prov_type = Helper::getCodeMasterId('PCT', $value2['type']);
            } catch (\Exception $e) {
                $msg .= " Provider type is invalid.";
            }

            //bank name
            try {
                Helper::getCodeMasterId("BNK", $value3['bank_name']);;
            } catch (\Exception $e) {
                $msg .= " bank name not found.";
            }

            try {

                if ($msg !== "") {
                    throw new \Exception($msg);
                }

                $prov_type = Helper::getCodeMasterId('PCT', $value1['provider_type']);
                if (!$value1['biz_prov_no'] or $value1['biz_prov_no'] == "") {
                    $no = Biz_Compproviders::select('biz_prov_no')->where('provider_type', $prov_type)
                        ->orderBy('biz_prov_no', 'desc')->first();
                    if (!$no) {
                        $no = "00001";
                    } else {
                        $no = (int) substr($no->biz_prov_no, 3);
                        $no += 1;
                        $no = str_pad($no, 5, '0', STR_PAD_LEFT);
                    }
                    $codeval = DB::table('code_masters')
                        ->select(DB::raw('concat(codeVal1,codeVal2) as codeval'))
                        ->where([['codeTYpe', 'PCT'], ['codeDesc', $value1['provider_type']]])
                        ->first();
                    $codeval = $codeval->codeval;
                    $value1['biz_prov_no'] = $codeval . $no;
                } else {
                    try {
                        $compprov_id_awal = Helper::getKey("id", "Biz_Compproviders", "biz_prov_no", $value1['biz_prov_no']);
                        if ($compprov_id_awal) {
                            $cek_prov = Biz_providers::where('compprovid', (string) $compprov_id_awal)->get();
                        }
                    } catch (\Exception $e) {
                        $compprov_id_awal = "baru";
                    }
                }

                // $cekcompprov    = biz_compproviders::where([['Branch', $value1['Branch']], ['provider_name', $value1['provider_name']]])
                //     ->orWhere('provider_email', $value1['provider_email'])
                //     ->first();
                if ($value1['DiscProvider'] && $value1['DiscProvider'] !== "") {
                    $pos = strpos($value1['DiscProvider'], ",");
                    if ($pos !== false) {
                        $value1['DiscProvider'] = str_replace(",", ".", $value1['DiscProvider']);
                    }
                    $value1['DiscProvider'] = doubleval($value1['DiscProvider']);
                }
                $comp = Biz_Compproviders::updateOrCreate(
                    [
                        'biz_prov_no' => $value1['biz_prov_no']
                    ],
                    [
                        'IsPartner' => $value1['IsPartner'],
                        'provider_type' => Helper::getCodeMasterId("PCT", $value1['provider_type']),
                        'provider_name' => $value1['provider_name'],
                        'biz_prov_no' => $value1['biz_prov_no'],
                        'biz_prov_code' => $value1['biz_prov_code'],
                        'Branch' => $value1['Branch'],
                        'country' => $country_code,
                        'prov' => ($value1['prov'] == "" ? "" : $prov_id),
                        'city' => $city_id,
                        'district' => $district_id,
                        'postal_code' => Helper::getKey("id", "Subdistricts", "postal_code", $value1['postal_code']),
                        'Address1' => $value1['Address1'],
                        'Profile' => $value1['Profile'],
                        'SIUPNo' => $value1['SIUPNo'],
                        'TaxNo' => $value1['TaxNo'],
                        'Domicile' => $value1['Domicile'],
                        'NotarialDeed' => $value1['NotarialDeed'],
                        'LicenseNo' => $value1['LicenseNo'],
                        'AggrEffectiveDate' => ( $value1['AggrEffectiveDate'] == null ? null : date('Y-m-d H:i:s', strtotime($value1['AggrEffectiveDate']))),
                        'AggrExpiryDate' => ( $value1['AggrExpiryDate'] == null ? null : date('Y-m-d H:i:s', strtotime($value1['AggrExpiryDate']))),
                        'RenewalDate' => ($value1['RenewalDate'] == "" ? null : date('Y-m-d H:i:s', strtotime($value1['RenewalDate']))),
                        'DiscProvider' => $value1['DiscProvider'],
                        'provider_email' => $value1['provider_email'],
                        'PhoneNo' => $value1['PhoneNo'],
                        'FaxNo' => $value1['FaxNo'],
                        'Remark' => $value1['Remark'],
                        'IsDifferentHospital' => $value1['IsDifferentHospital'],
                        'created_by' => $request->auth->id,
                        'last_modified_by' => $request->auth->id,
                        'status' => Helper::getCodeMasterId("MBS", $value1['status']),
                        'insurance_id' => $data["insurance_id"]
                    ]
                );

                if ($comp) {
                    $comp_id = $comp->id;
                    $cek = Biz_providers::select('id')->where('compprovid', $comp_id)->first();
                    if ($cek) {
                        $idprov = $cek->id;
                        $cek->delete();

                        $cek = BankInfos::where([['ref_type', 'biz_providers'], ['ref_id', $idprov]]);
                        if ($cek) {
                            $cek->delete();
                        }

                        $cek = Finances::where([['ref_type', 'biz_providers'], ['ref_id', $idprov]]);
                        if ($cek) {
                            $cek->delete();
                        }
                    }

                    $cek = BankInfos::where([['ref_type', 'biz_compproviders'], ['ref_id', $comp_id]]);
                    if ($cek) {
                        $cek->delete();
                    }

                    if (isset($value4)) {
                        $cek = Finances::where([['ref_type', 'biz_compproviders'], ['ref_id', $comp_id]]);
                        if ($cek) {
                            $cek->delete();
                        }
                    }

                    try {
                        $value2['type'] = Helper::getCodeMasterId("PCT", $value2['type']);
                        $value2['country'] = $pcountry_code;
                        $value2['province'] = ($value2['province'] == "" ? "" : $pprov_id);
                        $value2['city'] = $pcity_id;
                        $value2['district'] = $pdistrict_id;
                        $value2['postal_code'] = Helper::getKey("id", "Subdistricts", "postal_code", $value1['postal_code']);
                        $value2['merchant_id'] = $data["insurance_id"];
                        $value2['id_user'] = $request->auth->id;
                        $value3['id_user'] = $request->auth->id;

                        $prov = $this->providers->addNewProvFromComp($comp_id, $value2);

                        $prov_id = $prov->id;

                        $value3['bank_code'] = Helper::getCodeMasterId("BNK", $value3['bank_name']);

                        $bankcomp = Helper::addBankInfo($value3, "biz_compproviders", $comp_id);
                        $bankprov = Helper::addBankInfo($value3, "biz_providers", $prov_id);

                        if (isset($value4)) {
                            $cp = [];
                            foreach ($value4 as $val) {
                                ($val['ref_type'] == 'biz_compproviders' ? $val['ref_id'] = $comp_id : $val['ref_id'] = $prov_id);
                                $cp[] = [
                                    'ref_type' => $val['ref_type'],
                                    'ref_id' => $val['ref_id'],
                                    'pic_name' => $val['pic_name'],
                                    'pic_phone' => $val['pic_phone'],
                                    'pic_email' => $val['pic_email'],
                                    'pic_type' => $val['pic_type']
                                ];
                            }
                            $cps = Helper::addCP($cp, null, null);
                        }
                    } catch (\Exception $e) {
                        if (isset($comp_id)) {
                            $cek = Biz_Compproviders::find($comp_id);

                            $cek->delete();
                        }
                        if (isset($prov_id)) {
                            $cek = Biz_providers::find($prov_id);
                            $cek->delete();
                        }

                        if (isset($comp_id) && isset($prov_id)) {
                            $cek = BankInfos::where([['ref_type', 'biz_compproviders'], ['ref_id', $comp_id]])
                                ->orWhere([['ref_type', 'biz_providers'], ['ref_id', $prov_id]]);
                            if ($cek) {
                                $cek->delete();
                            }
                            $cek = Finances::where([['ref_type', 'companies'], ['ref_id', $comp_id]])
                                ->orWhere([['ref_type', 'biz_providers'], ['ref_id', $prov_id]]);;
                            if ($cek) {
                                $cek->delete();
                            }
                        }

                        throw new \Exception();
                    }

                    $result['valid'][$i] = $value;
                    $result['valid'][$i]['is_valid'] = $value['is_valid'];
                    $result['valid'][$i]['msg'] = $value['msg'];

                    $i++;
                }
            } catch (\Exception $msg) {

                $result['invalid'][$k] = $value;
                $result['invalid'][$k]['is_valid'] = false;
                $result['invalid'][$k]['msg'] = $msg->getMessage();
                $k++;
            }
        }
        return response()->json($result, 200);
    }

    public function exportTemplate(){
        // $filename = "ProvidersTemplate.xlsx";
        // $file=Storage::disk('public')->get($filename);
 
        // return (new Response($file, 200))
        //       ->header('Content-Type', 'application/x-msexcel; charset=utf-8');

        // return Storage::disk('public')->download('excels/ProvidersTemplate.xlsx');

        $pathToFile = resource_path('templates/ProvidersTemplate.xlsx');
        return response()->download($pathToFile);
    }
}
