<?php

namespace App\Http\Controllers;

// // use File;
use Illuminate\Support\Facades\File;
use App\Members;
use Illuminate\Support\Facades\DB as DB;
use App\BankInfos;
use App\CodeMasters;
use App\Companies;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Cities;
use App\Provinces;
use App\Districts;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MembersExport;
use App\Insurances;
use Maatwebsite\Excel\Concerns\ToArray;
use Barryvdh\DomPDF\Facade as PDF;


class MembersController extends Controller
{
    public function showAllMembers(Request $request, $id = null)
    {
        $sortby         = $request->input('sortby');
        $sortvalue      = $request->input('sortvalue');
        // $insurance_id   = $request->auth->insurance_id;
        $status         = false;
        $error          = "data not found";
        $members        = Members::with(
            [
                'member_title',
                'member_type',
                'company_id',
                'marital_status',
                'citizen',
                'country',
                'prov',
                'city',
                'district',
                'postal_code',
                'occupation',
                'source_income',
                'bank_infos' => function ($query) {
                    $query->where('ref_type', '=', 'members');
                },
                'member_plan',
                'policy_member_type',
                'premium_type',
                'term_payment',
                'status',
                'insurance_id',
                'created_by',
                'last_modified_by'
            ]
        );

        if ($request->auth->isInsurance) {
            $members   = $members->where('insurance_id', $request->auth->insurance_id);
        }

        $code_vip = CodeMasters::where([['codeType', 'MST'], ['codeDesc', 'VIP']])->first();
        $code_vip = $code_vip->code;
        $code_bl = CodeMasters::where([['codeType', 'MST'], ['codeDesc', 'BlacklistPerson']])->first();
        $code_bl = $code_bl->code;

        foreach ($members as $m) {
            $m->vip = "No";
            $m->bl = "No";
            if (strpos($m->member_setting, $code_vip) !== false) {
                $m->vip = "Yes";
            }
            if (strpos($m->member_setting, $code_bl) !== false) {
                $m->bl = "Yes";
            }
        }

        if ($id) {
            $members = $members->where('id', $id)->first();

            if ($members) {
                $status     = true;
                $error      = null;
            }
        } else {
            if ($request->has('keyword')) {
                $keyword    = $request->keyword;
                $wheres     = array(
                    "member_id", "member_name",
                    "phone_no", "identity_no", "policy_holder", "policy_no", "family_no", "created_at"
                );
                $members = Helper::dynamicSearch($members, $wheres, $keyword);

                if (!$members->count()) {
                    $members = Members::with(
                        [
                            'member_title',
                            'member_type',
                            'company_id',
                            'marital_status',
                            'citizen',
                            'country',
                            'prov',
                            'city',
                            'district',
                            'postal_code',
                            'occupation',
                            'source_income',
                            'bank_infos' => function ($query) {
                                $query->where('ref_type', '=', 'members');
                            },
                            'member_plan',
                            'policy_member_type',
                            'premium_type',
                            'term_payment',
                            'status',
                            'insurance_id',
                            'created_by',
                            'last_modified_by'
                        ]
                    )->where('insurance_id', '=', $request->auth->insurance_id);
                    $members = $members->whereHas('member_type', function ($query) use ($keyword) {
                        $query->where('codeDesc', 'like', '%' . $keyword . '%');
                    })->orWhereHas('status', function ($query) use ($keyword) {
                        $query->where('codeDesc', 'like', '%' . $keyword . '%');
                    })->orWhereHas('created_by', function ($query) use ($keyword) {
                        $query->where('name', 'like', '%' . $keyword . '%');
                    })->orWhereHas('last_modified_by', function ($query) use ($keyword) {
                        $query->where('name', 'like', '%' . $keyword . '%');
                    })->orWhere('created_at', 'like', '%' . $keyword . '%');

                    $code_vip = CodeMasters::where([['codeType', 'MST'], ['codeDesc', 'VIP']])->first();
                    $code_vip = $code_vip->code;
                    $code_bl = CodeMasters::where([['codeType', 'MST'], ['codeDesc', 'BlacklistPerson']])->first();
                    $code_bl = $code_bl->code;

                    foreach ($members as $m) {
                        $m->vip = "No";
                        $m->bl = "No";
                        if (strpos($m->member_setting, $code_vip) !== false) {
                            $m->vip = "Yes";
                        }
                        if (strpos($m->member_setting, $code_bl) !== false) {
                            $m->bl = "Yes";
                        }
                    }
                }
                if ($members->count()) {
                    $status     = true;
                    $error      = null;
                }
                $members    = Helper::sorting($members, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table      = 'members';
                    $filter     = $request->input('filter');
                    $filtername         = json_decode($filter[0]);
                    $filternameby       = $filtername->by;
                    $filternamevalue    = $filtername->value;
                    if ($filternameby == 'startendDate') {
                        $filterdate     = explode("/", $filternamevalue);
                        $where          = array('policy_effective_date', 'policy_expiry_date');
                        $members        = $members->whereBetween($where[0], [$filterdate[0], $filterdate[1]])
                            ->orWhereBetween($where[1], [$filterdate[0], $filterdate[1]]);
                    } else {
                        $members    = Helper::filterSearch($members, $table, $filter);
                    }

                    foreach ($filter as $f) {
                        $filterdata     = json_decode($f);
                        $filterby       = $filterdata->by;
                        $filtervalue    = $filterdata->value;

                        if (is_array($filtervalue)) {
                            if ($filterby == 'memberTitle') {
                                $members    = $members->whereHas('member_title', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'memberType') {
                                $members    = $members->whereHas('member_type', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'maritalStatus') {
                                $members    = $members->whereHas('marital_status', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'citizen') {
                                $members    = $members->whereHas('citizen', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'country') {
                                $members    = $members->whereHas('country', function ($query) use ($filtervalue) {
                                    $query->whereIn('country_code', $filtervalue)
                                        ->orWhereIn('country_desc', $filtervalue);
                                });
                            } elseif ($filterby == 'prov') {
                                $members    = $members->whereHas('prov', function ($query) use ($filtervalue) {
                                    $query->whereIn('prov_desc', $filtervalue);
                                });
                            } elseif ($filterby == 'city') {
                                $members    = $members->whereHas('city', function ($query) use ($filtervalue) {
                                    $query->whereIn('city_desc', $filtervalue);
                                });
                            } elseif ($filterby == 'district') {
                                $members    = $members->whereHas('district', function ($query) use ($filtervalue) {
                                    $query->whereIn('district_desc', $filtervalue);
                                });
                            } elseif ($filterby == 'postalCode') {
                                $members    = $members->whereHas('postal_code', function ($query) use ($filtervalue) {
                                    $query->whereIn('subdistrict_desc', $filtervalue);
                                });
                            } elseif ($filterby == 'occupation') {
                                $members    = $members->whereHas('occupation', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'sourceIncome') {
                                $members    = $members->whereHas('source_income', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'bank_branch' || $filterby == 'bank_account_name') {
                                $members->whereHas('bank_infos', function ($query) use ($filtervalue) {
                                    $query->whereIn('bank_branch', $filtervalue)
                                        ->orWhereIn('bank_account_name', $filtervalue);
                                });
                            } elseif ($filterby == 'memberPlan') {
                                $members    = $members->whereHas('member_plan', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'policyMemberType') {
                                $members    = $members->whereHas('policy_member_type', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'premiumType') {
                                $members    = $members->whereHas('premium_type', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'termPayment') {
                                $members    = $members->whereHas('term_payment', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'statusName') {
                                $members    = $members->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'createdBy') {
                                $members    = $members->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'lastModifiedBy') {
                                // var_dump($filtervalue);
                                //     die;
                                $members    = $members->whereHas('last_modified_by', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'phoneno') {
                                $members    = $members->where('phone_no', 'like', '%' . $filtervalue . '%');
                            } elseif ($filterby == 'vip') {
                                $code_vip = CodeMasters::where([['codeType', 'MST'], ['codeDesc', 'VIP']])->first();
                                $code_vip = json_decode($code_vip->code, true);
                                // $code[0] = $code_vip;
                                if ($filtervalue[0] == "Yes") {
                                    $members    = $members->where('member_setting', 'like', '%' . $code_vip . '%');
                                } else {
                                    $members    = $members->where('member_setting', 'not like', '%' . $code_vip . '%');
                                }
                            } elseif ($filterby == 'bl') {
                                $code_bl = CodeMasters::where([['codeType', 'MST'], ['codeDesc', 'BlacklistPerson']])->first();
                                $code_bl = json_decode($code_bl->code, true);
                                // $code[0] = $code_bl;
                                if ($filtervalue[0] == "Yes") {
                                    $members    = $members->where('member_setting', 'like', '%' . $code_bl . '%');
                                } else {
                                    $members    = $members->where('member_setting', 'not like', '%' . $code_bl . '%');
                                }
                            };
                        } else {
                            if ($filterby == 'memberTitle') {
                                $members    = $members->whereHas('member_title', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'memberType') {
                                $members    = $members->whereHas('member_type', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'maritalStatus') {
                                $members    = $members->whereHas('marital_status', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'citizen') {
                                $members    = $members->whereHas('citizen', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'country') {
                                $members    = $members->whereHas('country', function ($query) use ($filtervalue) {
                                    $query->where('country_code', 'like', '%' . $filtervalue . '%')
                                        ->orWhere('country_desc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'prov') {
                                $members    = $members->whereHas('prov', function ($query) use ($filtervalue) {
                                    $query->where('prov_desc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'city') {
                                $members    = $members->whereHas('city', function ($query) use ($filtervalue) {
                                    $query->where('city_desc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'district') {
                                $members    = $members->whereHas('district', function ($query) use ($filtervalue) {
                                    $query->where('district_desc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'postalCode') {
                                $members    = $members->whereHas('postal_code', function ($query) use ($filtervalue) {
                                    $query->where('subdistrict_desc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'occupation') {
                                $members    = $members->whereHas('occupation', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'sourceIncome') {
                                $members    = $members->whereHas('source_income', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'bank_branch' || $filterby == 'bank_account_name') {
                                $members->whereHas('bank_infos', function ($query) use ($filtervalue) {
                                    $query->where('bank_branch', 'like', '%' . $filtervalue . '%')
                                        ->orWhere('bank_account_name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'memberPlan') {
                                $members    = $members->whereHas('member_plan', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'policyMemberType') {
                                $members    = $members->whereHas('policy_member_type', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'premiumType') {
                                $members    = $members->whereHas('premium_type', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'termPayment') {
                                $members    = $members->whereHas('term_payment', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'statusName') {
                                $members    = $members->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'createdBy') {
                                $members    = $members->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'lastModifiedBy') {
                                // var_dump($filtervalue);
                                // die;
                                $members    = $members->whereHas('last_modified_by', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'phoneno') {
                                $members    = $members->where('phone_no', 'like', '%' . $filtervalue . '%');
                            } elseif ($filterby == 'vip') {
                                $code_vip = CodeMasters::where([['codeType', 'MST'], ['codeDesc', 'VIP']])->first();
                                $code_vip = json_decode($code_vip->code, true);
                                if ($filtervalue[0] == "Yes") {
                                    $members    = $members->where('member_setting', 'like', '%' . $code_vip . '%');
                                } else {
                                    $members    = $members->where('member_setting', 'not like', '%' . $code_vip . '%');
                                }
                            } elseif ($filterby == 'bl') {
                                $code_bl = CodeMasters::where([['codeType', 'MST'], ['codeDesc', 'BlacklistPerson']])->first();
                                $code_bl = json_decode($code_bl->code, true);
                                if ($filtervalue[0] == "Yes") {
                                    $members    = $members->where('member_setting', 'like', '%' . $code_bl . '%');
                                } else {
                                    $members    = $members->where('member_setting', 'not like', '%' . $code_bl . '%');
                                }
                            };
                        }
                    }
                }
            }

            $members = Helper::sorting($members, $request->input('sortby'), $request->input('sortvalue'));
            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $members    = $members->paginate($limit, ['*'], 'page', $page);
            $meta       = [
                'page'      => (int) $members->currentPage(),
                'perPage'   => (int) $members->perPage(),
                'total'     => (int) $members->total(),
                'totalPage' => (int) $members->lastPage()
            ];

            $code_vip = CodeMasters::where([['codeType', 'MST'], ['codeDesc', 'VIP']])->first();
            $code_vip = $code_vip->code;
            $code_bl = CodeMasters::where([['codeType', 'MST'], ['codeDesc', 'BlacklistPerson']])->first();
            $code_bl = $code_bl->code;

            foreach ($members as $m) {
                $m->vip = "No";
                $m->bl = "No";
                if (strpos($m->member_setting, $code_vip) !== false) {
                    $m->vip = "Yes";
                }
                if (strpos($m->member_setting, $code_bl) !== false) {
                    $m->bl = "Yes";
                }
            }

            $members   = $members->toArray()['data'];
            // print_r($members);
            // die;
        }

        if ($members) {
            $status = true;
            $error = "";
        }

        $response = [
            "status" => (bool) $status,
            "meta" => (isset($meta) ? $meta : null),
            "data" => (isset($members) ? $members : null),
            "error" => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function create(Request $request)
    {
        $this->validate($request, Members::getValidationRule());
        do {
            $date = date("dmy");
            $rand = str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
            $member_id = $request->member_id . $date . $rand;
            $cekmember   = Members::where('member_id', '=', $member_id)->first();
        } while ($cekmember);
        // try {
        if ($request->member_type == 38 or $request->member_type == "38") {
            $policy_holder = $member_id . " - " . $request->member_name;
        } else {
            $policy_holder = $request->comp_policy_holder;
        }

        $members = Members::create([
            'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->input('insurance_id')),
            'member_id' => $member_id,
            'member_title' => $request->member_title,
            'member_name' => $request->member_name,
            'member_type' => $request->member_type,
            'company_id' => $request->company_id,
            'birth_date' => $request->birth_date,
            'birth_place' => $request->birth_place,
            'gender' => strtolower($request->gender),
            'marital_status' => $request->marital_status,
            'citizen' => $request->citizen,
            'phone_no' => $request->phone_no,
            'email' => $request->email,
            'address' => $request->address,
            'country' => $request->country,
            'prov' => $request->prov,
            'city' => $request->city,
            'district' => $request->district,
            'postal_code' => $request->postal_code,
            'identity_no' => $request->identity_no,
            'family_no' => $request->family_no,
            'tax_no' => $request->tax_no,
            'occupation' => $request->occupation,
            'position' => $request->position,
            'source_income' => $request->source_income,
            'source_income_remark' => $request->source_income_remark,
            'salary' => $request->salary,
            'effective_date' => $request->effective_date,
            'expire_date' => $request->expire_date,
            'policy_no' => $request->policy_no,
            'policy_holder' => $policy_holder,
            'policy_member_type' => $request->policy_member_type,
            'family_code' => $request->family_code,
            'payor' => $request->payor,
            'policy_effective_date' => $request->policy_effective_date,
            'policy_expiry_date' => $request->policy_expiry_date,
            'policy_renewal_date' => $request->policy_renewal_date,
            'card_no' => $request->card_no,
            'member_plan' => $request->member_plan,
            'plan_amt' => $request->plan_amt,
            'premium_type' => $request->premium_type,
            'premium_member' => $request->premium_member,
            'term_payment' => $request->term_payment,
            'member_setting' => $request->member_setting,
            'member_setting_remark' => $request->member_setting_remark,
            'remark' => $request->remark,
            'status' => $request->status,
            'created_by' => $request->id_user,
            'last_modified_by' => $request->id_user
        ]);

        if ($members) {
            try {

                if (isset($request->bank_code)) {
                    Helper::addBankInfo($request, "members", $members->id);
                }
            } catch (\Exception $e) {
                $cek = Members::find($members->id);
                if ($cek) {
                    $cek->delete();
                }
                $cek = BankInfos::where([['ref_type', 'members'], ['ref_id', $members->id]]);
                if ($cek) {
                    $cek->delete();
                }
                return response()->json(['message' => 'Member Registration Failed!'], 409);
            }
        }

        return response()->json($members, 200);
        // } catch (\Exception $e) {
        //     return response()->json(['message' => 'Member Registration Failed!'], 409);
        // }
    }

    public function update($id, Request $request)
    {

        $this->validate($request, Members::getValidationRule());
        $members = members::find($id);
        if ($request->member_type == 38 or $request->member_type == "38") {
            $policy_holder = $request->member_id . " - " . $request->member_name;
        } else {
            $policy_holder = $request->comp_policy_holder;
        }

        $members->update([
            'insurance_id' => $request->auth->isInsurance ? $request->auth->insurance_id : $request->input('insurance_id'),
            'member_id' => $request->member_id,
            'member_title' => $request->member_title,
            'member_name' => $request->member_name,
            'member_type' => $request->member_type,
            'company_id' => $request->company_id,
            'birth_date' => $request->birth_date,
            'birth_place' => $request->birth_place,
            'gender' => strtolower($request->gender),
            'marital_status' => $request->marital_status,
            'citizen' => $request->citizen,
            'phone_no' => $request->phone_no,
            'email' => $request->email,
            'address' => $request->address,
            'country' => $request->country,
            'prov' => $request->prov,
            'city' => $request->city,
            'district' => $request->district,
            'postal_code' => $request->postal_code,
            'identity_no' => $request->identity_no,
            'family_no' => $request->family_no,
            'tax_no' => $request->tax_no,
            'occupation' => $request->occupation,
            'position' => $request->position,
            'source_income' => $request->source_income,
            'source_income_remark' => $request->source_income_remark,
            'salary' => $request->salary,
            'effective_date' => $request->effective_date,
            'expire_date' => $request->expire_date,
            'policy_no' => $request->policy_no,
            'policy_holder' => $policy_holder,
            'policy_member_type' => $request->policy_member_type,
            'family_code' => $request->family_code,
            'payor' => $request->payor,
            'policy_effective_date' => $request->policy_effective_date,
            'policy_expiry_date' => $request->policy_expiry_date,
            'policy_renewal_date' => $request->policy_renewal_date,
            'card_no' => $request->card_no,
            'member_plan' => $request->member_plan,
            'plan_amt' => $request->plan_amt,
            'premium_type' => $request->premium_type,
            'premium_member' => $request->premium_member,
            'term_payment' => $request->term_payment,
            'member_setting' => $request->member_setting,
            'member_setting_remark' => $request->member_setting_remark,
            'remark' => $request->remark,
            'status' => $request->status,
            'last_modified_by' => $request->id_user,
        ]);

        if ($members) {
            $bank_infos = BankInfos::where('ref_id', $id)->delete();

            if (isset($request->bank_code)) {
                Helper::addBankInfo($request, "members", $members->id);
            }
        }

        return response()->json($members, 200);
    }

    public function delete($id)
    {
        $members = Members::find($id);
        $members->delete();
        return response('Deleted Successfully', 200);
    }

    public function mass_delete(Request $request)
    {
        $members = Members::whereIn('id', $request->id);
        $members->delete();
        return response('Deleted Successfully', 200);
    }

    public function excelMembers(Request $request)
    {

        // $insurance_id = ($request->auth->isInsurance != "0" ? $request->auth->insurance_id : $request->insurance_id);
        $id = $request->id;

        $members = Members::with(
            [
                'member_title',
                'member_type',
                'company_id',
                'marital_status',
                'citizen',
                'country',
                'prov',
                'city',
                'district',
                'postal_code',
                'occupation',
                'source_income',
                'bank_infos' => function ($query) {
                    $query->with('bank_code')->where('ref_type', '=', 'members');
                },
                'member_plan',
                'policy_member_type',
                'premium_type',
                'term_payment',
                'status',
                'insurance_id',
                'created_by',
                'last_modified_by'
            ]
        );
        
        if ($request->auth->isInsurance == "1") {

            $members      = $members->where('insurance_id', $request->auth->insurance_id);
        }
        elseif ($request->auth->isInsurance == "0" && $request->insurance_id) {
            $members      = $members->where('insurance_id', $request->insurance_id);
        }

        if ($id or $id !== null) {
            $members->whereIn('id', $id);
        }

        $members = $members->get();


        foreach ($members as $m) {
            $other_policy_no = null;
            $other_policy_name = null;
            if ($m->member_setting_remark !== "" or $m->member_setting_remark) {
                $remark = explode("|", $m->member_setting_remark);
                $other_policy_no = $remark[0];
                $other_policy_name = $remark[1];
            }
            $m->other_policy_no = $other_policy_no;
            $m->other_policy_name = $other_policy_name;
        }

        $members = new MembersExport($members->toArray());
        $date = date("dmy");


        // return Excel::download(new MembersExport($insurance_id), 'Members.xlsx');
        return Excel::download($members, 'Members-' . $date . '.xlsx');
    }

    public function pdf(Request $request)
    {
        $id = $request->id;
        // $insurance_id = ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id);
        $members = Members::with(
            [
                'member_title',
                'member_type',
                'marital_status',
                'citizen',
                'country',
                'prov',
                'city',
                'district',
                'postal_code',
                'occupation',
                'source_income',
                'bank_infos' => function ($query) {
                    $query->where('ref_type', '=', 'members');
                },
                'member_plan',
                'policy_member_type',
                'premium_type',
                'term_payment',
                'status',
                'insurance_id',
                'created_by',
                'last_modified_by'
            ]
        );

        if ($request->auth->isInsurance == "1") {

            $members      = $members->where('insurance_id', $request->auth->insurance_id);
        }
        elseif ($request->auth->isInsurance == "0" && $request->insurance_id) {
            $members      = $members->where('insurance_id', $request->insurance_id);
        }

        if ($id or $id !== null) {
            $members->whereIn('id', $id);
        }

        $members = $members->get();

        // return $members;
        // die;

        $pdf = PDF::loadview('members', ['member' => $members->toArray()])->setPaper('a4', 'landscape');
        $date = date("dmy");

        // return $members;die;
        return $pdf->download('members-' . $date . '.pdf');
    }

    public function import(Request $request)
    {
        $data = $request->json()->all();
        $insurance_id = ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id);
        $result = [];
        $i = 0;
        $k = 0;
        foreach ($data['data'] as $value1) {
            $msg = "";

            //validasi
            //cek title
            if ($value1['member_title'] !== "" && $value1['member_title'] !== null) {
                try {
                    $cek = Helper::getCodeMasterId("MTL", $value1['member_title']);
                } catch (\Exception $e) {
                    $msg .= " Member Title invalid.";
                }
            }

            //member type
            try {
                $cek = Helper::getCodeMasterId("MBT", $value1['member_type']);
            } catch (\Exception $e) {
                $msg .= " Member Type should be 'Personal' or 'Company'";
            }

            //company name
            if (strtolower($value1['member_type']) == "company") {
                try {
                    $parent_comp_id = Helper::getKey("id", "Companies", "company_name", $value1['company_name']);
                } catch (\Exception $e) {
                    $msg .= " Company Name not found.";
                }
            }

            //gender
            if (strtolower($value1['gender']) !== "f" and strtolower($value1['gender']) !== "m") {
                $msg .= " Gender invalid.";
            }

            //marital status
            try {
                $marital = Helper::getCodeMasterId("MRS", $value1['marital_status']);
            } catch (\Exception $e) {
                $msg .= " Marital status invalid.";
            }

            //citizenship
            try {
                $cek = Helper::getCodeMasterId("CTT", $value1['citizen']);
            } catch (\Exception $e) {
                $msg .= " Citizenship invalid.";
            }

            //country
            try {
                $country_code = Helper::getKey("country_code", "Countries", "country_desc", $value1['country']);
            } catch (\Exception $e) {
                $msg .= " Country not found.";
            }

            // echo $country_code . " ";
            //province
            if (isset($country_code)) {
                if ($value1['prov'] !== "") {
                    try {
                        $prov_id = Provinces::where([["prov_desc", $value1['prov']], ['country_code', $country_code]])->first();
                        $prov_id = $prov_id->id;
                    } catch (\Exception $e) {
                        $msg .= " Province not found.";
                    }
                }
            }

            // echo $prov_id . " ";

            //city
            if (isset($prov_id)) {
                try {
                    $cek = explode(" ", strtolower($value1['city']));
                    if ($cek[0] == "kabupaten") {
                        $city_id = Cities::where('city_desc', substr($value1['city'], 10))->first();
                        $city_id = $city_id->id;
                    } elseif ($cek[0] == "kota") {
                        $city_id = Cities::where('city_desc', substr($value1['city'], 5))->first();
                        $city_id = $city_id->id;
                    } else {
                        $city = DB::table('cities')
                            ->where('city_desc', $value1['city'])
                            ->get();
                        if ($city->count() == 0) {
                            throw new \Exception();
                        } elseif ($city->count() == 1) {
                            $city_id = $city[0]->id;
                        } else {
                            $city_id = [];
                            foreach ($city as $c) {
                                array_push($city_id, $c->id);
                            }
                            $district = Districts::where('district_desc', $value1['district'])
                                ->whereIn('city_id', $city_id)
                                ->get();
                            if ($district->count()) {
                                $city_id = $district[0]->city_id;
                                $district_id = $district[0]->id;
                            } else {
                                throw new \Exception();
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $msg .= " City not found.";
                }
            }

            // echo $city_id . " ";

            //district
            if (isset($city_id) && !isset($district_id)) {
                try {
                    $district_id = Helper::getKey("id", "Districts", "district_desc", $value1['district']);
                } catch (\Exception $e) {
                    $msg .= " District not found.";
                }
            }

            // echo $district_id . " ";
            // die;
            //postal

            if (isset($district_id)) {
                try {
                    Helper::getKey("id", "Subdistricts", "postal_code", $value1['postal_code']);
                } catch (\Exception $e) {
                    $msg .= " Postal code not found.";
                }
            }

            //occupation
            try {
                Helper::getCodeMasterId("OCC", $value1['occupation']);
            } catch (\Exception $e) {
                $msg .= " Occupation invalid.";
            }

            //source income
            try {
                Helper::getCodeMasterId("MSI", $value1['source_income']);
            } catch (\Exception $e) {
                $msg .= " Source Income invalid.";
            }

            //policy member type
            try {
                Helper::getCodeMasterId("PMT", $value1['policy_member_type']);
            } catch (\Exception $e) {
                $msg .= " Membership invalid.";
            }

            //memberplan
            try {
                Helper::getCodeMasterId("MPL", $value1['member_plan']);
            } catch (\Exception $e) {
                $msg .= " Member Plan invalid.";
            }

            //premium type
            if ($value1['premium_type'] !== "") {
                try {
                    Helper::getCodeMasterId("PRT", $value1['premium_type']);
                } catch (\Exception $e) {
                    $msg .= " Premium Type invalid.";
                }
            }

            //term payment
            try {
                Helper::getCodeMasterId("TPT", $value1['term_payment']);
            } catch (\Exception $e) {
                $msg .= " Term Payment invalid.";
            }


            //status
            if (strtolower($value1['status']) !== "active" && strtolower($value1['status']) !== "in-active") {
                $msg .= " Member status should be 'Active' or 'In-Active'.";
            }


            //bank name
            try {
                Helper::getCodeMasterId("BNK", $value1['bank_name']);;
            } catch (\Exception $e) {
                $msg .= " bank name not found.";
            }

            try {

                if ($msg !== "") {
                    throw new \Exception($msg);
                }

                if (!$value1['member_id'] or $value1['member_id'] == "") {
                    do {
                        $date = date("dmy");
                        $rand = str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
                        $ins_name = Helper::getKey("insuranceName", "Insurances", "id", $insurance_id);
                        $ins_name = substr($ins_name, 0, 3);
                        $value1['member_id'] = $ins_name . $date . $rand;
                        $cekmember   = Members::where('member_id', '=', $value1['member_id'])->first();
                    } while ($cekmember);
                }

                if ($value1['member_type'] == "Company") {
                    $id_comp = Helper::getKey("id", "Companies", "company_name", $value1['company_name']);
                    $datacompany = Companies::where('id', $id_comp)->first();
                    $value1['policy_no'] = $datacompany->policy_number;
                    // $value1['policy_holder']=$datacompany->policy_holder;
                }


                $members = Members::updateOrCreate(
                    [
                        'member_id' => $value1['member_id'],
                        'insurance_id' => $insurance_id
                    ],
                    [
                        'member_title' => ($value1['member_title'] == "" ? "" : Helper::getCodeMasterId("MTL", $value1['member_title'])),
                        'member_name' => $value1['member_name'],
                        'member_type' => Helper::getCodeMasterId("MBT", $value1['member_type']),
                        'company_id' => ($value1['company_name'] !== "" ? Helper::getKey("id", "Companies", "company_name", $value1['company_name']) : null),
                        'birth_date' => date('Y-m-d H:i:s', strtotime($value1['birth_date'])),
                        'birth_place' => $value1['birth_place'],
                        'gender' => ($value1['gender'] == "" ? "" : strtolower(substr($value1['gender'], 0, 1))),
                        'marital_status' => Helper::getCodeMasterId("MRS", $value1['marital_status']),
                        'citizen' => Helper::getCodeMasterId("CTT", $value1['citizen']),
                        'phone_no' => $value1['phone_no'],
                        'email' => $value1['email'],
                        'address' => $value1['address'],
                        'country' =>  $country_code,
                        'prov' => ($value1['prov'] == "" ? "" : $prov_id),
                        'city' => $city_id,
                        'district' => $district_id,
                        'postal_code' => Helper::getKey("id", "Subdistricts", "postal_code", $value1['postal_code']),
                        'identity_no' => $value1['identity_no'],
                        'family_no' => $value1['family_no'],
                        'tax_no' => $value1['tax_no'],
                        'occupation' => Helper::getCodeMasterId("OCC", $value1['occupation']),
                        'position' => ($value1['position'] == "" ? "" : $value1['position']),
                        'source_income' => Helper::getCodeMasterId("MSI", $value1['source_income']),
                        'source_income_remark' => ($value1['source_income_remark'] == "" ? "" : $value1['source_income_remark']),
                        'salary' => $value1['salary'],
                        'effective_date' => date('Y-m-d H:i:s', strtotime($value1['effective_date'])),
                        'expire_date' => date('Y-m-d H:i:s', strtotime($value1['expire_date'])),
                        'policy_no' => $value1['policy_no'],
                        'policy_holder' => ($value1['company_name'] !== "" ? Helper::getKey("policy_holder", "Companies", "company_name", $value1['company_name']) :  $value1['member_id'] . " - " . $value1['member_name']),
                        'policy_member_type' => Helper::getCodeMasterId("PMT", $value1['policy_member_type']),
                        'family_code' => $value1['family_code'],
                        'payor' => $value1['payor'],
                        'policy_effective_date' => date('Y-m-d H:i:s', strtotime($value1['policy_effective_date'])),
                        'policy_expiry_date' => date('Y-m-d H:i:s', strtotime($value1['policy_expiry_date'])),
                        'policy_renewal_date' => ($value1['policy_renewal_date'] ? date('Y-m-d H:i:s', strtotime($value1['policy_renewal_date'])) : null),
                        'card_no' => $value1['card_no'],
                        'member_plan' => Helper::getCodeMasterId("MPL", $value1['member_plan']),
                        'plan_amt' => $value1['plan_amt'],
                        'premium_type' => ($value1['premium_type'] == "" ? "" : Helper::getCodeMasterId("PRT", $value1['premium_type'])),
                        'premium_member' => $value1['premium_member'],
                        'term_payment' => Helper::getCodeMasterId("TPT", $value1['term_payment']),
                        'member_setting' => $value1['member_setting'],
                        'member_setting_remark' => $value1['member_setting_remark'],
                        'remark' => $value1['remark'],
                        'status' => Helper::getCodeMasterId("MBS", $value1['status']),
                        'insurance_id' => $insurance_id,
                        'created_by' => $request->auth->id,
                        'last_modified_by' => $request->auth->id
                    ]
                );

                if ($members) {
                    $member_id = $members->id;
                    $cek = BankInfos::where([['ref_type', 'members'], ['ref_id', $member_id]]);
                    if ($cek) {
                        $cek->delete();
                    }
                    try {
                        $value1['bank_code'] = Helper::getCodeMasterId("BNK", $value1['bank_name']);
                        Helper::addBankInfo($value1, "members", $member_id);
                    } catch (\Exception $e) {
                        $cek = Members::find($member_id);
                        if ($cek) {
                            $cek->delete();
                        }
                        $cek = BankInfos::where([['ref_type', 'members'], ['ref_id', $member_id]]);
                        if ($cek) {
                            $cek->delete();
                        }
                        throw new \Exception();
                    }

                    $result['valid'][$i] = $members;
                    $result['valid'][$i]->is_valid = $value1['is_valid'];
                    $result['valid'][$i]->msg = $value1['msg'];

                    $i++;
                }
            } catch (\Exception $msg) {
                // echo "GAGAL";
                $result['invalid'][$k] = $value1;
                $result['invalid'][$k]['is_valid'] = false;
                $result['invalid'][$k]['msg'] = $msg->getMessage();
                $k++;
            }
        }
        return response()->json($result, 200);
    }
}
