<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Helpers\Helper;
use App\IziserviceCode;

class IziserviceCodeController extends Controller
{
    public function addServiceCode(Request $request)
    {
        do {
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
            $rand = substr(str_shuffle($permitted_chars), 0, 10);
            $cekrand   = IziserviceCode::where('iziservice_code', '=', $rand)->first();
        } while ($cekrand);

        $request->request->add(
            [
                'iziservice_code' => $rand,
                'created_by' => $request->auth->id
            ]
        );

        try {
            $servicecode   = new IziserviceCode();
            $params     = $request->all();
            $servicecode->fill($params);
            $servicecode->save();
            return response()->json(['data' => $servicecode, 'message' => 'Iziservice Code Registration Succeed'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Iziservice Code Registration Failed'], 409);
        }
    }

    public function showServiceCode(Request $request, $id = null)
    {
        $sortby         = $request->input('sortby');
        $sortvalue      = $request->input('sortvalue');
        $status         = false;
        $error          = "data not found";
        $getservicecode   = IziserviceCode::with(['plan','created_by', 'last_modified_by']);
        // ->get();
        // return $getservicecode;

        if ($id) {
            $getservicecode   = $getservicecode->where('id', $id)->first();

            if ($getservicecode) {
                $status     = true;
                $error      = null;
            }
        } else {
            if ($request->has('keyword')) {
                $keyword        = $request->keyword;
                $where          = array('iziservice_code', 'iziservice_name', 'description', 'created_at');
                $getservicecode   = Helper::dynamicSearch($getservicecode, $where, $keyword);

                if (!$getservicecode->count()) {
                    $getservicecode = IziserviceCode::with('created_by');
                    $getservicecode   = $getservicecode
                        ->whereHas('created_by', function ($query) use ($keyword) {
                            $query->where('name', 'like', '%' . $keyword . '%');
                        });
                }

                $getservicecode   = Helper::sorting($getservicecode, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table          = 'iziservice_codes';
                    $filter         = $request->input('filter');
                    $getservicecode   = Helper::filterSearch($getservicecode, $table, $filter);
                    foreach ($filter as $f) {
                        $filterdata         = json_decode($f);
                        $filterby           = $filterdata->by;
                        $filtervalue        = $filterdata->value;
                        if (is_array($filtervalue)) {
                            if ($filterby == 'created_by') {
                                $getservicecode   = $getservicecode->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            }elseif ($filterby == 'planDesc') {
                                $getservicecode   = $getservicecode->whereHas('plan', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            }elseif ($filterby == 'planCode') {
                                $getservicecode   = $getservicecode->whereHas('plan', function ($query) use ($filtervalue) {
                                    $query->whereIn('code', $filtervalue);
                                });
                            }
                        } else {
                            if ($filterby == 'created_by') {
                                $getservicecode  = $getservicecode->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            }elseif ($filterby == 'planDesc') {
                                $getservicecode  = $getservicecode->whereHas('plan', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            }elseif ($filterby == 'planCode') {
                                $getservicecode  = $getservicecode->whereHas('plan', function ($query) use ($filtervalue) {
                                    $query->where('code', 'like', '%' . $filtervalue . '%');
                                });
                            }
                        }
                    }
                }
            }

            if ($getservicecode->count()) {
                $status         = true;
                $error          = null;
            }

            $getservicecode   = Helper::sorting($getservicecode, $sortby, $sortvalue);
            $limit          = $request->has('limit') ? $request->input('limit') : 20;
            $page           = $request->has('page') ? $request->input('page') : 1;
            $getservicecode   = $getservicecode->paginate($limit, ['*'], 'page', $page);
            $meta           = [
                'page'          => (int) $getservicecode->currentPage(),
                'perPage'       => (int) $getservicecode->perPage(),
                'total'         => (int) $getservicecode->total(),
                'totalPage'     => (int) $getservicecode->lastPage()
            ];
            $getservicecode   = $getservicecode->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getservicecode) ? $getservicecode : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function updateServiceCode(Request $request, $id)
    {
        // $provtype       = $request->input('provtype');
        $request->request->add(['last_modified_by' => $request->auth->id]);
        $data   = IziserviceCode::find($id);

        if ($data != null) {
            $params = $request->all();
            $data->fill($params);
            $data->save();
            return response()->json(['status' => (bool) true, 'message' => 'Your data has been updated'], 200);
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'Update data failed'], 409);
        }
    }

    public function deleteServiceCode($id)
    {
        $getservicecode   = IziserviceCode::find($id);
        if ($getservicecode) {
            $getservicecode->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function massDeleteServiceCode(Request $request)
    {
        $getservicecode   = IziserviceCode::whereIn('id', $request->id);
        if ($getservicecode) {
            $getservicecode->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }
}
