<?php

namespace App\Http\Controllers;

// use App\Medicine;
use Illuminate\Http\Request;
use File;
use DB;

class TextSimiliarityController extends Controller
{
  public static function fixingJSOn($fileJSON = '', $mid=''){
      if ($fileJSON == '') return ["status" => false, "message" => "Json is required"];
      if ($mid=='') return ["status" => false, "message" => "Mid is required"];

      $medicine = DB::select("SELECT parent_service_id, service_name, isSubservice FROM provider_services WHERE mid = $mid");
      $JsonArr = $fileJSON;
      $deleteThis = false;
    // if ($request->getContent() == '') return ["status" => false, "message" => "Json is required"];
    // if (!isset($request->mid)) return ["status" => false, "message" => "Mid is required"];

    // $medicine = DB::select("SELECT parent_service_id, service_name, isSubservice FROM provider_services WHERE mid = $request->mid");
    // $JsonArr = json_decode($request->getContent(), true);
    // $deleteThis = false;

    foreach($JsonArr['Batch']['Documents']['Document'] as $jsonKey => $doc){
        // if(array_key_exists('DataTables', $doc)){
        if(isset($doc['DataTables'])){
            foreach($doc['DataTables']['DataTable'] as $datatableIndex => $datatable){
                if(count($datatable) > 4 and $datatable['Name']!='Summary'){
                    $prevSubtotal = '';
                    $subTotal = '';
                    $unsetIndex = 0;
                    foreach($datatable['Rows']['Row'] as $key => $row){
                        if($unsetIndex > 0){
                            unset($JsonArr['Batch']['Documents']['Document'][$jsonKey]['DataTables']['DataTable'][$datatableIndex]['Rows']['Row'][$key]);
                            $unsetIndex--;
                        }else{
                            if(isset($datatable['Rows']['Row'][$key+1])){
                                $nextRow = $datatable['Rows']['Row'][$key+1];
                                if(!array_key_exists('Value', $nextRow['Columns']['Column'][2])){
                                    print_r($nextRow);die;
                                }else{
                                    $nextSubService = $nextRow['Columns']['Column'][2]['Value'];
                                }
                            }

                            if(array_key_exists('Value', $row['Columns']['Column'][2])){
                                $subService = $row['Columns']['Column'][2]['Value'];
                            }else{
                                continue;
                            }

                            if(array_key_exists('Value', $row['Columns']['Column'][3]) and array_key_exists('Value', $row['Columns']['Column'][4])){    //pengecekan qty, dan billing amountnya
                                if(isset($datatable['Rows']['Row'][$key+1])){
                                    for($x = 1; $x <= 10; $x++){
                                        $nextSubService = $datatable['Rows']['Row'][$key+$x]['Columns']['Column'][2]['Value'];
                                        if(array_key_exists('Value', $datatable['Rows']['Row'][$key+$x]['Columns']['Column'][3]) and array_key_exists('Value', $datatable['Rows']['Row'][$key+$x]['Columns']['Column'][4])){
                                            break;
                                        }elseif(array_key_exists('Value', $datatable['Rows']['Row'][$key+$x]['Columns']['Column'][4]) and strpos(strtolower($nextSubService), 'total') !== false){
                                            break;
                                        }else{
                                            $fixedStringNext = self::predictVer2($nextSubService, $medicine);
                                            if(isset($fixedStringNext['high_prediction'])){
                                                if($fixedStringNext['isSubservice']==1){
                                                    $subService = $subService.' '.$nextSubService;
                                                    $unsetIndex++;
                                                }else{
                                                    break;
                                                }
                                            }else{
                                                $subService = $subService.' '.$nextSubService;
                                                $unsetIndex++;
                                            }
                                        }
                                    }
                                }
                                $fixedString = self::predictVer2($subService, $medicine);

                            }elseif(array_key_exists('Value', $row['Columns']['Column'][4]) and strpos(strtolower($subService), 'total') !== false){
                                if($subTotal == $prevSubtotal){
                                    $fixedString['high_prediction'] = 'Sub Total (Provider Service Not Found)';
                                }else{
                                    $fixedString['high_prediction'] = 'Sub Total '.$subTotal;
                                    $prevSubtotal = $subTotal;
                                }
                            }else{
                                $fixedString = self::predictVer2($subService, $medicine);
                                if(array_key_exists('high_prediction', $fixedString)){
                                    if($fixedString['isSubservice']==0){
                                        $subTotal = $fixedString['high_prediction'];
                                    }
                                }
                            }

                            if(array_key_exists('high_prediction', $fixedString)){
                                $JsonArr['Batch']['Documents']['Document'][$jsonKey]['DataTables']['DataTable'][$datatableIndex]['Rows']['Row'][$key]['Columns']['Column'][2]['Value'] = $fixedString['high_prediction'];
                            }else{
                                $JsonArr['Batch']['Documents']['Document'][$jsonKey]['DataTables']['DataTable'][$datatableIndex]['Rows']['Row'][$key]['Columns']['Column'][2]['Value'] = $subService;
                            }
                        }
                    }
                    $JsonArr['Batch']['Documents']['Document'][$jsonKey]['DataTables']['DataTable'][$datatableIndex]['Rows']['Row'] = array_values($JsonArr['Batch']['Documents']['Document'][$jsonKey]['DataTables']['DataTable'][$datatableIndex]['Rows']['Row']);
                }else{
                    //proses yg datatable summary tp ada datanya (ini beda strukturnya ada tiga kolom yaitu No, Name, dan Billing...)
                }
            }
        }
    }
    return $JsonArr;
  }

  public static function predictVer2($string, $medicine)
  {
    $text = strtolower($string);

    $arr_text = explode(" ", $text);
    $th = 37; // Nilai Threshold (Nilai yang menentukan hasil dari prediksi dapat diubah menyesuaikan dari keakuratan hasil prediksi)
    $phase1 = [];
    $phase2 = [];
    $final = [];

    foreach ($medicine as $real_name) { //processing phase1
      $test = $text;
      $lower_name = strtolower($real_name->service_name);
      for ($i = 0; $i < strlen($test); $i++) { //processing character on string
        $old_char = substr($test, $i, 1);
        $old_percentage = 0;
        if ($old_char !== " ") {
          foreach (range('a', 'z') as $value2) { //try to change character and check similarity after that
            $transform = substr_replace($test, $value2, $i, 1);
            similar_text($lower_name, $transform, $percentage);
            if ($percentage > $old_percentage) {
              if ($percentage > $th) { //try to get higher or equal th2 if not return to old character
                $test = substr_replace($transform, $value2, $i, 1);
                $phase1[] = ["real_name" => $real_name->service_name, "prediction" => mb_convert_encoding($test, 'UTF-8', 'UTF-8'), "percentage" => $percentage, "isSubservice" => $real_name->isSubservice];
              }
            }
            $old_percentage = $percentage;
          }
        }
      }
    }

    foreach ($phase1 as $key => $value) {
      if ($value["percentage"] > ($th + 10)) $arr_text = [];
    }

    if (count($arr_text) > 1) {
      foreach ($phase1 as $key => $value) {
        $phase2[$value["real_name"]] = $value["isSubservice"];
        foreach ($arr_text as $key2 => $value2) {
          if (strrpos($value["real_name"], $value2)) {
            if (isset($phase2[$value["real_name"]])) {
              $phase2[$value["real_name"]] += 1;
            } else {
              $phase2[$value["real_name"]] = 0;
            }
          }
        }
      }
    } else {
      foreach ($phase1 as $key => $value) {
        $phase2[$value["real_name"]] = 0;
        $phase2[$value["real_name"]] = $value["isSubservice"];
      }
    }

    // print_r($phase2);
    // print_r("<br>");
    // die;

    foreach ($phase2 as $key => $value) {
      similar_text(strtolower($key), $text, $percentage);
      if ($percentage >= $th) {
        $final["process"][] = ["real_name" => $key, "score" => $percentage, "isSubservice" => $value];
      }
    }

    if (isset($final["process"])) {
      $score = 0;
      foreach ($final["process"] as $key => $value) {
        if ($value["score"] > $score) {
          $final["high_prediction"] = $value["real_name"];
          $final["score"] = $value["score"];
          $final["isSubservice"] = $value["isSubservice"];
          $score = $value["score"];
        }
      }
    }
    return $final;
  }
}
