<?php

namespace App\Http\Controllers;

use App\ClaimActivity;
use App\ClaimDocs;
use App\CodeMasters;
use App\ClaimRegDoc;
use App\ClaimRegHdr;
use App\ClaimRegDocConverts;
use App\PendingClaim;
use App\PendingClaimConvert;
use App\User;
use Illuminate\Http\File as File;
use Illuminate\Support\Facades\Storage as Storage;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HistoryController;
use Google\Rpc\Help;
use Illuminate\Http\Request;
use App\Jobs\ClaimRegJob;
use App\Jobs\ConvertDocSupport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use DB;


class ClaimRegistrationController extends Controller
{
    private $historycontroller;

    public function __construct()
    {
        $this->historycontroller = new HistoryController;
    }

    //--------------------------------adapted from request template-------------------------------------

    public function uploadpdftemplate(Request $request)
    {
        $status     = false;
        $error      = "upload pdf failed";
        $rules      = [
            "img" => "required|mimes:pdf"
        ];
        $pdf        = $request->file('img');
        $basename   = $pdf->getClientOriginalName();
        $pdfname    = time() . "." . $pdf->getClientOriginalExtension();
        $filename   = Helper::uploadPdf($pdf, "temp/claimregdocs");
        if ($filename) {
            $status     = true;
            $error      = "";
        }
        $convertImage   = [];

        if ($status) {
            $convertImage   = $this->pdftoimageTemplates(base_path() . '/public/' . $filename);
            $convertImage['original_file'] = $filename;
            $response   = [
                "status"    => (bool) $status,
                "data"      => $convertImage,
                "name"      => $basename,
                "error"     => (isset($error) ? $error : null)
            ];
        } else {
            $response   = [
                "status"            => (bool) $status,
                "original_file"     => $filename,
                "name"              => $basename,
                "error"             => (isset($error) ? $error : null)
            ];
        }


        return response()->json($response);
    }

    public function pdftoimageTemplates($pdfFile)
    {
        $pdf        = new \Spatie\PdfToImage\Pdf($pdfFile);
        $filename   = pathinfo($pdfFile, PATHINFO_FILENAME);
        $total      =  $pdf->getNumberOfPages();
        $image      = [];

        $path = 'temp/claimregdocs/';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $i = 0;
        foreach (range(1, $total) as &$pageNumber) {
            $img[$i]['convert_path']   = $path . $filename . '-page' . $pageNumber . '.png';
            // $img['rel_path']    = '/storage/convert/claimregdocs/' . $category . '/' . $filename . '-page' . $pageNumber . '.png';
            $img[$i]['pageNumber']  = $pageNumber;
            $pdf->setPage($pageNumber)
                ->setOutputFormat('png')
                ->saveImage($img[$i]['convert_path']);
            $i++;
        }

        $image[]        = $img;

        $data = [
            'total'     =>  $pdf->getNumberOfPages(),
            'image'     => $image
        ];

        return $data;
    }

    public function checkOcrRead($supportdoctype)
    {
        // return $supportdoctype;
        Log::info("support doc type : ".$supportdoctype);
        if ($supportdoctype != null) {
            $cek = ClaimDocs::where('id', $supportdoctype)->first();
            if ($cek->isOcrRead == '1') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function assignClaim($claimregno, $insurance_id)
    {
        Log::info("masuk assign claim");

        $permission = 'Pages.Transaction.ClaimSubmission.Analyze';
        $userjunioranalyst = User::with(['permissions'])->whereHas('permissions', function ($query) use ($permission) {
            return $query->where('permission', $permission);
        })->where('isLogin', 1)->get('id')->map(function ($id) {
            $data = [
                "user_id" => $id->id,
                "pending_task" => ClaimRegHdr::select('assignTo')->where('status', 1)->Where('assignTo', $id->id)->count()
            ];
            return $data;
        });
        $sortdata = json_decode($userjunioranalyst);
        usort($sortdata, function ($a, $b) {
            return $a->pending_task > $b->pending_task;
        });

        //hitung final totalpages
        $finaltotalpages = 0;
        $path = 'Transaction/claim/' . $claimregno . '/originals';
        $files = ClaimRegDoc::where('claimregno', $claimregno)->get();
        if ($files) {
            foreach ($files as $perfile) {
                $countpage = 0;
                //getbasename
                $namafile = pathinfo($perfile->doc_name, PATHINFO_FILENAME);
                //ubah ke .txt
                $namafiletxt = $namafile . ".txt";
                if (Storage::disk('minio')->exists($path . "/" . $namafiletxt)) {
                    $convertresult = ClaimRegDocConverts::where('id_claimregdoc', $perfile->id)->get();
                    $countpage = $convertresult->count();
                }
                $finaltotalpages = $finaltotalpages + $countpage;
            }
        }

        Log::info("finaltotalpages : " . $finaltotalpages);

        $usertoassign = $sortdata ? $sortdata[0]->user_id : NULL;

        Log::info("user to assign id : " . $usertoassign);


        $updatebalance['ref_trans'] = 2;
        $updatebalance['ref_id'] = $claimregno;
        $updatebalance['insurance_id'] = $insurance_id;
        $updatebalance['qty'] = $finaltotalpages;
        $updatebalance['status'] = null;
        $this->historycontroller->runCreateHistoryQueue($updatebalance);

        // Log::info("finaltotalpages : ".$finaltotalpages);
        $modifhdr = ClaimRegHdr::where('claimregno', $claimregno)
            ->update([
                "isOcrRead" => '1',
                "assignTo" => $usertoassign
            ]);

        $deletePendingClaim = PendingClaim::where('claimregno', $claimregno)->delete();
    }

    //new with queue
    public function addRegistration2(Request $request)
    {
        Log::info("masuk proses registration");
        $permission = 'Pages.Transaction.ClaimSubmission.Analyze';
        $userjunioranalyst = User::with(['permissions'])->whereHas('permissions', function ($query) use ($permission) {
            return $query->where('permission', $permission);
        })->where('isLogin', 1)->get('id')->map(function ($id) {
            $data = [
                "user_id" => $id->id,
                // "task_count" => ClaimRegHdr::select('assignTo')->where('status', 240)->Where('assignTo', $id->id)->count(),
                "pending_task" => ClaimRegHdr::select('assignTo')->where('status', 1)->Where('assignTo', $id->id)->count()
            ];
            return $data;
        });
        $sortdata = json_decode($userjunioranalyst);
        usort($sortdata, function ($a, $b) {
            return $a->pending_task > $b->pending_task;
        });


        // $usertoassign = $sortdata ? $sortdata[0]->user_id : NULL;

        if (strlen($request->claimregno) == 3) {
            $date = date('Ymd');
            $plancode = Helper::getKey("code", "CodeMasters", "code", $request->plan);

            $jmlDoc = \DB::table('claimreghdr')->max('id');
            if (!$jmlDoc) {
                $jmlDoc = "00001";
            } else {
                // $jmlDoc = substr($jmlDoc->claim_doc_no, 4);
                $jmlDoc += 1;
                $jmlDoc = str_pad($jmlDoc, 5, '0', STR_PAD_LEFT);
            }
            $claimregno = $request->claimregno . "-" . $plancode . "-" . $date . "-" . $jmlDoc;

            $hdr = ClaimRegHdr::create([
                'insurance_id' => $request->auth->insurance_id,
                'doc_type' => $request->doc_type,
                'ref_no' => $request->ref_no,
                'memberid' => $request->memberid,
                'indexno' => $request->indexno,
                'vip' => $request->vip,
                'cardno' => $request->cardno,
                'policyno' => $request->policyno,
                'plan' => $request->plan,
                'benefit_plan' => null,
                'bizprovid' => $request->bizprovid,
                'companyid' => $request->companyid,
                'claimregno' => $claimregno,
                'remark' => $request->remark,
                'is_sent_mail' => 0,
                'status' => '1',
                'isOcrRead' => '0',
                'created_by' => $request->auth->id,
                'last_update_by' => $request->auth->id,
                'assignTo' => null
            ]);
        } else {
            $claimregno = $request->claimregno;
        }

        // move main doc asli
        $folderOri = $_SERVER['DOCUMENT_ROOT'] . '/assets/claim/' . $claimregno . "/originals/";
        if (!is_dir($folderOri)) {
            mkdir($folderOri, 0777, true);
            chmod($folderOri, 0777);
        }

        $pathOriMainDoc = Helper::moveImg($folderOri, $request->main['file_path'], $folderOri . $request->main['doc_name']);
        $posisi = strpos($pathOriMainDoc, "assets");
        $savepath = substr($pathOriMainDoc, $posisi);
        $docOriMain = ClaimRegDoc::create([
            'insurance_id' => $request->auth->insurance_id,
            'doc_type' => $request->main['doc_type'],
            'doc_name' => $request->main['doc_name'],
            'path' => $savepath,
            'doc_category' => "main",
            'claimregno' => $claimregno,
            'created_by' => $request->auth->id,
            'last_update_by' => $request->auth->id,
            'basename' => $request->main['doc_name'],
        ]);

        // move main doc convert & save ke claimregdoc convert
        $folderConvert = $_SERVER['DOCUMENT_ROOT'] . '/assets/claim/' . $claimregno . "/converts/";
        if (!is_dir($folderConvert)) {
            mkdir($folderConvert, 0777, true);
            chmod($folderOri, 0777);
        }
        $kk = 0;
        $pathConMainDoc = [];
        foreach ($request->main['convert_path'] as $eachMainConv) {
            $pathConMainDoc[$kk] = Helper::moveImg($folderConvert, $eachMainConv, $folderConvert . basename($eachMainConv));
            $posisi = strpos($pathConMainDoc[$kk], "assets");
            $savepath = substr($pathConMainDoc[$kk], $posisi);
            $docConMain = ClaimRegDocConverts::create([
                'id_claimregdoc' => $docOriMain->id,
                'path' => $savepath
            ]);
            $kk++;
        }

        // ------------- upload minio main document ------------- //

        Log::info("Create File Main di MinIO");
        //create folder claimregno/originals
        $pathOrig = 'Transaction/claim/' . $claimregno . '/originals';
        Storage::disk('minio')->makeDirectory($pathOrig, 0775, true);
        // $this->claimsubmissioncontroller->createOCR();

        //create folder claimregno/converts
        $pathConv = 'Transaction/claim/' . $claimregno . '/converts';
        Storage::disk('minio')->makeDirectory($pathConv, 0775, true);

        if (isset($pathOriMainDoc)) {
            $data['type'] = 'Upload';
            $data['category'] = 'main';
            $data['destinationOrig'] = $pathOrig;
            $data['sourceOrig'] = $pathOriMainDoc;
            $data['destinationConv'] = $pathConv;
            $data['sourceConv'] = $pathConMainDoc;
            // $data['pdffile'] = $pdffilecopy;
            $data['basename'] = $request->main['doc_name'];

            $dataa['insurance_id'] = $request->auth->insurance_id;
            $dataa['status_type'] = 'ACTIVE';
            $checkbalance = $this->historycontroller->getBalance($dataa);
            $totalpagesupport = 0;
            if (count($request->support) > 0) {
                foreach ($request->support as $docsup) {
                    $cekocrread = $this->checkOcrRead($docsup['doc_type']);
                    if ($cekocrread) {
                        $totalpagesupport = $totalpagesupport + count($docsup['convert_path']);
                    }
                }
            }

            $totalpages = count($request->main['convert_path']) + $totalpagesupport;
            // return ("check balance : ".$checkbalance." && total pages : ".$totalpages);
            if ($checkbalance >= $totalpages) {
                $balance_active = app('redis')->get("balance_{$dataa['insurance_id']}_ACTIVE");

                $abs_qty = abs($totalpages);
                $balance_active = abs($balance_active) - $abs_qty;

                app('redis')->set("balance_{$dataa['insurance_id']}_ACTIVE", $balance_active);
                app('redis')->expire("balance_{$dataa['insurance_id']}_ACTIVE", 900);
                Log::info("Queue Main Doc");
                $this->dispatch((new ClaimRegJob($data['type'], null, $data['category'], null, null, $data['destinationOrig'], $data['sourceOrig'], $data['destinationConv'], $data['sourceConv'], $data['basename'])));
                Log::info("Queue Done");
            } else {
                //save data pending claim disini
                $savePendingClaimMain = PendingClaim::create([
                    'claimregno' => $claimregno,
                    'type' => $data['type'],
                    'category' => $data['category'],
                    'destinationOrig' => $data['destinationOrig'],
                    'sourceOrig' => $data['sourceOrig'],
                    'destinationConv' => $data['destinationConv'],
                    'insurance_id' => $dataa['insurance_id'],
                    'basename' => $data['basename'],
                    'totalpages' => $totalpages,
                ]);

                foreach ($data['sourceConv'] as $pendingMainConv) {
                    $savePendingClaimMainConv = PendingClaimConvert::create([
                        'claimregno' => $claimregno,
                        'category' => 'main',
                        'path' => $pendingMainConv
                    ]);
                }
            }
        }

        //support document
        if (count($request->support) > 0) {
            // $this->dispatch((new ConvertDocSupport($request->auth->insurance_id, $request->auth->id, $request->support, $claimregno, $pathOrig, $pathConv, $folderOri, $_SERVER['DOCUMENT_ROOT'])));
            // $this->convertDocSupport($request->auth->insurance_id, $request->auth->id, $request->support, $claimregno, $pathOrig, $pathConv, $folderOri, $_SERVER['DOCUMENT_ROOT']);

            $ss = 0;
            $pathOriSupDoc = [];

            $aa = 0;
            $pathConvSupDoc = [];
            foreach ($request->support as $docsup) {
                // move support doc asli
                $folderOri = $_SERVER['DOCUMENT_ROOT'] . '/assets/claim/' . $claimregno . "/originals/";
                if (!is_dir($folderOri)) {
                    mkdir($folderOri, 0777, true);
                    chmod($folderOri, 0777);
                }

                $pathOriSupDoc[$ss] = Helper::moveImg($folderOri, $docsup['file_path'], $folderOri . $docsup['doc_name']);
                $posisi = strpos($pathOriSupDoc[$ss], "assets");
                $savepath = substr($pathOriSupDoc[$ss], $posisi);
                $docOriSup[$ss] = ClaimRegDoc::create([
                    'insurance_id' => $request->auth->insurance_id,
                    'doc_type' => $docsup['doc_type'],
                    'doc_name' => $docsup['doc_name'],
                    'path' => $savepath,
                    'doc_category' => $docsup['doc_type'] != null ? "support" : "treatment",
                    'claimregno' => $claimregno,
                    'created_by' => $request->auth->id,
                    'last_update_by' => $request->auth->id,
                    'basename' => $docsup['doc_name'],
                ]);

                $doctypeOriSup[$ss] = $docOriSup[$ss]->doc_type;

                // move sup doc convert & save ke claimregdoc convert
                $folderConvert = $_SERVER['DOCUMENT_ROOT'] . '/assets/claim/' . $claimregno . "/converts/";
                if (!is_dir($folderConvert)) {
                    mkdir($folderConvert, 0777, true);
                    chmod($folderOri, 0777);
                }
                $kk = 0;
                $pathConSupDoc = [];
                foreach ($docsup['convert_path'] as $eachSupConv) {
                    $pathConSupDoc[$kk] = Helper::moveImg($folderConvert, $eachSupConv, $folderConvert . basename($eachSupConv));
                    $posisi = strpos($pathConSupDoc[$kk], "assets");
                    $savepath = substr($pathConSupDoc[$kk], $posisi);
                    $docConSup[$kk] = ClaimRegDocConverts::create([
                        'id_claimregdoc' => $docOriSup[$ss]->id,
                        'path' => $savepath
                    ]);
                    // $pathConvSupDoc[$aa] = $savepath;
                    $pathConvSupDoc[$aa] = $pathConSupDoc[$kk];
                    $aa++;
                    $kk++;
                }
                $ss++;
            }


            // ------------- upload minio support document ------------- //

            //create folder claimregno/originals
            $pathOrig = 'Transaction/claim/' . $claimregno . '/originals';
            Storage::disk('minio')->makeDirectory($pathOrig, 0775, true);
            // $this->claimsubmissioncontroller->createOCR();

            //create folder claimregno/converts
            $pathConv = 'Transaction/claim/' . $claimregno . '/converts';
            Storage::disk('minio')->makeDirectory($pathConv, 0775, true);

            if (isset($pathOriSupDoc)) {

                $data['type'] = 'Upload';
                $data['category'] = 'support';
                $data['destinationOrig'] = $pathOrig;
                $data['sourceOrig'] = $pathOriSupDoc;
                $data['destinationConv'] = $pathConv;
                $data['sourceConv'] = $pathConvSupDoc;
                $data['supportDoctypes'] = $doctypeOriSup;
                $data['basename'] = $dataa['insurance_id']; //isi : id_insurance
                if ($checkbalance >= $totalpages) {
                    $balance_active = app('redis')->get("balance_{$dataa['insurance_id']}_ACTIVE");

                    $abs_qty = abs($totalpages);
                    $balance_active = abs($balance_active) - $abs_qty;

                    app('redis')->set("balance_{$dataa['insurance_id']}_ACTIVE", $balance_active);
                    app('redis')->expire("balance_{$dataa['insurance_id']}_ACTIVE", 900);
                    $this->dispatch((new ClaimRegJob($data['type'], $data['supportDoctypes'], $data['category'], null, null, $data['destinationOrig'], $data['sourceOrig'], $data['destinationConv'], $data['sourceConv'], $data['basename'])));
                } else {
                    //simpan pending claim di sini
                    $ss = 0;
                    foreach ($data['sourceOrig'] as $sourceOrig) {
                        $savePendingClaimSup = PendingClaim::create([
                            'claimregno' => $claimregno,
                            'type' => $data['type'],
                            'supportDoctypes' => $data['supportDoctypes'][$ss],
                            'category' => $data['category'],
                            'destinationOrig' => $data['destinationOrig'],
                            'sourceOrig' => $sourceOrig,
                            'destinationConv' => $data['destinationConv'],
                            'insurance_id' => $dataa['insurance_id'],
                            'basename' => $data['basename'],
                        ]);
                        $ss++;
                    }

                    foreach ($data['sourceConv'] as $pendingSupConv) {
                        $savePendingClaimSupConv = PendingClaimConvert::create([
                            'claimregno' => $claimregno,
                            'category' => 'support',
                            'path' => $pendingSupConv
                        ]);
                    }
                }
            }
        }

        $modifhdr = ClaimRegHdr::where('claimregno', $claimregno)
            ->update([
                "last_update_by" => $request->auth->id,
                "updated_at" => date('Y-m-d H:i:s')
            ]);

        if ($request->has('claimactivity') && count($request->input('claimactivity')) > 0) {
            $arrClaimActivity = array_filter($request->input('claimactivity'), 'strlen');
            $arrClaimActivity['created_by']     = $request->auth->id;
            $arrClaimActivity['last_update_by'] = $request->auth->id;
            $arrClaimActivity['created_at']     = Carbon::now()->toDateTimeString();
            $arrClaimActivity['updated_at']     = Carbon::now()->toDateTimeString();
            $arrClaimActivity['claimregno']     = $claimregno;
            $ClaimActivity = ClaimActivity::insert($arrClaimActivity);
        }

        return response()->json(['message' => 'sukses'], 200);
        // } catch (\Exception $e) {
        //     return response()->json(['message' => 'Claim registration failed'], 409);
        // }
    }

    public function convertDocSupport($insurance_id, $user_id, $doc_support, $claimregno, $pathOrig, $pathConv, $folderOri, $documentRoot)
    {
        ini_set('max_execution_time', 0);

        $x = 0;
        // $doc_support = $doc_support;
        // $data = $data;
        $pathOriSupportDoc = [];
        $pathConSupportDoc = [];
        Log::debug(count($doc_support));
        Log::debug($doc_support);
        foreach ($doc_support as &$doc) {

            Log::debug($x);
            if (!is_dir($folderOri)) {
                mkdir($folderOri, 0777, true);
                Log::debug("sudah bisa buat folder");
            }

            Log::debug($folderOri);
            Log::debug($doc['path']);
            Log::debug($doc['doc_name']);

            $pathOriSupportDoc[$x] = Helper::moveImg($folderOri, $documentRoot . '/' . $doc['path'], $folderOri . $doc['doc_name']);

            Log::debug("move image");

            $docOriSupport = ClaimRegDoc::create([
                'insurance_id' => $insurance_id,
                'doc_type' => $doc['doc_type'],
                'doc_name' => $doc['doc_name'],
                'path' => $pathOriSupportDoc[$x],
                'doc_category' => $doc['doc_type'] ? "support" : "treatment",
                'claimregno' => $claimregno,
                'created_by' => $user_id,
                'last_update_by' => $user_id,
                'basename' => $doc['doc_name'],
            ]);

            Log::debug("create reg doc");

            //-------------------------- call queue convert support -------------------------------//
            $data['type'] = 'Convert';
            $data['category'] = 'support';
            $data['claimregno'] = $claimregno;
            $data['idclaimregdocsupport'] = $docOriSupport->id;
            $data['destinationOrig'] = $pathOrig;
            $data['destinationConv'] = $pathConv;
            $data['pathOriSupportDoc'] = $pathOriSupportDoc[$x];
            // $data['convertpdffile'] = $convertpdffilecopy;
            $data['basename'] = $doc['doc_name'];
            $this->dispatch(new ClaimRegJob($data['type'], $data['pathOriSupportDoc'], $data['category'], $data['claimregno'], $data['idclaimregdocsupport'], $data['destinationOrig'], null, $data['destinationConv'], null, $data['basename']));

            $x++;
            Log::debug("Upload Done !!!");
        }
    }

    public function getDocumentRegistered(Request $request, $id = null)
    {

        $sortby                 = $request->input('sortby');
        $sortvalue              = $request->input('sortvalue');
        $status                 = true;
        $error                  = null;
        $getClaimRegistered     = ClaimRegHdr::with([
            'companyid',
            'insurance_id',
            'bizprovid',
            'plan',
            'benefit_plan',
            'created_by',
            'last_update_by',
            'claimregdoc',
            'claimactivity',
            'status',
            'diagnosis',
            'reasonid',
            'assignTo' => function ($query) {
                $query->select('id', 'name');
            }
        ]);

        if ($request->auth->isInsurance) {
            $getClaimRegistered   = $getClaimRegistered->where('insurance_id', $request->auth->insurance_id);
        }

        $cek_permission = User::with(['permissions' => function ($query) {
            $query->where('permission', 'Pages.Transaction.ClaimSubmission.Analyze');
        }])->where('id', $request->auth->id)->get();
        if (count($cek_permission[0]->permissions) > 0) {
            $getClaimRegistered = $getClaimRegistered->where('assignTo', $request->auth->id);
        }

        $permission = 'Pages.Transaction.ClaimSubmission.Approval';
        $userapprover = User::with(['permissions'])->whereHas('permissions', function ($query) use ($permission) {
            return $query->where('permission', $permission);
        })->where('id', $request->auth->id)->get();

        $permission = 'Pages.Transaction.ClaimSubmission.ManualAssign';
        $userManualAssign = User::with(['permissions'])->whereHas('permissions', function ($query) use ($permission) {
            return $query->where('permission', $permission);
        })->where('id', $request->auth->id)->get();

        if($userapprover->count() > 0 && $userManualAssign->count() > 0){
            $getClaimRegistered = $getClaimRegistered->whereIn('status', [1,3]);
        }else{
            if ($userapprover->count() > 0) {
                $getClaimRegistered = $getClaimRegistered->where('status', '=', '3');
            }
    
            if ($userManualAssign->count() > 0) {
                $getClaimRegistered = $getClaimRegistered->where('status', '=', '1');
            }
        }
        

        if ($request->has('except')) {
            $jaId = $request->input('except');
            $getClaimRegistered = $getClaimRegistered->where('assignTo', '!=', $jaId);
        }
        if ($id) {
            $getClaimRegistered = $getClaimRegistered->where('id', $id)->first();
            foreach ($getClaimRegistered->claimregdoc as $cc) {
                $cc->doc_type_desc = "";
                if ($cc->doc_category == "main") {
                    $doctype = CodeMasters::where('id', $cc->doc_type)->first();
                    $doctype = $doctype->codeDesc;
                    $cc->doc_type_desc = $doctype;
                } elseif ($cc->doc_category == "support") {
                    $doctype = ClaimDocs::where('id', $cc->doc_type)->first();
                    $doctype = $doctype->claim_doc_name;
                    $cc->doc_type_desc = $doctype;
                } else {
                    $cc->doc_type_desc = "";
                }
            }
            if (!$getClaimRegistered) {
                $status     = false;
                $error      = "data not found";
            }
        } else {
            // $getClaimRegistered     = $getClaimRegistered->orderBy('id'); //grouping data by detail parent column
            if ($request->has('keyword')) {
                $keyword        = $request->keyword;
                $where          = array(
                    "claimregno",
                    "memberid",
                    "plan",
                    "bizprovid",
                    "companyid",
                    "ref_no",
                    "remark",
                    "eob_status",
                    "payment_status",
                    "subtotal",
                    "created_at",
                    "indexno",
                    "cardno",
                    "policyno"
                );
                $getClaimRegistered = Helper::dynamicSearch($getClaimRegistered, $where, $keyword);
                if (!$getClaimRegistered->count()) {
                    $getClaimRegistered    = ClaimRegHdr::with([
                        'companyid',
                        'insurance_id',
                        'bizprovid',
                        'plan',
                        'benefit_plan',
                        'created_by',
                        'last_update_by',
                        'claimregdoc',
                        'claimactivity',
                        'status',
                        'diagnosis',
                        'reasonid'
                    ]);
                    $getClaimRegistered = $getClaimRegistered->whereHas('memberid', function ($query) use ($keyword) {
                        $query->where('member_name', 'like', '%' . $keyword . '%');
                    })->OrWhereHas('status', function ($query) use ($keyword) {
                        $query->where('codeDesc', 'like', '%' . $keyword . '%');
                    })->OrWhereHas('bizprovid', function ($query) use ($keyword) {
                        $query->where('name', 'like', '%' . $keyword . '%');
                    })->OrWhere('created_at', 'like', '%' . $keyword . '%');
                }
            } else {
                if ($request->has('filter')) {
                    $table          = 'claimreghdr';
                    $filter         = $request->input('filter');
                    $filtername         = json_decode($filter[0]);
                    $filternameby       = $filtername->by;
                    $filternamevalue    = $filtername->value;
                    if ($filternameby == 'rangeAmount') {
                        $filterdate             = explode("/", $filternamevalue);
                        $where                  = 'subtotal';
                        if ($filterdate[1] == "-") {
                            $getClaimRegistered       = $getClaimRegistered->where($where, ">=", $filterdate[0]);
                        } elseif ($filterdate[0] == "-") {
                            $getClaimRegistered       = $getClaimRegistered->where($where, "<=", $filterdate[1]);
                        } else {
                            $getClaimRegistered       = $getClaimRegistered->whereBetween($where, [$filterdate[0], $filterdate[1]]);
                        }
                    } else {
                        $getClaimRegistered    = Helper::filterSearch($getClaimRegistered, $table, $filter);
                    }
                    foreach ($filter as $f) {
                        $filterdata     = json_decode($f);
                        $filterby       = $filterdata->by;
                        $filtervalue    = $filterdata->value;

                        if (is_array($filtervalue)) {
                            if ($filterby == 'codeDesc') {
                                $getClaimRegistered = $getClaimRegistered->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'provider') {
                                $getClaimRegistered = $getClaimRegistered->whereHas('bizprovid', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'notes') {
                                $getClaimRegistered = $getClaimRegistered->whereHas('claimregactivity', function ($query) use ($filtervalue) {
                                    $query->whereIn('remark', $filtervalue);
                                });
                            } elseif ($filterby == 'plancodedesc') {
                                $getClaimRegistered   = $getClaimRegistered->whereHas('plan', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'assign_to') {
                                $getClaimRegistered   = $getClaimRegistered->whereHas('assignTo', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            }
                        } else {
                            if ($filterby == 'codeDesc') {
                                $getClaimRegistered = $getClaimRegistered->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'provider') {
                                $getClaimRegistered = $getClaimRegistered->whereHas('bizprovid', function ($query) use ($filtervalue) {
                                    $query->where('name',  'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'notes') {
                                $getClaimRegistered = $getClaimRegistered->whereHas('claimregactivity', function ($query) use ($filtervalue) {
                                    $query->whereIn('remark', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'plancodedesc') {
                                $getClaimRegistered   = $getClaimRegistered->whereHas('plan', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'assign_to') {
                                $getClaimRegistered   = $getClaimRegistered->whereHas('assignTo', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            }
                        }
                    }
                }
            }

            if ($getClaimRegistered->count() < 1) {
                $status     = false;
                $error      = "data not found";
            }

            $getClaimRegistered = $getClaimRegistered->orderBy('vip', 'desc')->orderBy('status', 'asc')->orderBy('created_at', 'asc');
            // $vips = $getClaimRegistered->where('vip','1')->where('status','1')->orderBy('created_at');
            // $nonvips = $getClaimRegistered->where('vip','0')->where('status','1')->orderBy('created_at');
            // $else = $getClaimRegistered->where('vip','0')->where('status','!=','1')->orderBy('created_at');

            // print_r($sortvalue);die();
            $getClaimRegistered     = Helper::sorting($getClaimRegistered, $sortby, $sortvalue);
            $limit                  = $request->has('limit') ? $request->input('limit') : 20;
            $page                   = $request->has('page') ? $request->input('page') : 1;
            $getClaimRegistered     = $getClaimRegistered->paginate($limit, ['*'], 'page', $page);

            $meta                   = [
                'page'          => (int) $getClaimRegistered->currentPage(),
                'perPage'       => (int) $getClaimRegistered->perPage(),
                'total'         => (int) $getClaimRegistered->total(),
                'totalPage'     => (int) $getClaimRegistered->lastPage()
            ];
            
            foreach ($getClaimRegistered as $m) {
                // return $m->claimregno;
                $count_support_doc = 0;
                $m->uploaded_support_doc = "";
                $tot_sup_per_plan = ClaimDocs::where('plan', $m->plan)->where('insurance_id',$m->insurance_id)->count();
                $treatment_sup_doc = ClaimRegDoc::where("doc_category","treatment")->where("claimregno",$m->claimregno)->count();
                $main_doc_count = ClaimRegDoc::where("doc_category","main")->where("claimregno",$m->claimregno)->count();
                // $m->uploaded_support_doc = count($tot_sup_per_plan);
                // return "support : ".count($tot_sup_per_plan)." - treatment : ".count($treatment_sup_doc)." - main : ".count($main_doc_count);
                $m->claimdocs = ClaimDocs::with(['claimregdoc' => function ($query) use($m){
                    $query->where('claimregno', $m->claimregno);
                }, 'claimregdoc.claimregdoc_converts'])->where('plan', $m->plan)->where('insurance_id',$m->insurance_id)->get();
                
                foreach ($m->claimregdoc as $cc) {
                    // echo "-".$cc->id."<br/>";
                    $cc->doc_type_desc = "";
                    $cc->isOcr = "";
                    $cc->isMandatory = "";
                    $cc->doc_type_desc = "";
                    if ($cc->doc_category == "main") {
                        $doctype = CodeMasters::where('id', $cc->doc_type)->withTrashed()->first();
                        // print_r($doctype->codeDesc);die();
                        $doctype = $doctype->codeDesc;

                        //insert to claimdocs
                        $cdocs['isOcr'] = 1;
                        $cdocs['isMandatory'] = 1;
                        $cdocs['claim_doc_name'] = $doctype;
                        $cdocs['claimregdoc'] = $cc;
                        $m->claimdocs[] = $cdocs;
                        //eof insert to claimdocs

                        // print_r($cc->id."-".$doctype);
                        $cc->doc_type_desc = $doctype;
                        $cc->isOcr = "1";
                        $cc->isMandatory = "1";
                    } elseif ($cc->doc_category == "support") {
                        $count_support_doc++;
                        $doctype = ClaimDocs::where('id', $cc->doc_type)->withTrashed()->first();
                        // echo $doctype."<br/>";
                        $doctypee = $doctype->claim_doc_name;
                        $isOcr = $doctype->isOcrRead;
                        $isMandatory = $doctype->is_mandatory;
                        $cc->isOcr = $isOcr;
                        $cc->isMandatory = $isMandatory;
                        $cc->doc_type_desc = $doctypee;
                    } else {
                        $cdocs['isOcr'] = 0;
                        $cdocs['isMandatory'] = 0;
                        $cdocs['claim_doc_name'] = 'Other';
                        $cdocs['claimregdoc'] = $cc;
                        $m->claimdocs[] = $cdocs;
                    }
                }
                $m->uploaded_support_doc = ($count_support_doc + $treatment_sup_doc+$main_doc_count). "/" . ($tot_sup_per_plan+$treatment_sup_doc+$main_doc_count);
            }
            
            // $getClaimRegistered     = $out;
            $getClaimRegistered     = $getClaimRegistered->toArray()['data'];
            //
        }

        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($getClaimRegistered) ? $getClaimRegistered : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function autoAssign($id = null)
    {
        $permission = 'Pages.Transaction.ClaimSubmission.Analyze';
        $msg        = '';
        if ($id) {

            $cekjunioranalyst = User::with(['permissions'])
                ->whereHas('permissions', function ($query) use ($permission) {
                    return $query->where('permission', $permission);
                })
                ->where('isLogin', 1)
                ->get('id')
                ->map(function ($id) {
                    $data = [
                        "user_id" => $id->id,
                        "task_count" => ClaimRegHdr::select('assignTo')->where('status', 6)->Where('assignTo', $id->id)->count()
                    ];
                    return $data;
                });
            $cekotheranalyst = User::with(['permissions'])
                ->whereHas('permissions', function ($query) use ($permission) {
                    return $query->where('permission', $permission);
                })
                ->where('isLogin', 1)
                ->where('id', '!=', $id)
                ->get('id');

            $sortdata = json_decode($cekjunioranalyst);
            usort($sortdata, function ($a, $b) {
                return $a->task_count > $b->task_count;
            });
            if (count($cekotheranalyst) == 0) {
                $msg = 'You are 1st login in this sistem, you have some work, please complete it soon';
                $doctoassign = ClaimRegHdr::where('status', 1)->where('isOcrRead', '1')
                    ->update(['assignTo' => $id]);
            } else {
                $countdocument = ClaimRegHdr::where('status', 1)->where('isOcrRead', '1')->select('id')->get();
                if ($countdocument->count() != 0) {
                    $countjunioranalyst = count($cekjunioranalyst);
                    $countdata = ceil($countdocument->count() / $countjunioranalyst);
                    for ($i = 0; $i < $countdocument->count(); $i++) {
                        $data[] =  $countdocument[$i]->id;
                    };
                    $chunk = array_chunk($data, $countdata);
                    for ($i = 0; $i < count($chunk); $i++) {
                        ClaimRegHdr::whereIn('id', $chunk[$i])->update(['assignTo' => $sortdata[$i]->user_id]);
                    }

                    $msg = 'You have some work, please complete it soon';
                } else {
                    $msg = 'There is no task to assign';
                }
            }
            return $msg;
        } else {
            return false;
        }
    }

    public function dashboardAnalyst(Request $request)
    {
        // return $request->auth->insurance_id;
        $startperiod    = $request->input('start');
        $endperiod      = $request->input('end');
        $claimhdr       = ClaimRegHdr::where('insurance_id', $request->auth->insurance_id);
        
        // return $startperiod.' - '.$endperiod;
        $totalclaimdocument     = $claimhdr->whereBetween(DB::raw('DATE(created_at)'),[$startperiod, $endperiod])->get();
        // return $startperiod.' - '.$endperiod.' - '.$totalclaimdocument;
        $totalclaimbystatus     = DB::select('SELECT cm.codeDesc as Status, count(ch.claimregno) as Qty FROM code_masters cm
                                    LEFT JOIN claimreghdr ch ON ch.`status` = cm.`code` AND ch.insurance_id = '.$request->auth->insurance_id.' AND ch.created_at BETWEEN "'.$startperiod.' 00:00:00" AND "'.$endperiod.' 23:59:59"
                                    WHERE cm.codeType="CLS" GROUP BY cm.codeDesc');
        // return $startperiod.' - '.$endperiod.' - '.$totalclaimbystatus;
        $totalclaimbyplan       = DB::select('SELECT cdmstr.`code` as "Code", tot.status AS "Description", tot.Qty FROM code_masters cdmstr
                                    JOIN (SELECT cm.codeDesc AS status, count(ch.claimregno) AS Qty FROM code_masters cm
                                    LEFT JOIN claimreghdr ch ON ch.plan = cm.`code` AND ch.insurance_id = 3 AND date(ch.created_at) BETWEEN "'.$startperiod.' 00:00:00" AND "'.$endperiod.' 23:59:59"
                                    WHERE cm.codeType = "PLN" GROUP BY cm.codeDesc) tot ON tot.status = cdmstr.codeDesc');
        $totalbydiagnosis       = DB::select('SELECT dg.diagnosisCode AS "Code", dg.diagnosisName AS "Name", COUNT(ch.claimregno) AS Qty 
                                    FROM claimreghdr ch
                                    JOIN diagnosis dg ON dg.id = ch.diagnosis
                                    WHERE ch.insurance_id = '.$request->auth->insurance_id.' AND DATE(ch.created_at) BETWEEN "'.$startperiod.' 00:00:00" AND "'.$endperiod.' 23:59:59"
                                    GROUP BY dg.diagnosisCode, dg.diagnosisName');

        
        foreach ($totalclaimbyplan as $plan) {
            $totalplan[] = $plan->Description;
            if (strpos($plan->Description, ' Reimburse') !== false) {
                if (strpos($plan->Description, 'In Patient ') !== false) {
                    $inpatientcountreimburse[] = $plan->Qty;
                } elseif (strpos($plan->Description, 'Out Patient ') !== false) {
                    $oupatientcountreimburse[] = $plan->Qty;
                }
                
            } elseif (strpos($plan->Description, ' Cashless') !== false) {
                $teshas2[] = $plan;
                if (strpos($plan->Description, 'In Patient ') !== false) {
                    $inpatientcountcashless[] = $plan->Qty;
                } elseif (strpos($plan->Description, 'Out Patient ') !== false) {
                    $oupatientcountcashless[] = $plan->Qty;
                }
            }
        }
 
        $response   = [
            "Totalclaim"    => $totalclaimdocument->count(),
            "ByStatus"      => $totalclaimbystatus,
            "ByPlan"        => [
                                    "Reimburse" => [
                                        "Inpatient" => $inpatientcountreimburse[0],
                                        "Outpatient" => $oupatientcountreimburse[0]
                                    ],
                                    "Cashless" => [
                                        "Inpatient" => $inpatientcountcashless[0],
                                        "Outpatient" => $oupatientcountcashless[0]
                                    ]
                                ],
            "ByDiagnosis"   => $totalbydiagnosis,
            "Table"         => $totalclaimdocument
        ];

        return response()->json($response);
    }
}
