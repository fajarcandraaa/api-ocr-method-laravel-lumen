<?php

namespace App\Http\Controllers;

use App\ClaimRegHdr;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class ListEOBController extends Controller
{

    private $attachmentfile;
    // public function __construct()
    // {
    //     //
    // }

    public function getEOBHdr(Request $request, $id = null)
    {
        $sortby                 = $request->input('sortby');
        $sortvalue              = $request->input('sortvlue');
        $status                 = false;
        $error                  = "data not found";
        $getEOB     = ClaimRegHdr::whereNotNull('eob_status')->with([
            'memberid',
            'companyid',
            'reasonid',
            'insurance_id',
            'eob_status',
            'bizprovid',
            'plan',
            'created_by',
            'last_update_by',
        ]);
        if ($id) {
            $getEOB = $getEOB->where('id', $id)->first();
            if ($getEOB) {
                $status     = true;
                $error      = null;
            }
        } else {
            if ($request->has('keyword')) {
                $keyword        = $request->keyword;
                $where          = array(
                    'memberid',
                    'companyid',
                    'reasonid',
                    'insurance_id',
                    'eob_status',
                    'bizprovid',
                    'plan',
                    'created_by',
                    'last_update_by',
                );
                $getEOB = Helper::dynamicSearch($getEOB, $where, $keyword);

                if (!$getEOB->count()) {
                    $getEOB = ClaimRegHdr::with([
                        'memberid',
                        'companyid',
                        'reasonid',
                        'insurance_id',
                        'bizprovid',
                        'plan',
                        'created_by',
                        'last_update_by',
                    ]);
                    // ->whereHas('', function ($query) use ($keyword) {
                    //     $query->where('', 'like', '%' . $keyword . '%');
                    // });
                }

                $status                 = true;
                $error                  = null;
                $getEOB     = Helper::sorting($getEOB, $sortby, $sortvalue);
            } else {
                $status                 = true;
                $error                  = null;
                if ($request->has('filter')) {
                    $table                  = 'claimreghdr';
                    $filter                 = $request->input('filter');
                    $getEOB     = Helper::filterSearch($getEOB, $table, $filter);
                    foreach ($filter as $f) {
                        $filterdata         = json_decode($f);
                        $filterby           = $filterdata->by;
                        $filtervalue        = $filterdata->value;
                        if (is_array($filtervalue)) {
                            if ($filterby == 'claimregno') {
                                $getEOB     = $getEOB->whereIn('claimregno', 'like', '%' . $filtervalue . '%');
                            }
                        } else {
                            if ($filterby == 'claimregno') {
                                $getEOB     = $getEOB->whereIn('claimregno', 'like', '%' . $filtervalue . '%');
                            }
                        }
                    }
                }
            }

            $getEOB     = Helper::sorting($getEOB, $sortby, $sortvalue);
            $limit                  = $request->has('limit') ? $request->input('limit') : 20;
            $page                   = $request->has('page') ? $request->input('page') : 1;
            $getEOB     = $getEOB->paginate($limit, ['*'], 'page', $page);

            $meta                   = [
                'page'          => (int) $getEOB->currentPage(),
                'perPage'       => (int) $getEOB->perPage(),
                'total'         => (int) $getEOB->total(),
                'totalPage'     => (int) $getEOB->lastPage()
            ];

            $getEOB     = $getEOB->toArray()['data'];
        }

        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($getEOB) ? $getEOB : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function getEOBDetail(Request $request)
    {
        $sortby                 = $request->input('sortby');
        $sortvalue              = $request->input('sortvlue');
        $status                 = true;
        $error                  = "";
        $getEOB     = ClaimRegHdr::whereNotNull('eob_status')->with([
            'memberid',
            'companyid',
            'reasonid',
            'insurance_id',
            'eob_status',
            'bizprovid',
            'plan',
            'claimregdtl'
        ]);

        // $getEOB = $getEOB->where('claimregno', $request->claimregno);
        if($getEOB->count() < 1){
            $status                 = false;
        $error                  = "data not found";
        }

        $getEOB     = Helper::sorting($getEOB, $sortby, $sortvalue);
        $limit                  = $request->has('limit') ? $request->input('limit') : 20;
        $page                   = $request->has('page') ? $request->input('page') : 1;
        $getEOB     = $getEOB->paginate($limit, ['*'], 'page', $page);

        $meta                   = [
            'page'          => (int) $getEOB->currentPage(),
            'perPage'       => (int) $getEOB->perPage(),
            'total'         => (int) $getEOB->total(),
            'totalPage'     => (int) $getEOB->lastPage()
        ];

        $getEOB     = $getEOB->toArray()['data'];


        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($getEOB) ? $getEOB : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function generatePdfAttachment(Request $request)
    {
        $claimreghdr        = ClaimRegHdr::with('claimregdtl', 'plan','memberid','companyid')->where('id',$request->input('id'))->first();
        $claimdata          = json_decode($claimreghdr,true);
        $startdate          = $claimdata['companyid'] == null ? $claimdata['memberid']['policy_effective_date'] : $claimdata['companyid']['effectivedate']; //'aaaa' : 'bbb';
        $enddate            = $claimdata['companyid'] == null ? $claimdata['memberid']['policy_expiry_date'] : $claimdata['companyid']['expirydate']; //'aaaa' : 'bbb';
        $membereffective    = str_replace('-','/',$claimdata['memberid']['effective_date']).' - '.str_replace('-','/',$claimdata['memberid']['expire_date']);
        
        $data       = [
            'data'     => $claimdata,
            'reciveddate' => str_replace('-','/',$claimdata['created_at']),
            'evectivedate' => str_replace('-','/',$startdate).' - '.str_replace('-','/',$enddate),
            'memberdate'    => $claimdata['memberid'] != null ? $membereffective : "-"
             
        ];        

        // $attachmentfile = view('attachmentofeob',$data);
        $attachmentfile = PDF::loadView('attachmentofeob',$data);
        return $attachmentfile;
    }

    public function emailEOB(Request $request)
    {
        $to_name            = "MEDLINX";
        $to_email           = $request->input('email');
        // $pdf                = $this->generatePdfAttachment($request);

        $emaildata = [
            'body' => 'test attachment'
        ];
        
        Mail::send('eobmail',$emaildata, function($message) use($to_email,$to_name,$pdf){
            $message->to($to_email)->subject('TEST MAIL ATTACHMENT !')->attachData($pdf->output(),"test.pdf");
        });

        return 'sukseeees';
    }
}
