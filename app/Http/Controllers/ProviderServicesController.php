<?php

namespace App\Http\Controllers;

use App\Biz_providers;
use App\ProviderServices;
use App\CodeMasters;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB as DB;
use App\Exports\ProviderServicesExport;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;


class ProviderServicesController extends Controller
{
    public function __construct(Request $request)
    {
        $this->id_admin = $request->auth->id;
        $this->id_ins = $request->auth->insurance_id;
    }
    public function addService(Request $request)
    {
        if (!isset($request->created_by)) {
            $request->request->add(
                [
                    'created_by' => $request->auth->id,
                    'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->input('insurance_id'))
                ]
            );
        }

        try {
            $provservice   = new ProviderServices();
            $params     = $request->all();
            $provservice->fill($params);
            $provservice->save();
            if($provservice && $request->input('isSubservice') == 0){
                $this->updatePosition($request->input('mid'), $request->input('position'), $provservice->id);
            }
            return response()->json(['data' => $provservice, 'message' => 'Provider Service Registration Succeed'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Provider Service Registration Failed'], 409);
        }
    }

    public function updatePosition($mid, $position, $id){
        $ProviderServices = ProviderServices::where('mid', $mid)
                                            ->where('parent_service_id', 0)
                                            ->where('position', '>=', $position)
                                            ->where('id', '!=', $id)
                                            ->orderBy('position', 'asc')
                                            ->get();
        if($ProviderServices){
            $no = $position;
            foreach($ProviderServices as $ps){
                $no++;
                ProviderServices::where('id', $ps->id)
                    ->update(['position' => $no]);
            }
        }
    }

    public function showServices(Request $request, $id = null)
    {
        $sortby         = $request->input('sortby');
        $sortvalue      = $request->input('sortvalue');
        $status         = false;
        $error          = "data not found";
        $getprovservice   = ProviderServices::with(['parent_service_id', 'service_child', 'insurance_id', 'provider_type', 'mid', 'created_by', 'last_modified_by']);
        if ($request->has('childService')) {
            // echo "masok";
            // die;
            $child = $request->childService;
            $getprovservice = ProviderServices::with(['parent_service_id', 'service_child', 'insurance_id', 'provider_type', 'mid', 'created_by', 'last_modified_by'])
                ->where([['parent_service_id', '!=', '0'], ['id', '!=', '1']]);
        }
        // ->get();
        // return $getprovservice;
        if ($request->auth->isInsurance) {
            $getprovservice   = $getprovservice->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id) {
            $getprovservice   = $getprovservice->where('id', $id)->first();

            if ($getprovservice) {
                $status     = true;
                $error      = null;
            }
        } elseif ($request->has('partialSearch')) {
            $keywords           = $request->partialSearch;
            $mid                = $request->mid;
            if (isset($request->benefit_codes)) {
                $benefit_codes = $request->benefit_codes;
            }
            $planC = Helper::getKey('codeVal1', 'CodeMasters', 'code', $request->plan);
            // return $planC;
            $benefit_id = CodeMasters::select('id')->where([['codeType', "BEN"], ['code', $planC]])->first();
            // return $benefit_id->id;
            $keyword = explode(" ", $keywords);
            $where              = 'service_name';
            $getprovservice     = Helper::partialSearch($getprovservice, $where, $keyword);
            $getprovservice     = $getprovservice->where('mid', $mid);
            // return $getprovservice->get();
            // return $keyword[0];
            if (isset($request->provider_service)) {
                // echo "hi";
                $provider_service = ProviderServices::where('service_name', 'like', '%' . $request->provider_service . '%')->where('mid',$mid)->first();//$request->provider_service;
                // return $provider_service->id;
                // return "insurance id : ".$request->auth->insurance_id." - Provider Service : ".$provider_service." - mid : ".$mid;
                // return $provider_service;
                $getprovservice = $getprovservice->where('parent_service_id',$provider_service->id);
                // $getprovservice = $getprovservice->whereHas('parent_service_id', function ($query) use ($provider_service) {
                //     $query->where('service_name', 'like', '%' . $provider_service . '%');
                // });

                // return $getprovservice->toSql();

            }
            // return $getprovservice->get();

            $getprovservice = $getprovservice->get();

            // return $getprovservice;

            $balik = [];
            $xx = 0;
            foreach ($getprovservice as $servicename) {
                $sc = $servicename->service_name;
                // echo $sc;
                $sn = DB::table(DB::raw('provider_services ps'))->where('ps.service_name', $sc)->first();
                // echo $sn->id;
                
                if ($sn) {
                    $ips = DB::table(DB::raw('mapping_services ms'))
                        ->where('ms.id_prov_service', $sn->id)
                        ->where('insurance_id', $request->auth->insurance_id)
                        ->first();
                    // echo $ips->id_iziservice_code.',';

                } else {
                    $ips = "";
                }
                
                // return $benefit_id->id;
                if ($ips) {
                    $mb = DB::table(DB::raw('mapping_benefits mb'))
                        ->where('mb.id_iziservice_code', $ips->id_iziservice_code)
                        ->where('benefit_type', $benefit_id->id);

                    // var_dump($mb);
                    if (isset($benefit_codes)) {
                        $mb = $mb->whereIn('benefit_code', $benefit_codes);
                    }
                    
                    $mb = $mb->first();
                    // return response()->json($mb);
                } else {
                    $mb = "";
                }
                // dd($mb);
                $benefit = $mb;
                // echo $benefit."<br>";
                if ($benefit) {
                    $balik[$xx]['servicename'] = $servicename->service_name;
                    $balik[$xx]['benefit'] = $benefit ? $benefit : "";
                    $balik[$xx]['provider_service'] = $servicename ? $servicename : "";

                    $xx++;
                }
            }

            // $getprovservice   = Helper::sorting($getprovservice, $sortby, $sortvalue);
        } else {
            if ($request->has('keyword')) {
                $keyword            = $request->keyword;
                $where              = array('service_code', 'service_name', 'description', 'mid', 'amount', 'created_at');
                $getprovservice     = Helper::dynamicSearch($getprovservice, $where, $keyword);
                $getprovservice     = $getprovservice->orWhereHas('provider_type', function ($query) use ($keyword) {
                    $query->where('codeDesc', 'like', '%' . $keyword . '%');
                })->orWhereHas('mid', function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%')->orWhereHas('compprov', function ($query) use ($keyword) {
                        $query->where('Branch', 'like', '%' . $keyword . '%');
                    });
                })->orWhereHas('created_by', function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%');
                })->orWhereHas('insurance_id', function ($query) use ($keyword) {
                    $query->where('insuranceName', 'like', '%' . $keyword . '%');
                });

                $getprovservice   = Helper::sorting($getprovservice, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table          = 'provider_services';
                    $filter         = $request->input('filter');
                    $getprovservice   = Helper::filterSearch($getprovservice, $table, $filter);
                    foreach ($filter as $f) {
                        $filterdata         = json_decode($f);
                        $filterby           = $filterdata->by;
                        $filtervalue        = $filterdata->value;
                        if (is_array($filtervalue)) {
                            if ($filterby == 'codeDesc') {
                                $getprovservice   = $getprovservice->whereHas('provider_type', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'branch') {
                                $getprovservice   = $getprovservice->whereHas('mid', function ($query) use ($filtervalue) {
                                    $query->whereHas('compprov', function ($query) use ($filtervalue) {
                                        $query->whereIn('Branch', $filtervalue);
                                    });
                                });
                            } elseif ($filterby == 'provider_name') {
                                $getprovservice   = $getprovservice->whereHas('mid', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'name') {
                                $getprovservice   = $getprovservice->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->whereIn('name', $filtervalue);
                                });
                            } elseif ($filterby == 'insuranceId') {
                                $getprovservice   = $getprovservice->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                    $query->whereIn('insuranceName', $filtervalue);
                                });
                            } elseif ($filterby == 'merchant_id') {
                                $getprovservice   = $getprovservice->whereHas('mid', function ($query) use ($filtervalue) {
                                    $query->whereIn('merchant_id', $filtervalue);
                                });
                            }
                        } else {
                            if ($filterby == 'codeDesc') {
                                $getprovservice  = $getprovservice->whereHas('provider_type', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'branch') {
                                $getprovservice   = $getprovservice->whereHas('mid', function ($query) use ($filtervalue) {
                                    $query->whereHas('compprov', function ($query) use ($filtervalue) {
                                        $query->where('Branch', 'like', '%' . $filtervalue . '%');
                                    });
                                });
                            } elseif ($filterby == 'provider_name') {
                                $getprovservice   = $getprovservice->whereHas('mid', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'name') {
                                $getprovservice   = $getprovservice->whereHas('created_by', function ($query) use ($filtervalue) {
                                    $query->where('name', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'insuranceId') {
                                $getprovservice   = $getprovservice->whereHas('insurance_id', function ($query) use ($filtervalue) {
                                    $query->where('insuranceName', 'like', '%' . $filtervalue . '%');
                                });
                            }
                            elseif ($filterby == 'merchant_id') {
                                $getprovservice   = $getprovservice->whereHas('mid', function ($query) use ($filtervalue) {
                                    $query->where('merchant_id', 'like', '%' . $filtervalue . '%');
                                });
                            }
                        }
                    }
                }
            }

            if ($getprovservice->count() or isset($balik)) {
                $status         = true;
                $error          = null;
            }


            if (!isset($balik)) {
                $getprovservice   = Helper::sorting($getprovservice, $sortby, $sortvalue);
                $limit          = $request->has('limit') ? $request->input('limit') : 20;
                $page           = $request->has('page') ? $request->input('page') : 1;
                $getprovservice   = $getprovservice->paginate($limit, ['*'], 'page', $page);
                $meta           = [
                    'page'          => (int) $getprovservice->currentPage(),
                    'perPage'       => (int) $getprovservice->perPage(),
                    'total'         => (int) $getprovservice->total(),
                    'totalPage'     => (int) $getprovservice->lastPage()
                ];
                $getprovservice   = $getprovservice->toArray()['data'];
            }
        }

        // return $balik;
        if (isset($balik) && count($balik) > 0) {
            
            $status         = true;
            $error          = null;
        }

        // return $balik;

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($balik) ? $balik : (isset($getprovservice) ? $getprovservice : null)),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function updateService(Request $request, $id)
    {
        // $provtype       = $request->input('provtype');
        $request->request->add(['last_modified_by' => $request->auth->id]);
        $data   = ProviderServices::find($id);

        if ($data != null) {
            $params = $request->all();
            $data->fill($params);
            $data->save();
            if($data && $request->input('isSubservice') == 0){
                $this->updatePosition($request->input('mid'), $request->input('position'), $data->id);
            }
            return response()->json(['status' => (bool) true, 'message' => $data->id.' Your data has been updated '], 200);
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'Update data failed'], 409);
        }
    }

    public function deleteService(Request $request, $id)
    {
        $getprovservice   = ProviderServices::find($id);
        $getprovservicechild   = ProviderServices::where('parent_service_id', $id);
        if ($getprovservice) {
            $getprovservice->delete();
            if ($getprovservicechild) {
                $getprovservicechild->delete();
            }
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function massDeleteServices(Request $request)
    {
        $getprovservice   = ProviderServices::whereIn('id', $request->id);
        $getprovservicechild   = ProviderServices::whereIn('parent_service_id', $request->id);
        if ($getprovservice) {
            $getprovservice->delete();
            if ($getprovservicechild) {
                $getprovservicechild->delete();
            }
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function import(Request $request)
    {
        $data = $request->json()->all();
        $result = [];
        $i = 0;
        $k = 0;
        foreach ($data['data'] as $value1) {
            $msg = "";
            //provider type
            try {
                $provider_type = Helper::getCodeMasterId("PCT", $value1['provider_type']);
            } catch (\Exception $e) {
                $msg .= " provider type not found.";
            }

            //mid
            $mid = Biz_providers::where("merchant_id", $value1['mid'])->first();
            if (!$mid) {
                $msg .= "MID {$value1['mid']} provider not found.";
            }


            try {

                if ($msg !== "") {
                    throw new \Exception($msg);
                }

                $ps = ProviderServices::updateOrCreate(
                    [
                        'service_name' => $value1['service_name'],
                        'mid' => $value1['mid'],
                        'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->input('insurance_id')),
                    ],
                    [
                        'parent_service_id' => null,
                        'service_code' => $value1['service_code'],
                        'amount' => $value1['amount'],
                        'description' => $value1['description'],
                        'provider_type' => $provider_type,
                        'created_by' => $this->id_admin,
                        'last_modified_by' => $this->id_admin,
                        'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->input('insurance_id')),
                    ]
                );

                $result['valid'][$i] = $ps;
                $result['valid'][$i]->is_valid = $value1['is_valid'];
                $result['valid'][$i]->msg = $value1['msg'];

                $i++;
            } catch (\Exception $msg) {
                $result['invalid'][$k] = $value1;
                $result['invalid'][$k]['is_valid'] = false;
                $result['invalid'][$k]['msg'] = $msg->getMessage();
                $k++;
            }
        }
        return response()->json($result, 200);
    }


    public function excelProviderServices(Request $request)
    {
        $id = $request->id;

        $ps = ProviderServices::with(
            'provider_type'
        );

        if ($id or $id !== null) {
            $ps->whereIn('id', $id);
        }

        $ps = $ps->get();

        $ps = new ProviderServicesExport($ps->toArray());
        $date = date("dmy");

        return Excel::download($ps, 'Provider_Services-' . $date . '.xlsx');
    }

    public function pdfProviderServices(Request $request)
    {
        $date = date("dmy");

        $id = $request->id;

        $ps = ProviderServices::with(
            'provider_type'
        );

        if ($id or $id !== null) {
            $ps->whereIn('id', $id);
        }

        $ps = $ps->get();

        $pdf = PDF::loadview('providerservices', ['ps' => $ps->toArray()]);
        return $pdf->download('Provider_Services-' . $date . '.pdf');
    }
}
