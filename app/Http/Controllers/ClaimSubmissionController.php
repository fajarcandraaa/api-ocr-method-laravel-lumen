<?php

namespace App\Http\Controllers;

use App\ClaimActivity;
use App\ClaimDocs;
use App\ClaimRegDtl;
use App\ClaimRegHdr;
use App\CodeMasters;
use App\Biz_providers;
use App\User;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\TextSimiliarityController;
use App\RolePermissions;
use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Cache\RetrievesMultipleKeys;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;
use App\ProviderServices;

class ClaimSubmissionController extends Controller
{
    private $helper;
    private $TextSimiliarity;

    public function __construct()
    {
        $this->helper = new Helper;
        $this->TextSimiliarity = new TextSimiliarityController;
    }

    public function saveSubmissionDraft(Request $request)
    {
        $claimregno = $request->claimregno;
        $draft = $request->draft;
        $path = 'Transaction/claim/' . $claimregno . '/originals';

        Storage::disk('minio')->put('Transaction/claim/' . $claimregno . '/originals/draft.txt', $draft);
        $exists = Storage::disk('minio')->exists('Transaction/claim/' . $claimregno . '/originals/draft.txt');

        if ($exists) {
            return "draft file created";
        } else {
            return "failed to create draft file";
        }
    }

    public function getAnalyst()
    {
        $roleid = RolePermissions::where('permission', 'Pages.Transaction.ClaimSubmission.Analyze')->get('role_id');
        $idroles = [];
        foreach ($roleid as $key => $value) {
            array_push($idroles, $value->role_id);
        }
        $cek_permission = User::whereIn('role_user', $idroles)->get();
        // $idanalysts=[];
        // foreach ($cek_permission as $key => $value) {
        //     array_push($idanalysts,$value->id);
        // }
        return $cek_permission;
    }

    public function manualAssignClaim(Request $request)
    {
        $update = ClaimRegHdr::whereIn('id', $request->idclaim)->update(array('assignTo' => $request->idanalyst));
        if ($update) {
            return "assign claim success";
        } else {
            return "assign claim failed";
        }
    }

    public function getSpv(Request $request)
    {
        // $roleid = Roles::where('rolename', 'User Supervisor of Claim Analyst')->get('id');
        // $idroles = [];
        // foreach ($roleid as $key => $value) {
        //     array_push($idroles, $value->id);
        // }
        // $id_spv = User::whereIn('role_user', $idroles)->where('insurance_id',$this->ins_id)->first();

        $roleid = RolePermissions::where('permission', 'Pages.Transaction.ClaimSubmission.ManualAssign')->get('role_id');
        $idroles = [];
        foreach ($roleid as $key => $value) {
            array_push($idroles, $value->role_id);
        }
        $cek_permission = User::whereIn('role_user', $idroles)
            ->where('insurance_id', $request->auth->insurance_id)->get();
        return $cek_permission;
    }

    public function analystRequestClaim(Request $request)
    {
        $notif = [
            'to' => $this->helper->getUserByRolePermission('Pages.Transaction.ClaimSubmission.ManualAssign'),
            'title' => "Claim Request",
            'type' => "REQUEST_CLAIM",
            'requester'  => $request->auth->id,
            'requestername'  => $request->auth->name,
            'message'   => "has requested additional work",
            'alert' => true,
            'notification' => true,
            'payload' => [
                'requester'  => $request->auth->id,
                'requestername'  => $request->auth->name,
                'insurance_id' => $request->auth->insurance_id
            ]
        ];

        $claimreq = $this->helper->sendNotifRequest($notif);

        if ($claimreq) {
            return "request claim succeed";
        } else {
            return "request claim failed";
        }
    }

    public function updateStatusClaim(Request $request, $id, $status)
    {
        $getClaimRegistered = ClaimRegHdr::with([
            'companyid',
            'reasonid',
            'insurance_id',
            'bizprovid',
            'plan',
            'benefit_plan',
            'created_by',
            'last_update_by',
            'claimregdoc',
            'claimactivity',
            'claimregdtl',
            'status'
        ]);

        $getClaimRegistered = $getClaimRegistered->where('id', $id)->first();
        $claimregno = $getClaimRegistered->claimregno;

        $getClaimRegistered->update(array('status' => $status));

        $arrClaimActivity['created_by']     = $request->auth->id;
        $arrClaimActivity['last_update_by'] = $request->auth->id;
        $arrClaimActivity['created_at']     = Carbon::now()->toDateTimeString();
        $arrClaimActivity['updated_at']     = Carbon::now()->toDateTimeString();
        $arrClaimActivity['claimregno']     = $claimregno;
        $arrClaimActivity['acttype']     = $status;
        $ClaimActivity = ClaimActivity::insert($arrClaimActivity);

        if ($getClaimRegistered) {
            return "update status success";
        } else {
            return "update status failed";
        }
    }

    public function getClaimsubmission(Request $request, $id = null)
    {
        $sortby                 = $request->input('sortby');
        $sortvalue              = $request->input('sortvlue');
        $status                 = false;
        $error                  = "data not found";
        $getClaimRegistered     = ClaimRegHdr::with([
            'created_by',
            'companyid',
            'reasonid',
            'insurance_id',
            'bizprovid',
            'plan',
            'benefit_plan',
            'created_by',
            'last_update_by',
            'claimregdoc',
            'claimactivity',
            'claimregdtl',
            'status'
        ]);
        //check if user has approval permission

        // $permission = 'Pages.Transaction.ClaimSubmission.Approval';
        // // try {
        // // print_r($request->main['basename']);die();
        // $userapprover = User::with(['permissions'])->whereHas('permissions', function ($query) use ($permission) {
        //     return $query->where('permission', $permission);
        // })->where('id', $request->auth->id)->get();

        // // return $userapprover;
        // if ($userapprover->count() > 0) {
        //     $getClaimRegistered = $getClaimRegistered->where('status', '=', '3');
        // }
        if ($request->has('except')) {
            $jaId = $request->input('except');
            $getClaimRegistered = $getClaimRegistered->where('assignTo', '!=', $jaId);
        }
        if ($id) {
            $getClaimRegistered = $getClaimRegistered->where('id', $id)->first();
            // return $getClaimRegistered;
            // $this->updateStatusClaim($id,'238');
            $planC = Helper::getKey('codeVal1', 'CodeMasters', 'code', $getClaimRegistered->plan);
            $benefit_id = CodeMasters::select('id')->where([['codeType', "BEN"], ['code', $planC]])->first();
            // return $benefit_id;
            $benefit_id = $benefit_id->id;
            foreach ($getClaimRegistered->claimregdoc as $cc) {
                $cc['doc_type_desc'] = "";
                if ($cc['doc_category'] == "main") {
                    $doctype = CodeMasters::where('id', $cc['doc_type'])->first();
                    $doctype = $doctype['codeDesc'];
                    $cc['doc_type_desc'] = $doctype;
                } elseif ($cc['doc_category'] == "support") {
                    $doctype = ClaimDocs::where('id', $cc['doc_type'])->first();
                    $doctype = $doctype['claim_doc_name'];
                    $cc['doc_type_desc'] = $doctype;
                } else {
                    $cc['doc_type_desc'] = "";
                }
            }

            if ($getClaimRegistered->status !== "1" && $getClaimRegistered->status !== "5" && $getClaimRegistered->status !== "2") {
                foreach ($getClaimRegistered->claimregdtl as $crd) {
                    $crd['benefit'] = "";
                    $sc = $crd->servicecode;
                    $sn = DB::table(DB::raw('provider_services ps'))->where('ps.service_name', $sc)->first();
                    if ($sn) {
                        $ips = DB::table(DB::raw('mapping_services ms'))->where('ms.id_prov_service', $sn->id)->where('insurance_id', $request->auth->insurance_id)->first();
                    } else {
                        $ips = "";
                    }

                    if ($ips) {
                        $mb = DB::table(DB::raw('mapping_benefits mb'))->where('mb.id_iziservice_code', $ips->id_iziservice_code)->where('benefit_type', $benefit_id)->first();
                    } else {
                        $mb = "";
                    }

                    $benefit = $mb;
                    $crd['benefit'] = $benefit ? $benefit : "";
                }
            }

            if ($getClaimRegistered) {
                $status     = true;
                $error      = null;

                $g = json_encode([$getClaimRegistered]);
                $arr = [];
                $statusclaimdoc     = json_decode($g, true)[0]['status']['codeDesc'];
                $basename = json_decode($g, true)[0]['claimregdoc'][0]['basename'];
                $path = json_decode($g, true)[0]['claimregdoc'][0]['path'];
                $explode = explode('/', $path);
                $explode2 = explode('.', $basename);
                $realpath = 'Transaction/' . $explode[1] . '/' . $explode[2] . '/' . $explode[3] . '/' . $explode2[0] . '.txt';
                // return $realpath;
                $fileminio = Storage::disk('minio')->get($realpath);
                // return $statusclaimdoc;
                if ($statusclaimdoc == 'Registered') {
                    $isifileminio = json_decode($fileminio, true);
                    // print_r($isifileminio);die();
                    $detailType = isset($isifileminio['DetailType']) ? $isifileminio['DetailType'] : "CONTINOUS";
                    $listdoctor = isset($isifileminio['Doctors']) ? $isifileminio['Doctors'] : []; 
                    $listfooter = isset($isifileminio['Footer']) ? $isifileminio['Footer'] : [];
                    $detailData = $isifileminio['Detail'];
                    $hitb = 0;
                    $list_benefits = [];
                    $uniqueservice = [];
                    $newDetailData = [];
                    $index = 0;
                    foreach ($detailData as $row) {

                        foreach ($row as $isidata) {
                            if ($isidata['objName'] == "Sub_Service") {
                                $serv_name = $isidata['objValue'];
                            }
                        }
                        // echo $serv_name."<br>";

                        $sn = DB::table(DB::raw('provider_services ps'))->where('ps.service_name', $serv_name)->first();
                        // echo $row[1]['objValue']."<br>";
                        // return $sn->id;
                        if ($sn) {
                            $ips = DB::table(DB::raw('mapping_services ms'))->where('ms.id_prov_service', $sn->id)->where('insurance_id', $request->auth->insurance_id)->first();
                        } else {
                            $ips = "";
                        }
                        // return $ips->id_iziservice_code;
                        // return $benefit_id;die;

                        if ($ips) {
                            $mb = DB::table(DB::raw('mapping_benefits mb'))->where('mb.id_iziservice_code', $ips->id_iziservice_code)->where('benefit_type', $benefit_id)->first();
                        } else {
                            $mb = "";
                        }
                        // return $mb->benefit_code;die;

                        if ($mb) {
                            $list_benefits[$hitb] = $mb->benefit_code . " - " . $mb->benefit_name;
                            $hitb++;
                        }
                        $benefit_name = $mb ? $mb->benefit_code . " - " . $mb->benefit_name : "";

                        // $uniqueservice[$pointerunique] = [
                        //     'Name' => $row[0]['Name'],
                        //     'No' => $row[0]['objValue'],
                        //     'Sub_Service' => $row[1]['objValue'],
                        //     'Qty' => $row[2]['objValue'],
                        //     'Billing_Amount' => $row[3]['objValue'],
                        //     'Trans_Date' => $row[4]['objValue'],
                        //     'Benefit' => $benefit_name,
                        //     'NoOCR' => [intval($row[0]['ocrThreshold']), intval($row[0]['ocrConfidence']), intval($row[0]['ocrThresholdMsg'])],
                        //     'Sub_ServiceOCR' => [intval($row[1]['ocrThreshold']), intval($row[1]['ocrConfidence']), intval($row[1]['ocrThresholdMsg'])],
                        //     'QtyOCR' => [intval($row[2]['ocrThreshold']), intval($row[2]['ocrConfidence']), intval($row[2]['ocrThresholdMsg'])],
                        //     'Billing_AmountOCR' => [intval($row[3]['ocrThreshold']), intval($row[3]['ocrConfidence']), intval($row[3]['ocrThresholdMsg'])],
                        //     'Trans_DateOCR' => [intval($row[4]['ocrThreshold']), intval($row[4]['ocrConfidence']), intval($row[4]['ocrThresholdMsg'])],
                        //     'isValid' => $row[4]['objValue'],
                        // ];
                        // $pointerunique++;

                        $addBenefit = [
                            "objName" => "Benefit",
                            "objType" => "",
                            "objValue" => $benefit_name,
                            "ocrThreshold" => "80.0",
                            "ocrConfidence" => "100.0",
                            "ocrThresholdMsg" => "",
                            "isValid" => true
                        ];

                        array_push($detailData[$index], $addBenefit);
                        $index++;
                    }


                    $totaldata["Header"] = $isifileminio["Header"];
                    // $totaldata["headerData"] = $isifileminio["headerData"];
                    //  unhide bawah ini juga
                    $totaldata["Summary"] = $isifileminio["Summary"];
                    $totaldata["Detail"] = $detailData;
                    $totaldata["DetailType"] = $detailType;
                    $totaldata["Doctors"] = $listdoctor;
                    $totaldata["Footer"]    = $listfooter;

                    $det = $totaldata;
                    // $det = "";
                    // $det = $newDetailData;
                    // $content = Fileget::get($fileminio);
                    foreach (json_decode($g, true) as $idx => $val) {
                        array_push($arr, [
                            'id' => $val['id'],
                            'claimregno'  => $val['claimregno'],
                            'memberid'  => $val['memberid'],
                            'policyno'  => $val['policyno'],
                            'indexno'  => $val['indexno'],
                            'cardno'  => $val['cardno'],
                            'reasonid'  => $val['reasonid'],
                            'plan'  => $val['plan'],
                            'benefit_plan' => $val['benefit_plan'],
                            'bizprovid'  => $val['bizprovid'],
                            'remark'  => $val['remark'],
                            'eob_status'  => $val['eob_status'],
                            'payment_status'  => $val['payment_status'],
                            'subtotal'  => $val['subtotal'],
                            'admissiondate'  => $val['admissiondate'],
                            'dischargedate'  => $val['dischargedate'],
                            'created_at' => $val['created_at'],
                            'created_by'  => $val['created_by'],
                            'last_update_by'  => $val['last_update_by'],
                            'ref_no'  => $val['ref_no'],
                            'diagnosis'  => $val['diagnosis'],
                            'status'  => $val['status'],
                            'companyid'  => $val['companyid'],
                            'insurance_id'  => $val['insurance_id'],
                            'claimregdoc'  => $val['claimregdoc'],
                            'claimactivity'  => $val['claimactivity'],
                            'claimregdtl'  => $det,
                            'primary_doctor' => $val['primary_doctor'],
                            'subtotal'  => $val['subtotal'],
                            'old_subtotal'=> $val['old_subtotal'],
                            'discount'          => $val['discount'],
                            'old_discount'      => $val['old_discount'],
                            'rounding'          => $val['rounding'],
                            'old_rounding'      => $val['old_rounding'],
                            'adminfee'          => $val['adminfee'],
                            'old_adminfee'      => $val['old_adminfee'],
                            'grandtotal'      => $val['grandtotal'],
                            'old_grandtotal'  => $val['old_grandtotal']
                        ]);
                    }
                    $getClaimRegistered = $arr[0];

                    // $rute       = storage_path('app/pdf/minio/').$explode2[0].'.txt';
                    // if(file_put_contents($rute, $fileminio)){
                    //     // header("Content-type: application/octet-stream");
                    //     header('Content-type: application/json');
                    //    $content = Fileget::get($rute);
                    //    print_r($content);
                    //     die();
                    // }
                    // return $fileminio;die();
                } else {
                    $isifiledraft = null;
                    if ($statusclaimdoc == 'Pending UW'  or $statusclaimdoc == 'On Progress') {
                        // return "masok";
                        $realpath = 'Transaction/' . $explode[1] . '/' . $explode[2] . '/' . $explode[3] . '/draft.txt';
                        // return $realpath;
                        $draft = Storage::disk('minio')->get($realpath);
                        $isifiledraft = json_decode($draft, true);
                        // return $isifiledraft;
                        // print_r($isifileminio);die();
                        $headerdraft = $isifiledraft['header'];
                        $summarydraft = $isifiledraft['summary'];
                        $isidraft = $isifiledraft['detail'];
                        $c = 0;
                        foreach ($isidraft as $data) {
                            // return $data['Sub_Service']['value'];
                            if ($data['Benefit']['value'] == "") {
                                $sn = DB::table(DB::raw('provider_services ps'))->where('ps.service_name', $data['Sub_Service']['value'])->first();
                                // echo $row[1]['objValue']."<br>";
                                // return $sn->id." - ".$request->auth->insurance_id;
                                if ($sn) {
                                    $ips = DB::table(DB::raw('mapping_services ms'))->where('ms.id_prov_service', $sn->id)->where('insurance_id', $request->auth->insurance_id)->first();
                                } else {
                                    $ips = "";
                                }
                                // dd($ips->id_iziservice_code);
                                // return $ips->id_iziservice_code;
                                // return $ips->id_iziservice_code." - ".$benefit_id;

                                if ($ips) {
                                    $mb = DB::table(DB::raw('mapping_benefits mb'))->where('mb.id_iziservice_code', $ips->id_iziservice_code)->where('benefit_type', $benefit_id)->first();
                                } else {
                                    $mb = "";
                                }
                                // if ($mb) {
                                //     $list_benefits[$hitb] = $mb->benefit_code . " - " . $mb->benefit_name;
                                //     $hitb++;
                                // }
                                if (!empty($mb)) {
                                    $benefit_name[$c] = $mb !== "" ? $mb->benefit_code . " - " . $mb->benefit_name : "";
                                } else {
                                    $benefit_name[$c] = $data['Benefit']['value'];
                                }
                                // $isidraft[$c]['Benefit']['value'] = $benefit_name[$c];
                                // $isidraft[9]['Benefit']['value'] = "hola";

                            } else {
                                $benefit_name[$c] = $data['Benefit']['value'];
                            }
                            $c++;
                            // echo $data['Benefit']['value']."<br/>";
                        }
                        $a = 0;
                        foreach ($benefit_name as $bb) {
                            $isidraft[$a]['Benefit']['value'] = $bb;
                            // echo $isidraft[$a]['Benefit']['value']."<br/>";
                            $a++;
                        }

                        $finaldraft["header"] = $headerdraft;
                        $finaldraft["detail"] = $isidraft;
                        $finaldraft["summary"] = $summarydraft;
                    }
                    // return json_decode($g, true);
                    foreach (json_decode($g, true) as $idx => $val) {
                        array_push($arr, [
                            'id' => $val['id'],
                            'claimregno'  => $val['claimregno'],
                            'memberid'  => $val['memberid'],
                            'policyno'  => $val['policyno'],
                            'indexno'  => $val['indexno'],
                            'cardno'  => $val['cardno'],
                            'reasonid'  => $val['reasonid'],
                            'plan'  => $val['plan'],
                            'benefit_plan' => $val['benefit_plan'],
                            'bizprovid'  => $val['bizprovid'],
                            'remark'  => $val['remark'],
                            'eob_status'  => $val['eob_status'],
                            'payment_status'  => $val['payment_status'],
                            'admissiondate'  => $val['admissiondate'],
                            'dischargedate'  => $val['dischargedate'],
                            'created_at' => $val['created_at'],
                            'created_by'  => $val['created_by'],
                            'last_update_by'  => $val['last_update_by'],
                            'ref_no'  => $val['ref_no'],
                            'diagnosis'  => $val['diagnosis'],
                            'status'  => $val['status'],
                            'companyid'  => $val['companyid'],
                            'insurance_id'  => $val['insurance_id'],
                            'claimregdoc'  => $val['claimregdoc'],
                            'claimactivity'  => $val['claimactivity'],
                            // 'claimregdtl'  => "AAAAAAAA",
                            'claimregdtl'  => ($isifiledraft ? $isifiledraft : $val['claimregdtl']),
                            'primary_doctor' => $val['primary_doctor'],
                            'subtotal'  => $val['subtotal'],
                            'old_subtotal'=> $val['old_subtotal'],
                            'discount'          => $val['discount'],
                            'old_discount'      => $val['old_discount'],
                            'rounding'          => $val['rounding'],
                            'old_rounding'      => $val['old_rounding'],
                            'adminfee'          => $val['adminfee'],
                            'old_adminfee'      => $val['old_adminfee'],
                            'grandtotal'      => $val['grandtotal'],
                            'old_grandtotal'  => $val['old_grandtotal']
                        ]);
                    }
                    $getClaimRegistered = $arr[0];
                }
            }
        } else {
            if ($request->has('keyword')) {
                $keyword        = $request->keyword;
                $where          = array(
                    'memberid',
                    'companyid',
                    'reasonid',
                    'insurance_id',
                    'bizprovid',
                    'plan',
                    'benefit_plan',
                    'created_by',
                    'last_update_by',
                    'claimregdoc',
                    'claimactivity',
                    'claimregdtl',
                    'status'
                );
                $getClaimRegistered = Helper::dynamicSearch($getClaimRegistered, $where, $keyword);

                if (!$getClaimRegistered->count()) {
                    $getClaimRegistered = ClaimRegHdr::with([
                        'companyid',
                        'reasonid',
                        'insurance_id',
                        'bizprovid',
                        'plan',
                        'benefit_plan',
                        'created_by',
                        'last_update_by',
                        'claimregdoc',
                        'claimactivity',
                        'claimregdtl',
                        'status'
                    ]);
                    // ->whereHas('memberid', function ($query) use ($keyword) {
                    //     $query->where('member_name', 'like', '%' . $keyword . '%');
                    // });
                }

                $status                 = true;
                $error                  = null;
                $getClaimRegistered     = Helper::sorting($getClaimRegistered, $sortby, $sortvalue);
            } else {
                $status                 = true;
                $error                  = null;
                if ($request->has('filter')) {
                    $table                  = 'claimreghdr';
                    $filter                 = $request->input('filter');
                    $getClaimRegistered     = Helper::filterSearch($getClaimRegistered, $table, $filter);
                    foreach ($filter as $f) {
                        $filterdata         = json_decode($f);
                        $filterby           = $filterdata->by;
                        $filtervalue        = $filterdata->value;
                        if (is_array($filtervalue)) {
                            if ($filterby == 'claimregno') {
                                $getClaimRegistered     = $getClaimRegistered->whereIn('claimregno', 'like', '%' . $filtervalue . '%');
                            } elseif ($filterby == 'codeDesc') {
                                $getClaimRegistered   = $getClaimRegistered->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            } elseif ($filterby == 'plan') {
                                $getClaimRegistered   = $getClaimRegistered->whereHas('plan', function ($query) use ($filtervalue) {
                                    $query->whereIn('codeDesc', $filtervalue);
                                });
                            }
                        } else {
                            if ($filterby == 'claimregno') {
                                $getClaimRegistered     = $getClaimRegistered->whereIn('claimregno', 'like', '%' . $filtervalue . '%');
                            } elseif ($filterby == 'codeDesc') {
                                $getClaimRegistered   = $getClaimRegistered->whereHas('status', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            } elseif ($filterby == 'plan') {
                                $getClaimRegistered   = $getClaimRegistered->whereHas('plan', function ($query) use ($filtervalue) {
                                    $query->where('codeDesc', 'like', '%' . $filtervalue . '%');
                                });
                            }
                        }
                    }
                }
            }

            $getClaimRegistered     = Helper::sorting($getClaimRegistered, $sortby, $sortvalue);
            $limit                  = $request->has('limit') ? $request->input('limit') : 20;
            $page                   = $request->has('page') ? $request->input('page') : 1;
            $getClaimRegistered     = $getClaimRegistered->paginate($limit, ['*'], 'page', $page);

            $meta                   = [
                'page'          => (int) $getClaimRegistered->currentPage(),
                'perPage'       => (int) $getClaimRegistered->perPage(),
                'total'         => (int) $getClaimRegistered->total(),
                'totalPage'     => (int) $getClaimRegistered->lastPage()
            ];

            foreach ($getClaimRegistered as $m) {
                foreach ($m->claimregdoc as $cc) {
                    $cc['doc_type_desc'] = "";
                    if ($cc['doc_category'] == "main") {
                        $doctype = CodeMasters::where('id', $cc['doc_type'])->first();
                        $doctype = $doctype['codeDesc'];
                        $cc['doc_type_desc'] = $doctype;
                    } elseif ($cc['doc_category'] == "support") {
                        // echo $cc->id."<br>";
                        $doctype = ClaimDocs::where('id', $cc['doc_type'])->first();
                        $doctype = $doctype['claim_doc_name'];
                        $cc['doc_type_desc'] = $doctype;
                    } else {
                        $cc['doc_type_desc'] = "";
                    }
                }
            }

            $getClaimRegistered     = $getClaimRegistered->toArray()['data'];
        }

        if (!$getClaimRegistered) {
            $status                 = false;
            $error                  = "data not found";
        }

        $response = [
            "status"        => (bool) $status,
            "data"          => (isset($getClaimRegistered) ? $getClaimRegistered : null),
            "meta"          => (isset($meta) ? $meta : null),
            "error"         => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

    public function sortclaimdtl($data)
    {
        $out = collect([]);
        foreach ($data as $item) {
            if ($item['detail_parentno'] === null) {
                // $item['child'] = collect([]); //with child
                $out->push($item);
            } else {
                $parent_index = $out->search(function ($e) use ($item) {
                    return $e['id'] == $item['detail_parentno'];
                });
                // $out[$parent_index]['child']->push($item); //with child
                $out->splice($parent_index + 1, 0, [$item]);
            }
        }

        return $out;
    }

    public function updateClaimsubmission(Request $request, $id)
    {
        $this->validate($request, [
            'claimreghdr'                       => 'required',
            // 'claimreghdr.memberid'              => 'integer|nullable',
            // 'claimreghdr.reasonid'              => 'integer|nullable',
            'claimreghdr.bizprovid'             => 'string|nullable',
            // 'claimreghdr.companyid'             => 'integer|nullable',
            'claimreghdr.claimregno'            => 'string',
            // 'claimreghdr.plan'                  => 'string',
            'claimreghdr.benefit_plan'          => 'string',
            // 'claimreghdr.ref_no'                => 'string',
            // 'claimreghdr.remark'                => 'string',
            // 'claimreghdr.eob_status'            => 'string',
            // 'claimreghdr.payment_status'        => 'string|nullable',
            // 'claimreghdr.status'                => 'string',
            // 'claimreghdr.is_sent_mail'          => 'string|nullable',
            'claimreghdr.subtotal'            => 'numeric',
            'claimreghdr.admissiondate'         => 'date',
            'claimreghdr.dischargedate'         => 'date',
            'claimreghdr.diagnosis'             => 'string',

            'claimregdtl'                       => 'required',
            'claimregdtl.*.id'                  => 'integer|nullable',
            'claimregdtl.*.detail_parentno'     => 'string|nullable',
            'claimregdtl.*.seq_number'          => 'integer|nullable',
            // 'claimregdtl.*.claimregno'          => 'string|nullable',
            'claimregdtl.*.servicecode'         => 'string|nullable',
            // 'claimregdtl.*.benefitcode'         => 'string|nullable',
            'claimregdtl.*.length_ofstay'       => 'string|nullable',
            'claimregdtl.*.qty'                 => 'integer|nullable',
            'claimregdtl.*.benefitentitlement'  => 'string|nullable',
            'claimregdtl.*.incuredexpense'      => 'numeric|nullable',
            'claimregdtl.*.amountreimbursed'    => 'string|nullable',
            'claimregdtl.*.excesscharge'        => 'string|nullable',
            'claimregdtl.*.amountdeclained'     => 'string|nullable',
            'claimregdtl.*.reasonCode'          => 'string|nullable',
            'claimregdtl.*.isExclude'           => 'string',

            'claimactivity'                     => 'required',
            'claimactivity.claimregno'          => 'string',
            'claimactivity.acttype'             => 'string',
            'claimactivity.remark'              => 'string',
        ]);
        $data       = ClaimRegHdr::find($id);

        // return $data;
        if ($data != null) {
            if ($request->has('claimreghdr') && count($request->input('claimreghdr')) > 0) {
                $params                   = $request->input('claimreghdr');
                $params['insurance_id']   = $request->auth->insurance_id;
                $params['created_by']     = $request->auth->id;
                $params['last_update_by'] = $request->auth->id;

                $sumClaimtoAmt = 0;
                if ($request->has('claimregdtl') && count($request->input('claimregdtl')) > 0) {
                    if ($request->has('claimreghdr.claimregno')) {
                        $ClaimRegDtlDel = ClaimRegDtl::where("claimregno", $request->input('claimreghdr.claimregno'));
                        $ClaimRegDtlDel->delete();
                    }

                    foreach ($request->input('claimregdtl') as $idxDtl => $dtl) {
                        $dataDtl = $dtl;
                        unset($dataDtl['id']);
                        $dataDtl['insurance_id'] = $request->auth->insurance_id;
                        $dataDtl['claimregno'] = $request->input('claimreghdr.claimregno');
                        $filterDtl = array_filter($dataDtl, 'strlen');

                        //SUM incuredexpense to save in ClaimRegHdr
                        isset($filterDtl['incuredexpense']) &&
                            $sumClaimtoAmt += doubleval($filterDtl['incuredexpense']);
                        //END

                        ClaimRegDtl::create($filterDtl);
                    }
                }

                $params['subtotal'] = doubleval($sumClaimtoAmt);
                $data->fill(array_filter($params, 'strlen'));
                $data->save();

                if ($request->has('claimactivity') && count($request->input('claimactivity')) > 0) {
                    $arrClaimActivity = array_filter($request->input('claimactivity'), 'strlen');
                    $arrClaimActivity['created_by']     = $request->auth->id;
                    $arrClaimActivity['last_update_by'] = $request->auth->id;
                    ClaimActivity::create($arrClaimActivity);
                }
                $reqclaimreghdr = $request->input('claimreghdr');
                $reqclaimreghdr['status'] = $request->input('claimactivity.acttype');
                
                ClaimRegHdr::where('id', $id)->update($reqclaimreghdr);
                return response()->json(['status' => (bool) true, 'message' => 'Your data has been update'], 200);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
        }
    }

    public function createOCRqueue($miniopath, $miniofilename)
    {
        //Start Set Redis for Provider service name
        if (!app('redis')->exists('Provider_service_name')) {
            $provservicename = ProviderServices::selectRaw('service_name')->where('parent_service_id', 0)->get();
            $collect = collect($provservicename)->map(function ($item) {
                return $item['service_name'];
            });
            app('redis')->set("Provider_service_name", $collect);
            app('redis')->expire('Provider_service_name', 86400);
        }
        $redisname = app('redis')->get("Provider_service_name");
        //End Set Redis for Provider service name
        $sorting    = false;
        $status     = false;
        $error      = "upload pdf failed";
        $pathpdf    = $miniopath . '/' . $miniofilename;
        $claimreg   = explode("/", $miniopath);
        $claimregno = $claimreg[2];
        $bizid      = ClaimRegHdr::with('bizprovid')->where('claimregno',$claimregno)->first();
        $mid        = Biz_providers::where('id', $bizid->bizprovid)->first('merchant_id');
        $providerServices = ProviderServices::select('service_name','position')
                                    ->where('mid', $mid->merchant_id)
                                    ->where('parent_service_id', 0)
                                    ->orderBy('position', 'asc')
                                    ->get();
        if($providerServices){
            $listProviderDetail = [];
            foreach($providerServices as $ps){
                $listProviderDetail[strtoupper($ps->service_name)] = [];
            }
        }
        $pdfname    = basename($pathpdf);
        $pdf        = Storage::disk('minio')->get($pathpdf);
        $rute       = storage_path('app/pdf/minio/') . $pdfname;
        if (file_put_contents($rute, $pdf)) {
            header("Content-type: application/pdf");

            if ($rute) {
                $status     = true;
                $error      = "";
            }
            $convertImage   = [];
            if ($status) {
                $convertImage   = $this->tesseractProcess($rute);
            }
            $response   = [
                "status"    => (bool) $status,
                "data"      => $convertImage,
                "error"     => (isset($error) ? $error : null)
            ];
            unlink($rute);
        }

        $txtfile = $response['data']['Result'];
        $txtfile = $this->TextSimiliarity->fixingJSOn($txtfile,$mid->merchant_id);
        $header = $txtfile['Batch']['Documents']['Document'];

        foreach ($header as $h) {
            
            //Start Mapping Header
            try {
                if (array_key_exists('DocumentLevelFields', $h) && !empty($h['DocumentLevelFields'])) {
                    $data[] = $h['DocumentLevelFields']['DocumentLevelField'];
                    if (count($data) > 0) {
                        $i = 0;
                        foreach ($h['DocumentLevelFields']['DocumentLevelField'] as $a) {
                            if ($a['Category'] == "header") {
                                $array[] = [
                                    "objName"           => $a['Name'],
                                    "objType"           => $a['Type'],
                                    "objValue"          => $a['Value'] ? $a['Value'] : '',
                                    "ocrThreshold"      => $a['OcrConfidenceThreshold'],
                                    "ocrConfidence"     => $a['OcrConfidence'],
                                    "ocrThresholdMsg"    => $a['OcrConfidenceThreshold'] > $a['OcrConfidence'] ? "OCR Confidence " . $a['OcrConfidence'] . " is Less than the threshold value " . $a['OcrConfidenceThreshold'] : "",
                                    "isValid"            => isset($a['Value']) ? $a['OcrConfidenceThreshold'] > $a['OcrConfidence'] ? false : true : false,
                                ];
                            }
                            if ($a['Category'] == "footer") {
                                $arrayfooter[] = [
                                    "objName"           => $a['Name'],
                                    "objType"           => $a['Type'],
                                    "objValue"          => $a['Value'] ? $a['Value'] : '',
                                    "ocrThreshold"      => $a['OcrConfidenceThreshold'],
                                    "ocrConfidence"     => $a['OcrConfidence'],
                                    "ocrThresholdMsg"    => $a['OcrConfidenceThreshold'] > $a['OcrConfidence'] ? "OCR Confidence " . $a['OcrConfidence'] . " is Less than the threshold value " . $a['OcrConfidenceThreshold'] : "",
                                    "isValid"            => isset($a['Value']) ? $a['OcrConfidenceThreshold'] > $a['OcrConfidence'] ? false : true : false,
                                ];
                            }
                            $i++;
                        }
                    }
                }
            } catch (\Exception $msg) {
                $array          = [];
                $arrayfooter    = [];
            }
            //End Mapping Header

            //Start Mapping Summary
            try {
                if (array_key_exists('DataTables', $h)) {
                    foreach ($h['DataTables']['DataTable'] as $k) {
                        if ($k['Name'] == 'Summary') {
                            $summary[] = $k['Rows']['Row'];
                            foreach ($k['Rows']['Row'] as $sum) {
                                if (array_key_exists('Columns', $sum)) {
                                    $columsummary[] = $sum['Columns']['Column'];
                                    foreach ($sum['Columns']['Column'] as $fixsummary) {
                                        $arraysummary[] = [
                                            "objName"           => $fixsummary['Name'],
                                            "objType"           => isset($fixsummary['Type']) ? $fixsummary['Type'] : '',
                                            "objValue"          => isset($fixsummary['Value']) ? $fixsummary['Value'] : '',
                                            "ocrThreshold"      => $fixsummary['OcrConfidenceThreshold'],
                                            "ocrConfidence"     => $fixsummary['OcrConfidence'],
                                            "ocrThresholdMsg"    => $fixsummary['OcrConfidenceThreshold'] > $fixsummary['OcrConfidence'] ? "OCR Confidence " . $fixsummary['OcrConfidence'] . " is Less than the threshold value " . $fixsummary['OcrConfidenceThreshold'] : "",
                                            "isValid"            => isset($fixsummary['Value']) ? $fixsummary['OcrConfidenceThreshold'] > $fixsummary['OcrConfidence'] ? false : true : false
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $msg) {
                $arraysummary = [];
            }
            //End Mapping Summary

            //Start Mapping Detail
            try {
                if (array_key_exists('DataTables', $h)) {
                    $arraydetail = [];
                    $arraysubtotal = [];
                    foreach ($h['DataTables']['DataTable'] as $k) {
                        if (strpos($k['Name'], 'dtl_') !== false) { //get file dengan awalan dtl_
                            if (strpos($k['Name'], 'dtl_myp_') !== false) { //get file dengan awalan dtl_myp_
                                $k['Rows']['Row'] = (!isset($k['Rows']['Row'][0]) ? [$k['Rows']['Row']] : $k['Rows']['Row']);
                                foreach ($k['Rows']['Row'] as $inv) {
                                    if (isset($inv["Columns"])) {

                                        $column = [];
                                        $include = false;
                                        
                                        foreach ($inv['Columns']['Column'] as $invoicedetail) {
                                            if (!empty($invoicedetail['Value'])) {
                                                $include = true;
                                                $invoicedetail['Value'] = trim($invoicedetail['Value']);
                                            }
                                            array_push($column, array(
                                                "objName"           => $invoicedetail['Name'],
                                                "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                "objValue"          => isset($invoicedetail['Value']) ? $invoicedetail['Value'] : '', //$invoicedetail['Value'],
                                                "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                            ));
                                        }
                                        if(isset($k['RuleName'])){
                                            $isProviderservice = $k['RuleName'];
                                        } else {
                                            $isProviderservice = $k['Name'];
                                        }

                                        if ($include) {
                                            array_push($column, array(
                                                "objName" => "Provider_Service",
                                                "objType" => "",
                                                "objValue" => $isProviderservice,
                                                "ocrThreshold" => "0",
                                                "ocrConfidence" => "0",
                                                "ocrThresholdMsg" => "",
                                                "isValid" => true
                                            ));
                                            $sorting = true;
                                            array_push($listProviderDetail[strtoupper($isProviderservice)], $column);
                                        }
                                    }
                                }
                            } elseif (strpos($k['Name'], 'dtl_Invoice_Detail') !== false) { //get file dengan awalan dtl_Invoice_Detail
                                $k['Rows']['Row'] = (!isset($k['Rows']['Row'][0]) ? [$k['Rows']['Row']] : $k['Rows']['Row']);
                                foreach ($k['Rows']['Row'] as $inv) {
                                    if (isset($inv["Columns"])) {
                                        $column = [];
                                        $include = false;
                                        foreach ($inv['Columns']['Column'] as $invoicedetail) {
                                            if (!empty($invoicedetail['Value'])) {
                                                $include = true;
                                                $invoicedetail['Value'] = trim($invoicedetail['Value']);
                                            }
                                            array_push($column, array(
                                                "objName"           => $invoicedetail['Name'],
                                                "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                "objValue"          => isset($invoicedetail['Value']) ? $invoicedetail['Value'] : '',
                                                "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                            ));
                                        }
                                        if(isset($k['RuleName'])){
                                            $isProviderservice = $k['RuleName'];
                                        } else {
                                            $isProviderservice = $k['Name'];
                                        }
                                        if ($include) {
                                            array_push($column, array(
                                                "objName" => "Provider_Service",
                                                "objType" => "",
                                                "objValue" => $isProviderservice,
                                                "ocrThreshold" => "0",
                                                "ocrConfidence" => "0",
                                                "ocrThresholdMsg" => "",
                                                "isValid" => true
                                            ));
                                            array_push($arraydetail, $column);
                                        }
                                    }
                                }
                            } elseif (strpos($k['Name'], 'dtl_O_') !== false) { //get file dengan awalan dtl_O_
                                if(isset($k['HeaderRow']['Columns']['Column']) && count($k['HeaderRow']['Columns']['Column'])){
                                    $default = [];
                                    foreach ($k['HeaderRow']['Columns']['Column'] as $headColumn) {
                                        array_push($default, array(
                                                "objName" => $headColumn["Name"],
                                                "objType" => "",
                                                "objValue" =>"",
                                                "ocrThreshold" => "",
                                                "ocrConfidence" => "",
                                                "ocrThresholdMsg" => "",
                                                "isValid" => true
                                            ));
                                    }
                                }

                                $k['Rows']['Row'] = (!isset($k['Rows']['Row'][0]) ? [$k['Rows']['Row']] : $k['Rows']['Row']);
                                $no = 0;
                                foreach ($k['Rows']['Row'] as $inv) {
                                    if (isset($inv["Columns"])) {
                                        $providerservice = [];
                                        $subservice = [];
                                        $include = false;
                                        foreach ($inv['Columns']['Column'] as $invoicedetail) {
                                            if (empty($invoicedetail['Value'])) {
                                                $include = true;
                                                $invoicedetail['Value'] = '-';
                                            }
                                            $isProviderservice = $invoicedetail['Value'];
                                            // }
                                            if (in_array($isProviderservice, json_decode($redisname))) {
                                                array_push($providerservice, array(
                                                    "objName"           => 'Provider_Service',
                                                    "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                    "objValue"          => isset($invoicedetail['Value']) ? $isProviderservice : '', 
                                                    "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                    "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                    "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                    "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                                ));
                                            } else {
                                                array_push($subservice, array(
                                                    "objName"           => $invoicedetail['Name'],
                                                    "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                    "objValue"          => isset($invoicedetail['Value']) ? $invoicedetail['Value'] : '', 
                                                    "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                    "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                    "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                    "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                                ));
                                            }
                                            $column = [
                                                "ProviderService" => $providerservice,
                                                "Subservice"    => $subservice
                                            ];
                                        }
                                        if (!empty($column['ProviderService'])) {
                                            $no++;
                                            $datamen[$no]['ProviderService'] = $column['ProviderService'];
                                        } elseif (isset($datamen[$no]['ProviderService'])) {
                                            $datamen[$no]['Subservice'][] = $column['Subservice'];
                                        }
                                    }
                                }
                                if (isset($datamen)) {
                                    $datapush = [];
                                    foreach ($datamen as $dtmen) {
                                        if (!isset($dtmen['Subservice'])) {
                                            $dtmen['Subservice'][] = $default;
                                        }
                                        foreach ($dtmen['Subservice'] as $dtmenSub) {
                                            array_push($dtmenSub, $dtmen['ProviderService'][0]);
                                            array_push($arraydetail, $dtmenSub);
                                        }
                                    }
                                    unset($datamen);
                                }
                            } elseif (strpos($k['Name'], 'dtl_OC_') !== false){
                                if(isset($k['HeaderRow']['Columns']['Column']) && count($k['HeaderRow']['Columns']['Column'])){
                                    $default = [];
                                    foreach ($k['HeaderRow']['Columns']['Column'] as $headColumn) {
                                        array_push($default, array(
                                                "objName" => $headColumn["Name"],
                                                "objType" => "",
                                                "objValue" =>"",
                                                "ocrThreshold" => "",
                                                "ocrConfidence" => "",
                                                "ocrThresholdMsg" => "",
                                                "isValid" => true
                                            ));
                                    }
                                }
                                $k['Rows']['Row'] = (!isset($k['Rows']['Row'][0]) ? [$k['Rows']['Row']] : $k['Rows']['Row']);
                                $no = 0;
                                foreach ($k['Rows']['Row'] as $key => $inv) {
                                    if (isset($inv["Columns"])) {
                                        $providerservice = [];
                                        $subservice = [];
                                        $subtotal = [];
                                        $skip=false;
                                        $include = false;
                                        foreach ($inv['Columns']['Column'] as $invoicedetail) {
                                            if($skip){
                                                continue;
                                            }
                                            if (empty($invoicedetail['Value'])) {
                                                $include = true;
                                                $invoicedetail['Value'] = '-';
                                            }
                                            
                                            if(strpos(strtolower($invoicedetail['Value']), 'total') !== false){
                                                $isProviderservice = 'subtotal';
                                            }else{
                                                $isProviderservice = $invoicedetail['Value'];
                                            }
                                            
                                            $cekinvoice[] = $isProviderservice;
                                            if (in_array($isProviderservice, json_decode($redisname))) {
                                                array_push($providerservice, array(
                                                    "objName"           => 'Provider_Service',
                                                    "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                    "objValue"          => isset($invoicedetail['Value']) ? $isProviderservice : '',
                                                    "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                    "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                    "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                    "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                                ));
                                            } elseif ($isProviderservice == 'subtotal'){
                                                $arraysubtotal[$invoicedetail['Value']] = $this->helper->findBox_insideBox($k['Rows']['Row'][$key]['Columns']['Column']);//$k['Rows']['Row'][$key]['Columns']['Column'];
                                                $skip = true;
                                            } else {
                                                array_push($subservice, array(
                                                    "objName"           => $invoicedetail['Name'],
                                                    "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                    "objValue"          => isset($invoicedetail['Value']) ? $invoicedetail['Value'] : '',
                                                    "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                    "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                    "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                    "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                                ));
                                            }

                                            $column = [
                                                "ProviderService" => $providerservice,
                                                "Subservice"    => $subservice
                                            ];

                                        }
                                        if(!$skip){
                                            if (!empty($column['ProviderService'])) {
                                                $no++;
                                                $datamen[$no]['ProviderService'] = $column['ProviderService'];
                                            } elseif (isset($datamen[$no]['ProviderService'])) {
                                                $datamen[$no]['Subservice'][] = $column['Subservice'];
                                            }
                                        }
                                    }
                                }
                                if (isset($datamen)) {
                                    $datapush[] = $datamen;
                                    foreach ($datamen as $dtmen) {
                                        if (!isset($dtmen['Subservice'])) {
                                            $dtmen['Subservice'][] = $default;
                                        }
                                        foreach ($dtmen['Subservice'] as $dtmenSub) {
                                            array_push($dtmenSub, $dtmen['ProviderService'][0]);
                                            array_push($arraydetail, $dtmenSub);
                                        }
                                    }
                                    unset($datamen);
                                    unset($default);
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $msg) {
                $arraydetail = [];
                $arraysubtotal = [];
            }
            //End Mapping Detail

            //Start Mapping List Doctor
            try {
                if (array_key_exists('DataTables', $h)) {
                    foreach ($h['DataTables']['DataTable'] as $d) {
                        if (!empty($d['Rows'])) {
                            foreach ($d['Rows']['Row'] as $doc) {
                                if(isset($doc['Columns'])){
                                    foreach ($doc['Columns']['Column'] as $listdoctor) {
                                        if ($listdoctor['Name'] == 'Sub_Service' && strpos($listdoctor['Value'], 'Dr ') !== False && strpos($listdoctor['Value'], 'Dr ') < 4) {
                                            $doctors[] = strtoupper($listdoctor['Value']);
                                            $arraydoctor = array_values(array_unique($doctors));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $msg) {
                $arraydoctor = [];
            }
            //End Mapping List Doctor
        }

        if($sorting){
            $arraydetail = collect($listProviderDetail)->flatMap(function ($list) {
                return $list;
            })->toArray();
        }

        //Start Update database
        if ($array) {
            if ($arrayfooter) {
                foreach ($arrayfooter as $footerarray) {
                    if ($footerarray['objName'] == "Sub_Total") {
                        $subtotalvalue = $footerarray['objValue'] ? $footerarray['objValue'] : 0;
                    }
                    if ($footerarray['objName'] == "Administration_Fee") {
                        $adminfeevalue = $footerarray['objValue'] ? $footerarray['objValue'] : 0;
                    }
                    if ($footerarray['objName'] == "Rounding") {
                        $roundingvalue = $footerarray['objValue'] ? $footerarray['objValue'] : 0;
                    }      
                    if ($footerarray['objName'] == "Discount") {
                        $discountvalue = $footerarray['objValue'] ? $footerarray['objValue'] : 0;
                    }            
                    if ($footerarray['objName'] == "Grand_Total") {
                        $grantotalvalue = $footerarray['objValue'] ? $footerarray['objValue'] : 0;
                    }  
                }
                foreach ($array as $headarray) {
                    if ($headarray['objName'] == "Admission_Date") {
                        $admissiondatevalue = explode(" ", $headarray['objValue']);
                    }
                    if ($headarray['objName'] == "Discharge_Date") {
                        $dischargedatevalue = explode(" ", $headarray['objValue']);
                    }
                    if ($headarray['objName'] == "Primary_Doctor") {
                        $primarydoctorvalue = $headarray['objValue'];
                    }
                }
                ClaimRegHdr::where('claimregno', $claimregno)->update(array(
                    'old_subtotal' => str_replace(",", "", $subtotalvalue),
                    'old_adminfee' => str_replace(",", "", $adminfeevalue),
                    'old_rounding' => str_replace(",", "", $roundingvalue),
                    'old_discount' => str_replace(",", "", $discountvalue),
                    'old_grandtotal' => str_replace(",", "", $grantotalvalue),
                    'admissiondate' => date("Y-m-d", strtotime(str_replace("/", "-", $admissiondatevalue[0]))),
                    'dischargedate' => date("Y-m-d", strtotime(str_replace("/", "-", $dischargedatevalue[0]))),
                    'primary_doctor' => isset($primarydoctorvalue) ? $primarydoctorvalue : "-"
                ));
            } else {
                foreach ($array as $headarray) {
                    if ($headarray['objName'] == "Admission_Date") {
                        $admissiondatevalue = explode(" ", $headarray['objValue']);
                    }
                    if ($headarray['objName'] == "Discharge_Date") {
                        $dischargedatevalue = explode(" ", $headarray['objValue']);
                    }
                    if ($headarray['objName'] == "Primary_Doctor") {
                        $primarydoctorvalue = $headarray['objValue'];
                    }
                }
                ClaimRegHdr::where('claimregno', $claimregno)->update(array(
                    'old_subtotal' => '0',
                    'old_adminfee' => '0',
                    'old_rounding' => '0',
                    'old_discount' => '0',
                    'old_grandtotal' => '0',
                    'admissiondate' => date("Y-m-d", strtotime(str_replace("/", "-", $admissiondatevalue[0]))),
                    'dischargedate' => date("Y-m-d", strtotime(str_replace("/", "-", $dischargedatevalue[0]))),
                    'primary_doctor' => isset($primarydoctorvalue) ? $primarydoctorvalue : "-"
                ));
            }
        }
        //End Update database

        //--------- SUMMARY ---------------
        $response = [
            "Header" => isset($array) ? $array : [],
            "Summary" => isset($arraysummary) ? array_chunk($arraysummary, 3) : [],
            "Detail" => isset($arraydetail) ? $arraydetail : [],
            "SubTotal" => isset($arraysubtotal) ? $arraysubtotal : [],
            "Footer"    => isset($arrayfooter) ? $arrayfooter : []
        ];

        for ($i = 0; $i < count($response['Detail']); $i++) {
            foreach ($response['Detail'][$i] as $responsedetail) {
                if (isset($responsedetail['objName'])) {
                    if ($responsedetail['objName'] == 'No') {
                        $det[] = $responsedetail['objValue'];
                    }
                }
            }
        }
        
        if (isset($det)) {
            $lastindex = count($det) - 1;
            $lastdata = $this->getLastIndex($det, $lastindex);
            $selisih = count($det) / $lastdata;
            $response['DetailType'] = ($selisih > 2) ? "REPEAT" : "CONTINOUS";
        } else {
            $response['DetailType'] = "NULL";
        }

        //Start write file in minIO
        $filename = explode(".", $miniofilename);
        Storage::disk('minio')->put($miniopath . '/' . $filename[0] . '.txt', json_encode($response, JSON_PRETTY_PRINT));
        Storage::disk('minio')->put($miniopath . '/' . $filename[0] . '_origin.txt', json_encode($txtfile, JSON_PRETTY_PRINT));
        return true;
    }

    public function createOCR(Request $request)
    {
        
        //Start Set Redis for Provider service name
        if (!app('redis')->exists('Provider_service_name')) {
            $provservicename = ProviderServices::selectRaw('service_name')->where('parent_service_id', 0)->get();
            $collect = collect($provservicename)->map(function ($item) {
                return $item['service_name'];
            });
            app('redis')->set("Provider_service_name", $collect);
            app('redis')->expire('Provider_service_name', 86400);
        }
        $redisname = app('redis')->get("Provider_service_name");
        //End Set Redis for Provider service name
        $sorting    = false;
        $status     = false;
        $error      = "upload pdf failed";
        $pathpdf    = $request->input('pathpdf'); // file path from minIO
        $claimreg   = explode("/", $pathpdf);
        $claimregno = $claimreg[2];
        $bizid      = ClaimRegHdr::with('bizprovid')->where('claimregno',$claimregno)->first();
        // return $bizid->bizprovid;
        $mid        = Biz_providers::where('id', $bizid->bizprovid)->first('merchant_id');
        $providerServices = ProviderServices::select('service_name','position')
                                    ->where('mid', $mid->merchant_id)
                                    ->where('parent_service_id', 0)
                                    ->orderBy('position', 'asc')
                                    ->get();
        if($providerServices){
            $listProviderDetail = [];
            foreach($providerServices as $ps){
                $listProviderDetail[strtoupper($ps->service_name)] = [];
            }
        }

        // return $bizid;
        // return $mid->merchant_id;
        $pathsave   = substr($pathpdf, 0, strrpos($pathpdf, '/'));
        // return $pathsave;
        $pdfname    = basename($pathpdf);
        $pdf        = Storage::disk('minio')->get($pathpdf);
        $rute       = storage_path('app/pdf/minio/') . $pdfname;
        if (file_put_contents($rute, $pdf)) {
            header("Content-type: application/pdf");

            if ($rute) {
                $status     = true;
                $error      = "";
            }
            $convertImage   = [];
            if ($status) {
                $convertImage   = $this->tesseractProcess($rute); //pass file from minIO to ephesoft API 
                // return $convertImage;
            }
            $response   = [
                "status"    => (bool) $status,
                "data"      => $convertImage,
                "error"     => (isset($error) ? $error : null)
            ];
            unlink($rute);
        }
        $txtfile = $response['data']['Result'];
        // return $txtfile;
        $txtfile = $this->TextSimiliarity->fixingJSOn($txtfile,$mid->merchant_id);
        // return $txtfile;
        // return json_encode($txtfile);
        // $response = json_decode(file_get_contents(resource_path('test.json')),true); //Siloam
        // $response = json_decode(file_get_contents(resource_path('test2.json')),true); //Mayapada
        // $response = json_decode(file_get_contents(resource_path('test3.json')),true); //MayapadaLigan
        $header = $txtfile['Batch']['Documents']['Document'];
        foreach ($header as $h) {
            
            //Start Mapping Header
            try {
                if (array_key_exists('DocumentLevelFields', $h) && !empty($h['DocumentLevelFields'])) {
                    $data[] = $h['DocumentLevelFields']['DocumentLevelField'];
                    if (count($data) > 0) {
                        $i = 0;
                        foreach ($h['DocumentLevelFields']['DocumentLevelField'] as $a) {
                            if ($a['Category'] == "header") {
                                $array[] = [
                                    "objName"           => $a['Name'],
                                    "objType"           => $a['Type'],
                                    "objValue"          => $a['Value'] ? $a['Value'] : '',
                                    "ocrThreshold"      => $a['OcrConfidenceThreshold'],
                                    "ocrConfidence"     => $a['OcrConfidence'],
                                    "ocrThresholdMsg"    => $a['OcrConfidenceThreshold'] > $a['OcrConfidence'] ? "OCR Confidence " . $a['OcrConfidence'] . " is Less than the threshold value " . $a['OcrConfidenceThreshold'] : "",
                                    "isValid"            => isset($a['Value']) ? $a['OcrConfidenceThreshold'] > $a['OcrConfidence'] ? false : true : false,
                                    // "category"          => $a['Category']
                                ];
                            }
                            if ($a['Category'] == "footer") {
                                $arrayfooter[] = [
                                    "objName"           => $a['Name'],
                                    "objType"           => $a['Type'],
                                    "objValue"          => $a['Value'] ? $a['Value'] : '',
                                    "ocrThreshold"      => $a['OcrConfidenceThreshold'],
                                    "ocrConfidence"     => $a['OcrConfidence'],
                                    "ocrThresholdMsg"    => $a['OcrConfidenceThreshold'] > $a['OcrConfidence'] ? "OCR Confidence " . $a['OcrConfidence'] . " is Less than the threshold value " . $a['OcrConfidenceThreshold'] : "",
                                    "isValid"            => isset($a['Value']) ? $a['OcrConfidenceThreshold'] > $a['OcrConfidence'] ? false : true : false,
                                    // "category"          => $a['Category']
                                ];
                            }
                            $i++;
                        }
                    }
                }
            } catch (\Exception $msg) {
                $array          = [];
                $arrayfooter    = [];
            }
            //End Mapping Header

            //Start Mapping Summary
            try {
                if (array_key_exists('DataTables', $h)) {
                    foreach ($h['DataTables']['DataTable'] as $k) {
                        if ($k['Name'] == 'Summary') {
                            $summary[] = $k['Rows']['Row'];
                            foreach ($k['Rows']['Row'] as $sum) {
                                if (array_key_exists('Columns', $sum)) {
                                    $columsummary[] = $sum['Columns']['Column'];
                                    foreach ($sum['Columns']['Column'] as $fixsummary) {
                                        $arraysummary[] = [
                                            "objName"           => $fixsummary['Name'],
                                            "objType"           => isset($fixsummary['Type']) ? $fixsummary['Type'] : '',
                                            "objValue"          => isset($fixsummary['Value']) ? $fixsummary['Value'] : '',
                                            "ocrThreshold"      => $fixsummary['OcrConfidenceThreshold'],
                                            "ocrConfidence"     => $fixsummary['OcrConfidence'],
                                            "ocrThresholdMsg"    => $fixsummary['OcrConfidenceThreshold'] > $fixsummary['OcrConfidence'] ? "OCR Confidence " . $fixsummary['OcrConfidence'] . " is Less than the threshold value " . $fixsummary['OcrConfidenceThreshold'] : "",
                                            "isValid"            => isset($fixsummary['Value']) ? $fixsummary['OcrConfidenceThreshold'] > $fixsummary['OcrConfidence'] ? false : true : false
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $msg) {
                $arraysummary = [];
            }
            //End Mapping Summary

            //Start Mapping Detail
            try {
                if (array_key_exists('DataTables', $h)) {
                    $arraydetail = [];
                    $arraysubtotal = [];
                    foreach ($h['DataTables']['DataTable'] as $k) {
                        //get file dengan awalan dtl_
                        if (strpos($k['Name'], 'dtl_') !== false) {
                            if (strpos($k['Name'], 'dtl_myp_') !== false) { //get file dengan awalan dtl_myp_
                                $k['Rows']['Row'] = (!isset($k['Rows']['Row'][0]) ? [$k['Rows']['Row']] : $k['Rows']['Row']);
                                foreach ($k['Rows']['Row'] as $inv) {
                                    if (isset($inv["Columns"])) {

                                        $column = [];
                                        $include = false;
                                        
                                        foreach ($inv['Columns']['Column'] as $invoicedetail) {
                                            if (!empty($invoicedetail['Value'])) {
                                                $include = true;
                                                $invoicedetail['Value'] = trim($invoicedetail['Value']);
                                            }
                                            array_push($column, array(
                                                "objName"           => $invoicedetail['Name'],
                                                "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                "objValue"          => isset($invoicedetail['Value']) ? $invoicedetail['Value'] : '', //$invoicedetail['Value'],
                                                "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                            ));
                                        }
                                        if(isset($k['RuleName'])){
                                            $isProviderservice = $k['RuleName'];
                                        } else {
                                            $isProviderservice = $k['Name'];
                                        }

                                        if ($include) {
                                            array_push($column, array(
                                                "objName" => "Provider_Service",
                                                "objType" => "",
                                                "objValue" => $isProviderservice,
                                                "ocrThreshold" => "0",
                                                "ocrConfidence" => "0",
                                                "ocrThresholdMsg" => "",
                                                "isValid" => true
                                            ));
                                            $sorting = true;
                                            array_push($listProviderDetail[strtoupper($isProviderservice)], $column);
                                        }
                                    }
                                }
                            } elseif (strpos($k['Name'], 'dtl_Invoice_Detail') !== false) { //get file dengan awalan dtl_Invoice_Detail
                                $k['Rows']['Row'] = (!isset($k['Rows']['Row'][0]) ? [$k['Rows']['Row']] : $k['Rows']['Row']);
                                foreach ($k['Rows']['Row'] as $inv) {
                                    if (isset($inv["Columns"])) {
                                        $column = [];
                                        $include = false;
                                        foreach ($inv['Columns']['Column'] as $invoicedetail) {
                                            if (!empty($invoicedetail['Value'])) {
                                                $include = true;
                                                $invoicedetail['Value'] = trim($invoicedetail['Value']);
                                            }
                                            array_push($column, array(
                                                "objName"           => $invoicedetail['Name'],
                                                "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                "objValue"          => isset($invoicedetail['Value']) ? $invoicedetail['Value'] : '', //$invoicedetail['Value'],
                                                "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                            ));
                                        }
                                        if(isset($k['RuleName'])){
                                            $isProviderservice = $k['RuleName'];
                                        } else {
                                            $isProviderservice = $k['Name'];
                                        }
                                        if ($include) {
                                            array_push($column, array(
                                                "objName" => "Provider_Service",
                                                "objType" => "",
                                                "objValue" => $isProviderservice,
                                                "ocrThreshold" => "0",
                                                "ocrConfidence" => "0",
                                                "ocrThresholdMsg" => "",
                                                "isValid" => true
                                            ));
                                            array_push($arraydetail, $column);
                                        }
                                    }
                                }
                            } elseif (strpos($k['Name'], 'dtl_O_') !== false) { //get file dengan awalan dtl_O_
                                if(isset($k['HeaderRow']['Columns']['Column']) && count($k['HeaderRow']['Columns']['Column'])){
                                    $default = [];
                                    foreach ($k['HeaderRow']['Columns']['Column'] as $headColumn) {
                                        array_push($default, array(
                                                "objName" => $headColumn["Name"],
                                                "objType" => "",
                                                "objValue" =>"",
                                                "ocrThreshold" => "",
                                                "ocrConfidence" => "",
                                                "ocrThresholdMsg" => "",
                                                "isValid" => true
                                            ));
                                    }
                                }

                                $k['Rows']['Row'] = (!isset($k['Rows']['Row'][0]) ? [$k['Rows']['Row']] : $k['Rows']['Row']);
                                $no = 0;
                                foreach ($k['Rows']['Row'] as $inv) {
                                    if (isset($inv["Columns"])) {
                                        $providerservice = [];
                                        $subservice = [];
                                        $include = false;
                                        foreach ($inv['Columns']['Column'] as $invoicedetail) {
                                            if (empty($invoicedetail['Value'])) {
                                                $include = true;
                                                $invoicedetail['Value'] = '-';
                                            }
                                            $isProviderservice = $invoicedetail['Value'];
                                            // }
                                            if (in_array($isProviderservice, json_decode($redisname))) {
                                                array_push($providerservice, array(
                                                    "objName"           => 'Provider_Service',
                                                    "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                    "objValue"          => isset($invoicedetail['Value']) ? $isProviderservice : '', //$invoicedetail['Value'],
                                                    "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                    "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                    "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                    "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                                ));
                                            } else {
                                                array_push($subservice, array(
                                                    "objName"           => $invoicedetail['Name'],
                                                    "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                    "objValue"          => isset($invoicedetail['Value']) ? $invoicedetail['Value'] : '', //$invoicedetail['Value'],
                                                    "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                    "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                    "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                    "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                                ));
                                            }
                                            $column = [
                                                "ProviderService" => $providerservice,
                                                "Subservice"    => $subservice
                                            ];
                                        }
                                        if (!empty($column['ProviderService'])) {
                                            $no++;
                                            $datamen[$no]['ProviderService'] = $column['ProviderService'];
                                        } elseif (isset($datamen[$no]['ProviderService'])) {
                                            $datamen[$no]['Subservice'][] = $column['Subservice'];
                                        }
                                    }
                                }
                                if (isset($datamen)) {
                                    $datapush = [];
                                    foreach ($datamen as $dtmen) {
                                        if (!isset($dtmen['Subservice'])) {
                                            $dtmen['Subservice'][] = $default;
                                        }
                                        foreach ($dtmen['Subservice'] as $dtmenSub) {
                                            array_push($dtmenSub, $dtmen['ProviderService'][0]);
                                            array_push($arraydetail, $dtmenSub);
                                        }
                                    }
                                    unset($datamen);
                                }
                            } elseif (strpos($k['Name'], 'dtl_OC_') !== false){
                                if(isset($k['HeaderRow']['Columns']['Column']) && count($k['HeaderRow']['Columns']['Column'])){
                                    $default = [];
                                    foreach ($k['HeaderRow']['Columns']['Column'] as $headColumn) {
                                        array_push($default, array(
                                                "objName" => $headColumn["Name"],
                                                "objType" => "",
                                                "objValue" =>"",
                                                "ocrThreshold" => "",
                                                "ocrConfidence" => "",
                                                "ocrThresholdMsg" => "",
                                                "isValid" => true
                                            ));
                                    }
                                }
                                $k['Rows']['Row'] = (!isset($k['Rows']['Row'][0]) ? [$k['Rows']['Row']] : $k['Rows']['Row']);
                                $no = 0;
                                foreach ($k['Rows']['Row'] as $key => $inv) {
                                    if (isset($inv["Columns"])) {
                                        $providerservice = [];
                                        $subservice = [];
                                        $subtotal = [];
                                        $skip=false;
                                        $include = false;
                                        foreach ($inv['Columns']['Column'] as $invoicedetail) {
                                            if($skip){
                                                continue;
                                            }
                                            if (empty($invoicedetail['Value'])) {
                                                $include = true;
                                                $invoicedetail['Value'] = '-';
                                            }
                                            
                                            if(strpos(strtolower($invoicedetail['Value']), 'total') !== false){
                                                $isProviderservice = 'subtotal';
                                            }else{
                                                $isProviderservice = $invoicedetail['Value'];
                                            }
                                            // }
                                            $cekinvoice[] = $isProviderservice;
                                            if (in_array($isProviderservice, json_decode($redisname))) {
                                                array_push($providerservice, array(
                                                    "objName"           => 'Provider_Service',
                                                    "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                    "objValue"          => isset($invoicedetail['Value']) ? $isProviderservice : '', //$invoicedetail['Value'],
                                                    "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                    "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                    "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                    "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                                ));
                                            } elseif ($isProviderservice == 'subtotal'){
                                                $arraysubtotal[$invoicedetail['Value']] = $this->helper->findBox_insideBox($k['Rows']['Row'][$key]['Columns']['Column']);//$k['Rows']['Row'][$key]['Columns']['Column'];
                                                $skip = true;
                                            } else {
                                                array_push($subservice, array(
                                                    "objName"           => $invoicedetail['Name'],
                                                    "objType"           => isset($invoicedetail['Type']) ? $invoicedetail['Type'] : '',
                                                    "objValue"          => isset($invoicedetail['Value']) ? $invoicedetail['Value'] : '', //$invoicedetail['Value'],
                                                    "ocrThreshold"      => $invoicedetail['OcrConfidenceThreshold'],
                                                    "ocrConfidence"     => $invoicedetail['OcrConfidence'],
                                                    "ocrThresholdMsg"    => $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? "OCR Confidence " . $invoicedetail['OcrConfidence'] . " is Less than the threshold value " . $invoicedetail['OcrConfidenceThreshold'] : "",
                                                    "isValid"            => isset($invoicedetail['Value']) ? $invoicedetail['OcrConfidenceThreshold'] > $invoicedetail['OcrConfidence'] ? false : true : false
                                                ));
                                            }

                                            $column = [
                                                "ProviderService" => $providerservice,
                                                "Subservice"    => $subservice
                                            ];

                                        }
                                        if(!$skip){
                                            if (!empty($column['ProviderService'])) {
                                                $no++;
                                                $datamen[$no]['ProviderService'] = $column['ProviderService'];
                                            } elseif (isset($datamen[$no]['ProviderService'])) {
                                                $datamen[$no]['Subservice'][] = $column['Subservice'];
                                            }
                                        }
                                    }
                                }
                                if (isset($datamen)) {
                                    $datapush[] = $datamen;
                                    foreach ($datamen as $dtmen) {
                                        if (!isset($dtmen['Subservice'])) {
                                            $dtmen['Subservice'][] = $default;
                                        }
                                        foreach ($dtmen['Subservice'] as $dtmenSub) {
                                            array_push($dtmenSub, $dtmen['ProviderService'][0]);
                                            array_push($arraydetail, $dtmenSub);
                                        }
                                    }
                                    unset($datamen);
                                    unset($default);
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $msg) {
                $arraydetail = [];
                $arraysubtotal = [];
            }
            //End Mapping Detail

            //Start Mapping List Doctor
            try {
                if (array_key_exists('DataTables', $h)) {
                    foreach ($h['DataTables']['DataTable'] as $d) {
                        if (!empty($d['Rows'])) {
                            foreach ($d['Rows']['Row'] as $doc) {
                                if(isset($doc['Columns'])){
                                    foreach ($doc['Columns']['Column'] as $listdoctor) {
                                        if ($listdoctor['Name'] == 'Sub_Service' && strpos($listdoctor['Value'], 'Dr ') !== False && strpos($listdoctor['Value'], 'Dr ') < 4) {
                                            $doctors[] = strtoupper($listdoctor['Value']);
                                            $arraydoctor = array_values(array_unique($doctors));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $msg) {
                $arraydoctor = [];
            }
            //End Mapping List Doctor
        }

        // return response()->json(['header' => $array, 'footer' => $arrayfooter],200);
        
        if($sorting){
            $arraydetail = collect($listProviderDetail)->flatMap(function ($list) {
                return $list;
            })->toArray();
        }
        //Start Update database
        // if ($array) {
        //     if ($arrayfooter) {
        //         foreach ($arrayfooter as $footerarray) {
        //             if ($footerarray['objName'] == "Sub_Total") {
        //                 $subtotalvalue = $footerarray['objValue'] ? $footerarray['objValue'] : 0;
        //             }
        //             if ($footerarray['objName'] == "Administration_Fee") {
        //                 $adminfeevalue = $footerarray['objValue'] ? $footerarray['objValue'] : 0;
        //             }
        //             if ($footerarray['objName'] == "Rounding") {
        //                 $roundingvalue = $footerarray['objValue'] ? $footerarray['objValue'] : 0;
        //             }      
        //             if ($footerarray['objName'] == "Discount") {
        //                 $discountvalue = $footerarray['objValue'] ? $footerarray['objValue'] : 0;
        //             }            
        //             if ($footerarray['objName'] == "Grand_Total") {
        //                 $grantotalvalue = $footerarray['objValue'] ? $footerarray['objValue'] : 0;
        //             }  
        //         }
        //         foreach ($array as $headarray) {
        //             if ($headarray['objName'] == "Admission_Date") {
        //                 $admissiondatevalue = explode(" ", $headarray['objValue']);
        //             }
        //             if ($headarray['objName'] == "Discharge_Date") {
        //                 $dischargedatevalue = explode(" ", $headarray['objValue']);
        //             }
        //             if ($headarray['objName'] == "Primary_Doctor") {
        //                 $primarydoctorvalue = $headarray['objValue'];
        //             }
        //         }
        //         ClaimRegHdr::where('claimregno', $claimregno)->update(array(
        //             'old_subtotal' => str_replace(",", "", $subtotalvalue),
        //             'old_adminfee' => str_replace(",", "", $adminfeevalue),
        //             'old_rounding' => str_replace(",", "", $roundingvalue),
        //             'old_discount' => str_replace(",", "", $discountvalue),
        //             'old_grandtotal' => str_replace(",", "", $grantotalvalue),
        //             'admissiondate' => date("Y-m-d", strtotime(str_replace("/", "-", $admissiondatevalue[0]))),
        //             'dischargedate' => date("Y-m-d", strtotime(str_replace("/", "-", $dischargedatevalue[0]))),
        //             'primary_doctor' => isset($primarydoctorvalue) ? $primarydoctorvalue : "-"
        //         ));
        //     } else {
        //         foreach ($array as $headarray) {
        //             if ($headarray['objName'] == "Admission_Date") {
        //                 $admissiondatevalue = explode(" ", $headarray['objValue']);
        //             }
        //             if ($headarray['objName'] == "Discharge_Date") {
        //                 $dischargedatevalue = explode(" ", $headarray['objValue']);
        //             }
        //             if ($headarray['objName'] == "Primary_Doctor") {
        //                 $primarydoctorvalue = $headarray['objValue'];
        //             }
        //         }
        //         ClaimRegHdr::where('claimregno', $claimregno)->update(array(
        //             'old_subtotal' => '0',
        //             'old_adminfee' => '0',
        //             'old_rounding' => '0',
        //             'old_discount' => '0',
        //             'old_grandtotal' => '0',
        //             'admissiondate' => date("Y-m-d", strtotime(str_replace("/", "-", $admissiondatevalue[0]))),
        //             'dischargedate' => date("Y-m-d", strtotime(str_replace("/", "-", $dischargedatevalue[0]))),
        //             'primary_doctor' => isset($primarydoctorvalue) ? $primarydoctorvalue : "-"
        //         ));
        //     }
        // }
        //End Update database

        //--------- SUMMARY ---------------
        $response = [
            "Header" => isset($array) ? $array : [], //$data,
            "Summary" => isset($arraysummary) ? array_chunk($arraysummary, 3) : [], //$arraysummary
            "Detail" => isset($arraydetail) ? $arraydetail : [],
            "SubTotal" => isset($arraysubtotal) ? $arraysubtotal : [],
            "Footer"    => isset($arrayfooter) ? $arrayfooter : []
        ];

        for ($i = 0; $i < count($response['Detail']); $i++) {
            foreach ($response['Detail'][$i] as $responsedetail) {
                if (isset($responsedetail['objName'])) {
                    if ($responsedetail['objName'] == 'No') {
                        $det[] = $responsedetail['objValue'];
                    }
                }
            }
        }
        
        // return $det;
        if (isset($det)) {
            $lastindex = count($det) - 1;
            // return $lastindex;
            $lastdata = $this->getLastIndex($det, $lastindex);
            // return $lastdata;
            $selisih = count($det) / $lastdata;
            // return $selisih;
            $response['DetailType'] = ($selisih > 2) ? "REPEAT" : "CONTINOUS";
        } else {
            $response['DetailType'] = "NULL";
        }
        // //Start write file in minIO
        // $filename = explode(".", $pdfname);
        // Storage::disk('minio')->put($pathsave . '/' . $filename[0] . '.txt', json_encode($response, JSON_PRETTY_PRINT));
        // Storage::disk('minio')->put($pathsave . '/' . $filename[0] . '_origin.txt', json_encode($txtfile, JSON_PRETTY_PRINT)); //upload tesseract result to minIO as .txt file
        // End write file in minIO
        return response()->json($response); //return result as JSON
    }

    public function getLastIndex($array, $index)
    {
        $value = $array[$index];
        if (is_numeric($value)) {
            return $value;
        } else {
            return $this->getLastIndex($array, $index-1);
        }
    }

    public function tesseractProcess($pdffile) //function integration with ephesoft API
    {
        $usrpswd            = env('USREPHESOFT') . ':' . env('PSWDEPHESOFT');
        $batchidentifier    = env('BATCH_IDENTIFIER');
        $code               = base64_encode($usrpswd);
        $auth               = 'Basic ' . $code;
        $file               = new \CURLFile($pdffile);
        $vars               = array("batchClassIdentifier" => $batchidentifier, "input.pdf" => $file);
        $ch                 = curl_init();
        curl_setopt($ch, CURLOPT_URL, env('EPHESOFT') . '/dcma/rest/ocrClassifyExtract');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $headers = [
            'Authorization:' . $auth
        ];
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $server_output = curl_exec($ch);
        // return $server_output;die();
        curl_close($ch);
        $json       = simplexml_load_string($server_output, 'SimpleXMLElement', LIBXML_NOCDATA);
        $jsonString = json_encode($json);
        $array      = json_decode($jsonString, true);
        return $array; //return results as XML
    }

    // public function minio()
    // {
    //     $key = env('MINIO_KEY');
    // $s3     = \Storage::disk('minio');
    // $client = $s3->getDriver()->getAdapter()->getClient();
    // $bucket = Config::get('filesystems.disks.minio.bucket');
    // // print_r($bucket);die();

    // $command = $client->getCommand('GetObject', [
    //     'Bucket' => $bucket,
    //     'Key'    => $key
    // ]);

    // $request = $client->createPresignedRequest($command, '+20 minutes');

    // return (string) $request->getUri();
    // }
}
