<?php
namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Exports\UserManagementExport;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AuthController;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    private $authcontroller;

    public function __construct(AuthController $authcontroller) {
        $this->authcontroller = $authcontroller;
    }

    public function register(Request $request)
    {
        //Register New User
        $this->validate($request, [
            'name'              => 'required|string',
            'phone'             => 'required',
            'email'             => 'required|email',
            'username'          => 'required|string',
            'role_user'         => 'required|integer',
            'insurance_id'      => 'required|integer',
            'isInsurance'       => 'string'
        ]);
            
        try {
            $hasher             = app()->make('hash');
            $user               = new User;
            $params             = $request->all();
            $params['insurance_id'] = ($request->auth->isInsurance ? $request->auth->insurance_id : $params['insurance_id']);
            $user->fill($params);
            $user->status       = 1;
            
            $cekuser = User::where('username', $request->input('username'))
                                    ->orWhere('email', $request->input('email'))
                                    ->first();
            if ($cekuser) {
                return response()->json(['message' => 'Username or email already exist!'], 409);
            } else {
                $user->save();
                $this->authcontroller->sendForgotPasswordMail();
                return response()->json(['user' => $user, 'message' => 'CREATED'], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }
    }

    public function getProfile(Request $request)
    {
        $this->validate($request, [
            'email'     => 'string'
        ]);
        $user = User::where('email', $request->input('email'))->orWhere('username', $request->input('email'))->first();
        if (!$user) {
            return response()->json([
                'error' => 'User does not exist.'
            ], 400);
        } else {
            return response()->json([
                'name'  => $user->name,
                'email' => $user->email,
                'phone' => $user->phone
            ], 200);
        }
    }

    //show all data from users table
    public function allUsers(Request $request, $id = null)
    {
        $sortby     = $request->input('sortby');
        $sortvalue  = $request->input('sortvalue');
        $status     = false;
        $error      = "data not found";
        $getusers   = User::with('roles')->with('insurance')->with('permissions');
        if ($request->auth->insurance_id) {
            $getusers   = $getusers->where('insurance_id', $request->auth->insurance_id);
        }
        if($id){
            $getusers = $getusers->with('roles')
                        ->where('id', $id)
                        ->first();

            if($getusers){
                $status = true;
                $error  = null;
            }

        }else{
            if($request->has('keyword')){
            $keyword    = $request->keyword;
            $wheres     = array("name", "email", "phone", "username");
            $getusers   = Helper::dynamicSearch($getusers,$wheres,$keyword);
            
            if(!$getusers->count()){
                
                $getusers   = User::with('roles')->with('insurance');

                if ($request->auth->isInsurance) {
                    $getusers   = $getusers->where('insurance_id', $request->auth->insurance_id);
                }
                
                $getusers= $getusers->WhereHas('roles', function ($query) use ($keyword) {
                        $query->where('rolename', 'like','%'.$keyword.'%');
                        });
            }
            
            $status = true;
            $error  = null;
            $getusers = Helper::sorting($getusers,$request->input('sortby'), $request->input('sortvalue'));
            } else {
                if($request->has('filter')){
                    $table      = 'users';
                    $filter     = $request->input('filter'); //input must be json format
                    $getusers   = Helper::filterSearch($getusers,$table,$filter);
                    foreach ($filter as $f) {
                        $filterdata     = json_decode($f);
                        $filterby       = $filterdata->by;
                        $filtervalue    = $filterdata->value;
                        if (is_array($filtervalue)) {
                            if ($filterby == 'rolename') {
                                $getusers   = $getusers->WhereHas('roles', function ($query) use ($filtervalue) {
                                    $query->whereIn('rolename', $filtervalue);
                                    });
                            }
                        } else {
                            if ($filterby == 'rolename') {
                                $getusers   = $getusers->WhereHas('roles', function ($query) use ($filtervalue) {
                                    $query->where('rolename', 'like','%'.$filtervalue.'%');
                                    });
                            }
                        }
                    }
                }
            }
        
            $getusers = Helper::sorting($getusers,$request->input('sortby'), $request->input('sortvalue'));
            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $getusers   = $getusers->paginate($limit,['*'],'page',$page);
            $meta       = [
                'page'      => (int) $getusers->currentPage(),
                'perPage'   => (int) $getusers->perPage(),
                'total'     => (int) $getusers->total(),
                'totalPage' => (int) $getusers->lastPage()
            ];
            $getusers   = $getusers->toArray()['data'];
        }
    
        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getusers) ?$getusers : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function filterGetAllUsers(Request $request)
    {
        $table      = 'users';
        $getusers   = User::with('roles')->with('insurance');
        if ($request->auth->isInsurance) {
            $getusers   = $getusers->where('insurance_id', $request->auth->insurance_id);
        }

        if($request->has('filter')){
            $filter     = $request->input('filter'); //input must be json format
            $getusers   = Helper::filterSearch($getusers,$table,$filter);
            foreach ($filter as $f) {
                $filterdata     = json_decode($f);
                $filterby       = $filterdata->by;
                $filtervalue    = $filterdata->value;

                if ($filterby == 'rolename') {
                    $getusers   = $getusers->WhereHas('roles', function ($query) use ($filtervalue) {
                        $query->where('rolename', 'like','%'.$filtervalue.'%');
                        });
                }
            }
        }
        
        $limit      = $request->has('limit') ? $request->input('limit') : 20;
        $page       = $request->has('page') ? $request->input('page') : 1;
        $getusers   = $getusers->paginate($limit,['*'],'page',$page);
        $meta       = [
            'page'      => (int) $getusers->currentPage(),
            'perPage'   => (int) $getusers->perPage(),
            'total'     => (int) $getusers->total(),
            'totalPage' => (int) $getusers->lastPage()
        ];
        $getusers   = $getusers->toArray()['data'];

        if(!$getusers){
            $error = "data not found";
            return response()->json(['status' => false, 'data' =>  $error], 409);
        } else {
            return response()->json(['status' => true, 'data' =>  $getusers, 'meta' => $meta], 200);
        }
    }

    //show selected data from users
    public function singleUser($id, $insurance_id)
    {
        try {
            if ($request->auth->isInsurance) {
                $user = User::where(['id' => $id, 'insurancer_id' => $insurance_id])->first();
            } else {
                $user = User::where('id',$id)->first();
            }

            return response()->json(['user' => $user], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    //update data from selected user
    public function updateUser(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required|string',
            'username'      => 'string',
            // 'phone'         => 'required',
            'email'         => 'required|email',
            'role_user'     => 'required|integer',
            'insurance_id'  => 'required|integer'
            ]);

        $data   = User::find($id);
        
        if ($data != null) {
            $params = $request->all();
            $cekuser = User::where(function ($query) use($request) {
                                $query->where('username', $request->input('username'))
                                ->orWhere('email', $request->input('email'));
                            })->where('id','!=',$id);

            if ($request->auth->isInsurance == "1") {
                $insurance_id = ($request->auth->isInsurance ? $request->auth->insurance_id : $params['insurance_id']);
                $cekuser = $cekuser->where('insurance_id', $insurance_id);
            }

            $cekuser = $cekuser->first();
            if ($cekuser) {
                return response()->json(['message' => 'Username or email already exist!'], 409);
            } else {
                $data->fill($params);
                $data->save();
                return response()->json(['status' => (bool) true ,'message' => 'Your data has been update'], 200);
            }     
        }
        else {
            return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
        }
    }

    //delete selected user
    public function deleteUser(Request $request, $id)
    {
        $user   = User::find($id);
        if ($user) {
            $user->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function excelUser(Request $request)
	{
        $insurance_id   = $request->insurance_id;
        return Excel::download(new UserManagementExport($insurance_id), 'usermanagement.xlsx');
    }
    
    public function massDeleteUser(Request $request)
    {
        $user   = User::whereIn('id',$request->id);
        if ($user) {
            $user->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    public function getUserAnalyst()
    {
        $permission = 'Pages.Transaction.ClaimSubmission.Analyze';
        $user       = User::with(['permissions'])->whereHas('permissions', function ($query) use ($permission) {
            return $query->where('permission', $permission);
        })->where('isLogin', 1)->get();
        
        $response = [
            "status"    => $user->count() == 0 ? false : true,
            "data"      => $user->count() == 0 ? [] : $user,
            "message"   => $user->count() == 0 ? "There is no one users analyst logged in" : "There is users analyst logged in"
        ];

        return response()->json($response);
    }

}