<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RolePermissions;
use App\Helpers\Helper;
use App\Http\Controllers\RolePermissionsController as RolePermissionsController;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    private $permissions;
    public function __construct()
    {
        $this->permissions = new RolePermissionsController;
    }
    public function getRoles(Request $request, $id = null)
    {
        $sortby             = $request->input('sortby');
        $sortvalue          = $request->input('sortvalue');
        $status             = true;
        $error              = null;
        $getroles           = Roles::with(['insurance_id', 'users', 'permissions']);

        if ($request->auth->isInsurance) {
            $getroles   = $getroles->where('insurance_id', $request->auth->insurance_id);
        }

        if ($id) {
            $getroles = $getroles->where('id', $id)->first();

            if (!$getroles) {
                $status     = false;
                $error      = "data not found";
            } else {
                // foreach ($getroles as $m) {
                $getroles->section = "Insurance";
                // }
            }
        } else {
            if ($request->has('keyword')) {
                $keyword    = $request->keyword;
                $wheres     = array("section", "rolename", "created_at");
                $getroles   = Helper::dynamicSearch($getroles, $wheres, $keyword);
                $getroles   = $getroles->orWhereHas('insurance_id', function ($query) use ($keyword) {
                    $query->where('insuranceName', 'like', '%' . $keyword . '%');
                });
                // if (!$getroles->count()) {
                // } else {
                // }
                $status     = true;
                $error      = null;
                $getroles   = Helper::sorting($getroles, $sortby, $sortvalue);
            } else {
                if ($request->has('filter')) {
                    $table     = 'roles';
                    $filter    = $request->input('filter');
                    $getroles   = Helper::filterSearch($getroles, $table, $filter);
                    foreach ($filter as $f) {
                        $filterdata     = json_decode($f);
                        $filterby       = $filterdata->by;
                        $filtervalue    = $filterdata->value;
                    }
                }
            }


            $getroles     = Helper::sorting($getroles, $sortby, $sortvalue);
            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $getroles     = $getroles->paginate($limit, ['*'], 'page', $page);
            $meta       = [
                'page'      => (int) $getroles->currentPage(),
                'perPage'   => (int) $getroles->perPage(),
                'total'     => (int) $getroles->total(),
                'totalPage' => (int) $getroles->lastPage()
            ];

            foreach ($getroles as $m) {
                $m->section = "Insurance";
            }

            $getroles  = $getroles->toArray()['data'];
        }
        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getroles) ? $getroles : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public static function checkDuplicate(Request $request)
    {
        $result = Helper::checkDuplicate('Roles', $request->type, $request->vals, $request->oldID);
        return response()->json($result);
    }

    public function addRoles(Request $request)
    {
        try {
            if (isset($request->set_default) && $request->set_default == "1") {
                DB::table('roles')
                    ->update(['set_default' => 0]);
            } else {
                $request->set_default = "0";
            }

            $insurance_id = ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id);

            $vals = $request->rolename."|".$insurance_id;

            $cek = Helper::checkDuplicate('Roles', "rolename-insurance", $vals, null);
            if(!$cek['status']){
                $roles = Roles::create([
                    'rolecode' => '',
                    'rolename' => $request->rolename,
                    'set_default' => $request->set_default,
                    'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id),
                    'isInsurance' => $request->isInsurance
                ]);
    
                $this->permissions->addRolePermissions($roles->id, $request->auth->id, $roles->insurance_id, $request);
            }else{
                throw new \Exception();
            }
            
            return response()->json(['message' => 'DATA CREATED SUCCESSFULLY'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Roles Registration Failed!'], 409);
        }
    }

    public function updateRoles(Request $request, $id)
    {
        $data   = Roles::find($id);

        if (isset($request->set_default) && $request->set_default == "1") {
            DB::table('roles')
                ->update(['set_default' => 0]);
        } else {
            $request->set_default = "0";
        }

        try {
            $insurance_id = ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id);

            $vals = $request->rolename."|".$insurance_id;

            $cek = Helper::checkDuplicate('Roles', "rolename-insurance", $vals, $id);
            
            if(!$cek['status']){

            $data->update([
                'rolecode' => '',
                'rolename' => $request->rolename,
                'set_default' => $request->set_default,
                'insurance_id' => ($request->auth->isInsurance ? $request->auth->insurance_id : $request->insurance_id),
                'isInsurance' => $request->isInsurance
            ]);

            $permissions = RolePermissions::where('role_id', $id)->delete();

            $this->permissions->addRolePermissions($data->id, $request->auth->id, $data->insurance_id, $request);
        }else{
            return response()->json(['status' => (bool) false, 'message' => $cek['message']], 409);
        }
            return response()->json(['status' => (bool) true, 'message' => 'Data successfully updated'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => (bool) false, 'message' => $e->getMessage()], 409);
        }
    }
}
