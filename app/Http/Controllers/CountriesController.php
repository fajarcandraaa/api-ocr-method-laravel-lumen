<?php
namespace App\Http\Controllers;

use App\Countries;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CountriesController extends Controller
{
    
    public function getCountries($country_code=null)
    {
        $status = true;
        $error = "";
        if($country_code){
            $countries = Countries::where('country_code',$country_code)->get();
            }else{
                $countries = Countries::all();
            }
        
        if(!$countries->count()){
            $status = false;
            $error = "data not available";
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($countries) ? $countries : null),
            "error"     => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

}