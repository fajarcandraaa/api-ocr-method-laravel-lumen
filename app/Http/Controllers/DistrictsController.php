<?php
namespace App\Http\Controllers;

use App\Districts;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class DistrictsController extends Controller
{
    
    public function getDistricts($city_id=null)
    {
        $status = true;
        $error = "";
        if($city_id){
            $districts = Districts::with('city')->where('city_id',$city_id)->get();
            }else{
                $districts = Districts::with('city')->get();
            }
        
        if(!$districts->count()){
            $status = false;
            $error = "data not available";
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($districts) ? $districts : null),
            "error"     => (isset($error) ? $error : null)
        ];

        return response()->json($response);
    }

}