<!DOCTYPE html>
<html>
<head>
	<title>Member Data</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 5.5pt;
		}
	</style>
	<center>
		<h5>Member Data</h4>
		
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>Member No</th>
				<th>Member Name</th>
				<th>Member Type</th>
				<th>Member Status</th>
				<th>Phone No</th>
				<th>KTP No</th>
				<th>Policy Holder</th>
				<th>Policy No</th>
				<th>Members Effective Date</th>
				<th>KK No</th>
				<th>VIP</th>
				<th>Blacklist Person</th>
				<th>Created By</th>
				<th>Created Time</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($member as $members)
			<tr>
				<td>{{$members['member_id']}}</td>
				<td>{{$members['member_name']}}</td>
				<td>{{$members['member_type']['codeDesc']}}</td>
				<td>{{$members['status']['codeDesc']}}</td>
				<td>{{$members['phone_no']}}</td>
				<td>{{$members['identity_no']}}</td>
				<td>{{$members['member_id']." - ".$members['member_name']}}</td>
				<td>{{$members['policy_no']}}</td>
				<td>{{date('d/m/Y',strtotime($members['effective_date']))}}</td>
				<td>{{$members['family_no']}}</td>
				<td>{{(strpos($members['member_setting'],'1')===false ? "No" :  "Yes")}}</td>
				<td>{{(strpos($members['member_setting'],'2')===false ? "No" :  "Yes")}}</td>
				<td>{{$members['created_by']['name']}}</td>
				<td>{{date('d/m/Y',strtotime($members['created_at']))}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>