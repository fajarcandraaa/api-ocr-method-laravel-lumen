<!DOCTYPE html>
<html>
<head>
	<title>Provider Services Data</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Provider Services Data</h4>
		
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Provider Service Code</th>
				<th>Provider Service Name</th>
				<th>Amount</th>
				<th>Description</th>				
				<th>Provider Type</th>
				<th>Provider</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($ps as $in)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$in['service_code']}}</td>
				<td>{{$in['service_name']}}</td>
				<td>{{$in['amount']}}</td>
				<td>{{$in['description']}}</td>
				<td>{{$in['provider_type']['codeDesc']}}</td>
				<td>{{$in['mid']}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>