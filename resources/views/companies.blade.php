<!DOCTYPE html>
<html>

<head>
	<title>Company Data</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
	<style type="text/css">
		table tr td,
		table tr th {
			font-size: 5.5pt;
		}
	</style>
	<center>
		<h5>Company Data</h4>

	</center>

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>Company Code</th>
				<th>Parent Company Name</th>
				<th>Company Name</th>
				<th>Branch</th>
				<th>Company Status</th>
				<th>Policy No</th>
				<th>Policy Holder</th>
				<th>Payor</th>
				<th>Phone No</th>
				<th>NPWP No</th>
				<th>Policy Effective Date</th>
				<th>Created By</th>
				<th>Created Time</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($comp as $c)
			<tr>
				<td>{{$c['company_code']}}</td>
				<td>{{($c['have_parent_comp'] == 1 ? $c['parent_comp_id']['company_name'] : "")}}</td>
				<td>{{$c['company_name']}}</td>
				<td>{{$c['branch']}}</td>
				<td>{{$c['status']['codeDesc']}}</td>
				<td>{{$c['policy_number']}}</td>
				<td>{{substr($c['policy_holder'],23)}}</td>
				<td>{{$c['payor']}}</td>
				<td>{{$c['phoneno']}}</td>
				<td>{{$c['tax_no']}}</td>
				<td>{{date('d/m/Y',strtotime($c['effectivedate']))}}</td>
				<td>{{$c['created_by']['name']}}</td>
				<td>{{date('d/m/Y',strtotime($c['created_at']))}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</body>

</html>