<!DOCTYPE html>
<html>
<head>
	<title>Diagnosis Data</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Diagnosis Data</h4>
		
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>DiagnosisCode</th>
				<th>DiagnosisName</th>
				<th>ScientificDesc</th>				
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($diag as $in)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$in->diagnosisCode}}</td>
				<td>{{$in->diagnosisName}}</td>
				<td>{{$in->scientificDesc}}</td>
				<td>{{$in->description}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>