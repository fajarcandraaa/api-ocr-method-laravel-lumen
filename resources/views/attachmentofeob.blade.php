<html lang="en" dir="ltr" class="vuesax-app-is-ltr" style="--vs-primary:115,103,240; --vs-success:40,199,111; --vs-danger:234,84,85; --vs-warning:255,159,67; --vs-dark:30,30,30; --vh:9.2px;">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" href="/favicon.ico">
	<link rel="stylesheet" type="text/css" href="/loader.css">
  <title>iziklaim | Insurance</title>

	<link href="style.css" rel="stylesheet">
	<link href="style2.css" rel="stylesheet">

	<script charset="utf-8" src="/js/chunk-0eb11055.9703f6ed.js"></script>
</head>

<body class="nimbus-is-editor">
	<noscript><strong>We're sorry but Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
	</noscript>
	<div id="loading-bg" style="display: none;">
		<div class="loading-logo"></div>
		<div class="loading">
			<img src="/izi_spinner.gif" alt="Logo" width="84">
		</div>
	</div>
	<div id="app" class="">
		<div class="layout--full-page">
			<div data-v-2984fc9a="">
				<div data-v-2984fc9a="" class="vx-row mb-4">
					<div data-v-2984fc9a="" class="vx-col sm:w-3/12 white-space"><strong data-v-2984fc9a="">PT. Insurance Name</strong>
						<p data-v-2984fc9a="" class="mb-4">Gedung ABC Blok 1 Lt. 4 Jl.Merpati dalam Jakarta 12345 t +62 21 1234 5678 f +62 21 1234 5789</p>
						<div data-v-2984fc9a="" class="mb-4">www.insurancename.com</div><strong data-v-2984fc9a="" class="mb-4"><div data-v-2984fc9a="">DANAU SUNTER JAYA, PT</div><div data-v-2984fc9a="">MS. JEQUELLINE</div><div data-v-2984fc9a="">GEDUNG DANAU SUNTER JAYA LT 100 
 JL. SUNTER ABC 
 JAKARTA 
 12346</div></strong>
					</div>
					<div data-v-2984fc9a="" class="vx-col sm:w-6/12 text-center">
						<h4 data-v-2984fc9a="">EXPLANATION OF BENEFIT - OUTPATIENT</h4>
						<div data-v-2984fc9a="">Date of Issue : July 09, 2020</div>
					</div>
					<div data-v-2984fc9a="" class="vx-col sm:w-3/12">
						<img data-v-2984fc9a="" src="https://api-insurance.iziklaim.com/assets/insuranceLogo/I4MzZB56qNUygA3RXYbgL73ALreSHB0qfVZv3tOG.png" style="width: 100%;">
					</div>
				</div>
				<div data-v-2984fc9a="" class="vx-row mb-4" style="width: 100%;">
					<div data-v-2984fc9a="" class="vx-col sm:w-1/2" style="width: 50%;">
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Claim No
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">{{ $data['claimregno'] }}</div>
						</div>
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Plan
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">{{ $data['plan']['codeDesc'] }}</div>
						</div>
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Document Received Date
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">{{ $reciveddate }}</div>
						</div>
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Policy No
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">{{ $data['companyid'] == null ? $data['memberid']['policy_no'] : $data['companyid']['policy_number'] }}</div>
						</div>
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Policy Holder
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">{{ $data['companyid'] == null ? $data['memberid']['policy_holder'] : $data['companyid']['policy_holder'] }}</div>
						</div>
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Policy Effective Date
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">{{ $evectivedate }}</div>
						</div>
					</div>

					<div data-v-2984fc9a="" class="vx-col sm:w-1/2">
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Treatment Date
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">19/08/2019</div>
						</div>
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Treatment Place
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">123456789012345 - RS MEDCORE TES</div>
						</div>
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Member
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">1507699000001995 - ARSAESALL</div>
						</div>
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Member Effective Date
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">1507699000001995 - ARSAESALL</div>
						</div>
						<div data-v-2984fc9a="" class="vs-row" style="display: flex; width: 100%;">
							<div data-v-2984fc9a="" class="vs-col vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%;"><strong data-v-2984fc9a="" style="font-size: 11px;">
            Diagnosis
            <span data-v-2984fc9a="" style="float: right;">:</span></strong>
							</div>
							<div data-v-2984fc9a="" class="vs-col pl-2 vs-xs- vs-sm- vs-lg-" style="margin-left: 0%; width: 50%; font-size: 10px;">TETANUS</div>
						</div>
					</div>
					<div data-v-2984fc9a="" class="w-full p-4">
						<hr data-v-2984fc9a="" class="mb-4">
						<div data-v-2984fc9a="" class="white-space mb-4" style="font-size: 11px;">Dear Sir / Madam, We are regret to inform you that we are unable to reimburse this claim for RP. 141,410 due to following reason:</div>
						<table data-v-2984fc9a="" class="mb-4" style="border-collapse: collapse;">
							<tr data-v-2984fc9a="" class="table-header">
								<th data-v-2984fc9a="" style="font-size: 11px;">No</th>
								<th data-v-2984fc9a="" colspan="2" style="font-size: 11px;">Benefit</th>
								<th data-v-2984fc9a="" style="font-size: 11px;">Length of Stay</th>
								<th data-v-2984fc9a="" style="font-size: 11px;">Qty</th>
								<th data-v-2984fc9a="" style="font-size: 11px;">Benefit Entitlement</th>
								<th data-v-2984fc9a="" style="font-size: 11px;">Incurred Expenses</th>
								<th data-v-2984fc9a="" style="font-size: 11px;">Amount Reimburesed</th>
								<th data-v-2984fc9a="" style="font-size: 11px;">Excess Charge</th>
								<th data-v-2984fc9a="" style="font-size: 11px;">Amount Declained</th>
								<th data-v-2984fc9a="" style="font-size: 11px;">Reason Code</th>
							</tr>
							<tr data-v-2984fc9a="">
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" style="font-size: 10px; min-width: 5rem;">B-OP-01</td>
								<td data-v-2984fc9a="" style="font-size: 10px;">Biaya Konsultasi Maks. per kunjungan</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">As Charge</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">25000</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">25000</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">RC-01</td>
							</tr>
							<tr data-v-2984fc9a="">
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">1</td>
								<td data-v-2984fc9a="" style="font-size: 10px; min-width: 5rem;">B-OP-03</td>
								<td data-v-2984fc9a="" style="font-size: 10px;">Biaya Obat - Obatan sesuai resep Maks. per kunjungan</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">As Charge</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">116410</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">116410</td>
								<td data-v-2984fc9a="" class="text-center" style="font-size: 10px;">RC-01</td>
							</tr>
							<tr data-v-2984fc9a="" class="table-total" style="border-top: 1px solid rgb(221, 221, 221);">
								<td data-v-2984fc9a="" colspan="5"></td>
								<td data-v-2984fc9a="" style="font-size: small;"><strong data-v-2984fc9a="" style="font-size: 11px;">Total Amount</strong>
								</td>
								<td data-v-2984fc9a="" style="font-size: 10px;">141,410</td>
								<td data-v-2984fc9a="" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" style="font-size: 10px;">0</td>
								<td data-v-2984fc9a="" style="font-size: 10px;">141,410</td>
								<td data-v-2984fc9a=""></td>
							</tr>
						</table>
						<div data-v-2984fc9a="" class="mb-4"><strong data-v-2984fc9a="" style="font-size: 11px;">Reason Code</strong>
							<div data-v-2984fc9a=""><span data-v-2984fc9a="" class="mr-4" style="font-size: 10px;">RC-01</span><span data-v-2984fc9a="" style="font-size: 10px;">MOHON DILENGKAPI RESUME MEDIS</span>
							</div>
						</div>
						<div data-v-2984fc9a="" class="mb-4"><strong data-v-2984fc9a="" style="font-size: 11px;">Note</strong>
							<div data-v-2984fc9a="" style="font-size: 10px;">Please attach a medical resume of Rp. 141,410. Maximum limit for filling a reclaim up to 90 days from the date of treatment</div>
						</div>
						<div data-v-2984fc9a="" class="mb-4"><strong data-v-2984fc9a=""><div data-v-2984fc9a="" class="mb-8" style="font-size: 11px;">Yours faithfully,</div><div data-v-2984fc9a="" style="font-size: 11px;">Group Insurance Department</div><div data-v-2984fc9a="" style="font-size: xx-small;">This is a computer generated letter, no signature is required</div></strong>
						</div>
						<!---->
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="/js/chunk-vendors.acd317c9.js"></script>
	<script src="/js/app.9fb24f2b.js"></script>
	<div class="nsc-panel nsc-panel-compact nsc-hide"></div>
</body>

</html>