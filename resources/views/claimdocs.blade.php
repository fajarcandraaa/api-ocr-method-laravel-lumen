<!DOCTYPE html>
<html>
<head>
	<title>Claim Documents Data</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Claim Documents Data</h4>
		
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Claim Document No</th>
				<th>Claim Document Name</th>
				<th>Plan</th>
				<th>Remark</th>
				<th>Is Mandatory</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($claimdocs as $in)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$in->claim_doc_no}}</td>
				<td>{{$in->claim_doc_name}}</td>
				<td>{{$in->plan}}</td>
				<td>{{$in->remark}}</td>
				<td>{{($in->is_mandatory == "1" ? "Yes" : "No" )}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>