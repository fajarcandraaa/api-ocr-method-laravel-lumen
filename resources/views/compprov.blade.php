<!DOCTYPE html>
<html>

<head>
	<title>Providers Company Data</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
	<style type="text/css">
		table tr td,
		table tr th {
			font-size: 5.5pt;
		}
	</style>
	<center>
		<h5>Providers Company Data</h4>

	</center>

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>Provider Company ID</th>
				<th>Provider Company Type</th>
				<th>Provider Company Name</th>
				<th>Country</th>
				<th>City</th>
				<th>Agreement Effective Date</th>
				<th>Agreement Expiry Date</th>
				<th>Provider Company Email</th>
				<th>Provider Company Phone No</th>
				<th>Created By</th>
				<th>Created Time</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach(json_decode($compprov,true) as $c)
			<tr>
				<td>{{$c['id']}}</td>
				<td>{{$c['provider_type']['codeDesc']}}</td>
				<!-- <td>halo</td> -->
				<td>{{$c['provider_name']}}</td>
				<td>{{$c['country']['country_desc']}}</td>
				<td>{{$c['city']['city_desc']}}</td>
				<td>{{date('d/m/Y',strtotime($c['AggrEffectiveDate']))}}</td>
				<td>{{date('d/m/Y',strtotime($c['AggrExpiryDate']))}}</td>
				<td>{{$c['provider_email']}}</td>
				<td>{{$c['PhoneNo']}}</td>
				<td>{{$c['created_by']['name']}}</td>
				<td>{{date('d/m/Y',strtotime($c['created_at']))}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</body>

</html>