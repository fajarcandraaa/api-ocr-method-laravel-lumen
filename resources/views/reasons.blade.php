<!DOCTYPE html>
<html>
<head>
	<title>Reasons Data</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Reasons Data</h4>
		
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Reason Code</th>
				<th>Reason Name</th>
				<th>Description</th>				
				<th>Explanation of Benefit Type</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($reasons as $in)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$in->reason_code}}</td>
				<td>{{$in->reason_name}}</td>
				<td>{{$in->description}}</td>
				<td>{{$in->eob_type}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>